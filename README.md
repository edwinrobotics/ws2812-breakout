# [WS2812 Multicolor LED Breakout](https://shop.edwinrobotics.com/breakout-boards/599-ws2812-multicolor-led-breakout.html) #

![DSC_0045_small.JPG](https://bitbucket.org/repo/xaKpEK/images/1640461813-DSC_0045_small.JPG)

This is a breakout board for the WS2812B RGB LED. The WS2812B is actually an RGB LED with a WS2811 built right into the LED! All the necessary pins are broken out to 0.1" spaced headers for easy bread-boarding. Several of these breakouts can even be chained together to form a display or an addressable string.

**FEATURES:**

* Intelligent reverse connect protection, the power supply reverse connection does not damage the IC.
* Control circuit and RGB chip are integrated in a package of 5050 components, form a complete control of pixel
point.
* Built-in signal reshaping circuit, after wave reshaping to the next driver, ensure wave-form distortion not
accumulate.
* Built-in electric reset circuit and power lost reset circuit.
* Each pixel of the three primary color can achieve 256 brightness display, completed 16777216 color full color
display, and scan frequency not less than 400Hz/s.
* Cascading port transmission signal by single line.
* Any two point the distance more than 5m transmission signal without any increase circuit.
* Send data at speeds of 800Kbps.
* The color of the light were highly consistent, cost-effective.

**SPECIFICATION:**

* Input Voltage Range: +3.5-+5.3 V DC
* Input Voltage Range: -0.5～VDD+0.5
* Logic Interface: VCC,Signal, GND. 
* Dimensions: 18.5x12.5mm

**DOCUMENTS:**

[Hookup Guide](https://learn.sparkfun.com/tutorials/ws2812-breakout-hookup-guide?_ga=1.210012957.928758210.1444282666)