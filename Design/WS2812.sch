<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="8.2.1">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="16" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="14" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Dimensions" color="10" fill="1" visible="yes" active="yes"/>
<layer number="115" name="Divisions" color="7" fill="0" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="117" name="IMP" color="12" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="Logo-TOP" color="10" fill="1" visible="yes" active="yes"/>
<layer number="120" name="logo-bottom" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="141" name="topsilk" color="14" fill="1" visible="yes" active="yes"/>
<layer number="142" name="bottom-silk" color="13" fill="1" visible="yes" active="yes"/>
<layer number="143" name="QR-code" color="6" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="234" name="Logo_b" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="253" name="LEGEND" color="10" fill="1" visible="yes" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="SparkFun-LED">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find discrete LEDs for illumination or indication, but no displays.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="LED-TRICOLOR-5050-IC">
<circle x="-0.7" y="2" radius="0.2236" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="2" width="0.127" layer="51"/>
<wire x1="-2.5" y1="-2.5" x2="-2.5" y2="2" width="0.127" layer="51"/>
<wire x1="-1.8" y1="2.5" x2="2.5" y2="2.5" width="0.127" layer="51"/>
<wire x1="2.5" y1="2.5" x2="2.5" y2="-2.5" width="0.127" layer="51"/>
<wire x1="2.5" y1="-2.5" x2="-2.5" y2="-2.5" width="0.127" layer="51"/>
<wire x1="-1" y1="2.5" x2="1" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-1" y1="-2.5" x2="1" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="0.7" x2="-2.5" y2="0.9" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-0.9" x2="-2.5" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="2.5" y1="-0.9" x2="2.5" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="2.5" y1="0.7" x2="2.5" y2="0.9" width="0.2032" layer="21"/>
<wire x1="-1.016" y1="-0.381" x2="-1.016" y2="0.381" width="0.2032" layer="21"/>
<wire x1="-1.016" y1="0.381" x2="-0.127" y2="0.381" width="0.2032" layer="21"/>
<wire x1="-0.127" y1="0.381" x2="-0.127" y2="-0.381" width="0.2032" layer="21"/>
<wire x1="-0.127" y1="-0.381" x2="-1.016" y2="-0.381" width="0.2032" layer="21"/>
<wire x1="0.254" y1="-0.127" x2="0.254" y2="0.127" width="0.2032" layer="21"/>
<wire x1="0.254" y1="0.127" x2="0.508" y2="0.127" width="0.2032" layer="21"/>
<wire x1="0.508" y1="0.127" x2="0.508" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="0.508" y1="-0.127" x2="0.254" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="2" x2="-1.8" y2="2.5" width="0.127" layer="51"/>
<rectangle x1="1.7" y1="-0.45" x2="2.7" y2="0.45" layer="51"/>
<rectangle x1="1.7" y1="1.15" x2="2.7" y2="2.05" layer="51"/>
<rectangle x1="1.7" y1="-2.05" x2="2.7" y2="-1.15" layer="51"/>
<rectangle x1="-2.7" y1="1.15" x2="-1.7" y2="2.05" layer="51" rot="R180"/>
<rectangle x1="-2.7" y1="-0.45" x2="-1.7" y2="0.45" layer="51" rot="R180"/>
<rectangle x1="-2.7" y1="-2.05" x2="-1.7" y2="-1.15" layer="51" rot="R180"/>
<smd name="1" x="-2.4" y="1.7" dx="2" dy="1.1" layer="1" roundness="100"/>
<smd name="2" x="-2.4" y="0" dx="2" dy="1.1" layer="1" roundness="100"/>
<smd name="3" x="-2.4" y="-1.7" dx="2" dy="1.1" layer="1" roundness="100"/>
<smd name="4" x="2.4" y="-1.7" dx="2" dy="1.1" layer="1" roundness="100"/>
<smd name="5" x="2.4" y="0" dx="2" dy="1.1" layer="1" roundness="100"/>
<smd name="6" x="2.4" y="1.7" dx="2" dy="1.1" layer="1" roundness="100"/>
<text x="-2.54" y="2.794" size="0.762" layer="25">&gt;Name</text>
<text x="-2.54" y="-3.556" size="0.762" layer="27">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="LED-TRICOLOR-WS28X1">
<wire x1="10.16" y1="7.62" x2="10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="-12.7" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-5.08" x2="-12.7" y2="7.62" width="0.254" layer="94"/>
<wire x1="-12.7" y1="7.62" x2="-5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="7.62" x2="2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="7.62" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="-0.508" y1="1.27" x2="-0.508" y2="0" width="0.254" layer="94"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-0.508" y1="-1.27" x2="0.762" y2="0" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="-0.508" y2="1.27" width="0.254" layer="94"/>
<wire x1="0.762" y1="1.27" x2="0.762" y2="0" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="0.762" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="2.032" y2="0" width="0.254" layer="94"/>
<wire x1="-0.508" y1="0" x2="-1.778" y2="0" width="0.254" layer="94"/>
<wire x1="-0.508" y1="-1.778" x2="-0.508" y2="-3.048" width="0.254" layer="94"/>
<wire x1="-0.508" y1="-3.048" x2="-0.508" y2="-4.318" width="0.254" layer="94"/>
<wire x1="-0.508" y1="-4.318" x2="0.762" y2="-3.048" width="0.254" layer="94"/>
<wire x1="0.762" y1="-3.048" x2="-0.508" y2="-1.778" width="0.254" layer="94"/>
<wire x1="0.762" y1="-1.778" x2="0.762" y2="-3.048" width="0.254" layer="94"/>
<wire x1="0.762" y1="-3.048" x2="0.762" y2="-4.318" width="0.254" layer="94"/>
<wire x1="0.762" y1="-3.048" x2="2.032" y2="-3.048" width="0.254" layer="94"/>
<wire x1="-0.508" y1="-3.048" x2="-1.778" y2="-3.048" width="0.254" layer="94"/>
<wire x1="-0.508" y1="4.318" x2="-0.508" y2="3.048" width="0.254" layer="94"/>
<wire x1="-0.508" y1="3.048" x2="-0.508" y2="1.778" width="0.254" layer="94"/>
<wire x1="-0.508" y1="1.778" x2="0.762" y2="3.048" width="0.254" layer="94"/>
<wire x1="0.762" y1="3.048" x2="-0.508" y2="4.318" width="0.254" layer="94"/>
<wire x1="0.762" y1="4.318" x2="0.762" y2="3.048" width="0.254" layer="94"/>
<wire x1="0.762" y1="3.048" x2="0.762" y2="1.778" width="0.254" layer="94"/>
<wire x1="0.762" y1="3.048" x2="2.032" y2="3.048" width="0.254" layer="94"/>
<wire x1="-0.508" y1="3.048" x2="-1.778" y2="3.048" width="0.254" layer="94"/>
<pin name="VDD" x="-15.24" y="5.08" visible="pin" length="short"/>
<pin name="VSS" x="-15.24" y="-2.54" visible="pin" length="short"/>
<pin name="DI" x="12.7" y="5.08" visible="pin" length="short" rot="R180"/>
<pin name="DO" x="12.7" y="-2.54" visible="pin" length="short" rot="R180"/>
<text x="-12.7" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<text x="-12.7" y="-7.62" size="1.778" layer="95">&gt;VALUE</text>
<wire x1="-2.54" y1="7.62" x2="-2.54" y2="-5.08" width="0.254" layer="94" style="shortdash"/>
<wire x1="2.54" y1="7.62" x2="2.54" y2="-5.08" width="0.254" layer="94" style="shortdash"/>
<text x="-1.524" y="5.5118" size="1.27" layer="94">RGB</text>
<text x="-3.175" y="-2.159" size="1.27" layer="94" rot="R90">WS28x1</text>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-5.08" width="0.254" layer="94" style="shortdash"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED-TRICOLOR-5050-IC" prefix="U">
<description>&lt;h3&gt; WS2812 RGB LED w/ WS2811 driver&lt;/h3&gt;&lt;p&gt;
5x5mm SMD LED with built-in controller IC.&lt;br&gt;
DIO-11598</description>
<gates>
<gate name="U1" symbol="LED-TRICOLOR-WS28X1" x="0" y="0"/>
</gates>
<devices>
<device name="WS2811" package="LED-TRICOLOR-5050-IC">
<connects>
<connect gate="U1" pin="DI" pad="2" route="any"/>
<connect gate="U1" pin="DO" pad="1"/>
<connect gate="U1" pin="VDD" pad="3 5"/>
<connect gate="U1" pin="VSS" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-11598"/>
<attribute name="VALUE" value="WS2811" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-samtec">
<description>&lt;b&gt;Samtec Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="SSW-103-02-S-S">
<description>&lt;b&gt;THROUGH-HOLE .025" SQ POST SOCKET&lt;/b&gt;&lt;p&gt;
Source: Samtec SSW.pdf</description>
<wire x1="-3.939" y1="1.155" x2="3.939" y2="1.155" width="0.2032" layer="21"/>
<wire x1="3.939" y1="1.155" x2="3.939" y2="-1.155" width="0.2032" layer="21"/>
<wire x1="3.939" y1="-1.155" x2="-3.939" y2="-1.155" width="0.2032" layer="21"/>
<wire x1="-3.939" y1="-1.155" x2="-3.939" y2="1.155" width="0.2032" layer="21"/>
<wire x1="-3.285" y1="0.755" x2="-1.785" y2="0.755" width="0.2032" layer="51"/>
<wire x1="-1.785" y1="0.755" x2="-1.785" y2="-0.745" width="0.2032" layer="51"/>
<wire x1="-1.785" y1="-0.745" x2="-3.285" y2="-0.745" width="0.2032" layer="51"/>
<wire x1="-3.285" y1="-0.745" x2="-3.285" y2="0.755" width="0.2032" layer="51"/>
<wire x1="-0.745" y1="0.755" x2="0.755" y2="0.755" width="0.2032" layer="51"/>
<wire x1="0.755" y1="0.755" x2="0.755" y2="-0.745" width="0.2032" layer="51"/>
<wire x1="0.755" y1="-0.745" x2="-0.745" y2="-0.745" width="0.2032" layer="51"/>
<wire x1="-0.745" y1="-0.745" x2="-0.745" y2="0.755" width="0.2032" layer="51"/>
<wire x1="1.795" y1="0.755" x2="3.295" y2="0.755" width="0.2032" layer="51"/>
<wire x1="3.295" y1="0.755" x2="3.295" y2="-0.745" width="0.2032" layer="51"/>
<wire x1="3.295" y1="-0.745" x2="1.795" y2="-0.745" width="0.2032" layer="51"/>
<wire x1="1.795" y1="-0.745" x2="1.795" y2="0.755" width="0.2032" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1" diameter="2" rot="R90"/>
<pad name="2" x="0" y="0" drill="1" diameter="2" rot="R270"/>
<pad name="3" x="2.54" y="0" drill="1" diameter="2" rot="R90"/>
<text x="-3.048" y="-3.048" size="1.6764" layer="21" font="vector">1</text>
<text x="-4.445" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.715" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="SSW-103-02-S-S-RA">
<description>&lt;b&gt;THROUGH-HOLE .025" SQ POST SOCKET&lt;/b&gt;&lt;p&gt;
Source: Samtec SSW.pdf</description>
<wire x1="-3.939" y1="-8.396" x2="3.939" y2="-8.396" width="0.2032" layer="21"/>
<wire x1="3.939" y1="-8.396" x2="3.939" y2="-0.106" width="0.2032" layer="21"/>
<wire x1="3.939" y1="-0.106" x2="-3.939" y2="-0.106" width="0.2032" layer="21"/>
<wire x1="-3.939" y1="-0.106" x2="-3.939" y2="-8.396" width="0.2032" layer="21"/>
<pad name="1" x="-2.54" y="1.524" drill="1" diameter="1.5" shape="octagon"/>
<pad name="2" x="0" y="1.524" drill="1" diameter="1.5" shape="octagon"/>
<pad name="3" x="2.54" y="1.524" drill="1" diameter="1.5" shape="octagon"/>
<text x="-3.135" y="-7.65" size="1.6764" layer="21" font="vector">1</text>
<text x="-4.445" y="-7.62" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.715" y="-7.62" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.794" y1="0" x2="-2.286" y2="1.778" layer="51"/>
<rectangle x1="-0.254" y1="0" x2="0.254" y2="1.778" layer="51"/>
<rectangle x1="2.286" y1="0" x2="2.794" y2="1.778" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="FPINV">
<wire x1="-1.778" y1="0.508" x2="0" y2="0.508" width="0.254" layer="94"/>
<wire x1="0" y1="0.508" x2="0" y2="-0.508" width="0.254" layer="94"/>
<wire x1="0" y1="-0.508" x2="-1.778" y2="-0.508" width="0.254" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.048" y="0.762" size="1.524" layer="95" rot="R180">&gt;NAME</text>
<pin name="1" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="FPIN">
<wire x1="-1.778" y1="0.508" x2="0" y2="0.508" width="0.254" layer="94"/>
<wire x1="0" y1="0.508" x2="0" y2="-0.508" width="0.254" layer="94"/>
<wire x1="0" y1="-0.508" x2="-1.778" y2="-0.508" width="0.254" layer="94"/>
<text x="-3.048" y="0.762" size="1.524" layer="95" rot="R180">&gt;NAME</text>
<pin name="1" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SSW-103-02-S-S" prefix="X">
<description>&lt;b&gt;THROUGH-HOLE .025" SQ POST SOCKET&lt;/b&gt;&lt;p&gt;
Source: Samtec SSW.pdf</description>
<gates>
<gate name="-1" symbol="FPINV" x="0" y="2.54" addlevel="always"/>
<gate name="-2" symbol="FPIN" x="0" y="0" addlevel="always"/>
<gate name="-3" symbol="FPIN" x="0" y="-2.54" addlevel="always"/>
</gates>
<devices>
<device name="" package="SSW-103-02-S-S">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="SSW-103-02-S-S" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="11P9367" constant="no"/>
</technology>
</technologies>
</device>
<device name="-RA" package="SSW-103-02-S-S-RA">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="SSW-103-02-S-S-RA" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="11P9368" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ER">
<packages>
<package name="FIDUCIAL_1MM">
<smd name="1" x="0" y="0" dx="1" dy="1" layer="1" roundness="100" stop="no" cream="no"/>
<polygon width="0.127" layer="29">
<vertex x="-1" y="0" curve="90"/>
<vertex x="0" y="-1" curve="90"/>
<vertex x="1" y="0" curve="90"/>
<vertex x="0" y="1" curve="90"/>
</polygon>
<polygon width="0.127" layer="41">
<vertex x="-1" y="0" curve="90"/>
<vertex x="0" y="-1" curve="90"/>
<vertex x="1" y="0" curve="90"/>
<vertex x="0" y="1" curve="90"/>
</polygon>
</package>
<package name="LOGO-TOP-9.5MMX4.5MM">
<rectangle x1="0.07111875" y1="-0.00431875" x2="0.17271875" y2="0.00431875" layer="119"/>
<rectangle x1="0.98551875" y1="-0.00431875" x2="1.09728125" y2="0.00431875" layer="119"/>
<rectangle x1="1.50368125" y1="-0.00431875" x2="2.1971" y2="0.00431875" layer="119"/>
<rectangle x1="2.61111875" y1="-0.00431875" x2="3.47471875" y2="0.00431875" layer="119"/>
<rectangle x1="4.0513" y1="-0.00431875" x2="4.75488125" y2="0.00431875" layer="119"/>
<rectangle x1="6.37031875" y1="-0.00431875" x2="6.9469" y2="0.00431875" layer="119"/>
<rectangle x1="7.3279" y1="-0.00431875" x2="8.02131875" y2="0.00431875" layer="119"/>
<rectangle x1="8.56488125" y1="-0.00431875" x2="9.2583" y2="0.00431875" layer="119"/>
<rectangle x1="0.04571875" y1="0.00431875" x2="0.19811875" y2="0.0127" layer="119"/>
<rectangle x1="0.97028125" y1="0.00431875" x2="1.11251875" y2="0.0127" layer="119"/>
<rectangle x1="1.4605" y1="0.00431875" x2="2.24028125" y2="0.0127" layer="119"/>
<rectangle x1="2.59588125" y1="0.00431875" x2="3.5179" y2="0.0127" layer="119"/>
<rectangle x1="4.01828125" y1="0.00431875" x2="4.7879" y2="0.0127" layer="119"/>
<rectangle x1="6.35508125" y1="0.00431875" x2="6.96468125" y2="0.0127" layer="119"/>
<rectangle x1="7.29488125" y1="0.00431875" x2="8.0645" y2="0.0127" layer="119"/>
<rectangle x1="8.5217" y1="0.00431875" x2="9.29131875" y2="0.0127" layer="119"/>
<rectangle x1="0.02031875" y1="0.0127" x2="0.22351875" y2="0.02108125" layer="119"/>
<rectangle x1="0.9525" y1="0.0127" x2="1.1303" y2="0.02108125" layer="119"/>
<rectangle x1="1.4351" y1="0.0127" x2="2.26568125" y2="0.02108125" layer="119"/>
<rectangle x1="2.5781" y1="0.0127" x2="3.5433" y2="0.02108125" layer="119"/>
<rectangle x1="3.98271875" y1="0.0127" x2="4.82091875" y2="0.02108125" layer="119"/>
<rectangle x1="6.3373" y1="0.0127" x2="6.97991875" y2="0.02108125" layer="119"/>
<rectangle x1="7.25931875" y1="0.0127" x2="8.09751875" y2="0.02108125" layer="119"/>
<rectangle x1="8.48868125" y1="0.0127" x2="9.32688125" y2="0.02108125" layer="119"/>
<rectangle x1="0.0127" y1="0.02108125" x2="0.23368125" y2="0.02971875" layer="119"/>
<rectangle x1="0.93471875" y1="0.02108125" x2="1.13791875" y2="0.02971875" layer="119"/>
<rectangle x1="1.4097" y1="0.02108125" x2="2.29108125" y2="0.02971875" layer="119"/>
<rectangle x1="2.56031875" y1="0.02108125" x2="3.5687" y2="0.02971875" layer="119"/>
<rectangle x1="3.96748125" y1="0.02108125" x2="4.84631875" y2="0.02971875" layer="119"/>
<rectangle x1="6.32968125" y1="0.02108125" x2="6.99008125" y2="0.02971875" layer="119"/>
<rectangle x1="7.23391875" y1="0.02108125" x2="8.1153" y2="0.02971875" layer="119"/>
<rectangle x1="8.46328125" y1="0.02108125" x2="9.35228125" y2="0.02971875" layer="119"/>
<rectangle x1="0.0127" y1="0.02971875" x2="0.23368125" y2="0.0381" layer="119"/>
<rectangle x1="0.9271" y1="0.02971875" x2="1.14808125" y2="0.0381" layer="119"/>
<rectangle x1="1.39191875" y1="0.02971875" x2="2.30631875" y2="0.0381" layer="119"/>
<rectangle x1="2.56031875" y1="0.02971875" x2="3.58648125" y2="0.0381" layer="119"/>
<rectangle x1="3.94208125" y1="0.02971875" x2="4.8641" y2="0.0381" layer="119"/>
<rectangle x1="6.31951875" y1="0.02971875" x2="6.9977" y2="0.0381" layer="119"/>
<rectangle x1="7.21868125" y1="0.02971875" x2="8.1407" y2="0.0381" layer="119"/>
<rectangle x1="8.4455" y1="0.02971875" x2="9.36751875" y2="0.0381" layer="119"/>
<rectangle x1="0.0127" y1="0.0381" x2="0.23368125" y2="0.04648125" layer="119"/>
<rectangle x1="0.91948125" y1="0.0381" x2="1.14808125" y2="0.04648125" layer="119"/>
<rectangle x1="1.36651875" y1="0.0381" x2="2.33171875" y2="0.04648125" layer="119"/>
<rectangle x1="2.56031875" y1="0.0381" x2="3.60171875" y2="0.04648125" layer="119"/>
<rectangle x1="3.9243" y1="0.0381" x2="4.88188125" y2="0.04648125" layer="119"/>
<rectangle x1="5.63371875" y1="0.0381" x2="5.7277" y2="0.04648125" layer="119"/>
<rectangle x1="6.31951875" y1="0.0381" x2="6.9977" y2="0.04648125" layer="119"/>
<rectangle x1="7.2009" y1="0.0381" x2="8.15848125" y2="0.04648125" layer="119"/>
<rectangle x1="8.42771875" y1="0.0381" x2="9.3853" y2="0.04648125" layer="119"/>
<rectangle x1="0.0127" y1="0.04648125" x2="0.23368125" y2="0.05511875" layer="119"/>
<rectangle x1="0.90931875" y1="0.04648125" x2="1.14808125" y2="0.05511875" layer="119"/>
<rectangle x1="1.35128125" y1="0.04648125" x2="2.3495" y2="0.05511875" layer="119"/>
<rectangle x1="2.56031875" y1="0.04648125" x2="3.62711875" y2="0.05511875" layer="119"/>
<rectangle x1="3.90651875" y1="0.04648125" x2="4.89711875" y2="0.05511875" layer="119"/>
<rectangle x1="5.6007" y1="0.04648125" x2="5.76071875" y2="0.05511875" layer="119"/>
<rectangle x1="6.31951875" y1="0.04648125" x2="6.9977" y2="0.05511875" layer="119"/>
<rectangle x1="7.1755" y1="0.04648125" x2="8.17371875" y2="0.05511875" layer="119"/>
<rectangle x1="8.41248125" y1="0.04648125" x2="9.40308125" y2="0.05511875" layer="119"/>
<rectangle x1="0.0127" y1="0.05511875" x2="0.23368125" y2="0.0635" layer="119"/>
<rectangle x1="0.9017" y1="0.05511875" x2="1.13791875" y2="0.0635" layer="119"/>
<rectangle x1="1.3335" y1="0.05511875" x2="2.36728125" y2="0.0635" layer="119"/>
<rectangle x1="2.56031875" y1="0.05511875" x2="3.63728125" y2="0.0635" layer="119"/>
<rectangle x1="3.89128125" y1="0.05511875" x2="4.9149" y2="0.0635" layer="119"/>
<rectangle x1="5.58291875" y1="0.05511875" x2="5.7785" y2="0.0635" layer="119"/>
<rectangle x1="6.31951875" y1="0.05511875" x2="6.99008125" y2="0.0635" layer="119"/>
<rectangle x1="7.16788125" y1="0.05511875" x2="8.18388125" y2="0.0635" layer="119"/>
<rectangle x1="8.3947" y1="0.05511875" x2="9.41831875" y2="0.0635" layer="119"/>
<rectangle x1="0.0127" y1="0.0635" x2="0.23368125" y2="0.07188125" layer="119"/>
<rectangle x1="0.89408125" y1="0.0635" x2="1.1303" y2="0.07188125" layer="119"/>
<rectangle x1="1.32588125" y1="0.0635" x2="2.3749" y2="0.07188125" layer="119"/>
<rectangle x1="2.56031875" y1="0.0635" x2="3.65251875" y2="0.07188125" layer="119"/>
<rectangle x1="3.8735" y1="0.0635" x2="4.93268125" y2="0.07188125" layer="119"/>
<rectangle x1="5.5753" y1="0.0635" x2="5.78611875" y2="0.07188125" layer="119"/>
<rectangle x1="6.32968125" y1="0.0635" x2="6.97991875" y2="0.07188125" layer="119"/>
<rectangle x1="7.1501" y1="0.0635" x2="8.19911875" y2="0.07188125" layer="119"/>
<rectangle x1="8.38708125" y1="0.0635" x2="9.4361" y2="0.07188125" layer="119"/>
<rectangle x1="0.0127" y1="0.07188125" x2="0.23368125" y2="0.08051875" layer="119"/>
<rectangle x1="0.88391875" y1="0.07188125" x2="1.12268125" y2="0.08051875" layer="119"/>
<rectangle x1="1.3081" y1="0.07188125" x2="2.39268125" y2="0.08051875" layer="119"/>
<rectangle x1="2.56031875" y1="0.07188125" x2="3.66268125" y2="0.08051875" layer="119"/>
<rectangle x1="3.86588125" y1="0.07188125" x2="4.9403" y2="0.08051875" layer="119"/>
<rectangle x1="5.56768125" y1="0.07188125" x2="5.78611875" y2="0.08051875" layer="119"/>
<rectangle x1="6.34491875" y1="0.07188125" x2="6.9723" y2="0.08051875" layer="119"/>
<rectangle x1="7.14248125" y1="0.07188125" x2="8.20928125" y2="0.08051875" layer="119"/>
<rectangle x1="8.3693" y1="0.07188125" x2="9.44371875" y2="0.08051875" layer="119"/>
<rectangle x1="0.0127" y1="0.08051875" x2="0.23368125" y2="0.0889" layer="119"/>
<rectangle x1="0.86868125" y1="0.08051875" x2="1.11251875" y2="0.0889" layer="119"/>
<rectangle x1="1.30048125" y1="0.08051875" x2="2.4003" y2="0.0889" layer="119"/>
<rectangle x1="2.56031875" y1="0.08051875" x2="3.67791875" y2="0.0889" layer="119"/>
<rectangle x1="3.85571875" y1="0.08051875" x2="4.95808125" y2="0.0889" layer="119"/>
<rectangle x1="5.56768125" y1="0.08051875" x2="5.78611875" y2="0.0889" layer="119"/>
<rectangle x1="6.3627" y1="0.08051875" x2="6.95451875" y2="0.0889" layer="119"/>
<rectangle x1="7.1247" y1="0.08051875" x2="8.2169" y2="0.0889" layer="119"/>
<rectangle x1="8.36168125" y1="0.08051875" x2="9.45388125" y2="0.0889" layer="119"/>
<rectangle x1="0.0127" y1="0.0889" x2="0.23368125" y2="0.09728125" layer="119"/>
<rectangle x1="0.85851875" y1="0.0889" x2="1.1049" y2="0.09728125" layer="119"/>
<rectangle x1="1.29031875" y1="0.0889" x2="2.40791875" y2="0.09728125" layer="119"/>
<rectangle x1="2.56031875" y1="0.0889" x2="3.68808125" y2="0.09728125" layer="119"/>
<rectangle x1="3.8481" y1="0.0889" x2="4.95808125" y2="0.09728125" layer="119"/>
<rectangle x1="5.56768125" y1="0.0889" x2="5.78611875" y2="0.09728125" layer="119"/>
<rectangle x1="6.3881" y1="0.0889" x2="6.9215" y2="0.09728125" layer="119"/>
<rectangle x1="7.11708125" y1="0.0889" x2="8.22451875" y2="0.09728125" layer="119"/>
<rectangle x1="8.35151875" y1="0.0889" x2="9.4615" y2="0.09728125" layer="119"/>
<rectangle x1="0.0127" y1="0.09728125" x2="0.23368125" y2="0.10591875" layer="119"/>
<rectangle x1="0.8509" y1="0.09728125" x2="1.09728125" y2="0.10591875" layer="119"/>
<rectangle x1="1.2827" y1="0.09728125" x2="1.55448125" y2="0.10591875" layer="119"/>
<rectangle x1="2.1463" y1="0.09728125" x2="2.41808125" y2="0.10591875" layer="119"/>
<rectangle x1="2.56031875" y1="0.09728125" x2="2.7813" y2="0.10591875" layer="119"/>
<rectangle x1="3.42391875" y1="0.09728125" x2="3.68808125" y2="0.10591875" layer="119"/>
<rectangle x1="3.84048125" y1="0.09728125" x2="4.1021" y2="0.10591875" layer="119"/>
<rectangle x1="4.70408125" y1="0.09728125" x2="4.9657" y2="0.10591875" layer="119"/>
<rectangle x1="5.56768125" y1="0.09728125" x2="5.78611875" y2="0.10591875" layer="119"/>
<rectangle x1="6.54811875" y1="0.09728125" x2="6.7691" y2="0.10591875" layer="119"/>
<rectangle x1="7.11708125" y1="0.09728125" x2="7.3787" y2="0.10591875" layer="119"/>
<rectangle x1="7.98068125" y1="0.09728125" x2="8.23468125" y2="0.10591875" layer="119"/>
<rectangle x1="8.3439" y1="0.09728125" x2="8.60551875" y2="0.10591875" layer="119"/>
<rectangle x1="9.2075" y1="0.09728125" x2="9.46911875" y2="0.10591875" layer="119"/>
<rectangle x1="0.0127" y1="0.10591875" x2="0.23368125" y2="0.1143" layer="119"/>
<rectangle x1="0.84328125" y1="0.10591875" x2="1.08711875" y2="0.1143" layer="119"/>
<rectangle x1="1.2827" y1="0.10591875" x2="1.52908125" y2="0.1143" layer="119"/>
<rectangle x1="2.16408125" y1="0.10591875" x2="2.41808125" y2="0.1143" layer="119"/>
<rectangle x1="2.56031875" y1="0.10591875" x2="2.7813" y2="0.1143" layer="119"/>
<rectangle x1="3.44931875" y1="0.10591875" x2="3.6957" y2="0.1143" layer="119"/>
<rectangle x1="3.84048125" y1="0.10591875" x2="4.08431875" y2="0.1143" layer="119"/>
<rectangle x1="4.71931875" y1="0.10591875" x2="4.9657" y2="0.1143" layer="119"/>
<rectangle x1="5.56768125" y1="0.10591875" x2="5.78611875" y2="0.1143" layer="119"/>
<rectangle x1="6.54811875" y1="0.10591875" x2="6.7691" y2="0.1143" layer="119"/>
<rectangle x1="7.10691875" y1="0.10591875" x2="7.36091875" y2="0.1143" layer="119"/>
<rectangle x1="7.99591875" y1="0.10591875" x2="8.2423" y2="0.1143" layer="119"/>
<rectangle x1="8.3439" y1="0.10591875" x2="8.59028125" y2="0.1143" layer="119"/>
<rectangle x1="9.22528125" y1="0.10591875" x2="9.46911875" y2="0.1143" layer="119"/>
<rectangle x1="0.0127" y1="0.1143" x2="0.23368125" y2="0.12268125" layer="119"/>
<rectangle x1="0.83311875" y1="0.1143" x2="1.07188125" y2="0.12268125" layer="119"/>
<rectangle x1="1.2827" y1="0.1143" x2="1.5113" y2="0.12268125" layer="119"/>
<rectangle x1="2.18948125" y1="0.1143" x2="2.41808125" y2="0.12268125" layer="119"/>
<rectangle x1="2.56031875" y1="0.1143" x2="2.7813" y2="0.12268125" layer="119"/>
<rectangle x1="3.4671" y1="0.1143" x2="3.6957" y2="0.12268125" layer="119"/>
<rectangle x1="3.84048125" y1="0.1143" x2="4.06908125" y2="0.12268125" layer="119"/>
<rectangle x1="4.7371" y1="0.1143" x2="4.97331875" y2="0.12268125" layer="119"/>
<rectangle x1="5.56768125" y1="0.1143" x2="5.78611875" y2="0.12268125" layer="119"/>
<rectangle x1="6.54811875" y1="0.1143" x2="6.7691" y2="0.12268125" layer="119"/>
<rectangle x1="7.10691875" y1="0.1143" x2="7.33551875" y2="0.12268125" layer="119"/>
<rectangle x1="8.00608125" y1="0.1143" x2="8.2423" y2="0.12268125" layer="119"/>
<rectangle x1="8.33628125" y1="0.1143" x2="8.5725" y2="0.12268125" layer="119"/>
<rectangle x1="9.24051875" y1="0.1143" x2="9.47928125" y2="0.12268125" layer="119"/>
<rectangle x1="0.0127" y1="0.12268125" x2="0.23368125" y2="0.13131875" layer="119"/>
<rectangle x1="0.8255" y1="0.12268125" x2="1.06171875" y2="0.13131875" layer="119"/>
<rectangle x1="1.2827" y1="0.12268125" x2="1.50368125" y2="0.13131875" layer="119"/>
<rectangle x1="2.1971" y1="0.12268125" x2="2.41808125" y2="0.13131875" layer="119"/>
<rectangle x1="2.56031875" y1="0.12268125" x2="2.7813" y2="0.13131875" layer="119"/>
<rectangle x1="3.47471875" y1="0.12268125" x2="3.6957" y2="0.13131875" layer="119"/>
<rectangle x1="3.84048125" y1="0.12268125" x2="4.05891875" y2="0.13131875" layer="119"/>
<rectangle x1="4.75488125" y1="0.12268125" x2="4.97331875" y2="0.13131875" layer="119"/>
<rectangle x1="5.56768125" y1="0.12268125" x2="5.78611875" y2="0.13131875" layer="119"/>
<rectangle x1="6.54811875" y1="0.12268125" x2="6.7691" y2="0.13131875" layer="119"/>
<rectangle x1="7.10691875" y1="0.12268125" x2="7.3279" y2="0.13131875" layer="119"/>
<rectangle x1="8.0137" y1="0.12268125" x2="8.2423" y2="0.13131875" layer="119"/>
<rectangle x1="8.33628125" y1="0.12268125" x2="8.56488125" y2="0.13131875" layer="119"/>
<rectangle x1="9.25068125" y1="0.12268125" x2="9.47928125" y2="0.13131875" layer="119"/>
<rectangle x1="0.0127" y1="0.13131875" x2="0.23368125" y2="0.1397" layer="119"/>
<rectangle x1="0.81788125" y1="0.13131875" x2="1.0541" y2="0.1397" layer="119"/>
<rectangle x1="1.2827" y1="0.13131875" x2="1.50368125" y2="0.1397" layer="119"/>
<rectangle x1="2.1971" y1="0.13131875" x2="2.41808125" y2="0.1397" layer="119"/>
<rectangle x1="2.56031875" y1="0.13131875" x2="2.7813" y2="0.1397" layer="119"/>
<rectangle x1="3.47471875" y1="0.13131875" x2="3.6957" y2="0.1397" layer="119"/>
<rectangle x1="3.84048125" y1="0.13131875" x2="4.05891875" y2="0.1397" layer="119"/>
<rectangle x1="4.75488125" y1="0.13131875" x2="4.97331875" y2="0.1397" layer="119"/>
<rectangle x1="5.56768125" y1="0.13131875" x2="5.78611875" y2="0.1397" layer="119"/>
<rectangle x1="6.54811875" y1="0.13131875" x2="6.7691" y2="0.1397" layer="119"/>
<rectangle x1="7.10691875" y1="0.13131875" x2="7.3279" y2="0.1397" layer="119"/>
<rectangle x1="8.02131875" y1="0.13131875" x2="8.2423" y2="0.1397" layer="119"/>
<rectangle x1="8.33628125" y1="0.13131875" x2="8.56488125" y2="0.1397" layer="119"/>
<rectangle x1="9.25068125" y1="0.13131875" x2="9.47928125" y2="0.1397" layer="119"/>
<rectangle x1="0.0127" y1="0.1397" x2="0.23368125" y2="0.14808125" layer="119"/>
<rectangle x1="0.8001" y1="0.1397" x2="1.04648125" y2="0.14808125" layer="119"/>
<rectangle x1="1.2827" y1="0.1397" x2="1.50368125" y2="0.14808125" layer="119"/>
<rectangle x1="2.1971" y1="0.1397" x2="2.41808125" y2="0.14808125" layer="119"/>
<rectangle x1="2.56031875" y1="0.1397" x2="2.7813" y2="0.14808125" layer="119"/>
<rectangle x1="3.47471875" y1="0.1397" x2="3.6957" y2="0.14808125" layer="119"/>
<rectangle x1="3.84048125" y1="0.1397" x2="4.05891875" y2="0.14808125" layer="119"/>
<rectangle x1="4.75488125" y1="0.1397" x2="4.97331875" y2="0.14808125" layer="119"/>
<rectangle x1="5.56768125" y1="0.1397" x2="5.78611875" y2="0.14808125" layer="119"/>
<rectangle x1="6.54811875" y1="0.1397" x2="6.7691" y2="0.14808125" layer="119"/>
<rectangle x1="7.10691875" y1="0.1397" x2="7.3279" y2="0.14808125" layer="119"/>
<rectangle x1="8.02131875" y1="0.1397" x2="8.2423" y2="0.14808125" layer="119"/>
<rectangle x1="8.33628125" y1="0.1397" x2="8.55471875" y2="0.14808125" layer="119"/>
<rectangle x1="9.25068125" y1="0.1397" x2="9.47928125" y2="0.14808125" layer="119"/>
<rectangle x1="0.0127" y1="0.14808125" x2="0.23368125" y2="0.15671875" layer="119"/>
<rectangle x1="0.79248125" y1="0.14808125" x2="1.03631875" y2="0.15671875" layer="119"/>
<rectangle x1="1.2827" y1="0.14808125" x2="1.50368125" y2="0.15671875" layer="119"/>
<rectangle x1="2.1971" y1="0.14808125" x2="2.41808125" y2="0.15671875" layer="119"/>
<rectangle x1="2.56031875" y1="0.14808125" x2="2.7813" y2="0.15671875" layer="119"/>
<rectangle x1="3.47471875" y1="0.14808125" x2="3.6957" y2="0.15671875" layer="119"/>
<rectangle x1="3.84048125" y1="0.14808125" x2="4.05891875" y2="0.15671875" layer="119"/>
<rectangle x1="4.75488125" y1="0.14808125" x2="4.97331875" y2="0.15671875" layer="119"/>
<rectangle x1="5.56768125" y1="0.14808125" x2="5.78611875" y2="0.15671875" layer="119"/>
<rectangle x1="6.54811875" y1="0.14808125" x2="6.7691" y2="0.15671875" layer="119"/>
<rectangle x1="7.10691875" y1="0.14808125" x2="7.3279" y2="0.15671875" layer="119"/>
<rectangle x1="8.03148125" y1="0.14808125" x2="8.2423" y2="0.15671875" layer="119"/>
<rectangle x1="8.33628125" y1="0.14808125" x2="8.55471875" y2="0.15671875" layer="119"/>
<rectangle x1="9.25068125" y1="0.14808125" x2="9.47928125" y2="0.15671875" layer="119"/>
<rectangle x1="0.0127" y1="0.15671875" x2="0.23368125" y2="0.1651" layer="119"/>
<rectangle x1="0.78231875" y1="0.15671875" x2="1.0287" y2="0.1651" layer="119"/>
<rectangle x1="1.2827" y1="0.15671875" x2="1.50368125" y2="0.1651" layer="119"/>
<rectangle x1="2.1971" y1="0.15671875" x2="2.41808125" y2="0.1651" layer="119"/>
<rectangle x1="2.56031875" y1="0.15671875" x2="2.7813" y2="0.1651" layer="119"/>
<rectangle x1="3.47471875" y1="0.15671875" x2="3.6957" y2="0.1651" layer="119"/>
<rectangle x1="3.84048125" y1="0.15671875" x2="4.05891875" y2="0.1651" layer="119"/>
<rectangle x1="4.75488125" y1="0.15671875" x2="4.97331875" y2="0.1651" layer="119"/>
<rectangle x1="5.56768125" y1="0.15671875" x2="5.78611875" y2="0.1651" layer="119"/>
<rectangle x1="6.54811875" y1="0.15671875" x2="6.7691" y2="0.1651" layer="119"/>
<rectangle x1="7.10691875" y1="0.15671875" x2="7.3279" y2="0.1651" layer="119"/>
<rectangle x1="8.03148125" y1="0.15671875" x2="8.2423" y2="0.1651" layer="119"/>
<rectangle x1="8.3439" y1="0.15671875" x2="8.5471" y2="0.1651" layer="119"/>
<rectangle x1="9.25068125" y1="0.15671875" x2="9.47928125" y2="0.1651" layer="119"/>
<rectangle x1="0.0127" y1="0.1651" x2="0.23368125" y2="0.17348125" layer="119"/>
<rectangle x1="0.7747" y1="0.1651" x2="1.02108125" y2="0.17348125" layer="119"/>
<rectangle x1="1.2827" y1="0.1651" x2="1.50368125" y2="0.17348125" layer="119"/>
<rectangle x1="2.1971" y1="0.1651" x2="2.41808125" y2="0.17348125" layer="119"/>
<rectangle x1="2.56031875" y1="0.1651" x2="2.7813" y2="0.17348125" layer="119"/>
<rectangle x1="3.47471875" y1="0.1651" x2="3.6957" y2="0.17348125" layer="119"/>
<rectangle x1="3.84048125" y1="0.1651" x2="4.05891875" y2="0.17348125" layer="119"/>
<rectangle x1="4.75488125" y1="0.1651" x2="4.97331875" y2="0.17348125" layer="119"/>
<rectangle x1="5.56768125" y1="0.1651" x2="5.78611875" y2="0.17348125" layer="119"/>
<rectangle x1="6.54811875" y1="0.1651" x2="6.7691" y2="0.17348125" layer="119"/>
<rectangle x1="7.10691875" y1="0.1651" x2="7.3279" y2="0.17348125" layer="119"/>
<rectangle x1="8.0391" y1="0.1651" x2="8.23468125" y2="0.17348125" layer="119"/>
<rectangle x1="8.3439" y1="0.1651" x2="8.5471" y2="0.17348125" layer="119"/>
<rectangle x1="9.25068125" y1="0.1651" x2="9.47928125" y2="0.17348125" layer="119"/>
<rectangle x1="0.0127" y1="0.17348125" x2="0.23368125" y2="0.18211875" layer="119"/>
<rectangle x1="0.76708125" y1="0.17348125" x2="1.01091875" y2="0.18211875" layer="119"/>
<rectangle x1="1.2827" y1="0.17348125" x2="1.50368125" y2="0.18211875" layer="119"/>
<rectangle x1="2.1971" y1="0.17348125" x2="2.41808125" y2="0.18211875" layer="119"/>
<rectangle x1="2.56031875" y1="0.17348125" x2="2.7813" y2="0.18211875" layer="119"/>
<rectangle x1="3.47471875" y1="0.17348125" x2="3.6957" y2="0.18211875" layer="119"/>
<rectangle x1="3.84048125" y1="0.17348125" x2="4.05891875" y2="0.18211875" layer="119"/>
<rectangle x1="4.75488125" y1="0.17348125" x2="4.97331875" y2="0.18211875" layer="119"/>
<rectangle x1="5.56768125" y1="0.17348125" x2="5.78611875" y2="0.18211875" layer="119"/>
<rectangle x1="6.54811875" y1="0.17348125" x2="6.7691" y2="0.18211875" layer="119"/>
<rectangle x1="7.10691875" y1="0.17348125" x2="7.3279" y2="0.18211875" layer="119"/>
<rectangle x1="8.0391" y1="0.17348125" x2="8.2169" y2="0.18211875" layer="119"/>
<rectangle x1="8.35151875" y1="0.17348125" x2="8.53948125" y2="0.18211875" layer="119"/>
<rectangle x1="9.25068125" y1="0.17348125" x2="9.47928125" y2="0.18211875" layer="119"/>
<rectangle x1="0.0127" y1="0.18211875" x2="0.23368125" y2="0.1905" layer="119"/>
<rectangle x1="0.75691875" y1="0.18211875" x2="1.0033" y2="0.1905" layer="119"/>
<rectangle x1="1.2827" y1="0.18211875" x2="1.50368125" y2="0.1905" layer="119"/>
<rectangle x1="2.1971" y1="0.18211875" x2="2.41808125" y2="0.1905" layer="119"/>
<rectangle x1="2.56031875" y1="0.18211875" x2="2.7813" y2="0.1905" layer="119"/>
<rectangle x1="3.47471875" y1="0.18211875" x2="3.6957" y2="0.1905" layer="119"/>
<rectangle x1="3.84048125" y1="0.18211875" x2="4.05891875" y2="0.1905" layer="119"/>
<rectangle x1="4.75488125" y1="0.18211875" x2="4.97331875" y2="0.1905" layer="119"/>
<rectangle x1="5.56768125" y1="0.18211875" x2="5.78611875" y2="0.1905" layer="119"/>
<rectangle x1="6.54811875" y1="0.18211875" x2="6.7691" y2="0.1905" layer="119"/>
<rectangle x1="7.10691875" y1="0.18211875" x2="7.3279" y2="0.1905" layer="119"/>
<rectangle x1="8.0645" y1="0.18211875" x2="8.19911875" y2="0.1905" layer="119"/>
<rectangle x1="8.37691875" y1="0.18211875" x2="8.5217" y2="0.1905" layer="119"/>
<rectangle x1="9.25068125" y1="0.18211875" x2="9.47928125" y2="0.1905" layer="119"/>
<rectangle x1="0.0127" y1="0.1905" x2="0.23368125" y2="0.19888125" layer="119"/>
<rectangle x1="0.7493" y1="0.1905" x2="0.99568125" y2="0.19888125" layer="119"/>
<rectangle x1="1.2827" y1="0.1905" x2="1.50368125" y2="0.19888125" layer="119"/>
<rectangle x1="2.1971" y1="0.1905" x2="2.41808125" y2="0.19888125" layer="119"/>
<rectangle x1="2.56031875" y1="0.1905" x2="2.7813" y2="0.19888125" layer="119"/>
<rectangle x1="3.47471875" y1="0.1905" x2="3.6957" y2="0.19888125" layer="119"/>
<rectangle x1="3.84048125" y1="0.1905" x2="4.05891875" y2="0.19888125" layer="119"/>
<rectangle x1="4.75488125" y1="0.1905" x2="4.97331875" y2="0.19888125" layer="119"/>
<rectangle x1="5.56768125" y1="0.1905" x2="5.78611875" y2="0.19888125" layer="119"/>
<rectangle x1="6.54811875" y1="0.1905" x2="6.7691" y2="0.19888125" layer="119"/>
<rectangle x1="7.10691875" y1="0.1905" x2="7.3279" y2="0.19888125" layer="119"/>
<rectangle x1="8.09751875" y1="0.1905" x2="8.1661" y2="0.19888125" layer="119"/>
<rectangle x1="8.41248125" y1="0.1905" x2="8.48868125" y2="0.19888125" layer="119"/>
<rectangle x1="9.25068125" y1="0.1905" x2="9.47928125" y2="0.19888125" layer="119"/>
<rectangle x1="0.0127" y1="0.19888125" x2="0.23368125" y2="0.20751875" layer="119"/>
<rectangle x1="0.74168125" y1="0.19888125" x2="0.98551875" y2="0.20751875" layer="119"/>
<rectangle x1="1.2827" y1="0.19888125" x2="1.50368125" y2="0.20751875" layer="119"/>
<rectangle x1="2.1971" y1="0.19888125" x2="2.41808125" y2="0.20751875" layer="119"/>
<rectangle x1="2.56031875" y1="0.19888125" x2="2.7813" y2="0.20751875" layer="119"/>
<rectangle x1="3.47471875" y1="0.19888125" x2="3.6957" y2="0.20751875" layer="119"/>
<rectangle x1="3.84048125" y1="0.19888125" x2="4.05891875" y2="0.20751875" layer="119"/>
<rectangle x1="4.75488125" y1="0.19888125" x2="4.97331875" y2="0.20751875" layer="119"/>
<rectangle x1="5.56768125" y1="0.19888125" x2="5.78611875" y2="0.20751875" layer="119"/>
<rectangle x1="6.54811875" y1="0.19888125" x2="6.7691" y2="0.20751875" layer="119"/>
<rectangle x1="7.10691875" y1="0.19888125" x2="7.3279" y2="0.20751875" layer="119"/>
<rectangle x1="9.25068125" y1="0.19888125" x2="9.47928125" y2="0.20751875" layer="119"/>
<rectangle x1="0.0127" y1="0.20751875" x2="0.23368125" y2="0.2159" layer="119"/>
<rectangle x1="0.7239" y1="0.20751875" x2="0.97028125" y2="0.2159" layer="119"/>
<rectangle x1="1.2827" y1="0.20751875" x2="1.50368125" y2="0.2159" layer="119"/>
<rectangle x1="2.1971" y1="0.20751875" x2="2.41808125" y2="0.2159" layer="119"/>
<rectangle x1="2.56031875" y1="0.20751875" x2="2.7813" y2="0.2159" layer="119"/>
<rectangle x1="3.47471875" y1="0.20751875" x2="3.6957" y2="0.2159" layer="119"/>
<rectangle x1="3.84048125" y1="0.20751875" x2="4.05891875" y2="0.2159" layer="119"/>
<rectangle x1="4.75488125" y1="0.20751875" x2="4.97331875" y2="0.2159" layer="119"/>
<rectangle x1="5.56768125" y1="0.20751875" x2="5.78611875" y2="0.2159" layer="119"/>
<rectangle x1="6.54811875" y1="0.20751875" x2="6.7691" y2="0.2159" layer="119"/>
<rectangle x1="7.10691875" y1="0.20751875" x2="7.3279" y2="0.2159" layer="119"/>
<rectangle x1="9.25068125" y1="0.20751875" x2="9.47928125" y2="0.2159" layer="119"/>
<rectangle x1="0.0127" y1="0.2159" x2="0.23368125" y2="0.22428125" layer="119"/>
<rectangle x1="0.71628125" y1="0.2159" x2="0.96011875" y2="0.22428125" layer="119"/>
<rectangle x1="1.2827" y1="0.2159" x2="1.50368125" y2="0.22428125" layer="119"/>
<rectangle x1="2.1971" y1="0.2159" x2="2.41808125" y2="0.22428125" layer="119"/>
<rectangle x1="2.56031875" y1="0.2159" x2="2.7813" y2="0.22428125" layer="119"/>
<rectangle x1="3.47471875" y1="0.2159" x2="3.6957" y2="0.22428125" layer="119"/>
<rectangle x1="3.84048125" y1="0.2159" x2="4.05891875" y2="0.22428125" layer="119"/>
<rectangle x1="4.75488125" y1="0.2159" x2="4.97331875" y2="0.22428125" layer="119"/>
<rectangle x1="5.56768125" y1="0.2159" x2="5.78611875" y2="0.22428125" layer="119"/>
<rectangle x1="6.54811875" y1="0.2159" x2="6.7691" y2="0.22428125" layer="119"/>
<rectangle x1="7.10691875" y1="0.2159" x2="7.3279" y2="0.22428125" layer="119"/>
<rectangle x1="9.25068125" y1="0.2159" x2="9.47928125" y2="0.22428125" layer="119"/>
<rectangle x1="0.0127" y1="0.22428125" x2="0.23368125" y2="0.23291875" layer="119"/>
<rectangle x1="0.70611875" y1="0.22428125" x2="0.9525" y2="0.23291875" layer="119"/>
<rectangle x1="1.2827" y1="0.22428125" x2="1.50368125" y2="0.23291875" layer="119"/>
<rectangle x1="2.1971" y1="0.22428125" x2="2.41808125" y2="0.23291875" layer="119"/>
<rectangle x1="2.56031875" y1="0.22428125" x2="2.7813" y2="0.23291875" layer="119"/>
<rectangle x1="3.47471875" y1="0.22428125" x2="3.6957" y2="0.23291875" layer="119"/>
<rectangle x1="3.84048125" y1="0.22428125" x2="4.05891875" y2="0.23291875" layer="119"/>
<rectangle x1="4.75488125" y1="0.22428125" x2="4.97331875" y2="0.23291875" layer="119"/>
<rectangle x1="5.56768125" y1="0.22428125" x2="5.78611875" y2="0.23291875" layer="119"/>
<rectangle x1="6.54811875" y1="0.22428125" x2="6.7691" y2="0.23291875" layer="119"/>
<rectangle x1="7.10691875" y1="0.22428125" x2="7.3279" y2="0.23291875" layer="119"/>
<rectangle x1="9.25068125" y1="0.22428125" x2="9.47928125" y2="0.23291875" layer="119"/>
<rectangle x1="0.0127" y1="0.23291875" x2="0.23368125" y2="0.2413" layer="119"/>
<rectangle x1="0.6985" y1="0.23291875" x2="0.94488125" y2="0.2413" layer="119"/>
<rectangle x1="1.2827" y1="0.23291875" x2="1.50368125" y2="0.2413" layer="119"/>
<rectangle x1="2.1971" y1="0.23291875" x2="2.41808125" y2="0.2413" layer="119"/>
<rectangle x1="2.56031875" y1="0.23291875" x2="2.7813" y2="0.2413" layer="119"/>
<rectangle x1="3.47471875" y1="0.23291875" x2="3.6957" y2="0.2413" layer="119"/>
<rectangle x1="3.84048125" y1="0.23291875" x2="4.05891875" y2="0.2413" layer="119"/>
<rectangle x1="4.75488125" y1="0.23291875" x2="4.97331875" y2="0.2413" layer="119"/>
<rectangle x1="5.56768125" y1="0.23291875" x2="5.78611875" y2="0.2413" layer="119"/>
<rectangle x1="6.54811875" y1="0.23291875" x2="6.7691" y2="0.2413" layer="119"/>
<rectangle x1="7.10691875" y1="0.23291875" x2="7.3279" y2="0.2413" layer="119"/>
<rectangle x1="9.25068125" y1="0.23291875" x2="9.47928125" y2="0.2413" layer="119"/>
<rectangle x1="0.0127" y1="0.2413" x2="0.23368125" y2="0.24968125" layer="119"/>
<rectangle x1="0.69088125" y1="0.2413" x2="0.93471875" y2="0.24968125" layer="119"/>
<rectangle x1="1.2827" y1="0.2413" x2="1.50368125" y2="0.24968125" layer="119"/>
<rectangle x1="2.1971" y1="0.2413" x2="2.41808125" y2="0.24968125" layer="119"/>
<rectangle x1="2.56031875" y1="0.2413" x2="2.7813" y2="0.24968125" layer="119"/>
<rectangle x1="3.47471875" y1="0.2413" x2="3.6957" y2="0.24968125" layer="119"/>
<rectangle x1="3.84048125" y1="0.2413" x2="4.05891875" y2="0.24968125" layer="119"/>
<rectangle x1="4.75488125" y1="0.2413" x2="4.97331875" y2="0.24968125" layer="119"/>
<rectangle x1="5.56768125" y1="0.2413" x2="5.78611875" y2="0.24968125" layer="119"/>
<rectangle x1="6.54811875" y1="0.2413" x2="6.7691" y2="0.24968125" layer="119"/>
<rectangle x1="7.10691875" y1="0.2413" x2="7.3279" y2="0.24968125" layer="119"/>
<rectangle x1="9.25068125" y1="0.2413" x2="9.47928125" y2="0.24968125" layer="119"/>
<rectangle x1="0.0127" y1="0.24968125" x2="0.23368125" y2="0.25831875" layer="119"/>
<rectangle x1="0.68071875" y1="0.24968125" x2="0.9271" y2="0.25831875" layer="119"/>
<rectangle x1="1.2827" y1="0.24968125" x2="1.50368125" y2="0.25831875" layer="119"/>
<rectangle x1="2.1971" y1="0.24968125" x2="2.41808125" y2="0.25831875" layer="119"/>
<rectangle x1="2.56031875" y1="0.24968125" x2="2.7813" y2="0.25831875" layer="119"/>
<rectangle x1="3.47471875" y1="0.24968125" x2="3.6957" y2="0.25831875" layer="119"/>
<rectangle x1="3.84048125" y1="0.24968125" x2="4.05891875" y2="0.25831875" layer="119"/>
<rectangle x1="4.75488125" y1="0.24968125" x2="4.97331875" y2="0.25831875" layer="119"/>
<rectangle x1="5.56768125" y1="0.24968125" x2="5.78611875" y2="0.25831875" layer="119"/>
<rectangle x1="6.54811875" y1="0.24968125" x2="6.7691" y2="0.25831875" layer="119"/>
<rectangle x1="7.10691875" y1="0.24968125" x2="7.3279" y2="0.25831875" layer="119"/>
<rectangle x1="9.25068125" y1="0.24968125" x2="9.47928125" y2="0.25831875" layer="119"/>
<rectangle x1="0.0127" y1="0.25831875" x2="0.23368125" y2="0.2667" layer="119"/>
<rectangle x1="0.6731" y1="0.25831875" x2="0.91948125" y2="0.2667" layer="119"/>
<rectangle x1="1.2827" y1="0.25831875" x2="1.50368125" y2="0.2667" layer="119"/>
<rectangle x1="2.1971" y1="0.25831875" x2="2.41808125" y2="0.2667" layer="119"/>
<rectangle x1="2.56031875" y1="0.25831875" x2="2.7813" y2="0.2667" layer="119"/>
<rectangle x1="3.47471875" y1="0.25831875" x2="3.6957" y2="0.2667" layer="119"/>
<rectangle x1="3.84048125" y1="0.25831875" x2="4.05891875" y2="0.2667" layer="119"/>
<rectangle x1="4.75488125" y1="0.25831875" x2="4.97331875" y2="0.2667" layer="119"/>
<rectangle x1="5.56768125" y1="0.25831875" x2="5.78611875" y2="0.2667" layer="119"/>
<rectangle x1="6.54811875" y1="0.25831875" x2="6.7691" y2="0.2667" layer="119"/>
<rectangle x1="7.10691875" y1="0.25831875" x2="7.3279" y2="0.2667" layer="119"/>
<rectangle x1="9.25068125" y1="0.25831875" x2="9.47928125" y2="0.2667" layer="119"/>
<rectangle x1="0.0127" y1="0.2667" x2="0.23368125" y2="0.27508125" layer="119"/>
<rectangle x1="0.66548125" y1="0.2667" x2="0.90931875" y2="0.27508125" layer="119"/>
<rectangle x1="1.2827" y1="0.2667" x2="1.50368125" y2="0.27508125" layer="119"/>
<rectangle x1="2.1971" y1="0.2667" x2="2.41808125" y2="0.27508125" layer="119"/>
<rectangle x1="2.56031875" y1="0.2667" x2="2.7813" y2="0.27508125" layer="119"/>
<rectangle x1="3.47471875" y1="0.2667" x2="3.6957" y2="0.27508125" layer="119"/>
<rectangle x1="3.84048125" y1="0.2667" x2="4.05891875" y2="0.27508125" layer="119"/>
<rectangle x1="4.75488125" y1="0.2667" x2="4.97331875" y2="0.27508125" layer="119"/>
<rectangle x1="5.56768125" y1="0.2667" x2="5.78611875" y2="0.27508125" layer="119"/>
<rectangle x1="6.54811875" y1="0.2667" x2="6.7691" y2="0.27508125" layer="119"/>
<rectangle x1="7.10691875" y1="0.2667" x2="7.3279" y2="0.27508125" layer="119"/>
<rectangle x1="9.25068125" y1="0.2667" x2="9.47928125" y2="0.27508125" layer="119"/>
<rectangle x1="0.0127" y1="0.27508125" x2="0.23368125" y2="0.28371875" layer="119"/>
<rectangle x1="0.6477" y1="0.27508125" x2="0.89408125" y2="0.28371875" layer="119"/>
<rectangle x1="1.2827" y1="0.27508125" x2="1.50368125" y2="0.28371875" layer="119"/>
<rectangle x1="2.1971" y1="0.27508125" x2="2.41808125" y2="0.28371875" layer="119"/>
<rectangle x1="2.56031875" y1="0.27508125" x2="2.7813" y2="0.28371875" layer="119"/>
<rectangle x1="3.45948125" y1="0.27508125" x2="3.6957" y2="0.28371875" layer="119"/>
<rectangle x1="3.84048125" y1="0.27508125" x2="4.05891875" y2="0.28371875" layer="119"/>
<rectangle x1="4.75488125" y1="0.27508125" x2="4.97331875" y2="0.28371875" layer="119"/>
<rectangle x1="5.56768125" y1="0.27508125" x2="5.78611875" y2="0.28371875" layer="119"/>
<rectangle x1="6.54811875" y1="0.27508125" x2="6.7691" y2="0.28371875" layer="119"/>
<rectangle x1="7.10691875" y1="0.27508125" x2="7.3279" y2="0.28371875" layer="119"/>
<rectangle x1="9.24051875" y1="0.27508125" x2="9.47928125" y2="0.28371875" layer="119"/>
<rectangle x1="0.0127" y1="0.28371875" x2="0.23368125" y2="0.2921" layer="119"/>
<rectangle x1="0.64008125" y1="0.28371875" x2="0.88391875" y2="0.2921" layer="119"/>
<rectangle x1="1.2827" y1="0.28371875" x2="1.50368125" y2="0.2921" layer="119"/>
<rectangle x1="2.1971" y1="0.28371875" x2="2.41808125" y2="0.2921" layer="119"/>
<rectangle x1="2.56031875" y1="0.28371875" x2="2.7813" y2="0.2921" layer="119"/>
<rectangle x1="3.4417" y1="0.28371875" x2="3.68808125" y2="0.2921" layer="119"/>
<rectangle x1="3.84048125" y1="0.28371875" x2="4.05891875" y2="0.2921" layer="119"/>
<rectangle x1="4.75488125" y1="0.28371875" x2="4.97331875" y2="0.2921" layer="119"/>
<rectangle x1="5.56768125" y1="0.28371875" x2="5.78611875" y2="0.2921" layer="119"/>
<rectangle x1="6.54811875" y1="0.28371875" x2="6.7691" y2="0.2921" layer="119"/>
<rectangle x1="7.10691875" y1="0.28371875" x2="7.3279" y2="0.2921" layer="119"/>
<rectangle x1="9.21511875" y1="0.28371875" x2="9.46911875" y2="0.2921" layer="119"/>
<rectangle x1="0.0127" y1="0.2921" x2="0.23368125" y2="0.30048125" layer="119"/>
<rectangle x1="0.62991875" y1="0.2921" x2="0.8763" y2="0.30048125" layer="119"/>
<rectangle x1="1.2827" y1="0.2921" x2="1.50368125" y2="0.30048125" layer="119"/>
<rectangle x1="2.1971" y1="0.2921" x2="2.41808125" y2="0.30048125" layer="119"/>
<rectangle x1="2.56031875" y1="0.2921" x2="2.7813" y2="0.30048125" layer="119"/>
<rectangle x1="3.4163" y1="0.2921" x2="3.68808125" y2="0.30048125" layer="119"/>
<rectangle x1="3.84048125" y1="0.2921" x2="4.05891875" y2="0.30048125" layer="119"/>
<rectangle x1="4.75488125" y1="0.2921" x2="4.97331875" y2="0.30048125" layer="119"/>
<rectangle x1="5.56768125" y1="0.2921" x2="5.78611875" y2="0.30048125" layer="119"/>
<rectangle x1="6.54811875" y1="0.2921" x2="6.7691" y2="0.30048125" layer="119"/>
<rectangle x1="7.10691875" y1="0.2921" x2="7.3279" y2="0.30048125" layer="119"/>
<rectangle x1="9.19988125" y1="0.2921" x2="9.46911875" y2="0.30048125" layer="119"/>
<rectangle x1="0.0127" y1="0.30048125" x2="0.88391875" y2="0.30911875" layer="119"/>
<rectangle x1="1.2827" y1="0.30048125" x2="1.50368125" y2="0.30911875" layer="119"/>
<rectangle x1="2.1971" y1="0.30048125" x2="2.41808125" y2="0.30911875" layer="119"/>
<rectangle x1="2.56031875" y1="0.30048125" x2="3.67791875" y2="0.30911875" layer="119"/>
<rectangle x1="3.84048125" y1="0.30048125" x2="4.05891875" y2="0.30911875" layer="119"/>
<rectangle x1="4.75488125" y1="0.30048125" x2="4.97331875" y2="0.30911875" layer="119"/>
<rectangle x1="5.56768125" y1="0.30048125" x2="5.78611875" y2="0.30911875" layer="119"/>
<rectangle x1="6.54811875" y1="0.30048125" x2="6.7691" y2="0.30911875" layer="119"/>
<rectangle x1="7.10691875" y1="0.30048125" x2="7.3279" y2="0.30911875" layer="119"/>
<rectangle x1="8.5725" y1="0.30048125" x2="9.4615" y2="0.30911875" layer="119"/>
<rectangle x1="0.0127" y1="0.30911875" x2="0.94488125" y2="0.3175" layer="119"/>
<rectangle x1="1.2827" y1="0.30911875" x2="1.50368125" y2="0.3175" layer="119"/>
<rectangle x1="2.1971" y1="0.30911875" x2="2.41808125" y2="0.3175" layer="119"/>
<rectangle x1="2.56031875" y1="0.30911875" x2="3.66268125" y2="0.3175" layer="119"/>
<rectangle x1="3.84048125" y1="0.30911875" x2="4.05891875" y2="0.3175" layer="119"/>
<rectangle x1="4.75488125" y1="0.30911875" x2="4.97331875" y2="0.3175" layer="119"/>
<rectangle x1="5.56768125" y1="0.30911875" x2="5.78611875" y2="0.3175" layer="119"/>
<rectangle x1="6.54811875" y1="0.30911875" x2="6.7691" y2="0.3175" layer="119"/>
<rectangle x1="7.10691875" y1="0.30911875" x2="7.3279" y2="0.3175" layer="119"/>
<rectangle x1="8.52931875" y1="0.30911875" x2="9.45388125" y2="0.3175" layer="119"/>
<rectangle x1="0.0127" y1="0.3175" x2="0.9779" y2="0.32588125" layer="119"/>
<rectangle x1="1.2827" y1="0.3175" x2="1.50368125" y2="0.32588125" layer="119"/>
<rectangle x1="2.1971" y1="0.3175" x2="2.41808125" y2="0.32588125" layer="119"/>
<rectangle x1="2.56031875" y1="0.3175" x2="3.65251875" y2="0.32588125" layer="119"/>
<rectangle x1="3.84048125" y1="0.3175" x2="4.05891875" y2="0.32588125" layer="119"/>
<rectangle x1="4.75488125" y1="0.3175" x2="4.97331875" y2="0.32588125" layer="119"/>
<rectangle x1="5.56768125" y1="0.3175" x2="5.78611875" y2="0.32588125" layer="119"/>
<rectangle x1="6.54811875" y1="0.3175" x2="6.7691" y2="0.32588125" layer="119"/>
<rectangle x1="7.10691875" y1="0.3175" x2="7.3279" y2="0.32588125" layer="119"/>
<rectangle x1="8.4963" y1="0.3175" x2="9.44371875" y2="0.32588125" layer="119"/>
<rectangle x1="0.0127" y1="0.32588125" x2="1.01091875" y2="0.33451875" layer="119"/>
<rectangle x1="1.2827" y1="0.32588125" x2="1.50368125" y2="0.33451875" layer="119"/>
<rectangle x1="2.1971" y1="0.32588125" x2="2.41808125" y2="0.33451875" layer="119"/>
<rectangle x1="2.56031875" y1="0.32588125" x2="3.6449" y2="0.33451875" layer="119"/>
<rectangle x1="3.84048125" y1="0.32588125" x2="4.05891875" y2="0.33451875" layer="119"/>
<rectangle x1="4.75488125" y1="0.32588125" x2="4.97331875" y2="0.33451875" layer="119"/>
<rectangle x1="5.56768125" y1="0.32588125" x2="5.78611875" y2="0.33451875" layer="119"/>
<rectangle x1="6.54811875" y1="0.32588125" x2="6.7691" y2="0.33451875" layer="119"/>
<rectangle x1="7.10691875" y1="0.32588125" x2="7.3279" y2="0.33451875" layer="119"/>
<rectangle x1="8.4709" y1="0.32588125" x2="9.42848125" y2="0.33451875" layer="119"/>
<rectangle x1="0.0127" y1="0.33451875" x2="1.04648125" y2="0.3429" layer="119"/>
<rectangle x1="1.2827" y1="0.33451875" x2="1.50368125" y2="0.3429" layer="119"/>
<rectangle x1="2.1971" y1="0.33451875" x2="2.41808125" y2="0.3429" layer="119"/>
<rectangle x1="2.56031875" y1="0.33451875" x2="3.6195" y2="0.3429" layer="119"/>
<rectangle x1="3.84048125" y1="0.33451875" x2="4.05891875" y2="0.3429" layer="119"/>
<rectangle x1="4.75488125" y1="0.33451875" x2="4.97331875" y2="0.3429" layer="119"/>
<rectangle x1="5.56768125" y1="0.33451875" x2="5.78611875" y2="0.3429" layer="119"/>
<rectangle x1="6.54811875" y1="0.33451875" x2="6.7691" y2="0.3429" layer="119"/>
<rectangle x1="7.10691875" y1="0.33451875" x2="7.3279" y2="0.3429" layer="119"/>
<rectangle x1="8.45311875" y1="0.33451875" x2="9.41831875" y2="0.3429" layer="119"/>
<rectangle x1="0.0127" y1="0.3429" x2="1.06171875" y2="0.35128125" layer="119"/>
<rectangle x1="1.2827" y1="0.3429" x2="1.50368125" y2="0.35128125" layer="119"/>
<rectangle x1="2.1971" y1="0.3429" x2="2.41808125" y2="0.35128125" layer="119"/>
<rectangle x1="2.56031875" y1="0.3429" x2="3.58648125" y2="0.35128125" layer="119"/>
<rectangle x1="3.84048125" y1="0.3429" x2="4.05891875" y2="0.35128125" layer="119"/>
<rectangle x1="4.75488125" y1="0.3429" x2="4.97331875" y2="0.35128125" layer="119"/>
<rectangle x1="5.56768125" y1="0.3429" x2="5.78611875" y2="0.35128125" layer="119"/>
<rectangle x1="6.54811875" y1="0.3429" x2="6.7691" y2="0.35128125" layer="119"/>
<rectangle x1="7.10691875" y1="0.3429" x2="7.3279" y2="0.35128125" layer="119"/>
<rectangle x1="8.43788125" y1="0.3429" x2="9.40308125" y2="0.35128125" layer="119"/>
<rectangle x1="0.0127" y1="0.35128125" x2="1.07188125" y2="0.35991875" layer="119"/>
<rectangle x1="1.2827" y1="0.35128125" x2="1.50368125" y2="0.35991875" layer="119"/>
<rectangle x1="2.1971" y1="0.35128125" x2="2.41808125" y2="0.35991875" layer="119"/>
<rectangle x1="2.56031875" y1="0.35128125" x2="3.60171875" y2="0.35991875" layer="119"/>
<rectangle x1="3.84048125" y1="0.35128125" x2="4.05891875" y2="0.35991875" layer="119"/>
<rectangle x1="4.75488125" y1="0.35128125" x2="4.97331875" y2="0.35991875" layer="119"/>
<rectangle x1="5.56768125" y1="0.35128125" x2="5.78611875" y2="0.35991875" layer="119"/>
<rectangle x1="6.54811875" y1="0.35128125" x2="6.7691" y2="0.35991875" layer="119"/>
<rectangle x1="7.10691875" y1="0.35128125" x2="7.3279" y2="0.35991875" layer="119"/>
<rectangle x1="8.41248125" y1="0.35128125" x2="9.3853" y2="0.35991875" layer="119"/>
<rectangle x1="0.0127" y1="0.35991875" x2="1.08711875" y2="0.3683" layer="119"/>
<rectangle x1="1.2827" y1="0.35991875" x2="1.50368125" y2="0.3683" layer="119"/>
<rectangle x1="2.1971" y1="0.35991875" x2="2.41808125" y2="0.3683" layer="119"/>
<rectangle x1="2.56031875" y1="0.35991875" x2="3.62711875" y2="0.3683" layer="119"/>
<rectangle x1="3.84048125" y1="0.35991875" x2="4.05891875" y2="0.3683" layer="119"/>
<rectangle x1="4.75488125" y1="0.35991875" x2="4.97331875" y2="0.3683" layer="119"/>
<rectangle x1="5.56768125" y1="0.35991875" x2="5.78611875" y2="0.3683" layer="119"/>
<rectangle x1="6.54811875" y1="0.35991875" x2="6.7691" y2="0.3683" layer="119"/>
<rectangle x1="7.10691875" y1="0.35991875" x2="7.3279" y2="0.3683" layer="119"/>
<rectangle x1="8.40231875" y1="0.35991875" x2="9.3599" y2="0.3683" layer="119"/>
<rectangle x1="0.0127" y1="0.3683" x2="1.09728125" y2="0.37668125" layer="119"/>
<rectangle x1="1.2827" y1="0.3683" x2="1.50368125" y2="0.37668125" layer="119"/>
<rectangle x1="2.1971" y1="0.3683" x2="2.41808125" y2="0.37668125" layer="119"/>
<rectangle x1="2.56031875" y1="0.3683" x2="3.6449" y2="0.37668125" layer="119"/>
<rectangle x1="3.84048125" y1="0.3683" x2="4.05891875" y2="0.37668125" layer="119"/>
<rectangle x1="4.75488125" y1="0.3683" x2="4.97331875" y2="0.37668125" layer="119"/>
<rectangle x1="5.56768125" y1="0.3683" x2="5.78611875" y2="0.37668125" layer="119"/>
<rectangle x1="6.54811875" y1="0.3683" x2="6.7691" y2="0.37668125" layer="119"/>
<rectangle x1="7.10691875" y1="0.3683" x2="7.3279" y2="0.37668125" layer="119"/>
<rectangle x1="8.38708125" y1="0.3683" x2="9.34211875" y2="0.37668125" layer="119"/>
<rectangle x1="0.0127" y1="0.37668125" x2="1.11251875" y2="0.38531875" layer="119"/>
<rectangle x1="1.2827" y1="0.37668125" x2="1.50368125" y2="0.38531875" layer="119"/>
<rectangle x1="2.1971" y1="0.37668125" x2="2.41808125" y2="0.38531875" layer="119"/>
<rectangle x1="2.56031875" y1="0.37668125" x2="3.65251875" y2="0.38531875" layer="119"/>
<rectangle x1="3.84048125" y1="0.37668125" x2="4.05891875" y2="0.38531875" layer="119"/>
<rectangle x1="4.75488125" y1="0.37668125" x2="4.97331875" y2="0.38531875" layer="119"/>
<rectangle x1="5.56768125" y1="0.37668125" x2="5.78611875" y2="0.38531875" layer="119"/>
<rectangle x1="6.54811875" y1="0.37668125" x2="6.7691" y2="0.38531875" layer="119"/>
<rectangle x1="7.10691875" y1="0.37668125" x2="7.3279" y2="0.38531875" layer="119"/>
<rectangle x1="8.3693" y1="0.37668125" x2="9.31671875" y2="0.38531875" layer="119"/>
<rectangle x1="0.0127" y1="0.38531875" x2="1.12268125" y2="0.3937" layer="119"/>
<rectangle x1="1.2827" y1="0.38531875" x2="1.50368125" y2="0.3937" layer="119"/>
<rectangle x1="2.1971" y1="0.38531875" x2="2.41808125" y2="0.3937" layer="119"/>
<rectangle x1="2.56031875" y1="0.38531875" x2="3.66268125" y2="0.3937" layer="119"/>
<rectangle x1="3.84048125" y1="0.38531875" x2="4.05891875" y2="0.3937" layer="119"/>
<rectangle x1="4.75488125" y1="0.38531875" x2="4.97331875" y2="0.3937" layer="119"/>
<rectangle x1="5.56768125" y1="0.38531875" x2="5.78611875" y2="0.3937" layer="119"/>
<rectangle x1="6.54811875" y1="0.38531875" x2="6.7691" y2="0.3937" layer="119"/>
<rectangle x1="7.10691875" y1="0.38531875" x2="7.3279" y2="0.3937" layer="119"/>
<rectangle x1="8.36168125" y1="0.38531875" x2="9.2837" y2="0.3937" layer="119"/>
<rectangle x1="0.0127" y1="0.3937" x2="1.1303" y2="0.40208125" layer="119"/>
<rectangle x1="1.2827" y1="0.3937" x2="1.50368125" y2="0.40208125" layer="119"/>
<rectangle x1="2.1971" y1="0.3937" x2="2.41808125" y2="0.40208125" layer="119"/>
<rectangle x1="2.56031875" y1="0.3937" x2="3.67791875" y2="0.40208125" layer="119"/>
<rectangle x1="3.84048125" y1="0.3937" x2="4.05891875" y2="0.40208125" layer="119"/>
<rectangle x1="4.75488125" y1="0.3937" x2="4.97331875" y2="0.40208125" layer="119"/>
<rectangle x1="5.56768125" y1="0.3937" x2="5.78611875" y2="0.40208125" layer="119"/>
<rectangle x1="6.54811875" y1="0.3937" x2="6.7691" y2="0.40208125" layer="119"/>
<rectangle x1="7.10691875" y1="0.3937" x2="7.3279" y2="0.40208125" layer="119"/>
<rectangle x1="8.35151875" y1="0.3937" x2="9.24051875" y2="0.40208125" layer="119"/>
<rectangle x1="0.0127" y1="0.40208125" x2="0.23368125" y2="0.41071875" layer="119"/>
<rectangle x1="0.86868125" y1="0.40208125" x2="1.13791875" y2="0.41071875" layer="119"/>
<rectangle x1="1.2827" y1="0.40208125" x2="1.50368125" y2="0.41071875" layer="119"/>
<rectangle x1="2.1971" y1="0.40208125" x2="2.41808125" y2="0.41071875" layer="119"/>
<rectangle x1="2.56031875" y1="0.40208125" x2="2.7813" y2="0.41071875" layer="119"/>
<rectangle x1="3.4163" y1="0.40208125" x2="3.68808125" y2="0.41071875" layer="119"/>
<rectangle x1="3.84048125" y1="0.40208125" x2="4.05891875" y2="0.41071875" layer="119"/>
<rectangle x1="4.75488125" y1="0.40208125" x2="4.97331875" y2="0.41071875" layer="119"/>
<rectangle x1="5.56768125" y1="0.40208125" x2="5.78611875" y2="0.41071875" layer="119"/>
<rectangle x1="6.54811875" y1="0.40208125" x2="6.7691" y2="0.41071875" layer="119"/>
<rectangle x1="7.10691875" y1="0.40208125" x2="7.3279" y2="0.41071875" layer="119"/>
<rectangle x1="8.10768125" y1="0.40208125" x2="8.1661" y2="0.41071875" layer="119"/>
<rectangle x1="8.3439" y1="0.40208125" x2="8.61568125" y2="0.41071875" layer="119"/>
<rectangle x1="0.0127" y1="0.41071875" x2="0.23368125" y2="0.4191" layer="119"/>
<rectangle x1="0.9017" y1="0.41071875" x2="1.14808125" y2="0.4191" layer="119"/>
<rectangle x1="1.2827" y1="0.41071875" x2="1.50368125" y2="0.4191" layer="119"/>
<rectangle x1="2.1971" y1="0.41071875" x2="2.41808125" y2="0.4191" layer="119"/>
<rectangle x1="2.56031875" y1="0.41071875" x2="2.7813" y2="0.4191" layer="119"/>
<rectangle x1="3.44931875" y1="0.41071875" x2="3.68808125" y2="0.4191" layer="119"/>
<rectangle x1="3.84048125" y1="0.41071875" x2="4.05891875" y2="0.4191" layer="119"/>
<rectangle x1="4.75488125" y1="0.41071875" x2="4.97331875" y2="0.4191" layer="119"/>
<rectangle x1="5.56768125" y1="0.41071875" x2="5.78611875" y2="0.4191" layer="119"/>
<rectangle x1="6.54811875" y1="0.41071875" x2="6.7691" y2="0.4191" layer="119"/>
<rectangle x1="7.10691875" y1="0.41071875" x2="7.3279" y2="0.4191" layer="119"/>
<rectangle x1="8.0645" y1="0.41071875" x2="8.20928125" y2="0.4191" layer="119"/>
<rectangle x1="8.3439" y1="0.41071875" x2="8.59028125" y2="0.4191" layer="119"/>
<rectangle x1="0.0127" y1="0.4191" x2="0.23368125" y2="0.42748125" layer="119"/>
<rectangle x1="0.91948125" y1="0.4191" x2="1.14808125" y2="0.42748125" layer="119"/>
<rectangle x1="1.2827" y1="0.4191" x2="1.50368125" y2="0.42748125" layer="119"/>
<rectangle x1="2.1971" y1="0.4191" x2="2.41808125" y2="0.42748125" layer="119"/>
<rectangle x1="2.56031875" y1="0.4191" x2="2.7813" y2="0.42748125" layer="119"/>
<rectangle x1="3.4671" y1="0.4191" x2="3.6957" y2="0.42748125" layer="119"/>
<rectangle x1="3.84048125" y1="0.4191" x2="4.05891875" y2="0.42748125" layer="119"/>
<rectangle x1="4.75488125" y1="0.4191" x2="4.97331875" y2="0.42748125" layer="119"/>
<rectangle x1="5.56768125" y1="0.4191" x2="5.78611875" y2="0.42748125" layer="119"/>
<rectangle x1="6.54811875" y1="0.4191" x2="6.7691" y2="0.42748125" layer="119"/>
<rectangle x1="7.10691875" y1="0.4191" x2="7.3279" y2="0.42748125" layer="119"/>
<rectangle x1="8.04671875" y1="0.4191" x2="8.22451875" y2="0.42748125" layer="119"/>
<rectangle x1="8.3439" y1="0.4191" x2="8.5725" y2="0.42748125" layer="119"/>
<rectangle x1="0.0127" y1="0.42748125" x2="0.23368125" y2="0.43611875" layer="119"/>
<rectangle x1="0.9271" y1="0.42748125" x2="1.14808125" y2="0.43611875" layer="119"/>
<rectangle x1="1.2827" y1="0.42748125" x2="1.50368125" y2="0.43611875" layer="119"/>
<rectangle x1="2.1971" y1="0.42748125" x2="2.41808125" y2="0.43611875" layer="119"/>
<rectangle x1="2.56031875" y1="0.42748125" x2="2.7813" y2="0.43611875" layer="119"/>
<rectangle x1="3.47471875" y1="0.42748125" x2="3.6957" y2="0.43611875" layer="119"/>
<rectangle x1="3.84048125" y1="0.42748125" x2="4.05891875" y2="0.43611875" layer="119"/>
<rectangle x1="4.75488125" y1="0.42748125" x2="4.97331875" y2="0.43611875" layer="119"/>
<rectangle x1="5.56768125" y1="0.42748125" x2="5.78611875" y2="0.43611875" layer="119"/>
<rectangle x1="6.54811875" y1="0.42748125" x2="6.7691" y2="0.43611875" layer="119"/>
<rectangle x1="7.10691875" y1="0.42748125" x2="7.3279" y2="0.43611875" layer="119"/>
<rectangle x1="8.03148125" y1="0.42748125" x2="8.2423" y2="0.43611875" layer="119"/>
<rectangle x1="8.3439" y1="0.42748125" x2="8.56488125" y2="0.43611875" layer="119"/>
<rectangle x1="0.0127" y1="0.43611875" x2="0.23368125" y2="0.4445" layer="119"/>
<rectangle x1="0.9271" y1="0.43611875" x2="1.14808125" y2="0.4445" layer="119"/>
<rectangle x1="1.2827" y1="0.43611875" x2="1.50368125" y2="0.4445" layer="119"/>
<rectangle x1="2.1971" y1="0.43611875" x2="2.41808125" y2="0.4445" layer="119"/>
<rectangle x1="2.56031875" y1="0.43611875" x2="2.7813" y2="0.4445" layer="119"/>
<rectangle x1="3.47471875" y1="0.43611875" x2="3.6957" y2="0.4445" layer="119"/>
<rectangle x1="3.84048125" y1="0.43611875" x2="4.05891875" y2="0.4445" layer="119"/>
<rectangle x1="4.75488125" y1="0.43611875" x2="4.97331875" y2="0.4445" layer="119"/>
<rectangle x1="5.56768125" y1="0.43611875" x2="5.78611875" y2="0.4445" layer="119"/>
<rectangle x1="6.54811875" y1="0.43611875" x2="6.7691" y2="0.4445" layer="119"/>
<rectangle x1="7.10691875" y1="0.43611875" x2="7.3279" y2="0.4445" layer="119"/>
<rectangle x1="8.02131875" y1="0.43611875" x2="8.2423" y2="0.4445" layer="119"/>
<rectangle x1="8.3439" y1="0.43611875" x2="8.56488125" y2="0.4445" layer="119"/>
<rectangle x1="0.0127" y1="0.4445" x2="0.23368125" y2="0.45288125" layer="119"/>
<rectangle x1="0.9271" y1="0.4445" x2="1.14808125" y2="0.45288125" layer="119"/>
<rectangle x1="1.2827" y1="0.4445" x2="1.50368125" y2="0.45288125" layer="119"/>
<rectangle x1="2.1971" y1="0.4445" x2="2.41808125" y2="0.45288125" layer="119"/>
<rectangle x1="2.56031875" y1="0.4445" x2="2.7813" y2="0.45288125" layer="119"/>
<rectangle x1="3.47471875" y1="0.4445" x2="3.6957" y2="0.45288125" layer="119"/>
<rectangle x1="3.84048125" y1="0.4445" x2="4.05891875" y2="0.45288125" layer="119"/>
<rectangle x1="4.75488125" y1="0.4445" x2="4.97331875" y2="0.45288125" layer="119"/>
<rectangle x1="5.56768125" y1="0.4445" x2="5.78611875" y2="0.45288125" layer="119"/>
<rectangle x1="6.54811875" y1="0.4445" x2="6.7691" y2="0.45288125" layer="119"/>
<rectangle x1="7.10691875" y1="0.4445" x2="7.3279" y2="0.45288125" layer="119"/>
<rectangle x1="8.02131875" y1="0.4445" x2="8.2423" y2="0.45288125" layer="119"/>
<rectangle x1="8.3439" y1="0.4445" x2="8.56488125" y2="0.45288125" layer="119"/>
<rectangle x1="0.0127" y1="0.45288125" x2="0.23368125" y2="0.46151875" layer="119"/>
<rectangle x1="0.9271" y1="0.45288125" x2="1.14808125" y2="0.46151875" layer="119"/>
<rectangle x1="1.2827" y1="0.45288125" x2="1.50368125" y2="0.46151875" layer="119"/>
<rectangle x1="2.1971" y1="0.45288125" x2="2.41808125" y2="0.46151875" layer="119"/>
<rectangle x1="2.56031875" y1="0.45288125" x2="2.7813" y2="0.46151875" layer="119"/>
<rectangle x1="3.47471875" y1="0.45288125" x2="3.6957" y2="0.46151875" layer="119"/>
<rectangle x1="3.84048125" y1="0.45288125" x2="4.05891875" y2="0.46151875" layer="119"/>
<rectangle x1="4.75488125" y1="0.45288125" x2="4.97331875" y2="0.46151875" layer="119"/>
<rectangle x1="5.56768125" y1="0.45288125" x2="5.78611875" y2="0.46151875" layer="119"/>
<rectangle x1="6.54811875" y1="0.45288125" x2="6.7691" y2="0.46151875" layer="119"/>
<rectangle x1="7.10691875" y1="0.45288125" x2="7.3279" y2="0.46151875" layer="119"/>
<rectangle x1="8.02131875" y1="0.45288125" x2="8.2423" y2="0.46151875" layer="119"/>
<rectangle x1="8.3439" y1="0.45288125" x2="8.56488125" y2="0.46151875" layer="119"/>
<rectangle x1="0.0127" y1="0.46151875" x2="0.23368125" y2="0.4699" layer="119"/>
<rectangle x1="0.9271" y1="0.46151875" x2="1.14808125" y2="0.4699" layer="119"/>
<rectangle x1="1.2827" y1="0.46151875" x2="1.50368125" y2="0.4699" layer="119"/>
<rectangle x1="2.1971" y1="0.46151875" x2="2.41808125" y2="0.4699" layer="119"/>
<rectangle x1="2.56031875" y1="0.46151875" x2="2.7813" y2="0.4699" layer="119"/>
<rectangle x1="3.47471875" y1="0.46151875" x2="3.6957" y2="0.4699" layer="119"/>
<rectangle x1="3.84048125" y1="0.46151875" x2="4.05891875" y2="0.4699" layer="119"/>
<rectangle x1="4.75488125" y1="0.46151875" x2="4.97331875" y2="0.4699" layer="119"/>
<rectangle x1="5.56768125" y1="0.46151875" x2="5.78611875" y2="0.4699" layer="119"/>
<rectangle x1="6.54811875" y1="0.46151875" x2="6.7691" y2="0.4699" layer="119"/>
<rectangle x1="7.10691875" y1="0.46151875" x2="7.3279" y2="0.4699" layer="119"/>
<rectangle x1="8.02131875" y1="0.46151875" x2="8.2423" y2="0.4699" layer="119"/>
<rectangle x1="8.3439" y1="0.46151875" x2="8.56488125" y2="0.4699" layer="119"/>
<rectangle x1="0.0127" y1="0.4699" x2="0.23368125" y2="0.47828125" layer="119"/>
<rectangle x1="0.9271" y1="0.4699" x2="1.14808125" y2="0.47828125" layer="119"/>
<rectangle x1="1.2827" y1="0.4699" x2="1.50368125" y2="0.47828125" layer="119"/>
<rectangle x1="2.1971" y1="0.4699" x2="2.41808125" y2="0.47828125" layer="119"/>
<rectangle x1="2.56031875" y1="0.4699" x2="2.7813" y2="0.47828125" layer="119"/>
<rectangle x1="3.47471875" y1="0.4699" x2="3.6957" y2="0.47828125" layer="119"/>
<rectangle x1="3.84048125" y1="0.4699" x2="4.05891875" y2="0.47828125" layer="119"/>
<rectangle x1="4.75488125" y1="0.4699" x2="4.97331875" y2="0.47828125" layer="119"/>
<rectangle x1="5.56768125" y1="0.4699" x2="5.78611875" y2="0.47828125" layer="119"/>
<rectangle x1="6.54811875" y1="0.4699" x2="6.7691" y2="0.47828125" layer="119"/>
<rectangle x1="7.10691875" y1="0.4699" x2="7.3279" y2="0.47828125" layer="119"/>
<rectangle x1="8.02131875" y1="0.4699" x2="8.2423" y2="0.47828125" layer="119"/>
<rectangle x1="8.3439" y1="0.4699" x2="8.56488125" y2="0.47828125" layer="119"/>
<rectangle x1="0.0127" y1="0.47828125" x2="0.23368125" y2="0.48691875" layer="119"/>
<rectangle x1="0.9271" y1="0.47828125" x2="1.14808125" y2="0.48691875" layer="119"/>
<rectangle x1="1.2827" y1="0.47828125" x2="1.50368125" y2="0.48691875" layer="119"/>
<rectangle x1="2.1971" y1="0.47828125" x2="2.41808125" y2="0.48691875" layer="119"/>
<rectangle x1="2.56031875" y1="0.47828125" x2="2.7813" y2="0.48691875" layer="119"/>
<rectangle x1="3.47471875" y1="0.47828125" x2="3.6957" y2="0.48691875" layer="119"/>
<rectangle x1="3.84048125" y1="0.47828125" x2="4.05891875" y2="0.48691875" layer="119"/>
<rectangle x1="4.75488125" y1="0.47828125" x2="4.97331875" y2="0.48691875" layer="119"/>
<rectangle x1="5.56768125" y1="0.47828125" x2="5.78611875" y2="0.48691875" layer="119"/>
<rectangle x1="6.54811875" y1="0.47828125" x2="6.7691" y2="0.48691875" layer="119"/>
<rectangle x1="7.10691875" y1="0.47828125" x2="7.3279" y2="0.48691875" layer="119"/>
<rectangle x1="8.02131875" y1="0.47828125" x2="8.2423" y2="0.48691875" layer="119"/>
<rectangle x1="8.3439" y1="0.47828125" x2="8.56488125" y2="0.48691875" layer="119"/>
<rectangle x1="0.0127" y1="0.48691875" x2="0.23368125" y2="0.4953" layer="119"/>
<rectangle x1="0.9271" y1="0.48691875" x2="1.14808125" y2="0.4953" layer="119"/>
<rectangle x1="1.2827" y1="0.48691875" x2="1.50368125" y2="0.4953" layer="119"/>
<rectangle x1="2.1971" y1="0.48691875" x2="2.41808125" y2="0.4953" layer="119"/>
<rectangle x1="2.56031875" y1="0.48691875" x2="2.7813" y2="0.4953" layer="119"/>
<rectangle x1="3.47471875" y1="0.48691875" x2="3.6957" y2="0.4953" layer="119"/>
<rectangle x1="3.84048125" y1="0.48691875" x2="4.05891875" y2="0.4953" layer="119"/>
<rectangle x1="4.75488125" y1="0.48691875" x2="4.97331875" y2="0.4953" layer="119"/>
<rectangle x1="5.56768125" y1="0.48691875" x2="5.78611875" y2="0.4953" layer="119"/>
<rectangle x1="6.54811875" y1="0.48691875" x2="6.7691" y2="0.4953" layer="119"/>
<rectangle x1="7.10691875" y1="0.48691875" x2="7.3279" y2="0.4953" layer="119"/>
<rectangle x1="8.02131875" y1="0.48691875" x2="8.2423" y2="0.4953" layer="119"/>
<rectangle x1="8.3439" y1="0.48691875" x2="8.56488125" y2="0.4953" layer="119"/>
<rectangle x1="0.0127" y1="0.4953" x2="0.23368125" y2="0.50368125" layer="119"/>
<rectangle x1="0.9271" y1="0.4953" x2="1.14808125" y2="0.50368125" layer="119"/>
<rectangle x1="1.2827" y1="0.4953" x2="1.50368125" y2="0.50368125" layer="119"/>
<rectangle x1="2.1971" y1="0.4953" x2="2.41808125" y2="0.50368125" layer="119"/>
<rectangle x1="2.56031875" y1="0.4953" x2="2.7813" y2="0.50368125" layer="119"/>
<rectangle x1="3.47471875" y1="0.4953" x2="3.6957" y2="0.50368125" layer="119"/>
<rectangle x1="3.84048125" y1="0.4953" x2="4.05891875" y2="0.50368125" layer="119"/>
<rectangle x1="4.75488125" y1="0.4953" x2="4.97331875" y2="0.50368125" layer="119"/>
<rectangle x1="5.56768125" y1="0.4953" x2="5.78611875" y2="0.50368125" layer="119"/>
<rectangle x1="6.54811875" y1="0.4953" x2="6.7691" y2="0.50368125" layer="119"/>
<rectangle x1="7.10691875" y1="0.4953" x2="7.3279" y2="0.50368125" layer="119"/>
<rectangle x1="8.02131875" y1="0.4953" x2="8.2423" y2="0.50368125" layer="119"/>
<rectangle x1="8.3439" y1="0.4953" x2="8.56488125" y2="0.50368125" layer="119"/>
<rectangle x1="0.0127" y1="0.50368125" x2="0.23368125" y2="0.51231875" layer="119"/>
<rectangle x1="0.9271" y1="0.50368125" x2="1.14808125" y2="0.51231875" layer="119"/>
<rectangle x1="1.2827" y1="0.50368125" x2="1.50368125" y2="0.51231875" layer="119"/>
<rectangle x1="2.1971" y1="0.50368125" x2="2.41808125" y2="0.51231875" layer="119"/>
<rectangle x1="2.56031875" y1="0.50368125" x2="2.7813" y2="0.51231875" layer="119"/>
<rectangle x1="3.47471875" y1="0.50368125" x2="3.6957" y2="0.51231875" layer="119"/>
<rectangle x1="3.84048125" y1="0.50368125" x2="4.05891875" y2="0.51231875" layer="119"/>
<rectangle x1="4.75488125" y1="0.50368125" x2="4.97331875" y2="0.51231875" layer="119"/>
<rectangle x1="5.56768125" y1="0.50368125" x2="5.78611875" y2="0.51231875" layer="119"/>
<rectangle x1="6.54811875" y1="0.50368125" x2="6.7691" y2="0.51231875" layer="119"/>
<rectangle x1="7.10691875" y1="0.50368125" x2="7.3279" y2="0.51231875" layer="119"/>
<rectangle x1="8.02131875" y1="0.50368125" x2="8.2423" y2="0.51231875" layer="119"/>
<rectangle x1="8.3439" y1="0.50368125" x2="8.56488125" y2="0.51231875" layer="119"/>
<rectangle x1="9.31671875" y1="0.50368125" x2="9.4107" y2="0.51231875" layer="119"/>
<rectangle x1="0.0127" y1="0.51231875" x2="0.23368125" y2="0.5207" layer="119"/>
<rectangle x1="0.9271" y1="0.51231875" x2="1.14808125" y2="0.5207" layer="119"/>
<rectangle x1="1.2827" y1="0.51231875" x2="1.50368125" y2="0.5207" layer="119"/>
<rectangle x1="2.1971" y1="0.51231875" x2="2.41808125" y2="0.5207" layer="119"/>
<rectangle x1="2.56031875" y1="0.51231875" x2="2.7813" y2="0.5207" layer="119"/>
<rectangle x1="3.47471875" y1="0.51231875" x2="3.6957" y2="0.5207" layer="119"/>
<rectangle x1="3.84048125" y1="0.51231875" x2="4.05891875" y2="0.5207" layer="119"/>
<rectangle x1="4.75488125" y1="0.51231875" x2="4.97331875" y2="0.5207" layer="119"/>
<rectangle x1="5.56768125" y1="0.51231875" x2="5.78611875" y2="0.5207" layer="119"/>
<rectangle x1="6.54811875" y1="0.51231875" x2="6.7691" y2="0.5207" layer="119"/>
<rectangle x1="7.10691875" y1="0.51231875" x2="7.3279" y2="0.5207" layer="119"/>
<rectangle x1="8.02131875" y1="0.51231875" x2="8.2423" y2="0.5207" layer="119"/>
<rectangle x1="8.3439" y1="0.51231875" x2="8.56488125" y2="0.5207" layer="119"/>
<rectangle x1="9.2837" y1="0.51231875" x2="9.44371875" y2="0.5207" layer="119"/>
<rectangle x1="0.0127" y1="0.5207" x2="0.23368125" y2="0.52908125" layer="119"/>
<rectangle x1="0.9271" y1="0.5207" x2="1.14808125" y2="0.52908125" layer="119"/>
<rectangle x1="1.2827" y1="0.5207" x2="1.50368125" y2="0.52908125" layer="119"/>
<rectangle x1="2.1971" y1="0.5207" x2="2.41808125" y2="0.52908125" layer="119"/>
<rectangle x1="2.56031875" y1="0.5207" x2="2.7813" y2="0.52908125" layer="119"/>
<rectangle x1="3.47471875" y1="0.5207" x2="3.6957" y2="0.52908125" layer="119"/>
<rectangle x1="3.84048125" y1="0.5207" x2="4.05891875" y2="0.52908125" layer="119"/>
<rectangle x1="4.75488125" y1="0.5207" x2="4.97331875" y2="0.52908125" layer="119"/>
<rectangle x1="5.56768125" y1="0.5207" x2="5.78611875" y2="0.52908125" layer="119"/>
<rectangle x1="6.54811875" y1="0.5207" x2="6.7691" y2="0.52908125" layer="119"/>
<rectangle x1="7.10691875" y1="0.5207" x2="7.3279" y2="0.52908125" layer="119"/>
<rectangle x1="8.02131875" y1="0.5207" x2="8.2423" y2="0.52908125" layer="119"/>
<rectangle x1="8.3439" y1="0.5207" x2="8.56488125" y2="0.52908125" layer="119"/>
<rectangle x1="9.27608125" y1="0.5207" x2="9.4615" y2="0.52908125" layer="119"/>
<rectangle x1="0.0127" y1="0.52908125" x2="0.23368125" y2="0.53771875" layer="119"/>
<rectangle x1="0.9271" y1="0.52908125" x2="1.14808125" y2="0.53771875" layer="119"/>
<rectangle x1="1.2827" y1="0.52908125" x2="1.50368125" y2="0.53771875" layer="119"/>
<rectangle x1="2.1971" y1="0.52908125" x2="2.41808125" y2="0.53771875" layer="119"/>
<rectangle x1="2.56031875" y1="0.52908125" x2="2.7813" y2="0.53771875" layer="119"/>
<rectangle x1="3.47471875" y1="0.52908125" x2="3.6957" y2="0.53771875" layer="119"/>
<rectangle x1="3.84048125" y1="0.52908125" x2="4.05891875" y2="0.53771875" layer="119"/>
<rectangle x1="4.75488125" y1="0.52908125" x2="4.97331875" y2="0.53771875" layer="119"/>
<rectangle x1="5.56768125" y1="0.52908125" x2="5.78611875" y2="0.53771875" layer="119"/>
<rectangle x1="6.54811875" y1="0.52908125" x2="6.7691" y2="0.53771875" layer="119"/>
<rectangle x1="7.10691875" y1="0.52908125" x2="7.3279" y2="0.53771875" layer="119"/>
<rectangle x1="8.02131875" y1="0.52908125" x2="8.2423" y2="0.53771875" layer="119"/>
<rectangle x1="8.3439" y1="0.52908125" x2="8.56488125" y2="0.53771875" layer="119"/>
<rectangle x1="9.26591875" y1="0.52908125" x2="9.46911875" y2="0.53771875" layer="119"/>
<rectangle x1="0.0127" y1="0.53771875" x2="0.23368125" y2="0.5461" layer="119"/>
<rectangle x1="0.9271" y1="0.53771875" x2="1.14808125" y2="0.5461" layer="119"/>
<rectangle x1="1.2827" y1="0.53771875" x2="1.50368125" y2="0.5461" layer="119"/>
<rectangle x1="2.1971" y1="0.53771875" x2="2.41808125" y2="0.5461" layer="119"/>
<rectangle x1="2.56031875" y1="0.53771875" x2="2.7813" y2="0.5461" layer="119"/>
<rectangle x1="3.47471875" y1="0.53771875" x2="3.6957" y2="0.5461" layer="119"/>
<rectangle x1="3.84048125" y1="0.53771875" x2="4.05891875" y2="0.5461" layer="119"/>
<rectangle x1="4.75488125" y1="0.53771875" x2="4.97331875" y2="0.5461" layer="119"/>
<rectangle x1="5.56768125" y1="0.53771875" x2="5.78611875" y2="0.5461" layer="119"/>
<rectangle x1="6.54811875" y1="0.53771875" x2="6.7691" y2="0.5461" layer="119"/>
<rectangle x1="7.10691875" y1="0.53771875" x2="7.3279" y2="0.5461" layer="119"/>
<rectangle x1="8.02131875" y1="0.53771875" x2="8.2423" y2="0.5461" layer="119"/>
<rectangle x1="8.3439" y1="0.53771875" x2="8.56488125" y2="0.5461" layer="119"/>
<rectangle x1="9.2583" y1="0.53771875" x2="9.47928125" y2="0.5461" layer="119"/>
<rectangle x1="0.0127" y1="0.5461" x2="0.23368125" y2="0.55448125" layer="119"/>
<rectangle x1="0.9271" y1="0.5461" x2="1.14808125" y2="0.55448125" layer="119"/>
<rectangle x1="1.2827" y1="0.5461" x2="1.50368125" y2="0.55448125" layer="119"/>
<rectangle x1="2.1971" y1="0.5461" x2="2.41808125" y2="0.55448125" layer="119"/>
<rectangle x1="2.56031875" y1="0.5461" x2="2.7813" y2="0.55448125" layer="119"/>
<rectangle x1="3.47471875" y1="0.5461" x2="3.6957" y2="0.55448125" layer="119"/>
<rectangle x1="3.84048125" y1="0.5461" x2="4.05891875" y2="0.55448125" layer="119"/>
<rectangle x1="4.75488125" y1="0.5461" x2="4.97331875" y2="0.55448125" layer="119"/>
<rectangle x1="5.56768125" y1="0.5461" x2="5.78611875" y2="0.55448125" layer="119"/>
<rectangle x1="6.54811875" y1="0.5461" x2="6.7691" y2="0.55448125" layer="119"/>
<rectangle x1="7.10691875" y1="0.5461" x2="7.3279" y2="0.55448125" layer="119"/>
<rectangle x1="8.02131875" y1="0.5461" x2="8.2423" y2="0.55448125" layer="119"/>
<rectangle x1="8.3439" y1="0.5461" x2="8.56488125" y2="0.55448125" layer="119"/>
<rectangle x1="9.2583" y1="0.5461" x2="9.47928125" y2="0.55448125" layer="119"/>
<rectangle x1="0.0127" y1="0.55448125" x2="0.23368125" y2="0.56311875" layer="119"/>
<rectangle x1="0.9271" y1="0.55448125" x2="1.14808125" y2="0.56311875" layer="119"/>
<rectangle x1="1.2827" y1="0.55448125" x2="1.50368125" y2="0.56311875" layer="119"/>
<rectangle x1="2.1971" y1="0.55448125" x2="2.41808125" y2="0.56311875" layer="119"/>
<rectangle x1="2.56031875" y1="0.55448125" x2="2.7813" y2="0.56311875" layer="119"/>
<rectangle x1="3.47471875" y1="0.55448125" x2="3.6957" y2="0.56311875" layer="119"/>
<rectangle x1="3.84048125" y1="0.55448125" x2="4.05891875" y2="0.56311875" layer="119"/>
<rectangle x1="4.75488125" y1="0.55448125" x2="4.97331875" y2="0.56311875" layer="119"/>
<rectangle x1="5.56768125" y1="0.55448125" x2="5.78611875" y2="0.56311875" layer="119"/>
<rectangle x1="6.54811875" y1="0.55448125" x2="6.7691" y2="0.56311875" layer="119"/>
<rectangle x1="7.10691875" y1="0.55448125" x2="7.3279" y2="0.56311875" layer="119"/>
<rectangle x1="8.02131875" y1="0.55448125" x2="8.2423" y2="0.56311875" layer="119"/>
<rectangle x1="8.3439" y1="0.55448125" x2="8.56488125" y2="0.56311875" layer="119"/>
<rectangle x1="9.2583" y1="0.55448125" x2="9.47928125" y2="0.56311875" layer="119"/>
<rectangle x1="0.0127" y1="0.56311875" x2="0.23368125" y2="0.5715" layer="119"/>
<rectangle x1="0.9271" y1="0.56311875" x2="1.14808125" y2="0.5715" layer="119"/>
<rectangle x1="1.2827" y1="0.56311875" x2="1.50368125" y2="0.5715" layer="119"/>
<rectangle x1="2.1971" y1="0.56311875" x2="2.41808125" y2="0.5715" layer="119"/>
<rectangle x1="2.56031875" y1="0.56311875" x2="2.7813" y2="0.5715" layer="119"/>
<rectangle x1="3.47471875" y1="0.56311875" x2="3.6957" y2="0.5715" layer="119"/>
<rectangle x1="3.84048125" y1="0.56311875" x2="4.05891875" y2="0.5715" layer="119"/>
<rectangle x1="4.75488125" y1="0.56311875" x2="4.97331875" y2="0.5715" layer="119"/>
<rectangle x1="5.56768125" y1="0.56311875" x2="5.78611875" y2="0.5715" layer="119"/>
<rectangle x1="6.54811875" y1="0.56311875" x2="6.7691" y2="0.5715" layer="119"/>
<rectangle x1="7.10691875" y1="0.56311875" x2="7.3279" y2="0.5715" layer="119"/>
<rectangle x1="8.02131875" y1="0.56311875" x2="8.2423" y2="0.5715" layer="119"/>
<rectangle x1="8.3439" y1="0.56311875" x2="8.56488125" y2="0.5715" layer="119"/>
<rectangle x1="9.25068125" y1="0.56311875" x2="9.47928125" y2="0.5715" layer="119"/>
<rectangle x1="0.0127" y1="0.5715" x2="0.23368125" y2="0.57988125" layer="119"/>
<rectangle x1="0.9271" y1="0.5715" x2="1.14808125" y2="0.57988125" layer="119"/>
<rectangle x1="1.2827" y1="0.5715" x2="1.50368125" y2="0.57988125" layer="119"/>
<rectangle x1="2.1971" y1="0.5715" x2="2.41808125" y2="0.57988125" layer="119"/>
<rectangle x1="2.56031875" y1="0.5715" x2="2.7813" y2="0.57988125" layer="119"/>
<rectangle x1="3.47471875" y1="0.5715" x2="3.6957" y2="0.57988125" layer="119"/>
<rectangle x1="3.84048125" y1="0.5715" x2="4.05891875" y2="0.57988125" layer="119"/>
<rectangle x1="4.74471875" y1="0.5715" x2="4.97331875" y2="0.57988125" layer="119"/>
<rectangle x1="5.56768125" y1="0.5715" x2="5.78611875" y2="0.57988125" layer="119"/>
<rectangle x1="6.54811875" y1="0.5715" x2="6.7691" y2="0.57988125" layer="119"/>
<rectangle x1="7.10691875" y1="0.5715" x2="7.3279" y2="0.57988125" layer="119"/>
<rectangle x1="8.02131875" y1="0.5715" x2="8.2423" y2="0.57988125" layer="119"/>
<rectangle x1="8.3439" y1="0.5715" x2="8.56488125" y2="0.57988125" layer="119"/>
<rectangle x1="9.25068125" y1="0.5715" x2="9.47928125" y2="0.57988125" layer="119"/>
<rectangle x1="0.0127" y1="0.57988125" x2="0.23368125" y2="0.58851875" layer="119"/>
<rectangle x1="0.91948125" y1="0.57988125" x2="1.14808125" y2="0.58851875" layer="119"/>
<rectangle x1="1.2827" y1="0.57988125" x2="1.5113" y2="0.58851875" layer="119"/>
<rectangle x1="2.18948125" y1="0.57988125" x2="2.41808125" y2="0.58851875" layer="119"/>
<rectangle x1="2.56031875" y1="0.57988125" x2="2.7813" y2="0.58851875" layer="119"/>
<rectangle x1="3.45948125" y1="0.57988125" x2="3.6957" y2="0.58851875" layer="119"/>
<rectangle x1="3.84048125" y1="0.57988125" x2="4.06908125" y2="0.58851875" layer="119"/>
<rectangle x1="4.7371" y1="0.57988125" x2="4.97331875" y2="0.58851875" layer="119"/>
<rectangle x1="5.56768125" y1="0.57988125" x2="5.78611875" y2="0.58851875" layer="119"/>
<rectangle x1="6.54811875" y1="0.57988125" x2="6.7691" y2="0.58851875" layer="119"/>
<rectangle x1="7.10691875" y1="0.57988125" x2="7.34568125" y2="0.58851875" layer="119"/>
<rectangle x1="8.0137" y1="0.57988125" x2="8.2423" y2="0.58851875" layer="119"/>
<rectangle x1="8.3439" y1="0.57988125" x2="8.5725" y2="0.58851875" layer="119"/>
<rectangle x1="9.24051875" y1="0.57988125" x2="9.47928125" y2="0.58851875" layer="119"/>
<rectangle x1="0.0127" y1="0.58851875" x2="0.23368125" y2="0.5969" layer="119"/>
<rectangle x1="0.89408125" y1="0.58851875" x2="1.14808125" y2="0.5969" layer="119"/>
<rectangle x1="1.2827" y1="0.58851875" x2="1.52908125" y2="0.5969" layer="119"/>
<rectangle x1="2.16408125" y1="0.58851875" x2="2.41808125" y2="0.5969" layer="119"/>
<rectangle x1="2.56031875" y1="0.58851875" x2="2.7813" y2="0.5969" layer="119"/>
<rectangle x1="3.4417" y1="0.58851875" x2="3.6957" y2="0.5969" layer="119"/>
<rectangle x1="3.84048125" y1="0.58851875" x2="4.08431875" y2="0.5969" layer="119"/>
<rectangle x1="4.71931875" y1="0.58851875" x2="4.9657" y2="0.5969" layer="119"/>
<rectangle x1="5.56768125" y1="0.58851875" x2="5.78611875" y2="0.5969" layer="119"/>
<rectangle x1="6.54811875" y1="0.58851875" x2="6.7691" y2="0.5969" layer="119"/>
<rectangle x1="7.10691875" y1="0.58851875" x2="7.36091875" y2="0.5969" layer="119"/>
<rectangle x1="7.99591875" y1="0.58851875" x2="8.2423" y2="0.5969" layer="119"/>
<rectangle x1="8.3439" y1="0.58851875" x2="8.59028125" y2="0.5969" layer="119"/>
<rectangle x1="9.22528125" y1="0.58851875" x2="9.47928125" y2="0.5969" layer="119"/>
<rectangle x1="0.0127" y1="0.5969" x2="0.23368125" y2="0.60528125" layer="119"/>
<rectangle x1="0.86868125" y1="0.5969" x2="1.13791875" y2="0.60528125" layer="119"/>
<rectangle x1="1.2827" y1="0.5969" x2="1.55448125" y2="0.60528125" layer="119"/>
<rectangle x1="2.13868125" y1="0.5969" x2="2.40791875" y2="0.60528125" layer="119"/>
<rectangle x1="2.56031875" y1="0.5969" x2="2.7813" y2="0.60528125" layer="119"/>
<rectangle x1="3.4163" y1="0.5969" x2="3.68808125" y2="0.60528125" layer="119"/>
<rectangle x1="3.84048125" y1="0.5969" x2="4.10971875" y2="0.60528125" layer="119"/>
<rectangle x1="4.69391875" y1="0.5969" x2="4.9657" y2="0.60528125" layer="119"/>
<rectangle x1="5.56768125" y1="0.5969" x2="5.78611875" y2="0.60528125" layer="119"/>
<rectangle x1="6.54811875" y1="0.5969" x2="6.7691" y2="0.60528125" layer="119"/>
<rectangle x1="7.11708125" y1="0.5969" x2="7.3787" y2="0.60528125" layer="119"/>
<rectangle x1="7.97051875" y1="0.5969" x2="8.2423" y2="0.60528125" layer="119"/>
<rectangle x1="8.3439" y1="0.5969" x2="8.61568125" y2="0.60528125" layer="119"/>
<rectangle x1="9.19988125" y1="0.5969" x2="9.46911875" y2="0.60528125" layer="119"/>
<rectangle x1="0.0127" y1="0.60528125" x2="1.13791875" y2="0.61391875" layer="119"/>
<rectangle x1="1.29031875" y1="0.60528125" x2="2.40791875" y2="0.61391875" layer="119"/>
<rectangle x1="2.56031875" y1="0.60528125" x2="3.68808125" y2="0.61391875" layer="119"/>
<rectangle x1="3.8481" y1="0.60528125" x2="4.95808125" y2="0.61391875" layer="119"/>
<rectangle x1="5.17651875" y1="0.60528125" x2="6.1849" y2="0.61391875" layer="119"/>
<rectangle x1="6.38048125" y1="0.60528125" x2="6.92911875" y2="0.61391875" layer="119"/>
<rectangle x1="7.1247" y1="0.60528125" x2="8.23468125" y2="0.61391875" layer="119"/>
<rectangle x1="8.35151875" y1="0.60528125" x2="9.4615" y2="0.61391875" layer="119"/>
<rectangle x1="0.0127" y1="0.61391875" x2="1.1303" y2="0.6223" layer="119"/>
<rectangle x1="1.30048125" y1="0.61391875" x2="2.4003" y2="0.6223" layer="119"/>
<rectangle x1="2.56031875" y1="0.61391875" x2="3.67791875" y2="0.6223" layer="119"/>
<rectangle x1="3.85571875" y1="0.61391875" x2="4.94791875" y2="0.6223" layer="119"/>
<rectangle x1="5.15111875" y1="0.61391875" x2="6.2103" y2="0.6223" layer="119"/>
<rectangle x1="6.35508125" y1="0.61391875" x2="6.95451875" y2="0.6223" layer="119"/>
<rectangle x1="7.13231875" y1="0.61391875" x2="8.22451875" y2="0.6223" layer="119"/>
<rectangle x1="8.36168125" y1="0.61391875" x2="9.45388125" y2="0.6223" layer="119"/>
<rectangle x1="0.0127" y1="0.6223" x2="1.12268125" y2="0.63068125" layer="119"/>
<rectangle x1="1.31571875" y1="0.6223" x2="2.38251875" y2="0.63068125" layer="119"/>
<rectangle x1="2.56031875" y1="0.6223" x2="3.66268125" y2="0.63068125" layer="119"/>
<rectangle x1="3.86588125" y1="0.6223" x2="4.9403" y2="0.63068125" layer="119"/>
<rectangle x1="5.13588125" y1="0.6223" x2="6.22808125" y2="0.63068125" layer="119"/>
<rectangle x1="6.34491875" y1="0.6223" x2="6.9723" y2="0.63068125" layer="119"/>
<rectangle x1="7.14248125" y1="0.6223" x2="8.2169" y2="0.63068125" layer="119"/>
<rectangle x1="8.3693" y1="0.6223" x2="9.44371875" y2="0.63068125" layer="119"/>
<rectangle x1="0.0127" y1="0.63068125" x2="1.1049" y2="0.63931875" layer="119"/>
<rectangle x1="1.32588125" y1="0.63068125" x2="2.3749" y2="0.63931875" layer="119"/>
<rectangle x1="2.56031875" y1="0.63068125" x2="3.65251875" y2="0.63931875" layer="119"/>
<rectangle x1="3.88111875" y1="0.63068125" x2="4.93268125" y2="0.63931875" layer="119"/>
<rectangle x1="5.12571875" y1="0.63068125" x2="6.2357" y2="0.63931875" layer="119"/>
<rectangle x1="6.32968125" y1="0.63068125" x2="6.97991875" y2="0.63931875" layer="119"/>
<rectangle x1="7.1501" y1="0.63068125" x2="8.20928125" y2="0.63931875" layer="119"/>
<rectangle x1="8.37691875" y1="0.63068125" x2="9.4361" y2="0.63931875" layer="119"/>
<rectangle x1="0.0127" y1="0.63931875" x2="1.08711875" y2="0.6477" layer="119"/>
<rectangle x1="1.34111875" y1="0.63931875" x2="2.35711875" y2="0.6477" layer="119"/>
<rectangle x1="2.56031875" y1="0.63931875" x2="3.63728125" y2="0.6477" layer="119"/>
<rectangle x1="3.89128125" y1="0.63931875" x2="4.9149" y2="0.6477" layer="119"/>
<rectangle x1="5.1181" y1="0.63931875" x2="6.24331875" y2="0.6477" layer="119"/>
<rectangle x1="6.31951875" y1="0.63931875" x2="6.99008125" y2="0.6477" layer="119"/>
<rectangle x1="7.16788125" y1="0.63931875" x2="8.1915" y2="0.6477" layer="119"/>
<rectangle x1="8.3947" y1="0.63931875" x2="9.41831875" y2="0.6477" layer="119"/>
<rectangle x1="0.0127" y1="0.6477" x2="1.07188125" y2="0.65608125" layer="119"/>
<rectangle x1="1.3589" y1="0.6477" x2="2.34188125" y2="0.65608125" layer="119"/>
<rectangle x1="2.56031875" y1="0.6477" x2="3.6195" y2="0.65608125" layer="119"/>
<rectangle x1="3.90651875" y1="0.6477" x2="4.89711875" y2="0.65608125" layer="119"/>
<rectangle x1="5.1181" y1="0.6477" x2="6.24331875" y2="0.65608125" layer="119"/>
<rectangle x1="6.31951875" y1="0.6477" x2="6.9977" y2="0.65608125" layer="119"/>
<rectangle x1="7.18311875" y1="0.6477" x2="8.17371875" y2="0.65608125" layer="119"/>
<rectangle x1="8.41248125" y1="0.6477" x2="9.40308125" y2="0.65608125" layer="119"/>
<rectangle x1="0.0127" y1="0.65608125" x2="1.0541" y2="0.66471875" layer="119"/>
<rectangle x1="1.37668125" y1="0.65608125" x2="2.3241" y2="0.66471875" layer="119"/>
<rectangle x1="2.56031875" y1="0.65608125" x2="3.60171875" y2="0.66471875" layer="119"/>
<rectangle x1="3.9243" y1="0.65608125" x2="4.88188125" y2="0.66471875" layer="119"/>
<rectangle x1="5.1181" y1="0.65608125" x2="6.24331875" y2="0.66471875" layer="119"/>
<rectangle x1="6.31951875" y1="0.65608125" x2="6.9977" y2="0.66471875" layer="119"/>
<rectangle x1="7.2009" y1="0.65608125" x2="8.15848125" y2="0.66471875" layer="119"/>
<rectangle x1="8.42771875" y1="0.65608125" x2="9.3853" y2="0.66471875" layer="119"/>
<rectangle x1="0.0127" y1="0.66471875" x2="1.03631875" y2="0.6731" layer="119"/>
<rectangle x1="1.39191875" y1="0.66471875" x2="2.30631875" y2="0.6731" layer="119"/>
<rectangle x1="2.56031875" y1="0.66471875" x2="3.58648125" y2="0.6731" layer="119"/>
<rectangle x1="3.9497" y1="0.66471875" x2="4.8641" y2="0.6731" layer="119"/>
<rectangle x1="5.1181" y1="0.66471875" x2="6.24331875" y2="0.6731" layer="119"/>
<rectangle x1="6.31951875" y1="0.66471875" x2="6.9977" y2="0.6731" layer="119"/>
<rectangle x1="7.21868125" y1="0.66471875" x2="8.13308125" y2="0.6731" layer="119"/>
<rectangle x1="8.45311875" y1="0.66471875" x2="9.36751875" y2="0.6731" layer="119"/>
<rectangle x1="0.02031875" y1="0.6731" x2="1.01091875" y2="0.68148125" layer="119"/>
<rectangle x1="1.41731875" y1="0.6731" x2="2.28091875" y2="0.68148125" layer="119"/>
<rectangle x1="2.56031875" y1="0.6731" x2="3.56108125" y2="0.68148125" layer="119"/>
<rectangle x1="3.9751" y1="0.6731" x2="4.8387" y2="0.68148125" layer="119"/>
<rectangle x1="5.12571875" y1="0.6731" x2="6.2357" y2="0.68148125" layer="119"/>
<rectangle x1="6.32968125" y1="0.6731" x2="6.99008125" y2="0.68148125" layer="119"/>
<rectangle x1="7.24408125" y1="0.6731" x2="8.10768125" y2="0.68148125" layer="119"/>
<rectangle x1="8.47851875" y1="0.6731" x2="9.34211875" y2="0.68148125" layer="119"/>
<rectangle x1="0.03048125" y1="0.68148125" x2="0.98551875" y2="0.69011875" layer="119"/>
<rectangle x1="1.44271875" y1="0.68148125" x2="2.25551875" y2="0.69011875" layer="119"/>
<rectangle x1="2.5781" y1="0.68148125" x2="3.53568125" y2="0.69011875" layer="119"/>
<rectangle x1="4.0005" y1="0.68148125" x2="4.8133" y2="0.69011875" layer="119"/>
<rectangle x1="5.13588125" y1="0.68148125" x2="6.22808125" y2="0.69011875" layer="119"/>
<rectangle x1="6.3373" y1="0.68148125" x2="6.9723" y2="0.69011875" layer="119"/>
<rectangle x1="7.26948125" y1="0.68148125" x2="8.08228125" y2="0.69011875" layer="119"/>
<rectangle x1="8.50391875" y1="0.68148125" x2="9.31671875" y2="0.69011875" layer="119"/>
<rectangle x1="0.04571875" y1="0.69011875" x2="0.9525" y2="0.6985" layer="119"/>
<rectangle x1="1.47828125" y1="0.69011875" x2="2.2225" y2="0.6985" layer="119"/>
<rectangle x1="2.59588125" y1="0.69011875" x2="3.50011875" y2="0.6985" layer="119"/>
<rectangle x1="4.0259" y1="0.69011875" x2="4.78028125" y2="0.6985" layer="119"/>
<rectangle x1="5.1435" y1="0.69011875" x2="6.21791875" y2="0.6985" layer="119"/>
<rectangle x1="6.34491875" y1="0.69011875" x2="6.95451875" y2="0.6985" layer="119"/>
<rectangle x1="7.3025" y1="0.69011875" x2="8.05688125" y2="0.6985" layer="119"/>
<rectangle x1="8.52931875" y1="0.69011875" x2="9.29131875" y2="0.6985" layer="119"/>
<rectangle x1="0.08128125" y1="0.6985" x2="0.91948125" y2="0.70688125" layer="119"/>
<rectangle x1="1.5113" y1="0.6985" x2="2.18948125" y2="0.70688125" layer="119"/>
<rectangle x1="2.62128125" y1="0.6985" x2="3.45948125" y2="0.70688125" layer="119"/>
<rectangle x1="4.06908125" y1="0.6985" x2="4.7371" y2="0.70688125" layer="119"/>
<rectangle x1="5.1689" y1="0.6985" x2="6.19251875" y2="0.70688125" layer="119"/>
<rectangle x1="6.37031875" y1="0.6985" x2="6.92911875" y2="0.70688125" layer="119"/>
<rectangle x1="7.34568125" y1="0.6985" x2="8.0137" y2="0.70688125" layer="119"/>
<rectangle x1="8.5725" y1="0.6985" x2="9.25068125" y2="0.70688125" layer="119"/>
<rectangle x1="0.10668125" y1="0.89331875" x2="1.7907" y2="0.9017" layer="119"/>
<rectangle x1="2.07771875" y1="0.89331875" x2="3.14451875" y2="0.9017" layer="119"/>
<rectangle x1="4.2291" y1="0.89331875" x2="4.37388125" y2="0.9017" layer="119"/>
<rectangle x1="5.7531" y1="0.89331875" x2="5.88771875" y2="0.9017" layer="119"/>
<rectangle x1="6.3627" y1="0.89331875" x2="7.29488125" y2="0.9017" layer="119"/>
<rectangle x1="7.7089" y1="0.89331875" x2="7.82828125" y2="0.9017" layer="119"/>
<rectangle x1="9.2583" y1="0.89331875" x2="9.37768125" y2="0.9017" layer="119"/>
<rectangle x1="0.08128125" y1="0.9017" x2="1.8161" y2="0.91008125" layer="119"/>
<rectangle x1="2.05231875" y1="0.9017" x2="3.19531875" y2="0.91008125" layer="119"/>
<rectangle x1="4.2037" y1="0.9017" x2="4.38911875" y2="0.91008125" layer="119"/>
<rectangle x1="5.7277" y1="0.9017" x2="5.92328125" y2="0.91008125" layer="119"/>
<rectangle x1="6.3373" y1="0.9017" x2="7.32028125" y2="0.91008125" layer="119"/>
<rectangle x1="7.66571875" y1="0.9017" x2="7.86891875" y2="0.91008125" layer="119"/>
<rectangle x1="9.21511875" y1="0.9017" x2="9.4107" y2="0.91008125" layer="119"/>
<rectangle x1="0.0635" y1="0.91008125" x2="1.83388125" y2="0.91871875" layer="119"/>
<rectangle x1="2.03708125" y1="0.91008125" x2="3.23088125" y2="0.91871875" layer="119"/>
<rectangle x1="4.1783" y1="0.91008125" x2="4.39928125" y2="0.91871875" layer="119"/>
<rectangle x1="5.72008125" y1="0.91008125" x2="5.93851875" y2="0.91871875" layer="119"/>
<rectangle x1="6.32968125" y1="0.91008125" x2="7.33551875" y2="0.91871875" layer="119"/>
<rectangle x1="7.65048125" y1="0.91008125" x2="7.89431875" y2="0.91871875" layer="119"/>
<rectangle x1="9.19988125" y1="0.91008125" x2="9.4361" y2="0.91871875" layer="119"/>
<rectangle x1="0.05588125" y1="0.91871875" x2="1.84911875" y2="0.9271" layer="119"/>
<rectangle x1="2.02691875" y1="0.91871875" x2="3.2639" y2="0.9271" layer="119"/>
<rectangle x1="4.16051875" y1="0.91871875" x2="4.41451875" y2="0.9271" layer="119"/>
<rectangle x1="5.7023" y1="0.91871875" x2="5.9563" y2="0.9271" layer="119"/>
<rectangle x1="6.3119" y1="0.91871875" x2="7.34568125" y2="0.9271" layer="119"/>
<rectangle x1="7.6327" y1="0.91871875" x2="7.9121" y2="0.9271" layer="119"/>
<rectangle x1="9.1821" y1="0.91871875" x2="9.44371875" y2="0.9271" layer="119"/>
<rectangle x1="0.0381" y1="0.9271" x2="1.8669" y2="0.93548125" layer="119"/>
<rectangle x1="2.01168125" y1="0.9271" x2="3.2893" y2="0.93548125" layer="119"/>
<rectangle x1="4.14528125" y1="0.9271" x2="4.42468125" y2="0.93548125" layer="119"/>
<rectangle x1="5.68451875" y1="0.9271" x2="5.97408125" y2="0.93548125" layer="119"/>
<rectangle x1="6.30428125" y1="0.9271" x2="7.3533" y2="0.93548125" layer="119"/>
<rectangle x1="7.62508125" y1="0.9271" x2="7.92988125" y2="0.93548125" layer="119"/>
<rectangle x1="9.16431875" y1="0.9271" x2="9.4615" y2="0.93548125" layer="119"/>
<rectangle x1="0.03048125" y1="0.93548125" x2="1.87451875" y2="0.94411875" layer="119"/>
<rectangle x1="2.00151875" y1="0.93548125" x2="3.3147" y2="0.94411875" layer="119"/>
<rectangle x1="4.1275" y1="0.93548125" x2="4.43991875" y2="0.94411875" layer="119"/>
<rectangle x1="5.66928125" y1="0.93548125" x2="5.98931875" y2="0.94411875" layer="119"/>
<rectangle x1="6.29411875" y1="0.93548125" x2="7.37108125" y2="0.94411875" layer="119"/>
<rectangle x1="7.61491875" y1="0.93548125" x2="7.9375" y2="0.94411875" layer="119"/>
<rectangle x1="9.1567" y1="0.93548125" x2="9.46911875" y2="0.94411875" layer="119"/>
<rectangle x1="0.0127" y1="0.94411875" x2="1.88468125" y2="0.9525" layer="119"/>
<rectangle x1="1.9939" y1="0.94411875" x2="3.3401" y2="0.9525" layer="119"/>
<rectangle x1="4.11988125" y1="0.94411875" x2="4.4577" y2="0.9525" layer="119"/>
<rectangle x1="5.65911875" y1="0.94411875" x2="5.99948125" y2="0.9525" layer="119"/>
<rectangle x1="6.2865" y1="0.94411875" x2="7.3787" y2="0.9525" layer="119"/>
<rectangle x1="7.6073" y1="0.94411875" x2="7.94511875" y2="0.9525" layer="119"/>
<rectangle x1="9.14908125" y1="0.94411875" x2="9.47928125" y2="0.9525" layer="119"/>
<rectangle x1="0.00508125" y1="0.9525" x2="1.88468125" y2="0.96088125" layer="119"/>
<rectangle x1="1.97611875" y1="0.9525" x2="3.3655" y2="0.96088125" layer="119"/>
<rectangle x1="4.10971875" y1="0.9525" x2="4.46531875" y2="0.96088125" layer="119"/>
<rectangle x1="5.64388125" y1="0.9525" x2="6.0071" y2="0.96088125" layer="119"/>
<rectangle x1="6.27888125" y1="0.9525" x2="7.3787" y2="0.96088125" layer="119"/>
<rectangle x1="7.59968125" y1="0.9525" x2="7.95528125" y2="0.96088125" layer="119"/>
<rectangle x1="9.13891875" y1="0.9525" x2="9.4869" y2="0.96088125" layer="119"/>
<rectangle x1="0.00508125" y1="0.96088125" x2="1.8923" y2="0.96951875" layer="119"/>
<rectangle x1="1.97611875" y1="0.96088125" x2="3.38328125" y2="0.96951875" layer="119"/>
<rectangle x1="4.10971875" y1="0.96088125" x2="4.4831" y2="0.96951875" layer="119"/>
<rectangle x1="5.6261" y1="0.96088125" x2="6.0071" y2="0.96951875" layer="119"/>
<rectangle x1="6.27888125" y1="0.96088125" x2="7.38631875" y2="0.96951875" layer="119"/>
<rectangle x1="7.59968125" y1="0.96088125" x2="7.95528125" y2="0.96951875" layer="119"/>
<rectangle x1="9.1313" y1="0.96088125" x2="9.4869" y2="0.96951875" layer="119"/>
<rectangle x1="-0.00508125" y1="0.96951875" x2="1.8923" y2="0.9779" layer="119"/>
<rectangle x1="1.9685" y1="0.96951875" x2="3.39851875" y2="0.9779" layer="119"/>
<rectangle x1="4.10971875" y1="0.96951875" x2="4.50088125" y2="0.9779" layer="119"/>
<rectangle x1="5.60831875" y1="0.96951875" x2="6.01471875" y2="0.9779" layer="119"/>
<rectangle x1="6.26871875" y1="0.96951875" x2="7.39648125" y2="0.9779" layer="119"/>
<rectangle x1="7.58951875" y1="0.96951875" x2="7.9629" y2="0.9779" layer="119"/>
<rectangle x1="9.12368125" y1="0.96951875" x2="9.49451875" y2="0.9779" layer="119"/>
<rectangle x1="-0.00508125" y1="0.9779" x2="1.89991875" y2="0.98628125" layer="119"/>
<rectangle x1="1.9685" y1="0.9779" x2="3.42391875" y2="0.98628125" layer="119"/>
<rectangle x1="4.10971875" y1="0.9779" x2="4.51611875" y2="0.98628125" layer="119"/>
<rectangle x1="5.6007" y1="0.9779" x2="6.01471875" y2="0.98628125" layer="119"/>
<rectangle x1="6.2611" y1="0.9779" x2="7.39648125" y2="0.98628125" layer="119"/>
<rectangle x1="7.58951875" y1="0.9779" x2="7.9629" y2="0.98628125" layer="119"/>
<rectangle x1="9.11351875" y1="0.9779" x2="9.49451875" y2="0.98628125" layer="119"/>
<rectangle x1="-0.00508125" y1="0.98628125" x2="1.89991875" y2="0.99491875" layer="119"/>
<rectangle x1="1.9685" y1="0.98628125" x2="3.4417" y2="0.99491875" layer="119"/>
<rectangle x1="4.10971875" y1="0.98628125" x2="4.52628125" y2="0.99491875" layer="119"/>
<rectangle x1="5.58291875" y1="0.98628125" x2="6.01471875" y2="0.99491875" layer="119"/>
<rectangle x1="6.2611" y1="0.98628125" x2="7.4041" y2="0.99491875" layer="119"/>
<rectangle x1="7.58951875" y1="0.98628125" x2="7.9629" y2="0.99491875" layer="119"/>
<rectangle x1="9.1059" y1="0.98628125" x2="9.49451875" y2="0.99491875" layer="119"/>
<rectangle x1="-0.00508125" y1="0.99491875" x2="1.89991875" y2="1.0033" layer="119"/>
<rectangle x1="1.9685" y1="0.99491875" x2="3.45948125" y2="1.0033" layer="119"/>
<rectangle x1="4.10971875" y1="0.99491875" x2="4.54151875" y2="1.0033" layer="119"/>
<rectangle x1="5.5753" y1="0.99491875" x2="6.01471875" y2="1.0033" layer="119"/>
<rectangle x1="6.2611" y1="0.99491875" x2="7.4041" y2="1.0033" layer="119"/>
<rectangle x1="7.58951875" y1="0.99491875" x2="7.9629" y2="1.0033" layer="119"/>
<rectangle x1="9.08811875" y1="0.99491875" x2="9.49451875" y2="1.0033" layer="119"/>
<rectangle x1="-0.00508125" y1="1.0033" x2="1.89991875" y2="1.01168125" layer="119"/>
<rectangle x1="1.9685" y1="1.0033" x2="3.47471875" y2="1.01168125" layer="119"/>
<rectangle x1="4.10971875" y1="1.0033" x2="4.5593" y2="1.01168125" layer="119"/>
<rectangle x1="5.55751875" y1="1.0033" x2="6.01471875" y2="1.01168125" layer="119"/>
<rectangle x1="6.2611" y1="1.0033" x2="7.4041" y2="1.01168125" layer="119"/>
<rectangle x1="7.58951875" y1="1.0033" x2="7.9629" y2="1.01168125" layer="119"/>
<rectangle x1="9.0805" y1="1.0033" x2="9.49451875" y2="1.01168125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.01168125" x2="1.89991875" y2="1.02031875" layer="119"/>
<rectangle x1="1.9685" y1="1.01168125" x2="3.4925" y2="1.02031875" layer="119"/>
<rectangle x1="4.10971875" y1="1.01168125" x2="4.56691875" y2="1.02031875" layer="119"/>
<rectangle x1="5.54228125" y1="1.01168125" x2="6.01471875" y2="1.02031875" layer="119"/>
<rectangle x1="6.2611" y1="1.01168125" x2="7.39648125" y2="1.02031875" layer="119"/>
<rectangle x1="7.58951875" y1="1.01168125" x2="7.9629" y2="1.02031875" layer="119"/>
<rectangle x1="9.07288125" y1="1.01168125" x2="9.49451875" y2="1.02031875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.02031875" x2="1.89991875" y2="1.0287" layer="119"/>
<rectangle x1="1.9685" y1="1.02031875" x2="3.50011875" y2="1.0287" layer="119"/>
<rectangle x1="4.10971875" y1="1.02031875" x2="4.5847" y2="1.0287" layer="119"/>
<rectangle x1="5.53211875" y1="1.02031875" x2="6.01471875" y2="1.0287" layer="119"/>
<rectangle x1="6.2611" y1="1.02031875" x2="7.39648125" y2="1.0287" layer="119"/>
<rectangle x1="7.58951875" y1="1.02031875" x2="7.9629" y2="1.0287" layer="119"/>
<rectangle x1="9.06271875" y1="1.02031875" x2="9.49451875" y2="1.0287" layer="119"/>
<rectangle x1="-0.00508125" y1="1.0287" x2="1.8923" y2="1.03708125" layer="119"/>
<rectangle x1="1.9685" y1="1.0287" x2="3.5179" y2="1.03708125" layer="119"/>
<rectangle x1="4.10971875" y1="1.0287" x2="4.60248125" y2="1.03708125" layer="119"/>
<rectangle x1="5.51688125" y1="1.0287" x2="6.01471875" y2="1.03708125" layer="119"/>
<rectangle x1="6.26871875" y1="1.0287" x2="7.39648125" y2="1.03708125" layer="119"/>
<rectangle x1="7.58951875" y1="1.0287" x2="7.9629" y2="1.03708125" layer="119"/>
<rectangle x1="9.0551" y1="1.0287" x2="9.49451875" y2="1.03708125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.03708125" x2="1.8923" y2="1.04571875" layer="119"/>
<rectangle x1="1.9685" y1="1.03708125" x2="3.53568125" y2="1.04571875" layer="119"/>
<rectangle x1="4.10971875" y1="1.03708125" x2="4.6101" y2="1.04571875" layer="119"/>
<rectangle x1="5.4991" y1="1.03708125" x2="6.01471875" y2="1.04571875" layer="119"/>
<rectangle x1="6.27888125" y1="1.03708125" x2="7.38631875" y2="1.04571875" layer="119"/>
<rectangle x1="7.58951875" y1="1.03708125" x2="7.9629" y2="1.04571875" layer="119"/>
<rectangle x1="9.04748125" y1="1.03708125" x2="9.49451875" y2="1.04571875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.04571875" x2="1.88468125" y2="1.0541" layer="119"/>
<rectangle x1="1.9685" y1="1.04571875" x2="3.55091875" y2="1.0541" layer="119"/>
<rectangle x1="4.10971875" y1="1.04571875" x2="4.62788125" y2="1.0541" layer="119"/>
<rectangle x1="5.49148125" y1="1.04571875" x2="6.01471875" y2="1.0541" layer="119"/>
<rectangle x1="6.27888125" y1="1.04571875" x2="7.38631875" y2="1.0541" layer="119"/>
<rectangle x1="7.58951875" y1="1.04571875" x2="7.9629" y2="1.0541" layer="119"/>
<rectangle x1="9.03731875" y1="1.04571875" x2="9.49451875" y2="1.0541" layer="119"/>
<rectangle x1="-0.00508125" y1="1.0541" x2="1.88468125" y2="1.06248125" layer="119"/>
<rectangle x1="1.9685" y1="1.0541" x2="3.56108125" y2="1.06248125" layer="119"/>
<rectangle x1="4.10971875" y1="1.0541" x2="4.6355" y2="1.06248125" layer="119"/>
<rectangle x1="5.4737" y1="1.0541" x2="6.01471875" y2="1.06248125" layer="119"/>
<rectangle x1="6.2865" y1="1.0541" x2="7.3787" y2="1.06248125" layer="119"/>
<rectangle x1="7.58951875" y1="1.0541" x2="7.9629" y2="1.06248125" layer="119"/>
<rectangle x1="9.0297" y1="1.0541" x2="9.49451875" y2="1.06248125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.06248125" x2="1.87451875" y2="1.07111875" layer="119"/>
<rectangle x1="1.9685" y1="1.06248125" x2="3.57631875" y2="1.07111875" layer="119"/>
<rectangle x1="4.10971875" y1="1.06248125" x2="4.65328125" y2="1.07111875" layer="119"/>
<rectangle x1="5.45591875" y1="1.06248125" x2="6.01471875" y2="1.07111875" layer="119"/>
<rectangle x1="6.29411875" y1="1.06248125" x2="7.37108125" y2="1.07111875" layer="119"/>
<rectangle x1="7.58951875" y1="1.06248125" x2="7.9629" y2="1.07111875" layer="119"/>
<rectangle x1="9.02208125" y1="1.06248125" x2="9.49451875" y2="1.07111875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.07111875" x2="1.8669" y2="1.0795" layer="119"/>
<rectangle x1="1.9685" y1="1.07111875" x2="3.5941" y2="1.0795" layer="119"/>
<rectangle x1="4.10971875" y1="1.07111875" x2="4.66851875" y2="1.0795" layer="119"/>
<rectangle x1="5.4483" y1="1.07111875" x2="6.01471875" y2="1.0795" layer="119"/>
<rectangle x1="6.30428125" y1="1.07111875" x2="7.36091875" y2="1.0795" layer="119"/>
<rectangle x1="7.58951875" y1="1.07111875" x2="7.9629" y2="1.0795" layer="119"/>
<rectangle x1="9.01191875" y1="1.07111875" x2="9.49451875" y2="1.0795" layer="119"/>
<rectangle x1="-0.00508125" y1="1.0795" x2="1.85928125" y2="1.08788125" layer="119"/>
<rectangle x1="1.9685" y1="1.0795" x2="3.60171875" y2="1.08788125" layer="119"/>
<rectangle x1="4.10971875" y1="1.0795" x2="4.6863" y2="1.08788125" layer="119"/>
<rectangle x1="5.43051875" y1="1.0795" x2="6.01471875" y2="1.08788125" layer="119"/>
<rectangle x1="6.3119" y1="1.0795" x2="7.3533" y2="1.08788125" layer="119"/>
<rectangle x1="7.58951875" y1="1.0795" x2="7.9629" y2="1.08788125" layer="119"/>
<rectangle x1="9.0043" y1="1.0795" x2="9.49451875" y2="1.08788125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.08788125" x2="1.8415" y2="1.09651875" layer="119"/>
<rectangle x1="1.9685" y1="1.08788125" x2="3.6195" y2="1.09651875" layer="119"/>
<rectangle x1="4.10971875" y1="1.08788125" x2="4.69391875" y2="1.09651875" layer="119"/>
<rectangle x1="5.4229" y1="1.08788125" x2="6.01471875" y2="1.09651875" layer="119"/>
<rectangle x1="6.31951875" y1="1.08788125" x2="7.33551875" y2="1.09651875" layer="119"/>
<rectangle x1="7.58951875" y1="1.08788125" x2="7.9629" y2="1.09651875" layer="119"/>
<rectangle x1="8.99668125" y1="1.08788125" x2="9.49451875" y2="1.09651875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.09651875" x2="1.82371875" y2="1.1049" layer="119"/>
<rectangle x1="1.9685" y1="1.09651875" x2="3.63728125" y2="1.1049" layer="119"/>
<rectangle x1="4.10971875" y1="1.09651875" x2="4.7117" y2="1.1049" layer="119"/>
<rectangle x1="5.40511875" y1="1.09651875" x2="6.01471875" y2="1.1049" layer="119"/>
<rectangle x1="6.3373" y1="1.09651875" x2="7.32028125" y2="1.1049" layer="119"/>
<rectangle x1="7.58951875" y1="1.09651875" x2="7.9629" y2="1.1049" layer="119"/>
<rectangle x1="8.98651875" y1="1.09651875" x2="9.49451875" y2="1.1049" layer="119"/>
<rectangle x1="-0.00508125" y1="1.1049" x2="1.79831875" y2="1.11328125" layer="119"/>
<rectangle x1="1.9685" y1="1.1049" x2="3.6449" y2="1.11328125" layer="119"/>
<rectangle x1="4.10971875" y1="1.1049" x2="4.72948125" y2="1.11328125" layer="119"/>
<rectangle x1="5.3975" y1="1.1049" x2="6.01471875" y2="1.11328125" layer="119"/>
<rectangle x1="6.35508125" y1="1.1049" x2="7.3025" y2="1.11328125" layer="119"/>
<rectangle x1="7.58951875" y1="1.1049" x2="7.9629" y2="1.11328125" layer="119"/>
<rectangle x1="8.9789" y1="1.1049" x2="9.49451875" y2="1.11328125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.11328125" x2="1.7653" y2="1.12191875" layer="119"/>
<rectangle x1="1.9685" y1="1.11328125" x2="3.65251875" y2="1.12191875" layer="119"/>
<rectangle x1="4.10971875" y1="1.11328125" x2="4.7371" y2="1.12191875" layer="119"/>
<rectangle x1="5.37971875" y1="1.11328125" x2="6.01471875" y2="1.12191875" layer="119"/>
<rectangle x1="6.3881" y1="1.11328125" x2="7.26948125" y2="1.12191875" layer="119"/>
<rectangle x1="7.58951875" y1="1.11328125" x2="7.9629" y2="1.12191875" layer="119"/>
<rectangle x1="8.96111875" y1="1.11328125" x2="9.49451875" y2="1.12191875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.12191875" x2="0.3683" y2="1.1303" layer="119"/>
<rectangle x1="1.9685" y1="1.12191875" x2="2.34188125" y2="1.1303" layer="119"/>
<rectangle x1="3.05308125" y1="1.12191875" x2="3.6703" y2="1.1303" layer="119"/>
<rectangle x1="4.10971875" y1="1.12191875" x2="4.75488125" y2="1.1303" layer="119"/>
<rectangle x1="5.3721" y1="1.12191875" x2="6.01471875" y2="1.1303" layer="119"/>
<rectangle x1="6.6421" y1="1.12191875" x2="7.01548125" y2="1.1303" layer="119"/>
<rectangle x1="7.58951875" y1="1.12191875" x2="7.9629" y2="1.1303" layer="119"/>
<rectangle x1="8.96111875" y1="1.12191875" x2="9.49451875" y2="1.1303" layer="119"/>
<rectangle x1="-0.00508125" y1="1.1303" x2="0.3683" y2="1.13868125" layer="119"/>
<rectangle x1="1.9685" y1="1.1303" x2="2.34188125" y2="1.13868125" layer="119"/>
<rectangle x1="3.10388125" y1="1.1303" x2="3.67791875" y2="1.13868125" layer="119"/>
<rectangle x1="4.10971875" y1="1.1303" x2="4.77011875" y2="1.13868125" layer="119"/>
<rectangle x1="5.35431875" y1="1.1303" x2="6.01471875" y2="1.13868125" layer="119"/>
<rectangle x1="6.6421" y1="1.1303" x2="7.01548125" y2="1.13868125" layer="119"/>
<rectangle x1="7.58951875" y1="1.1303" x2="7.9629" y2="1.13868125" layer="119"/>
<rectangle x1="8.94588125" y1="1.1303" x2="9.49451875" y2="1.13868125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.13868125" x2="0.3683" y2="1.14731875" layer="119"/>
<rectangle x1="1.9685" y1="1.13868125" x2="2.34188125" y2="1.14731875" layer="119"/>
<rectangle x1="3.12928125" y1="1.13868125" x2="3.68808125" y2="1.14731875" layer="119"/>
<rectangle x1="4.10971875" y1="1.13868125" x2="4.78028125" y2="1.14731875" layer="119"/>
<rectangle x1="5.33908125" y1="1.13868125" x2="6.01471875" y2="1.14731875" layer="119"/>
<rectangle x1="6.6421" y1="1.13868125" x2="7.01548125" y2="1.14731875" layer="119"/>
<rectangle x1="7.58951875" y1="1.13868125" x2="7.9629" y2="1.14731875" layer="119"/>
<rectangle x1="8.93571875" y1="1.13868125" x2="9.49451875" y2="1.14731875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.14731875" x2="0.3683" y2="1.1557" layer="119"/>
<rectangle x1="1.9685" y1="1.14731875" x2="2.34188125" y2="1.1557" layer="119"/>
<rectangle x1="3.15468125" y1="1.14731875" x2="3.70331875" y2="1.1557" layer="119"/>
<rectangle x1="4.10971875" y1="1.14731875" x2="4.79551875" y2="1.1557" layer="119"/>
<rectangle x1="5.32891875" y1="1.14731875" x2="6.01471875" y2="1.1557" layer="119"/>
<rectangle x1="6.6421" y1="1.14731875" x2="7.01548125" y2="1.1557" layer="119"/>
<rectangle x1="7.58951875" y1="1.14731875" x2="7.9629" y2="1.1557" layer="119"/>
<rectangle x1="8.9281" y1="1.14731875" x2="9.49451875" y2="1.1557" layer="119"/>
<rectangle x1="-0.00508125" y1="1.1557" x2="0.3683" y2="1.16408125" layer="119"/>
<rectangle x1="1.9685" y1="1.1557" x2="2.34188125" y2="1.16408125" layer="119"/>
<rectangle x1="3.18008125" y1="1.1557" x2="3.71348125" y2="1.16408125" layer="119"/>
<rectangle x1="4.10971875" y1="1.1557" x2="4.80568125" y2="1.16408125" layer="119"/>
<rectangle x1="5.31368125" y1="1.1557" x2="6.01471875" y2="1.16408125" layer="119"/>
<rectangle x1="6.6421" y1="1.1557" x2="7.01548125" y2="1.16408125" layer="119"/>
<rectangle x1="7.58951875" y1="1.1557" x2="7.9629" y2="1.16408125" layer="119"/>
<rectangle x1="8.92048125" y1="1.1557" x2="9.49451875" y2="1.16408125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.16408125" x2="0.3683" y2="1.17271875" layer="119"/>
<rectangle x1="1.9685" y1="1.16408125" x2="2.34188125" y2="1.17271875" layer="119"/>
<rectangle x1="3.20548125" y1="1.16408125" x2="3.7211" y2="1.17271875" layer="119"/>
<rectangle x1="4.10971875" y1="1.16408125" x2="4.82091875" y2="1.17271875" layer="119"/>
<rectangle x1="5.2959" y1="1.16408125" x2="6.01471875" y2="1.17271875" layer="119"/>
<rectangle x1="6.6421" y1="1.16408125" x2="7.01548125" y2="1.17271875" layer="119"/>
<rectangle x1="7.58951875" y1="1.16408125" x2="7.9629" y2="1.17271875" layer="119"/>
<rectangle x1="8.91031875" y1="1.16408125" x2="9.49451875" y2="1.17271875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.17271875" x2="0.3683" y2="1.1811" layer="119"/>
<rectangle x1="1.9685" y1="1.17271875" x2="2.34188125" y2="1.1811" layer="119"/>
<rectangle x1="3.22071875" y1="1.17271875" x2="3.72871875" y2="1.1811" layer="119"/>
<rectangle x1="4.10971875" y1="1.17271875" x2="4.8387" y2="1.1811" layer="119"/>
<rectangle x1="5.28828125" y1="1.17271875" x2="6.01471875" y2="1.1811" layer="119"/>
<rectangle x1="6.6421" y1="1.17271875" x2="7.01548125" y2="1.1811" layer="119"/>
<rectangle x1="7.58951875" y1="1.17271875" x2="7.9629" y2="1.1811" layer="119"/>
<rectangle x1="8.9027" y1="1.17271875" x2="9.49451875" y2="1.1811" layer="119"/>
<rectangle x1="-0.00508125" y1="1.1811" x2="0.3683" y2="1.18948125" layer="119"/>
<rectangle x1="1.9685" y1="1.1811" x2="2.34188125" y2="1.18948125" layer="119"/>
<rectangle x1="3.2385" y1="1.1811" x2="3.73888125" y2="1.18948125" layer="119"/>
<rectangle x1="4.10971875" y1="1.1811" x2="4.84631875" y2="1.18948125" layer="119"/>
<rectangle x1="5.2705" y1="1.1811" x2="6.01471875" y2="1.18948125" layer="119"/>
<rectangle x1="6.6421" y1="1.1811" x2="7.01548125" y2="1.18948125" layer="119"/>
<rectangle x1="7.58951875" y1="1.1811" x2="7.9629" y2="1.18948125" layer="119"/>
<rectangle x1="8.89508125" y1="1.1811" x2="9.49451875" y2="1.18948125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.18948125" x2="0.3683" y2="1.19811875" layer="119"/>
<rectangle x1="1.9685" y1="1.18948125" x2="2.34188125" y2="1.19811875" layer="119"/>
<rectangle x1="3.2639" y1="1.18948125" x2="3.7465" y2="1.19811875" layer="119"/>
<rectangle x1="4.10971875" y1="1.18948125" x2="4.8641" y2="1.19811875" layer="119"/>
<rectangle x1="5.26288125" y1="1.18948125" x2="6.01471875" y2="1.19811875" layer="119"/>
<rectangle x1="6.6421" y1="1.18948125" x2="7.01548125" y2="1.19811875" layer="119"/>
<rectangle x1="7.58951875" y1="1.18948125" x2="7.9629" y2="1.19811875" layer="119"/>
<rectangle x1="8.88491875" y1="1.18948125" x2="9.49451875" y2="1.19811875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.19811875" x2="0.3683" y2="1.2065" layer="119"/>
<rectangle x1="1.9685" y1="1.19811875" x2="2.34188125" y2="1.2065" layer="119"/>
<rectangle x1="3.28168125" y1="1.19811875" x2="3.75411875" y2="1.2065" layer="119"/>
<rectangle x1="4.10971875" y1="1.19811875" x2="4.88188125" y2="1.2065" layer="119"/>
<rectangle x1="5.2451" y1="1.19811875" x2="6.01471875" y2="1.2065" layer="119"/>
<rectangle x1="6.6421" y1="1.19811875" x2="7.01548125" y2="1.2065" layer="119"/>
<rectangle x1="7.58951875" y1="1.19811875" x2="7.9629" y2="1.2065" layer="119"/>
<rectangle x1="8.8773" y1="1.19811875" x2="9.49451875" y2="1.2065" layer="119"/>
<rectangle x1="-0.00508125" y1="1.2065" x2="0.3683" y2="1.21488125" layer="119"/>
<rectangle x1="1.9685" y1="1.2065" x2="2.34188125" y2="1.21488125" layer="119"/>
<rectangle x1="3.29691875" y1="1.2065" x2="3.76428125" y2="1.21488125" layer="119"/>
<rectangle x1="4.10971875" y1="1.2065" x2="4.8895" y2="1.21488125" layer="119"/>
<rectangle x1="5.23748125" y1="1.2065" x2="6.01471875" y2="1.21488125" layer="119"/>
<rectangle x1="6.6421" y1="1.2065" x2="7.01548125" y2="1.21488125" layer="119"/>
<rectangle x1="7.58951875" y1="1.2065" x2="7.9629" y2="1.21488125" layer="119"/>
<rectangle x1="8.86968125" y1="1.2065" x2="9.49451875" y2="1.21488125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.21488125" x2="0.3683" y2="1.22351875" layer="119"/>
<rectangle x1="1.9685" y1="1.21488125" x2="2.34188125" y2="1.22351875" layer="119"/>
<rectangle x1="3.30708125" y1="1.21488125" x2="3.7719" y2="1.22351875" layer="119"/>
<rectangle x1="4.10971875" y1="1.21488125" x2="4.90728125" y2="1.22351875" layer="119"/>
<rectangle x1="5.2197" y1="1.21488125" x2="6.01471875" y2="1.22351875" layer="119"/>
<rectangle x1="6.6421" y1="1.21488125" x2="7.01548125" y2="1.22351875" layer="119"/>
<rectangle x1="7.58951875" y1="1.21488125" x2="7.9629" y2="1.22351875" layer="119"/>
<rectangle x1="8.85951875" y1="1.21488125" x2="9.49451875" y2="1.22351875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.22351875" x2="0.3683" y2="1.2319" layer="119"/>
<rectangle x1="1.9685" y1="1.22351875" x2="2.34188125" y2="1.2319" layer="119"/>
<rectangle x1="3.32231875" y1="1.22351875" x2="3.77951875" y2="1.2319" layer="119"/>
<rectangle x1="4.10971875" y1="1.22351875" x2="4.92251875" y2="1.2319" layer="119"/>
<rectangle x1="5.21208125" y1="1.22351875" x2="6.01471875" y2="1.2319" layer="119"/>
<rectangle x1="6.6421" y1="1.22351875" x2="7.01548125" y2="1.2319" layer="119"/>
<rectangle x1="7.58951875" y1="1.22351875" x2="7.9629" y2="1.2319" layer="119"/>
<rectangle x1="8.8519" y1="1.22351875" x2="9.49451875" y2="1.2319" layer="119"/>
<rectangle x1="-0.00508125" y1="1.2319" x2="0.3683" y2="1.24028125" layer="119"/>
<rectangle x1="1.9685" y1="1.2319" x2="2.34188125" y2="1.24028125" layer="119"/>
<rectangle x1="3.3401" y1="1.2319" x2="3.78968125" y2="1.24028125" layer="119"/>
<rectangle x1="4.10971875" y1="1.2319" x2="4.93268125" y2="1.24028125" layer="119"/>
<rectangle x1="5.1943" y1="1.2319" x2="6.01471875" y2="1.24028125" layer="119"/>
<rectangle x1="6.6421" y1="1.2319" x2="7.01548125" y2="1.24028125" layer="119"/>
<rectangle x1="7.58951875" y1="1.2319" x2="7.9629" y2="1.24028125" layer="119"/>
<rectangle x1="8.83411875" y1="1.2319" x2="9.49451875" y2="1.24028125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.24028125" x2="0.3683" y2="1.24891875" layer="119"/>
<rectangle x1="1.9685" y1="1.24028125" x2="2.34188125" y2="1.24891875" layer="119"/>
<rectangle x1="3.35788125" y1="1.24028125" x2="3.7973" y2="1.24891875" layer="119"/>
<rectangle x1="4.10971875" y1="1.24028125" x2="4.94791875" y2="1.24891875" layer="119"/>
<rectangle x1="5.17651875" y1="1.24028125" x2="6.01471875" y2="1.24891875" layer="119"/>
<rectangle x1="6.6421" y1="1.24028125" x2="7.01548125" y2="1.24891875" layer="119"/>
<rectangle x1="7.58951875" y1="1.24028125" x2="7.9629" y2="1.24891875" layer="119"/>
<rectangle x1="8.8265" y1="1.24028125" x2="9.49451875" y2="1.24891875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.24891875" x2="0.3683" y2="1.2573" layer="119"/>
<rectangle x1="1.9685" y1="1.24891875" x2="2.34188125" y2="1.2573" layer="119"/>
<rectangle x1="3.3655" y1="1.24891875" x2="3.7973" y2="1.2573" layer="119"/>
<rectangle x1="4.10971875" y1="1.24891875" x2="4.9657" y2="1.2573" layer="119"/>
<rectangle x1="5.1689" y1="1.24891875" x2="6.01471875" y2="1.2573" layer="119"/>
<rectangle x1="6.6421" y1="1.24891875" x2="7.01548125" y2="1.2573" layer="119"/>
<rectangle x1="7.58951875" y1="1.24891875" x2="7.9629" y2="1.2573" layer="119"/>
<rectangle x1="8.81888125" y1="1.24891875" x2="9.49451875" y2="1.2573" layer="119"/>
<rectangle x1="-0.00508125" y1="1.2573" x2="0.3683" y2="1.26568125" layer="119"/>
<rectangle x1="1.9685" y1="1.2573" x2="2.34188125" y2="1.26568125" layer="119"/>
<rectangle x1="3.37311875" y1="1.2573" x2="3.80491875" y2="1.26568125" layer="119"/>
<rectangle x1="4.10971875" y1="1.2573" x2="4.97331875" y2="1.26568125" layer="119"/>
<rectangle x1="5.15111875" y1="1.2573" x2="6.01471875" y2="1.26568125" layer="119"/>
<rectangle x1="6.6421" y1="1.2573" x2="7.01548125" y2="1.26568125" layer="119"/>
<rectangle x1="7.58951875" y1="1.2573" x2="7.9629" y2="1.26568125" layer="119"/>
<rectangle x1="8.80871875" y1="1.2573" x2="9.49451875" y2="1.26568125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.26568125" x2="0.3683" y2="1.27431875" layer="119"/>
<rectangle x1="1.9685" y1="1.26568125" x2="2.34188125" y2="1.27431875" layer="119"/>
<rectangle x1="3.3909" y1="1.26568125" x2="3.81508125" y2="1.27431875" layer="119"/>
<rectangle x1="4.10971875" y1="1.26568125" x2="4.9911" y2="1.27431875" layer="119"/>
<rectangle x1="5.1435" y1="1.26568125" x2="6.01471875" y2="1.27431875" layer="119"/>
<rectangle x1="6.6421" y1="1.26568125" x2="7.01548125" y2="1.27431875" layer="119"/>
<rectangle x1="7.58951875" y1="1.26568125" x2="7.9629" y2="1.27431875" layer="119"/>
<rectangle x1="8.8011" y1="1.26568125" x2="9.49451875" y2="1.27431875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.27431875" x2="0.3683" y2="1.2827" layer="119"/>
<rectangle x1="1.9685" y1="1.27431875" x2="2.34188125" y2="1.2827" layer="119"/>
<rectangle x1="3.39851875" y1="1.27431875" x2="3.81508125" y2="1.2827" layer="119"/>
<rectangle x1="4.10971875" y1="1.27431875" x2="5.00888125" y2="1.2827" layer="119"/>
<rectangle x1="5.12571875" y1="1.27431875" x2="6.01471875" y2="1.2827" layer="119"/>
<rectangle x1="6.6421" y1="1.27431875" x2="7.01548125" y2="1.2827" layer="119"/>
<rectangle x1="7.58951875" y1="1.27431875" x2="7.9629" y2="1.2827" layer="119"/>
<rectangle x1="8.79348125" y1="1.27431875" x2="9.49451875" y2="1.2827" layer="119"/>
<rectangle x1="-0.00508125" y1="1.2827" x2="0.3683" y2="1.29108125" layer="119"/>
<rectangle x1="1.9685" y1="1.2827" x2="2.34188125" y2="1.29108125" layer="119"/>
<rectangle x1="3.4163" y1="1.2827" x2="3.8227" y2="1.29108125" layer="119"/>
<rectangle x1="4.10971875" y1="1.2827" x2="4.4831" y2="1.29108125" layer="119"/>
<rectangle x1="4.50088125" y1="1.2827" x2="5.0165" y2="1.29108125" layer="119"/>
<rectangle x1="5.11048125" y1="1.2827" x2="5.61848125" y2="1.29108125" layer="119"/>
<rectangle x1="5.63371875" y1="1.2827" x2="6.01471875" y2="1.29108125" layer="119"/>
<rectangle x1="6.6421" y1="1.2827" x2="7.01548125" y2="1.29108125" layer="119"/>
<rectangle x1="7.58951875" y1="1.2827" x2="7.9629" y2="1.29108125" layer="119"/>
<rectangle x1="8.78331875" y1="1.2827" x2="9.49451875" y2="1.29108125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.29108125" x2="0.3683" y2="1.29971875" layer="119"/>
<rectangle x1="1.9685" y1="1.29108125" x2="2.34188125" y2="1.29971875" layer="119"/>
<rectangle x1="3.42391875" y1="1.29108125" x2="3.8227" y2="1.29971875" layer="119"/>
<rectangle x1="4.10971875" y1="1.29108125" x2="4.4831" y2="1.29971875" layer="119"/>
<rectangle x1="4.5085" y1="1.29108125" x2="5.03428125" y2="1.29971875" layer="119"/>
<rectangle x1="5.10031875" y1="1.29108125" x2="5.60831875" y2="1.29971875" layer="119"/>
<rectangle x1="5.63371875" y1="1.29108125" x2="6.01471875" y2="1.29971875" layer="119"/>
<rectangle x1="6.6421" y1="1.29108125" x2="7.01548125" y2="1.29971875" layer="119"/>
<rectangle x1="7.58951875" y1="1.29108125" x2="7.9629" y2="1.29971875" layer="119"/>
<rectangle x1="8.7757" y1="1.29108125" x2="9.49451875" y2="1.29971875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.29971875" x2="0.3683" y2="1.3081" layer="119"/>
<rectangle x1="1.9685" y1="1.29971875" x2="2.34188125" y2="1.3081" layer="119"/>
<rectangle x1="3.43408125" y1="1.29971875" x2="3.83031875" y2="1.3081" layer="119"/>
<rectangle x1="4.10971875" y1="1.29971875" x2="4.4831" y2="1.3081" layer="119"/>
<rectangle x1="4.52628125" y1="1.29971875" x2="5.04951875" y2="1.3081" layer="119"/>
<rectangle x1="5.08508125" y1="1.29971875" x2="5.59308125" y2="1.3081" layer="119"/>
<rectangle x1="5.63371875" y1="1.29971875" x2="6.01471875" y2="1.3081" layer="119"/>
<rectangle x1="6.6421" y1="1.29971875" x2="7.01548125" y2="1.3081" layer="119"/>
<rectangle x1="7.58951875" y1="1.29971875" x2="7.9629" y2="1.3081" layer="119"/>
<rectangle x1="8.76808125" y1="1.29971875" x2="9.49451875" y2="1.3081" layer="119"/>
<rectangle x1="-0.00508125" y1="1.3081" x2="0.3683" y2="1.31648125" layer="119"/>
<rectangle x1="1.9685" y1="1.3081" x2="2.34188125" y2="1.31648125" layer="119"/>
<rectangle x1="3.4417" y1="1.3081" x2="3.83031875" y2="1.31648125" layer="119"/>
<rectangle x1="4.10971875" y1="1.3081" x2="4.4831" y2="1.31648125" layer="119"/>
<rectangle x1="4.5339" y1="1.3081" x2="5.05968125" y2="1.31648125" layer="119"/>
<rectangle x1="5.07491875" y1="1.3081" x2="5.58291875" y2="1.31648125" layer="119"/>
<rectangle x1="5.63371875" y1="1.3081" x2="6.01471875" y2="1.31648125" layer="119"/>
<rectangle x1="6.6421" y1="1.3081" x2="7.01548125" y2="1.31648125" layer="119"/>
<rectangle x1="7.58951875" y1="1.3081" x2="7.9629" y2="1.31648125" layer="119"/>
<rectangle x1="8.75791875" y1="1.3081" x2="9.49451875" y2="1.31648125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.31648125" x2="0.3683" y2="1.32511875" layer="119"/>
<rectangle x1="1.9685" y1="1.31648125" x2="2.34188125" y2="1.32511875" layer="119"/>
<rectangle x1="3.44931875" y1="1.31648125" x2="3.84048125" y2="1.32511875" layer="119"/>
<rectangle x1="4.10971875" y1="1.31648125" x2="4.4831" y2="1.32511875" layer="119"/>
<rectangle x1="4.55168125" y1="1.31648125" x2="5.56768125" y2="1.32511875" layer="119"/>
<rectangle x1="5.63371875" y1="1.31648125" x2="6.01471875" y2="1.32511875" layer="119"/>
<rectangle x1="6.6421" y1="1.31648125" x2="7.01548125" y2="1.32511875" layer="119"/>
<rectangle x1="7.58951875" y1="1.31648125" x2="7.9629" y2="1.32511875" layer="119"/>
<rectangle x1="8.7503" y1="1.31648125" x2="9.49451875" y2="1.32511875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.32511875" x2="0.3683" y2="1.3335" layer="119"/>
<rectangle x1="1.9685" y1="1.32511875" x2="2.34188125" y2="1.3335" layer="119"/>
<rectangle x1="3.45948125" y1="1.32511875" x2="3.84048125" y2="1.3335" layer="119"/>
<rectangle x1="4.10971875" y1="1.32511875" x2="4.4831" y2="1.3335" layer="119"/>
<rectangle x1="4.5593" y1="1.32511875" x2="5.5499" y2="1.3335" layer="119"/>
<rectangle x1="5.63371875" y1="1.32511875" x2="6.01471875" y2="1.3335" layer="119"/>
<rectangle x1="6.6421" y1="1.32511875" x2="7.01548125" y2="1.3335" layer="119"/>
<rectangle x1="7.58951875" y1="1.32511875" x2="7.9629" y2="1.3335" layer="119"/>
<rectangle x1="8.74268125" y1="1.32511875" x2="9.49451875" y2="1.3335" layer="119"/>
<rectangle x1="-0.00508125" y1="1.3335" x2="0.3683" y2="1.34188125" layer="119"/>
<rectangle x1="1.9685" y1="1.3335" x2="2.34188125" y2="1.34188125" layer="119"/>
<rectangle x1="3.4671" y1="1.3335" x2="3.8481" y2="1.34188125" layer="119"/>
<rectangle x1="4.10971875" y1="1.3335" x2="4.4831" y2="1.34188125" layer="119"/>
<rectangle x1="4.57708125" y1="1.3335" x2="5.54228125" y2="1.34188125" layer="119"/>
<rectangle x1="5.63371875" y1="1.3335" x2="6.01471875" y2="1.34188125" layer="119"/>
<rectangle x1="6.6421" y1="1.3335" x2="7.01548125" y2="1.34188125" layer="119"/>
<rectangle x1="7.58951875" y1="1.3335" x2="7.9629" y2="1.34188125" layer="119"/>
<rectangle x1="8.73251875" y1="1.3335" x2="9.49451875" y2="1.34188125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.34188125" x2="0.3683" y2="1.35051875" layer="119"/>
<rectangle x1="1.9685" y1="1.34188125" x2="2.34188125" y2="1.35051875" layer="119"/>
<rectangle x1="3.47471875" y1="1.34188125" x2="3.8481" y2="1.35051875" layer="119"/>
<rectangle x1="4.10971875" y1="1.34188125" x2="4.4831" y2="1.35051875" layer="119"/>
<rectangle x1="4.5847" y1="1.34188125" x2="5.5245" y2="1.35051875" layer="119"/>
<rectangle x1="5.63371875" y1="1.34188125" x2="6.01471875" y2="1.35051875" layer="119"/>
<rectangle x1="6.6421" y1="1.34188125" x2="7.01548125" y2="1.35051875" layer="119"/>
<rectangle x1="7.58951875" y1="1.34188125" x2="7.9629" y2="1.35051875" layer="119"/>
<rectangle x1="8.7249" y1="1.34188125" x2="9.49451875" y2="1.35051875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.35051875" x2="0.3683" y2="1.3589" layer="119"/>
<rectangle x1="1.9685" y1="1.35051875" x2="2.34188125" y2="1.3589" layer="119"/>
<rectangle x1="3.47471875" y1="1.35051875" x2="3.85571875" y2="1.3589" layer="119"/>
<rectangle x1="4.10971875" y1="1.35051875" x2="4.4831" y2="1.3589" layer="119"/>
<rectangle x1="4.60248125" y1="1.35051875" x2="5.51688125" y2="1.3589" layer="119"/>
<rectangle x1="5.63371875" y1="1.35051875" x2="6.01471875" y2="1.3589" layer="119"/>
<rectangle x1="6.6421" y1="1.35051875" x2="7.01548125" y2="1.3589" layer="119"/>
<rectangle x1="7.58951875" y1="1.35051875" x2="7.9629" y2="1.3589" layer="119"/>
<rectangle x1="8.70711875" y1="1.35051875" x2="9.49451875" y2="1.3589" layer="119"/>
<rectangle x1="-0.00508125" y1="1.3589" x2="0.3683" y2="1.36728125" layer="119"/>
<rectangle x1="1.9685" y1="1.3589" x2="2.34188125" y2="1.36728125" layer="119"/>
<rectangle x1="3.48488125" y1="1.3589" x2="3.86588125" y2="1.36728125" layer="119"/>
<rectangle x1="4.10971875" y1="1.3589" x2="4.4831" y2="1.36728125" layer="119"/>
<rectangle x1="4.6101" y1="1.3589" x2="5.4991" y2="1.36728125" layer="119"/>
<rectangle x1="5.63371875" y1="1.3589" x2="6.01471875" y2="1.36728125" layer="119"/>
<rectangle x1="6.6421" y1="1.3589" x2="7.01548125" y2="1.36728125" layer="119"/>
<rectangle x1="7.58951875" y1="1.3589" x2="7.9629" y2="1.36728125" layer="119"/>
<rectangle x1="8.6995" y1="1.3589" x2="9.49451875" y2="1.36728125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.36728125" x2="0.3683" y2="1.37591875" layer="119"/>
<rectangle x1="1.9685" y1="1.36728125" x2="2.34188125" y2="1.37591875" layer="119"/>
<rectangle x1="3.4925" y1="1.36728125" x2="3.86588125" y2="1.37591875" layer="119"/>
<rectangle x1="4.10971875" y1="1.36728125" x2="4.4831" y2="1.37591875" layer="119"/>
<rectangle x1="4.62788125" y1="1.36728125" x2="5.48131875" y2="1.37591875" layer="119"/>
<rectangle x1="5.63371875" y1="1.36728125" x2="6.01471875" y2="1.37591875" layer="119"/>
<rectangle x1="6.6421" y1="1.36728125" x2="7.01548125" y2="1.37591875" layer="119"/>
<rectangle x1="7.58951875" y1="1.36728125" x2="7.9629" y2="1.37591875" layer="119"/>
<rectangle x1="8.69188125" y1="1.36728125" x2="9.49451875" y2="1.37591875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.37591875" x2="0.3683" y2="1.3843" layer="119"/>
<rectangle x1="1.9685" y1="1.37591875" x2="2.34188125" y2="1.3843" layer="119"/>
<rectangle x1="3.4925" y1="1.37591875" x2="3.8735" y2="1.3843" layer="119"/>
<rectangle x1="4.10971875" y1="1.37591875" x2="4.4831" y2="1.3843" layer="119"/>
<rectangle x1="4.6355" y1="1.37591875" x2="5.4737" y2="1.3843" layer="119"/>
<rectangle x1="5.63371875" y1="1.37591875" x2="6.01471875" y2="1.3843" layer="119"/>
<rectangle x1="6.6421" y1="1.37591875" x2="7.01548125" y2="1.3843" layer="119"/>
<rectangle x1="7.58951875" y1="1.37591875" x2="7.9629" y2="1.3843" layer="119"/>
<rectangle x1="8.68171875" y1="1.37591875" x2="9.49451875" y2="1.3843" layer="119"/>
<rectangle x1="-0.00508125" y1="1.3843" x2="0.3683" y2="1.39268125" layer="119"/>
<rectangle x1="1.9685" y1="1.3843" x2="2.34188125" y2="1.39268125" layer="119"/>
<rectangle x1="3.4925" y1="1.3843" x2="3.8735" y2="1.39268125" layer="119"/>
<rectangle x1="4.10971875" y1="1.3843" x2="4.4831" y2="1.39268125" layer="119"/>
<rectangle x1="4.65328125" y1="1.3843" x2="5.45591875" y2="1.39268125" layer="119"/>
<rectangle x1="5.63371875" y1="1.3843" x2="6.01471875" y2="1.39268125" layer="119"/>
<rectangle x1="6.6421" y1="1.3843" x2="7.01548125" y2="1.39268125" layer="119"/>
<rectangle x1="7.58951875" y1="1.3843" x2="7.9629" y2="1.39268125" layer="119"/>
<rectangle x1="8.6741" y1="1.3843" x2="9.49451875" y2="1.39268125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.39268125" x2="0.3683" y2="1.40131875" layer="119"/>
<rectangle x1="1.9685" y1="1.39268125" x2="2.34188125" y2="1.40131875" layer="119"/>
<rectangle x1="3.50011875" y1="1.39268125" x2="3.8735" y2="1.40131875" layer="119"/>
<rectangle x1="4.10971875" y1="1.39268125" x2="4.4831" y2="1.40131875" layer="119"/>
<rectangle x1="4.6609" y1="1.39268125" x2="5.4483" y2="1.40131875" layer="119"/>
<rectangle x1="5.63371875" y1="1.39268125" x2="6.01471875" y2="1.40131875" layer="119"/>
<rectangle x1="6.6421" y1="1.39268125" x2="7.01548125" y2="1.40131875" layer="119"/>
<rectangle x1="7.58951875" y1="1.39268125" x2="7.9629" y2="1.40131875" layer="119"/>
<rectangle x1="8.66648125" y1="1.39268125" x2="9.1059" y2="1.40131875" layer="119"/>
<rectangle x1="9.11351875" y1="1.39268125" x2="9.49451875" y2="1.40131875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.40131875" x2="0.3683" y2="1.4097" layer="119"/>
<rectangle x1="1.9685" y1="1.40131875" x2="2.34188125" y2="1.4097" layer="119"/>
<rectangle x1="3.50011875" y1="1.40131875" x2="3.8735" y2="1.4097" layer="119"/>
<rectangle x1="4.10971875" y1="1.40131875" x2="4.4831" y2="1.4097" layer="119"/>
<rectangle x1="4.67868125" y1="1.40131875" x2="5.43051875" y2="1.4097" layer="119"/>
<rectangle x1="5.63371875" y1="1.40131875" x2="6.01471875" y2="1.4097" layer="119"/>
<rectangle x1="6.6421" y1="1.40131875" x2="7.01548125" y2="1.4097" layer="119"/>
<rectangle x1="7.58951875" y1="1.40131875" x2="7.9629" y2="1.4097" layer="119"/>
<rectangle x1="8.65631875" y1="1.40131875" x2="9.09828125" y2="1.4097" layer="119"/>
<rectangle x1="9.11351875" y1="1.40131875" x2="9.49451875" y2="1.4097" layer="119"/>
<rectangle x1="-0.00508125" y1="1.4097" x2="0.3683" y2="1.41808125" layer="119"/>
<rectangle x1="1.9685" y1="1.4097" x2="2.34188125" y2="1.41808125" layer="119"/>
<rectangle x1="3.50011875" y1="1.4097" x2="3.8735" y2="1.41808125" layer="119"/>
<rectangle x1="4.10971875" y1="1.4097" x2="4.4831" y2="1.41808125" layer="119"/>
<rectangle x1="4.6863" y1="1.4097" x2="5.4229" y2="1.41808125" layer="119"/>
<rectangle x1="5.63371875" y1="1.4097" x2="6.01471875" y2="1.41808125" layer="119"/>
<rectangle x1="6.6421" y1="1.4097" x2="7.01548125" y2="1.41808125" layer="119"/>
<rectangle x1="7.58951875" y1="1.4097" x2="7.9629" y2="1.41808125" layer="119"/>
<rectangle x1="8.6487" y1="1.4097" x2="9.08811875" y2="1.41808125" layer="119"/>
<rectangle x1="9.11351875" y1="1.4097" x2="9.49451875" y2="1.41808125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.41808125" x2="0.3683" y2="1.42671875" layer="119"/>
<rectangle x1="1.9685" y1="1.41808125" x2="2.34188125" y2="1.42671875" layer="119"/>
<rectangle x1="3.50011875" y1="1.41808125" x2="3.8735" y2="1.42671875" layer="119"/>
<rectangle x1="4.10971875" y1="1.41808125" x2="4.4831" y2="1.42671875" layer="119"/>
<rectangle x1="4.70408125" y1="1.41808125" x2="5.40511875" y2="1.42671875" layer="119"/>
<rectangle x1="5.63371875" y1="1.41808125" x2="6.01471875" y2="1.42671875" layer="119"/>
<rectangle x1="6.6421" y1="1.41808125" x2="7.01548125" y2="1.42671875" layer="119"/>
<rectangle x1="7.58951875" y1="1.41808125" x2="7.9629" y2="1.42671875" layer="119"/>
<rectangle x1="8.64108125" y1="1.41808125" x2="9.0805" y2="1.42671875" layer="119"/>
<rectangle x1="9.11351875" y1="1.41808125" x2="9.49451875" y2="1.42671875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.42671875" x2="0.3683" y2="1.4351" layer="119"/>
<rectangle x1="1.9685" y1="1.42671875" x2="2.34188125" y2="1.4351" layer="119"/>
<rectangle x1="3.50011875" y1="1.42671875" x2="3.8735" y2="1.4351" layer="119"/>
<rectangle x1="4.10971875" y1="1.42671875" x2="4.4831" y2="1.4351" layer="119"/>
<rectangle x1="4.71931875" y1="1.42671875" x2="5.38988125" y2="1.4351" layer="119"/>
<rectangle x1="5.63371875" y1="1.42671875" x2="6.01471875" y2="1.4351" layer="119"/>
<rectangle x1="6.6421" y1="1.42671875" x2="7.01548125" y2="1.4351" layer="119"/>
<rectangle x1="7.58951875" y1="1.42671875" x2="7.9629" y2="1.4351" layer="119"/>
<rectangle x1="8.63091875" y1="1.42671875" x2="9.07288125" y2="1.4351" layer="119"/>
<rectangle x1="9.11351875" y1="1.42671875" x2="9.49451875" y2="1.4351" layer="119"/>
<rectangle x1="-0.00508125" y1="1.4351" x2="0.3683" y2="1.44348125" layer="119"/>
<rectangle x1="1.9685" y1="1.4351" x2="2.34188125" y2="1.44348125" layer="119"/>
<rectangle x1="3.50011875" y1="1.4351" x2="3.8735" y2="1.44348125" layer="119"/>
<rectangle x1="4.10971875" y1="1.4351" x2="4.4831" y2="1.44348125" layer="119"/>
<rectangle x1="4.72948125" y1="1.4351" x2="5.37971875" y2="1.44348125" layer="119"/>
<rectangle x1="5.63371875" y1="1.4351" x2="6.01471875" y2="1.44348125" layer="119"/>
<rectangle x1="6.6421" y1="1.4351" x2="7.01548125" y2="1.44348125" layer="119"/>
<rectangle x1="7.58951875" y1="1.4351" x2="7.9629" y2="1.44348125" layer="119"/>
<rectangle x1="8.6233" y1="1.4351" x2="9.06271875" y2="1.44348125" layer="119"/>
<rectangle x1="9.11351875" y1="1.4351" x2="9.49451875" y2="1.44348125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.44348125" x2="0.3683" y2="1.45211875" layer="119"/>
<rectangle x1="1.9685" y1="1.44348125" x2="2.34188125" y2="1.45211875" layer="119"/>
<rectangle x1="3.50011875" y1="1.44348125" x2="3.8735" y2="1.45211875" layer="119"/>
<rectangle x1="4.10971875" y1="1.44348125" x2="4.4831" y2="1.45211875" layer="119"/>
<rectangle x1="4.7371" y1="1.44348125" x2="5.36448125" y2="1.45211875" layer="119"/>
<rectangle x1="5.63371875" y1="1.44348125" x2="6.01471875" y2="1.45211875" layer="119"/>
<rectangle x1="6.6421" y1="1.44348125" x2="7.01548125" y2="1.45211875" layer="119"/>
<rectangle x1="7.58951875" y1="1.44348125" x2="7.9629" y2="1.45211875" layer="119"/>
<rectangle x1="8.61568125" y1="1.44348125" x2="9.0551" y2="1.45211875" layer="119"/>
<rectangle x1="9.11351875" y1="1.44348125" x2="9.49451875" y2="1.45211875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.45211875" x2="0.3683" y2="1.4605" layer="119"/>
<rectangle x1="1.9685" y1="1.45211875" x2="2.34188125" y2="1.4605" layer="119"/>
<rectangle x1="3.50011875" y1="1.45211875" x2="3.8735" y2="1.4605" layer="119"/>
<rectangle x1="4.10971875" y1="1.45211875" x2="4.4831" y2="1.4605" layer="119"/>
<rectangle x1="4.75488125" y1="1.45211875" x2="5.35431875" y2="1.4605" layer="119"/>
<rectangle x1="5.63371875" y1="1.45211875" x2="6.01471875" y2="1.4605" layer="119"/>
<rectangle x1="6.6421" y1="1.45211875" x2="7.01548125" y2="1.4605" layer="119"/>
<rectangle x1="7.58951875" y1="1.45211875" x2="7.9629" y2="1.4605" layer="119"/>
<rectangle x1="8.60551875" y1="1.45211875" x2="9.03731875" y2="1.4605" layer="119"/>
<rectangle x1="9.11351875" y1="1.45211875" x2="9.49451875" y2="1.4605" layer="119"/>
<rectangle x1="-0.00508125" y1="1.4605" x2="0.3683" y2="1.46888125" layer="119"/>
<rectangle x1="1.9685" y1="1.4605" x2="2.34188125" y2="1.46888125" layer="119"/>
<rectangle x1="3.50011875" y1="1.4605" x2="3.8735" y2="1.46888125" layer="119"/>
<rectangle x1="4.10971875" y1="1.4605" x2="4.4831" y2="1.46888125" layer="119"/>
<rectangle x1="4.7625" y1="1.4605" x2="5.33908125" y2="1.46888125" layer="119"/>
<rectangle x1="5.63371875" y1="1.4605" x2="6.01471875" y2="1.46888125" layer="119"/>
<rectangle x1="6.6421" y1="1.4605" x2="7.01548125" y2="1.46888125" layer="119"/>
<rectangle x1="7.58951875" y1="1.4605" x2="7.9629" y2="1.46888125" layer="119"/>
<rectangle x1="8.59028125" y1="1.4605" x2="9.0297" y2="1.46888125" layer="119"/>
<rectangle x1="9.11351875" y1="1.4605" x2="9.49451875" y2="1.46888125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.46888125" x2="0.3683" y2="1.47751875" layer="119"/>
<rectangle x1="1.9685" y1="1.46888125" x2="2.34188125" y2="1.47751875" layer="119"/>
<rectangle x1="3.50011875" y1="1.46888125" x2="3.8735" y2="1.47751875" layer="119"/>
<rectangle x1="4.10971875" y1="1.46888125" x2="4.4831" y2="1.47751875" layer="119"/>
<rectangle x1="4.78028125" y1="1.46888125" x2="5.3213" y2="1.47751875" layer="119"/>
<rectangle x1="5.63371875" y1="1.46888125" x2="6.01471875" y2="1.47751875" layer="119"/>
<rectangle x1="6.6421" y1="1.46888125" x2="7.01548125" y2="1.47751875" layer="119"/>
<rectangle x1="7.58951875" y1="1.46888125" x2="7.9629" y2="1.47751875" layer="119"/>
<rectangle x1="8.58011875" y1="1.46888125" x2="9.02208125" y2="1.47751875" layer="119"/>
<rectangle x1="9.11351875" y1="1.46888125" x2="9.49451875" y2="1.47751875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.47751875" x2="0.3683" y2="1.4859" layer="119"/>
<rectangle x1="1.9685" y1="1.47751875" x2="2.34188125" y2="1.4859" layer="119"/>
<rectangle x1="3.50011875" y1="1.47751875" x2="3.8735" y2="1.4859" layer="119"/>
<rectangle x1="4.10971875" y1="1.47751875" x2="4.4831" y2="1.4859" layer="119"/>
<rectangle x1="4.79551875" y1="1.47751875" x2="5.31368125" y2="1.4859" layer="119"/>
<rectangle x1="5.63371875" y1="1.47751875" x2="6.01471875" y2="1.4859" layer="119"/>
<rectangle x1="6.6421" y1="1.47751875" x2="7.01548125" y2="1.4859" layer="119"/>
<rectangle x1="7.58951875" y1="1.47751875" x2="7.9629" y2="1.4859" layer="119"/>
<rectangle x1="8.5725" y1="1.47751875" x2="9.01191875" y2="1.4859" layer="119"/>
<rectangle x1="9.11351875" y1="1.47751875" x2="9.49451875" y2="1.4859" layer="119"/>
<rectangle x1="-0.00508125" y1="1.4859" x2="0.3683" y2="1.49428125" layer="119"/>
<rectangle x1="1.9685" y1="1.4859" x2="2.34188125" y2="1.49428125" layer="119"/>
<rectangle x1="3.50011875" y1="1.4859" x2="3.8735" y2="1.49428125" layer="119"/>
<rectangle x1="4.10971875" y1="1.4859" x2="4.4831" y2="1.49428125" layer="119"/>
<rectangle x1="4.80568125" y1="1.4859" x2="5.2959" y2="1.49428125" layer="119"/>
<rectangle x1="5.63371875" y1="1.4859" x2="6.01471875" y2="1.49428125" layer="119"/>
<rectangle x1="6.6421" y1="1.4859" x2="7.01548125" y2="1.49428125" layer="119"/>
<rectangle x1="7.58951875" y1="1.4859" x2="7.9629" y2="1.49428125" layer="119"/>
<rectangle x1="8.56488125" y1="1.4859" x2="9.0043" y2="1.49428125" layer="119"/>
<rectangle x1="9.11351875" y1="1.4859" x2="9.49451875" y2="1.49428125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.49428125" x2="0.3683" y2="1.50291875" layer="119"/>
<rectangle x1="1.9685" y1="1.49428125" x2="2.34188125" y2="1.50291875" layer="119"/>
<rectangle x1="3.50011875" y1="1.49428125" x2="3.8735" y2="1.50291875" layer="119"/>
<rectangle x1="4.10971875" y1="1.49428125" x2="4.4831" y2="1.50291875" layer="119"/>
<rectangle x1="4.82091875" y1="1.49428125" x2="5.27811875" y2="1.50291875" layer="119"/>
<rectangle x1="5.63371875" y1="1.49428125" x2="6.01471875" y2="1.50291875" layer="119"/>
<rectangle x1="6.6421" y1="1.49428125" x2="7.01548125" y2="1.50291875" layer="119"/>
<rectangle x1="7.58951875" y1="1.49428125" x2="7.9629" y2="1.50291875" layer="119"/>
<rectangle x1="8.55471875" y1="1.49428125" x2="8.99668125" y2="1.50291875" layer="119"/>
<rectangle x1="9.11351875" y1="1.49428125" x2="9.49451875" y2="1.50291875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.50291875" x2="0.3683" y2="1.5113" layer="119"/>
<rectangle x1="1.9685" y1="1.50291875" x2="2.34188125" y2="1.5113" layer="119"/>
<rectangle x1="3.50011875" y1="1.50291875" x2="3.8735" y2="1.5113" layer="119"/>
<rectangle x1="4.10971875" y1="1.50291875" x2="4.4831" y2="1.5113" layer="119"/>
<rectangle x1="4.83108125" y1="1.50291875" x2="5.2705" y2="1.5113" layer="119"/>
<rectangle x1="5.63371875" y1="1.50291875" x2="6.01471875" y2="1.5113" layer="119"/>
<rectangle x1="6.6421" y1="1.50291875" x2="7.01548125" y2="1.5113" layer="119"/>
<rectangle x1="7.58951875" y1="1.50291875" x2="7.9629" y2="1.5113" layer="119"/>
<rectangle x1="8.5471" y1="1.50291875" x2="8.98651875" y2="1.5113" layer="119"/>
<rectangle x1="9.11351875" y1="1.50291875" x2="9.49451875" y2="1.5113" layer="119"/>
<rectangle x1="-0.00508125" y1="1.5113" x2="0.3683" y2="1.51968125" layer="119"/>
<rectangle x1="1.9685" y1="1.5113" x2="2.34188125" y2="1.51968125" layer="119"/>
<rectangle x1="3.50011875" y1="1.5113" x2="3.8735" y2="1.51968125" layer="119"/>
<rectangle x1="4.10971875" y1="1.5113" x2="4.4831" y2="1.51968125" layer="119"/>
<rectangle x1="4.84631875" y1="1.5113" x2="5.25271875" y2="1.51968125" layer="119"/>
<rectangle x1="5.63371875" y1="1.5113" x2="6.01471875" y2="1.51968125" layer="119"/>
<rectangle x1="6.6421" y1="1.5113" x2="7.01548125" y2="1.51968125" layer="119"/>
<rectangle x1="7.58951875" y1="1.5113" x2="7.9629" y2="1.51968125" layer="119"/>
<rectangle x1="8.53948125" y1="1.5113" x2="8.9789" y2="1.51968125" layer="119"/>
<rectangle x1="9.12368125" y1="1.5113" x2="9.49451875" y2="1.51968125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.51968125" x2="0.3683" y2="1.52831875" layer="119"/>
<rectangle x1="1.9685" y1="1.51968125" x2="2.34188125" y2="1.52831875" layer="119"/>
<rectangle x1="3.50011875" y1="1.51968125" x2="3.8735" y2="1.52831875" layer="119"/>
<rectangle x1="4.10971875" y1="1.51968125" x2="4.4831" y2="1.52831875" layer="119"/>
<rectangle x1="4.8641" y1="1.51968125" x2="5.23748125" y2="1.52831875" layer="119"/>
<rectangle x1="5.63371875" y1="1.51968125" x2="6.01471875" y2="1.52831875" layer="119"/>
<rectangle x1="6.6421" y1="1.51968125" x2="7.01548125" y2="1.52831875" layer="119"/>
<rectangle x1="7.58951875" y1="1.51968125" x2="7.9629" y2="1.52831875" layer="119"/>
<rectangle x1="8.52931875" y1="1.51968125" x2="8.97128125" y2="1.52831875" layer="119"/>
<rectangle x1="9.12368125" y1="1.51968125" x2="9.49451875" y2="1.52831875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.52831875" x2="0.3683" y2="1.5367" layer="119"/>
<rectangle x1="1.9685" y1="1.52831875" x2="2.34188125" y2="1.5367" layer="119"/>
<rectangle x1="3.50011875" y1="1.52831875" x2="3.8735" y2="1.5367" layer="119"/>
<rectangle x1="4.10971875" y1="1.52831875" x2="4.4831" y2="1.5367" layer="119"/>
<rectangle x1="4.88188125" y1="1.52831875" x2="5.2197" y2="1.5367" layer="119"/>
<rectangle x1="5.63371875" y1="1.52831875" x2="6.01471875" y2="1.5367" layer="119"/>
<rectangle x1="6.6421" y1="1.52831875" x2="7.01548125" y2="1.5367" layer="119"/>
<rectangle x1="7.58951875" y1="1.52831875" x2="7.9629" y2="1.5367" layer="119"/>
<rectangle x1="8.5217" y1="1.52831875" x2="8.96111875" y2="1.5367" layer="119"/>
<rectangle x1="9.12368125" y1="1.52831875" x2="9.49451875" y2="1.5367" layer="119"/>
<rectangle x1="-0.00508125" y1="1.5367" x2="0.3683" y2="1.54508125" layer="119"/>
<rectangle x1="1.9685" y1="1.5367" x2="2.34188125" y2="1.54508125" layer="119"/>
<rectangle x1="3.50011875" y1="1.5367" x2="3.8735" y2="1.54508125" layer="119"/>
<rectangle x1="4.10971875" y1="1.5367" x2="4.4831" y2="1.54508125" layer="119"/>
<rectangle x1="4.8895" y1="1.5367" x2="5.20191875" y2="1.54508125" layer="119"/>
<rectangle x1="5.63371875" y1="1.5367" x2="6.01471875" y2="1.54508125" layer="119"/>
<rectangle x1="6.6421" y1="1.5367" x2="7.01548125" y2="1.54508125" layer="119"/>
<rectangle x1="7.58951875" y1="1.5367" x2="7.9629" y2="1.54508125" layer="119"/>
<rectangle x1="8.51408125" y1="1.5367" x2="8.9535" y2="1.54508125" layer="119"/>
<rectangle x1="9.12368125" y1="1.5367" x2="9.49451875" y2="1.54508125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.54508125" x2="0.3683" y2="1.55371875" layer="119"/>
<rectangle x1="1.9685" y1="1.54508125" x2="2.34188125" y2="1.55371875" layer="119"/>
<rectangle x1="3.50011875" y1="1.54508125" x2="3.8735" y2="1.55371875" layer="119"/>
<rectangle x1="4.10971875" y1="1.54508125" x2="4.4831" y2="1.55371875" layer="119"/>
<rectangle x1="4.90728125" y1="1.54508125" x2="5.18668125" y2="1.55371875" layer="119"/>
<rectangle x1="5.63371875" y1="1.54508125" x2="6.01471875" y2="1.55371875" layer="119"/>
<rectangle x1="6.6421" y1="1.54508125" x2="7.01548125" y2="1.55371875" layer="119"/>
<rectangle x1="7.58951875" y1="1.54508125" x2="7.9629" y2="1.55371875" layer="119"/>
<rectangle x1="8.50391875" y1="1.54508125" x2="8.94588125" y2="1.55371875" layer="119"/>
<rectangle x1="9.12368125" y1="1.54508125" x2="9.49451875" y2="1.55371875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.55371875" x2="0.3683" y2="1.5621" layer="119"/>
<rectangle x1="1.9685" y1="1.55371875" x2="2.34188125" y2="1.5621" layer="119"/>
<rectangle x1="3.50011875" y1="1.55371875" x2="3.8735" y2="1.5621" layer="119"/>
<rectangle x1="4.10971875" y1="1.55371875" x2="4.4831" y2="1.5621" layer="119"/>
<rectangle x1="4.92251875" y1="1.55371875" x2="5.1689" y2="1.5621" layer="119"/>
<rectangle x1="5.63371875" y1="1.55371875" x2="6.01471875" y2="1.5621" layer="119"/>
<rectangle x1="6.6421" y1="1.55371875" x2="7.01548125" y2="1.5621" layer="119"/>
<rectangle x1="7.58951875" y1="1.55371875" x2="7.9629" y2="1.5621" layer="119"/>
<rectangle x1="8.4963" y1="1.55371875" x2="8.9281" y2="1.5621" layer="119"/>
<rectangle x1="9.12368125" y1="1.55371875" x2="9.49451875" y2="1.5621" layer="119"/>
<rectangle x1="-0.00508125" y1="1.5621" x2="0.3683" y2="1.57048125" layer="119"/>
<rectangle x1="1.9685" y1="1.5621" x2="2.34188125" y2="1.57048125" layer="119"/>
<rectangle x1="3.50011875" y1="1.5621" x2="3.8735" y2="1.57048125" layer="119"/>
<rectangle x1="4.10971875" y1="1.5621" x2="4.4831" y2="1.57048125" layer="119"/>
<rectangle x1="4.94791875" y1="1.5621" x2="5.1435" y2="1.57048125" layer="119"/>
<rectangle x1="5.63371875" y1="1.5621" x2="6.01471875" y2="1.57048125" layer="119"/>
<rectangle x1="6.6421" y1="1.5621" x2="7.01548125" y2="1.57048125" layer="119"/>
<rectangle x1="7.58951875" y1="1.5621" x2="7.9629" y2="1.57048125" layer="119"/>
<rectangle x1="8.47851875" y1="1.5621" x2="8.92048125" y2="1.57048125" layer="119"/>
<rectangle x1="9.12368125" y1="1.5621" x2="9.49451875" y2="1.57048125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.57048125" x2="0.3683" y2="1.57911875" layer="119"/>
<rectangle x1="1.9685" y1="1.57048125" x2="2.34188125" y2="1.57911875" layer="119"/>
<rectangle x1="3.50011875" y1="1.57048125" x2="3.8735" y2="1.57911875" layer="119"/>
<rectangle x1="4.10971875" y1="1.57048125" x2="4.4831" y2="1.57911875" layer="119"/>
<rectangle x1="4.97331875" y1="1.57048125" x2="5.1181" y2="1.57911875" layer="119"/>
<rectangle x1="5.63371875" y1="1.57048125" x2="6.01471875" y2="1.57911875" layer="119"/>
<rectangle x1="6.6421" y1="1.57048125" x2="7.01548125" y2="1.57911875" layer="119"/>
<rectangle x1="7.58951875" y1="1.57048125" x2="7.9629" y2="1.57911875" layer="119"/>
<rectangle x1="8.4709" y1="1.57048125" x2="8.91031875" y2="1.57911875" layer="119"/>
<rectangle x1="9.12368125" y1="1.57048125" x2="9.49451875" y2="1.57911875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.57911875" x2="0.3683" y2="1.5875" layer="119"/>
<rectangle x1="1.9685" y1="1.57911875" x2="2.34188125" y2="1.5875" layer="119"/>
<rectangle x1="3.50011875" y1="1.57911875" x2="3.8735" y2="1.5875" layer="119"/>
<rectangle x1="4.10971875" y1="1.57911875" x2="4.4831" y2="1.5875" layer="119"/>
<rectangle x1="4.99871875" y1="1.57911875" x2="5.0927" y2="1.5875" layer="119"/>
<rectangle x1="5.63371875" y1="1.57911875" x2="6.01471875" y2="1.5875" layer="119"/>
<rectangle x1="6.6421" y1="1.57911875" x2="7.01548125" y2="1.5875" layer="119"/>
<rectangle x1="7.58951875" y1="1.57911875" x2="7.9629" y2="1.5875" layer="119"/>
<rectangle x1="8.46328125" y1="1.57911875" x2="8.9027" y2="1.5875" layer="119"/>
<rectangle x1="9.12368125" y1="1.57911875" x2="9.49451875" y2="1.5875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.5875" x2="0.3683" y2="1.59588125" layer="119"/>
<rectangle x1="1.9685" y1="1.5875" x2="2.34188125" y2="1.59588125" layer="119"/>
<rectangle x1="3.50011875" y1="1.5875" x2="3.8735" y2="1.59588125" layer="119"/>
<rectangle x1="4.10971875" y1="1.5875" x2="4.4831" y2="1.59588125" layer="119"/>
<rectangle x1="5.63371875" y1="1.5875" x2="6.01471875" y2="1.59588125" layer="119"/>
<rectangle x1="6.6421" y1="1.5875" x2="7.01548125" y2="1.59588125" layer="119"/>
<rectangle x1="7.58951875" y1="1.5875" x2="7.9629" y2="1.59588125" layer="119"/>
<rectangle x1="8.45311875" y1="1.5875" x2="8.89508125" y2="1.59588125" layer="119"/>
<rectangle x1="9.12368125" y1="1.5875" x2="9.49451875" y2="1.59588125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.59588125" x2="1.3843" y2="1.60451875" layer="119"/>
<rectangle x1="1.9685" y1="1.59588125" x2="2.34188125" y2="1.60451875" layer="119"/>
<rectangle x1="3.50011875" y1="1.59588125" x2="3.8735" y2="1.60451875" layer="119"/>
<rectangle x1="4.10971875" y1="1.59588125" x2="4.4831" y2="1.60451875" layer="119"/>
<rectangle x1="5.63371875" y1="1.59588125" x2="6.01471875" y2="1.60451875" layer="119"/>
<rectangle x1="6.6421" y1="1.59588125" x2="7.01548125" y2="1.60451875" layer="119"/>
<rectangle x1="7.58951875" y1="1.59588125" x2="7.9629" y2="1.60451875" layer="119"/>
<rectangle x1="8.4455" y1="1.59588125" x2="8.88491875" y2="1.60451875" layer="119"/>
<rectangle x1="9.12368125" y1="1.59588125" x2="9.49451875" y2="1.60451875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.60451875" x2="1.41731875" y2="1.6129" layer="119"/>
<rectangle x1="1.9685" y1="1.60451875" x2="2.34188125" y2="1.6129" layer="119"/>
<rectangle x1="3.50011875" y1="1.60451875" x2="3.8735" y2="1.6129" layer="119"/>
<rectangle x1="4.10971875" y1="1.60451875" x2="4.4831" y2="1.6129" layer="119"/>
<rectangle x1="5.63371875" y1="1.60451875" x2="6.01471875" y2="1.6129" layer="119"/>
<rectangle x1="6.6421" y1="1.60451875" x2="7.01548125" y2="1.6129" layer="119"/>
<rectangle x1="7.58951875" y1="1.60451875" x2="7.9629" y2="1.6129" layer="119"/>
<rectangle x1="8.43788125" y1="1.60451875" x2="8.8773" y2="1.6129" layer="119"/>
<rectangle x1="9.12368125" y1="1.60451875" x2="9.49451875" y2="1.6129" layer="119"/>
<rectangle x1="-0.00508125" y1="1.6129" x2="1.4351" y2="1.62128125" layer="119"/>
<rectangle x1="1.9685" y1="1.6129" x2="2.34188125" y2="1.62128125" layer="119"/>
<rectangle x1="3.50011875" y1="1.6129" x2="3.8735" y2="1.62128125" layer="119"/>
<rectangle x1="4.10971875" y1="1.6129" x2="4.4831" y2="1.62128125" layer="119"/>
<rectangle x1="5.63371875" y1="1.6129" x2="6.01471875" y2="1.62128125" layer="119"/>
<rectangle x1="6.6421" y1="1.6129" x2="7.01548125" y2="1.62128125" layer="119"/>
<rectangle x1="7.58951875" y1="1.6129" x2="7.9629" y2="1.62128125" layer="119"/>
<rectangle x1="8.42771875" y1="1.6129" x2="8.86968125" y2="1.62128125" layer="119"/>
<rectangle x1="9.12368125" y1="1.6129" x2="9.49451875" y2="1.62128125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.62128125" x2="1.4605" y2="1.62991875" layer="119"/>
<rectangle x1="1.9685" y1="1.62128125" x2="2.34188125" y2="1.62991875" layer="119"/>
<rectangle x1="3.50011875" y1="1.62128125" x2="3.8735" y2="1.62991875" layer="119"/>
<rectangle x1="4.10971875" y1="1.62128125" x2="4.4831" y2="1.62991875" layer="119"/>
<rectangle x1="5.63371875" y1="1.62128125" x2="6.01471875" y2="1.62991875" layer="119"/>
<rectangle x1="6.6421" y1="1.62128125" x2="7.01548125" y2="1.62991875" layer="119"/>
<rectangle x1="7.58951875" y1="1.62128125" x2="7.9629" y2="1.62991875" layer="119"/>
<rectangle x1="8.4201" y1="1.62128125" x2="8.85951875" y2="1.62991875" layer="119"/>
<rectangle x1="9.12368125" y1="1.62128125" x2="9.49451875" y2="1.62991875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.62991875" x2="1.46811875" y2="1.6383" layer="119"/>
<rectangle x1="1.9685" y1="1.62991875" x2="2.34188125" y2="1.6383" layer="119"/>
<rectangle x1="3.50011875" y1="1.62991875" x2="3.8735" y2="1.6383" layer="119"/>
<rectangle x1="4.10971875" y1="1.62991875" x2="4.4831" y2="1.6383" layer="119"/>
<rectangle x1="5.63371875" y1="1.62991875" x2="6.01471875" y2="1.6383" layer="119"/>
<rectangle x1="6.6421" y1="1.62991875" x2="7.01548125" y2="1.6383" layer="119"/>
<rectangle x1="7.58951875" y1="1.62991875" x2="7.9629" y2="1.6383" layer="119"/>
<rectangle x1="8.41248125" y1="1.62991875" x2="8.8519" y2="1.6383" layer="119"/>
<rectangle x1="9.12368125" y1="1.62991875" x2="9.49451875" y2="1.6383" layer="119"/>
<rectangle x1="-0.00508125" y1="1.6383" x2="1.47828125" y2="1.64668125" layer="119"/>
<rectangle x1="1.9685" y1="1.6383" x2="2.34188125" y2="1.64668125" layer="119"/>
<rectangle x1="3.50011875" y1="1.6383" x2="3.8735" y2="1.64668125" layer="119"/>
<rectangle x1="4.10971875" y1="1.6383" x2="4.4831" y2="1.64668125" layer="119"/>
<rectangle x1="5.63371875" y1="1.6383" x2="6.01471875" y2="1.64668125" layer="119"/>
<rectangle x1="6.6421" y1="1.6383" x2="7.01548125" y2="1.64668125" layer="119"/>
<rectangle x1="7.58951875" y1="1.6383" x2="7.97051875" y2="1.64668125" layer="119"/>
<rectangle x1="8.40231875" y1="1.6383" x2="8.84428125" y2="1.64668125" layer="119"/>
<rectangle x1="9.12368125" y1="1.6383" x2="9.49451875" y2="1.64668125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.64668125" x2="1.4859" y2="1.65531875" layer="119"/>
<rectangle x1="1.9685" y1="1.64668125" x2="2.34188125" y2="1.65531875" layer="119"/>
<rectangle x1="3.50011875" y1="1.64668125" x2="3.8735" y2="1.65531875" layer="119"/>
<rectangle x1="4.10971875" y1="1.64668125" x2="4.4831" y2="1.65531875" layer="119"/>
<rectangle x1="5.63371875" y1="1.64668125" x2="6.01471875" y2="1.65531875" layer="119"/>
<rectangle x1="6.6421" y1="1.64668125" x2="7.01548125" y2="1.65531875" layer="119"/>
<rectangle x1="7.58951875" y1="1.64668125" x2="7.97051875" y2="1.65531875" layer="119"/>
<rectangle x1="8.3947" y1="1.64668125" x2="8.83411875" y2="1.65531875" layer="119"/>
<rectangle x1="9.12368125" y1="1.64668125" x2="9.49451875" y2="1.65531875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.65531875" x2="1.49351875" y2="1.6637" layer="119"/>
<rectangle x1="1.9685" y1="1.65531875" x2="2.34188125" y2="1.6637" layer="119"/>
<rectangle x1="3.50011875" y1="1.65531875" x2="3.8735" y2="1.6637" layer="119"/>
<rectangle x1="4.10971875" y1="1.65531875" x2="4.4831" y2="1.6637" layer="119"/>
<rectangle x1="5.63371875" y1="1.65531875" x2="6.01471875" y2="1.6637" layer="119"/>
<rectangle x1="6.6421" y1="1.65531875" x2="7.01548125" y2="1.6637" layer="119"/>
<rectangle x1="7.58951875" y1="1.65531875" x2="7.97051875" y2="1.6637" layer="119"/>
<rectangle x1="8.38708125" y1="1.65531875" x2="8.81888125" y2="1.6637" layer="119"/>
<rectangle x1="9.12368125" y1="1.65531875" x2="9.49451875" y2="1.6637" layer="119"/>
<rectangle x1="-0.00508125" y1="1.6637" x2="1.50368125" y2="1.67208125" layer="119"/>
<rectangle x1="1.9685" y1="1.6637" x2="2.34188125" y2="1.67208125" layer="119"/>
<rectangle x1="3.50011875" y1="1.6637" x2="3.8735" y2="1.67208125" layer="119"/>
<rectangle x1="4.10971875" y1="1.6637" x2="4.4831" y2="1.67208125" layer="119"/>
<rectangle x1="5.63371875" y1="1.6637" x2="6.01471875" y2="1.67208125" layer="119"/>
<rectangle x1="6.6421" y1="1.6637" x2="7.01548125" y2="1.67208125" layer="119"/>
<rectangle x1="7.58951875" y1="1.6637" x2="7.97051875" y2="1.67208125" layer="119"/>
<rectangle x1="8.3693" y1="1.6637" x2="8.80871875" y2="1.67208125" layer="119"/>
<rectangle x1="9.12368125" y1="1.6637" x2="9.49451875" y2="1.67208125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.67208125" x2="1.50368125" y2="1.68071875" layer="119"/>
<rectangle x1="1.9685" y1="1.67208125" x2="2.34188125" y2="1.68071875" layer="119"/>
<rectangle x1="3.50011875" y1="1.67208125" x2="3.8735" y2="1.68071875" layer="119"/>
<rectangle x1="4.10971875" y1="1.67208125" x2="4.4831" y2="1.68071875" layer="119"/>
<rectangle x1="5.63371875" y1="1.67208125" x2="6.01471875" y2="1.68071875" layer="119"/>
<rectangle x1="6.6421" y1="1.67208125" x2="7.01548125" y2="1.68071875" layer="119"/>
<rectangle x1="7.58951875" y1="1.67208125" x2="7.97051875" y2="1.68071875" layer="119"/>
<rectangle x1="8.3693" y1="1.67208125" x2="8.8011" y2="1.68071875" layer="119"/>
<rectangle x1="9.12368125" y1="1.67208125" x2="9.49451875" y2="1.68071875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.68071875" x2="1.5113" y2="1.6891" layer="119"/>
<rectangle x1="1.9685" y1="1.68071875" x2="2.34188125" y2="1.6891" layer="119"/>
<rectangle x1="3.50011875" y1="1.68071875" x2="3.8735" y2="1.6891" layer="119"/>
<rectangle x1="4.10971875" y1="1.68071875" x2="4.4831" y2="1.6891" layer="119"/>
<rectangle x1="5.63371875" y1="1.68071875" x2="6.01471875" y2="1.6891" layer="119"/>
<rectangle x1="6.6421" y1="1.68071875" x2="7.01548125" y2="1.6891" layer="119"/>
<rectangle x1="7.58951875" y1="1.68071875" x2="7.97051875" y2="1.6891" layer="119"/>
<rectangle x1="8.35151875" y1="1.68071875" x2="8.79348125" y2="1.6891" layer="119"/>
<rectangle x1="9.12368125" y1="1.68071875" x2="9.49451875" y2="1.6891" layer="119"/>
<rectangle x1="-0.00508125" y1="1.6891" x2="1.5113" y2="1.69748125" layer="119"/>
<rectangle x1="1.9685" y1="1.6891" x2="2.34188125" y2="1.69748125" layer="119"/>
<rectangle x1="3.50011875" y1="1.6891" x2="3.8735" y2="1.69748125" layer="119"/>
<rectangle x1="4.10971875" y1="1.6891" x2="4.4831" y2="1.69748125" layer="119"/>
<rectangle x1="5.63371875" y1="1.6891" x2="6.01471875" y2="1.69748125" layer="119"/>
<rectangle x1="6.6421" y1="1.6891" x2="7.01548125" y2="1.69748125" layer="119"/>
<rectangle x1="7.58951875" y1="1.6891" x2="7.97051875" y2="1.69748125" layer="119"/>
<rectangle x1="8.3439" y1="1.6891" x2="8.78331875" y2="1.69748125" layer="119"/>
<rectangle x1="9.12368125" y1="1.6891" x2="9.49451875" y2="1.69748125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.69748125" x2="1.5113" y2="1.70611875" layer="119"/>
<rectangle x1="1.9685" y1="1.69748125" x2="2.34188125" y2="1.70611875" layer="119"/>
<rectangle x1="3.50011875" y1="1.69748125" x2="3.8735" y2="1.70611875" layer="119"/>
<rectangle x1="4.10971875" y1="1.69748125" x2="4.4831" y2="1.70611875" layer="119"/>
<rectangle x1="5.63371875" y1="1.69748125" x2="6.01471875" y2="1.70611875" layer="119"/>
<rectangle x1="6.6421" y1="1.69748125" x2="7.01548125" y2="1.70611875" layer="119"/>
<rectangle x1="7.58951875" y1="1.69748125" x2="7.97051875" y2="1.70611875" layer="119"/>
<rectangle x1="8.33628125" y1="1.69748125" x2="8.7757" y2="1.70611875" layer="119"/>
<rectangle x1="9.12368125" y1="1.69748125" x2="9.49451875" y2="1.70611875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.70611875" x2="1.5113" y2="1.7145" layer="119"/>
<rectangle x1="1.9685" y1="1.70611875" x2="2.34188125" y2="1.7145" layer="119"/>
<rectangle x1="3.50011875" y1="1.70611875" x2="3.8735" y2="1.7145" layer="119"/>
<rectangle x1="4.10971875" y1="1.70611875" x2="4.4831" y2="1.7145" layer="119"/>
<rectangle x1="5.63371875" y1="1.70611875" x2="6.01471875" y2="1.7145" layer="119"/>
<rectangle x1="6.6421" y1="1.70611875" x2="7.01548125" y2="1.7145" layer="119"/>
<rectangle x1="7.58951875" y1="1.70611875" x2="7.97051875" y2="1.7145" layer="119"/>
<rectangle x1="8.32611875" y1="1.70611875" x2="8.76808125" y2="1.7145" layer="119"/>
<rectangle x1="9.12368125" y1="1.70611875" x2="9.49451875" y2="1.7145" layer="119"/>
<rectangle x1="-0.00508125" y1="1.7145" x2="1.5113" y2="1.72288125" layer="119"/>
<rectangle x1="1.9685" y1="1.7145" x2="2.34188125" y2="1.72288125" layer="119"/>
<rectangle x1="3.50011875" y1="1.7145" x2="3.8735" y2="1.72288125" layer="119"/>
<rectangle x1="4.10971875" y1="1.7145" x2="4.4831" y2="1.72288125" layer="119"/>
<rectangle x1="5.63371875" y1="1.7145" x2="6.01471875" y2="1.72288125" layer="119"/>
<rectangle x1="6.6421" y1="1.7145" x2="7.01548125" y2="1.72288125" layer="119"/>
<rectangle x1="7.58951875" y1="1.7145" x2="7.97051875" y2="1.72288125" layer="119"/>
<rectangle x1="8.3185" y1="1.7145" x2="8.75791875" y2="1.72288125" layer="119"/>
<rectangle x1="9.12368125" y1="1.7145" x2="9.49451875" y2="1.72288125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.72288125" x2="1.5113" y2="1.73151875" layer="119"/>
<rectangle x1="1.9685" y1="1.72288125" x2="2.34188125" y2="1.73151875" layer="119"/>
<rectangle x1="3.50011875" y1="1.72288125" x2="3.8735" y2="1.73151875" layer="119"/>
<rectangle x1="4.10971875" y1="1.72288125" x2="4.4831" y2="1.73151875" layer="119"/>
<rectangle x1="5.63371875" y1="1.72288125" x2="6.01471875" y2="1.73151875" layer="119"/>
<rectangle x1="6.6421" y1="1.72288125" x2="7.01548125" y2="1.73151875" layer="119"/>
<rectangle x1="7.58951875" y1="1.72288125" x2="7.97051875" y2="1.73151875" layer="119"/>
<rectangle x1="8.31088125" y1="1.72288125" x2="8.7503" y2="1.73151875" layer="119"/>
<rectangle x1="9.12368125" y1="1.72288125" x2="9.49451875" y2="1.73151875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.73151875" x2="1.5113" y2="1.7399" layer="119"/>
<rectangle x1="1.9685" y1="1.73151875" x2="2.34188125" y2="1.7399" layer="119"/>
<rectangle x1="3.50011875" y1="1.73151875" x2="3.8735" y2="1.7399" layer="119"/>
<rectangle x1="4.10971875" y1="1.73151875" x2="4.4831" y2="1.7399" layer="119"/>
<rectangle x1="5.63371875" y1="1.73151875" x2="6.01471875" y2="1.7399" layer="119"/>
<rectangle x1="6.6421" y1="1.73151875" x2="7.01548125" y2="1.7399" layer="119"/>
<rectangle x1="7.58951875" y1="1.73151875" x2="7.97051875" y2="1.7399" layer="119"/>
<rectangle x1="8.30071875" y1="1.73151875" x2="8.74268125" y2="1.7399" layer="119"/>
<rectangle x1="9.12368125" y1="1.73151875" x2="9.49451875" y2="1.7399" layer="119"/>
<rectangle x1="-0.00508125" y1="1.7399" x2="1.5113" y2="1.74828125" layer="119"/>
<rectangle x1="1.9685" y1="1.7399" x2="2.34188125" y2="1.74828125" layer="119"/>
<rectangle x1="3.50011875" y1="1.7399" x2="3.8735" y2="1.74828125" layer="119"/>
<rectangle x1="4.10971875" y1="1.7399" x2="4.4831" y2="1.74828125" layer="119"/>
<rectangle x1="5.63371875" y1="1.7399" x2="6.01471875" y2="1.74828125" layer="119"/>
<rectangle x1="6.6421" y1="1.7399" x2="7.01548125" y2="1.74828125" layer="119"/>
<rectangle x1="7.58951875" y1="1.7399" x2="7.97051875" y2="1.74828125" layer="119"/>
<rectangle x1="8.2931" y1="1.7399" x2="8.73251875" y2="1.74828125" layer="119"/>
<rectangle x1="9.12368125" y1="1.7399" x2="9.49451875" y2="1.74828125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.74828125" x2="1.50368125" y2="1.75691875" layer="119"/>
<rectangle x1="1.9685" y1="1.74828125" x2="2.34188125" y2="1.75691875" layer="119"/>
<rectangle x1="3.50011875" y1="1.74828125" x2="3.8735" y2="1.75691875" layer="119"/>
<rectangle x1="4.10971875" y1="1.74828125" x2="4.4831" y2="1.75691875" layer="119"/>
<rectangle x1="5.63371875" y1="1.74828125" x2="6.01471875" y2="1.75691875" layer="119"/>
<rectangle x1="6.6421" y1="1.74828125" x2="7.01548125" y2="1.75691875" layer="119"/>
<rectangle x1="7.58951875" y1="1.74828125" x2="7.97051875" y2="1.75691875" layer="119"/>
<rectangle x1="8.28548125" y1="1.74828125" x2="8.7249" y2="1.75691875" layer="119"/>
<rectangle x1="9.12368125" y1="1.74828125" x2="9.49451875" y2="1.75691875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.75691875" x2="1.50368125" y2="1.7653" layer="119"/>
<rectangle x1="1.9685" y1="1.75691875" x2="2.34188125" y2="1.7653" layer="119"/>
<rectangle x1="3.50011875" y1="1.75691875" x2="3.8735" y2="1.7653" layer="119"/>
<rectangle x1="4.10971875" y1="1.75691875" x2="4.4831" y2="1.7653" layer="119"/>
<rectangle x1="5.63371875" y1="1.75691875" x2="6.01471875" y2="1.7653" layer="119"/>
<rectangle x1="6.6421" y1="1.75691875" x2="7.01548125" y2="1.7653" layer="119"/>
<rectangle x1="7.58951875" y1="1.75691875" x2="7.97051875" y2="1.7653" layer="119"/>
<rectangle x1="8.27531875" y1="1.75691875" x2="8.70711875" y2="1.7653" layer="119"/>
<rectangle x1="9.12368125" y1="1.75691875" x2="9.49451875" y2="1.7653" layer="119"/>
<rectangle x1="-0.00508125" y1="1.7653" x2="1.49351875" y2="1.77368125" layer="119"/>
<rectangle x1="1.9685" y1="1.7653" x2="2.34188125" y2="1.77368125" layer="119"/>
<rectangle x1="3.50011875" y1="1.7653" x2="3.8735" y2="1.77368125" layer="119"/>
<rectangle x1="4.10971875" y1="1.7653" x2="4.4831" y2="1.77368125" layer="119"/>
<rectangle x1="5.63371875" y1="1.7653" x2="6.01471875" y2="1.77368125" layer="119"/>
<rectangle x1="6.6421" y1="1.7653" x2="7.01548125" y2="1.77368125" layer="119"/>
<rectangle x1="7.58951875" y1="1.7653" x2="7.97051875" y2="1.77368125" layer="119"/>
<rectangle x1="8.2677" y1="1.7653" x2="8.6995" y2="1.77368125" layer="119"/>
<rectangle x1="9.12368125" y1="1.7653" x2="9.49451875" y2="1.77368125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.77368125" x2="1.4859" y2="1.78231875" layer="119"/>
<rectangle x1="1.9685" y1="1.77368125" x2="2.34188125" y2="1.78231875" layer="119"/>
<rectangle x1="3.50011875" y1="1.77368125" x2="3.8735" y2="1.78231875" layer="119"/>
<rectangle x1="4.10971875" y1="1.77368125" x2="4.4831" y2="1.78231875" layer="119"/>
<rectangle x1="5.63371875" y1="1.77368125" x2="6.01471875" y2="1.78231875" layer="119"/>
<rectangle x1="6.6421" y1="1.77368125" x2="7.01548125" y2="1.78231875" layer="119"/>
<rectangle x1="7.58951875" y1="1.77368125" x2="7.97051875" y2="1.78231875" layer="119"/>
<rectangle x1="8.26008125" y1="1.77368125" x2="8.69188125" y2="1.78231875" layer="119"/>
<rectangle x1="9.12368125" y1="1.77368125" x2="9.49451875" y2="1.78231875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.78231875" x2="1.47828125" y2="1.7907" layer="119"/>
<rectangle x1="1.9685" y1="1.78231875" x2="2.34188125" y2="1.7907" layer="119"/>
<rectangle x1="3.50011875" y1="1.78231875" x2="3.8735" y2="1.7907" layer="119"/>
<rectangle x1="4.10971875" y1="1.78231875" x2="4.4831" y2="1.7907" layer="119"/>
<rectangle x1="5.63371875" y1="1.78231875" x2="6.01471875" y2="1.7907" layer="119"/>
<rectangle x1="6.6421" y1="1.78231875" x2="7.01548125" y2="1.7907" layer="119"/>
<rectangle x1="7.58951875" y1="1.78231875" x2="7.97051875" y2="1.7907" layer="119"/>
<rectangle x1="8.2423" y1="1.78231875" x2="8.68171875" y2="1.7907" layer="119"/>
<rectangle x1="9.12368125" y1="1.78231875" x2="9.49451875" y2="1.7907" layer="119"/>
<rectangle x1="-0.00508125" y1="1.7907" x2="1.46811875" y2="1.79908125" layer="119"/>
<rectangle x1="1.9685" y1="1.7907" x2="2.34188125" y2="1.79908125" layer="119"/>
<rectangle x1="3.50011875" y1="1.7907" x2="3.8735" y2="1.79908125" layer="119"/>
<rectangle x1="4.10971875" y1="1.7907" x2="4.4831" y2="1.79908125" layer="119"/>
<rectangle x1="5.63371875" y1="1.7907" x2="6.01471875" y2="1.79908125" layer="119"/>
<rectangle x1="6.6421" y1="1.7907" x2="7.01548125" y2="1.79908125" layer="119"/>
<rectangle x1="7.58951875" y1="1.7907" x2="7.97051875" y2="1.79908125" layer="119"/>
<rectangle x1="8.23468125" y1="1.7907" x2="8.6741" y2="1.79908125" layer="119"/>
<rectangle x1="9.12368125" y1="1.7907" x2="9.49451875" y2="1.79908125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.79908125" x2="1.45288125" y2="1.80771875" layer="119"/>
<rectangle x1="1.9685" y1="1.79908125" x2="2.34188125" y2="1.80771875" layer="119"/>
<rectangle x1="3.50011875" y1="1.79908125" x2="3.8735" y2="1.80771875" layer="119"/>
<rectangle x1="4.10971875" y1="1.79908125" x2="4.4831" y2="1.80771875" layer="119"/>
<rectangle x1="5.63371875" y1="1.79908125" x2="6.01471875" y2="1.80771875" layer="119"/>
<rectangle x1="6.6421" y1="1.79908125" x2="7.01548125" y2="1.80771875" layer="119"/>
<rectangle x1="7.58951875" y1="1.79908125" x2="7.97051875" y2="1.80771875" layer="119"/>
<rectangle x1="8.22451875" y1="1.79908125" x2="8.66648125" y2="1.80771875" layer="119"/>
<rectangle x1="9.12368125" y1="1.79908125" x2="9.49451875" y2="1.80771875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.80771875" x2="1.4351" y2="1.8161" layer="119"/>
<rectangle x1="1.9685" y1="1.80771875" x2="2.34188125" y2="1.8161" layer="119"/>
<rectangle x1="3.50011875" y1="1.80771875" x2="3.8735" y2="1.8161" layer="119"/>
<rectangle x1="4.10971875" y1="1.80771875" x2="4.4831" y2="1.8161" layer="119"/>
<rectangle x1="5.63371875" y1="1.80771875" x2="6.01471875" y2="1.8161" layer="119"/>
<rectangle x1="6.6421" y1="1.80771875" x2="7.01548125" y2="1.8161" layer="119"/>
<rectangle x1="7.58951875" y1="1.80771875" x2="7.97051875" y2="1.8161" layer="119"/>
<rectangle x1="8.2169" y1="1.80771875" x2="8.65631875" y2="1.8161" layer="119"/>
<rectangle x1="9.12368125" y1="1.80771875" x2="9.49451875" y2="1.8161" layer="119"/>
<rectangle x1="-0.00508125" y1="1.8161" x2="1.4097" y2="1.82448125" layer="119"/>
<rectangle x1="1.9685" y1="1.8161" x2="2.34188125" y2="1.82448125" layer="119"/>
<rectangle x1="3.50011875" y1="1.8161" x2="3.8735" y2="1.82448125" layer="119"/>
<rectangle x1="4.10971875" y1="1.8161" x2="4.4831" y2="1.82448125" layer="119"/>
<rectangle x1="5.63371875" y1="1.8161" x2="6.01471875" y2="1.82448125" layer="119"/>
<rectangle x1="6.6421" y1="1.8161" x2="7.01548125" y2="1.82448125" layer="119"/>
<rectangle x1="7.58951875" y1="1.8161" x2="7.97051875" y2="1.82448125" layer="119"/>
<rectangle x1="8.20928125" y1="1.8161" x2="8.6487" y2="1.82448125" layer="119"/>
<rectangle x1="9.12368125" y1="1.8161" x2="9.49451875" y2="1.82448125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.82448125" x2="0.3683" y2="1.83311875" layer="119"/>
<rectangle x1="1.9685" y1="1.82448125" x2="2.34188125" y2="1.83311875" layer="119"/>
<rectangle x1="3.50011875" y1="1.82448125" x2="3.8735" y2="1.83311875" layer="119"/>
<rectangle x1="4.10971875" y1="1.82448125" x2="4.4831" y2="1.83311875" layer="119"/>
<rectangle x1="5.63371875" y1="1.82448125" x2="6.01471875" y2="1.83311875" layer="119"/>
<rectangle x1="6.6421" y1="1.82448125" x2="7.01548125" y2="1.83311875" layer="119"/>
<rectangle x1="7.58951875" y1="1.82448125" x2="7.97051875" y2="1.83311875" layer="119"/>
<rectangle x1="8.19911875" y1="1.82448125" x2="8.64108125" y2="1.83311875" layer="119"/>
<rectangle x1="9.12368125" y1="1.82448125" x2="9.49451875" y2="1.83311875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.83311875" x2="0.3683" y2="1.8415" layer="119"/>
<rectangle x1="1.9685" y1="1.83311875" x2="2.34188125" y2="1.8415" layer="119"/>
<rectangle x1="3.50011875" y1="1.83311875" x2="3.8735" y2="1.8415" layer="119"/>
<rectangle x1="4.10971875" y1="1.83311875" x2="4.4831" y2="1.8415" layer="119"/>
<rectangle x1="5.63371875" y1="1.83311875" x2="6.01471875" y2="1.8415" layer="119"/>
<rectangle x1="6.6421" y1="1.83311875" x2="7.01548125" y2="1.8415" layer="119"/>
<rectangle x1="7.58951875" y1="1.83311875" x2="7.97051875" y2="1.8415" layer="119"/>
<rectangle x1="8.1915" y1="1.83311875" x2="8.63091875" y2="1.8415" layer="119"/>
<rectangle x1="9.12368125" y1="1.83311875" x2="9.49451875" y2="1.8415" layer="119"/>
<rectangle x1="-0.00508125" y1="1.8415" x2="0.3683" y2="1.84988125" layer="119"/>
<rectangle x1="1.9685" y1="1.8415" x2="2.34188125" y2="1.84988125" layer="119"/>
<rectangle x1="3.50011875" y1="1.8415" x2="3.8735" y2="1.84988125" layer="119"/>
<rectangle x1="4.10971875" y1="1.8415" x2="4.4831" y2="1.84988125" layer="119"/>
<rectangle x1="5.63371875" y1="1.8415" x2="6.01471875" y2="1.84988125" layer="119"/>
<rectangle x1="6.6421" y1="1.8415" x2="7.01548125" y2="1.84988125" layer="119"/>
<rectangle x1="7.58951875" y1="1.8415" x2="7.97051875" y2="1.84988125" layer="119"/>
<rectangle x1="8.18388125" y1="1.8415" x2="8.6233" y2="1.84988125" layer="119"/>
<rectangle x1="9.12368125" y1="1.8415" x2="9.49451875" y2="1.84988125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.84988125" x2="0.3683" y2="1.85851875" layer="119"/>
<rectangle x1="1.9685" y1="1.84988125" x2="2.34188125" y2="1.85851875" layer="119"/>
<rectangle x1="3.50011875" y1="1.84988125" x2="3.8735" y2="1.85851875" layer="119"/>
<rectangle x1="4.10971875" y1="1.84988125" x2="4.4831" y2="1.85851875" layer="119"/>
<rectangle x1="5.63371875" y1="1.84988125" x2="6.01471875" y2="1.85851875" layer="119"/>
<rectangle x1="6.6421" y1="1.84988125" x2="7.01548125" y2="1.85851875" layer="119"/>
<rectangle x1="7.58951875" y1="1.84988125" x2="7.97051875" y2="1.85851875" layer="119"/>
<rectangle x1="8.17371875" y1="1.84988125" x2="8.61568125" y2="1.85851875" layer="119"/>
<rectangle x1="9.12368125" y1="1.84988125" x2="9.49451875" y2="1.85851875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.85851875" x2="0.3683" y2="1.8669" layer="119"/>
<rectangle x1="1.9685" y1="1.85851875" x2="2.34188125" y2="1.8669" layer="119"/>
<rectangle x1="3.50011875" y1="1.85851875" x2="3.8735" y2="1.8669" layer="119"/>
<rectangle x1="4.10971875" y1="1.85851875" x2="4.4831" y2="1.8669" layer="119"/>
<rectangle x1="5.63371875" y1="1.85851875" x2="6.01471875" y2="1.8669" layer="119"/>
<rectangle x1="6.6421" y1="1.85851875" x2="7.01548125" y2="1.8669" layer="119"/>
<rectangle x1="7.58951875" y1="1.85851875" x2="7.97051875" y2="1.8669" layer="119"/>
<rectangle x1="8.1661" y1="1.85851875" x2="8.5979" y2="1.8669" layer="119"/>
<rectangle x1="9.12368125" y1="1.85851875" x2="9.49451875" y2="1.8669" layer="119"/>
<rectangle x1="-0.00508125" y1="1.8669" x2="0.3683" y2="1.87528125" layer="119"/>
<rectangle x1="1.9685" y1="1.8669" x2="2.34188125" y2="1.87528125" layer="119"/>
<rectangle x1="3.50011875" y1="1.8669" x2="3.8735" y2="1.87528125" layer="119"/>
<rectangle x1="4.10971875" y1="1.8669" x2="4.4831" y2="1.87528125" layer="119"/>
<rectangle x1="5.63371875" y1="1.8669" x2="6.01471875" y2="1.87528125" layer="119"/>
<rectangle x1="6.6421" y1="1.8669" x2="7.01548125" y2="1.87528125" layer="119"/>
<rectangle x1="7.58951875" y1="1.8669" x2="7.97051875" y2="1.87528125" layer="119"/>
<rectangle x1="8.15848125" y1="1.8669" x2="8.5979" y2="1.87528125" layer="119"/>
<rectangle x1="9.12368125" y1="1.8669" x2="9.49451875" y2="1.87528125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.87528125" x2="0.3683" y2="1.88391875" layer="119"/>
<rectangle x1="1.9685" y1="1.87528125" x2="2.34188125" y2="1.88391875" layer="119"/>
<rectangle x1="3.50011875" y1="1.87528125" x2="3.8735" y2="1.88391875" layer="119"/>
<rectangle x1="4.10971875" y1="1.87528125" x2="4.4831" y2="1.88391875" layer="119"/>
<rectangle x1="5.63371875" y1="1.87528125" x2="6.01471875" y2="1.88391875" layer="119"/>
<rectangle x1="6.6421" y1="1.87528125" x2="7.01548125" y2="1.88391875" layer="119"/>
<rectangle x1="7.58951875" y1="1.87528125" x2="7.97051875" y2="1.88391875" layer="119"/>
<rectangle x1="8.14831875" y1="1.87528125" x2="8.58011875" y2="1.88391875" layer="119"/>
<rectangle x1="9.12368125" y1="1.87528125" x2="9.49451875" y2="1.88391875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.88391875" x2="0.3683" y2="1.8923" layer="119"/>
<rectangle x1="1.9685" y1="1.88391875" x2="2.34188125" y2="1.8923" layer="119"/>
<rectangle x1="3.50011875" y1="1.88391875" x2="3.8735" y2="1.8923" layer="119"/>
<rectangle x1="4.10971875" y1="1.88391875" x2="4.4831" y2="1.8923" layer="119"/>
<rectangle x1="5.63371875" y1="1.88391875" x2="6.01471875" y2="1.8923" layer="119"/>
<rectangle x1="6.6421" y1="1.88391875" x2="7.01548125" y2="1.8923" layer="119"/>
<rectangle x1="7.58951875" y1="1.88391875" x2="7.97051875" y2="1.8923" layer="119"/>
<rectangle x1="8.1407" y1="1.88391875" x2="8.5725" y2="1.8923" layer="119"/>
<rectangle x1="9.12368125" y1="1.88391875" x2="9.49451875" y2="1.8923" layer="119"/>
<rectangle x1="-0.00508125" y1="1.8923" x2="0.3683" y2="1.90068125" layer="119"/>
<rectangle x1="1.9685" y1="1.8923" x2="2.34188125" y2="1.90068125" layer="119"/>
<rectangle x1="3.50011875" y1="1.8923" x2="3.8735" y2="1.90068125" layer="119"/>
<rectangle x1="4.10971875" y1="1.8923" x2="4.4831" y2="1.90068125" layer="119"/>
<rectangle x1="5.63371875" y1="1.8923" x2="6.01471875" y2="1.90068125" layer="119"/>
<rectangle x1="6.6421" y1="1.8923" x2="7.01548125" y2="1.90068125" layer="119"/>
<rectangle x1="7.58951875" y1="1.8923" x2="7.97051875" y2="1.90068125" layer="119"/>
<rectangle x1="8.12291875" y1="1.8923" x2="8.56488125" y2="1.90068125" layer="119"/>
<rectangle x1="9.12368125" y1="1.8923" x2="9.49451875" y2="1.90068125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.90068125" x2="0.3683" y2="1.90931875" layer="119"/>
<rectangle x1="1.9685" y1="1.90068125" x2="2.34188125" y2="1.90931875" layer="119"/>
<rectangle x1="3.50011875" y1="1.90068125" x2="3.8735" y2="1.90931875" layer="119"/>
<rectangle x1="4.10971875" y1="1.90068125" x2="4.4831" y2="1.90931875" layer="119"/>
<rectangle x1="5.63371875" y1="1.90068125" x2="6.01471875" y2="1.90931875" layer="119"/>
<rectangle x1="6.6421" y1="1.90068125" x2="7.01548125" y2="1.90931875" layer="119"/>
<rectangle x1="7.58951875" y1="1.90068125" x2="7.97051875" y2="1.90931875" layer="119"/>
<rectangle x1="8.1153" y1="1.90068125" x2="8.55471875" y2="1.90931875" layer="119"/>
<rectangle x1="9.12368125" y1="1.90068125" x2="9.49451875" y2="1.90931875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.90931875" x2="0.3683" y2="1.9177" layer="119"/>
<rectangle x1="1.9685" y1="1.90931875" x2="2.34188125" y2="1.9177" layer="119"/>
<rectangle x1="3.50011875" y1="1.90931875" x2="3.8735" y2="1.9177" layer="119"/>
<rectangle x1="4.10971875" y1="1.90931875" x2="4.4831" y2="1.9177" layer="119"/>
<rectangle x1="5.63371875" y1="1.90931875" x2="6.01471875" y2="1.9177" layer="119"/>
<rectangle x1="6.6421" y1="1.90931875" x2="7.01548125" y2="1.9177" layer="119"/>
<rectangle x1="7.58951875" y1="1.90931875" x2="7.97051875" y2="1.9177" layer="119"/>
<rectangle x1="8.10768125" y1="1.90931875" x2="8.5471" y2="1.9177" layer="119"/>
<rectangle x1="9.12368125" y1="1.90931875" x2="9.49451875" y2="1.9177" layer="119"/>
<rectangle x1="-0.00508125" y1="1.9177" x2="0.3683" y2="1.92608125" layer="119"/>
<rectangle x1="1.9685" y1="1.9177" x2="2.34188125" y2="1.92608125" layer="119"/>
<rectangle x1="3.50011875" y1="1.9177" x2="3.8735" y2="1.92608125" layer="119"/>
<rectangle x1="4.10971875" y1="1.9177" x2="4.4831" y2="1.92608125" layer="119"/>
<rectangle x1="5.63371875" y1="1.9177" x2="6.01471875" y2="1.92608125" layer="119"/>
<rectangle x1="6.6421" y1="1.9177" x2="7.01548125" y2="1.92608125" layer="119"/>
<rectangle x1="7.58951875" y1="1.9177" x2="7.97051875" y2="1.92608125" layer="119"/>
<rectangle x1="8.09751875" y1="1.9177" x2="8.53948125" y2="1.92608125" layer="119"/>
<rectangle x1="9.12368125" y1="1.9177" x2="9.49451875" y2="1.92608125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.92608125" x2="0.3683" y2="1.93471875" layer="119"/>
<rectangle x1="1.9685" y1="1.92608125" x2="2.34188125" y2="1.93471875" layer="119"/>
<rectangle x1="3.50011875" y1="1.92608125" x2="3.8735" y2="1.93471875" layer="119"/>
<rectangle x1="4.10971875" y1="1.92608125" x2="4.4831" y2="1.93471875" layer="119"/>
<rectangle x1="5.63371875" y1="1.92608125" x2="6.01471875" y2="1.93471875" layer="119"/>
<rectangle x1="6.6421" y1="1.92608125" x2="7.01548125" y2="1.93471875" layer="119"/>
<rectangle x1="7.58951875" y1="1.92608125" x2="7.97051875" y2="1.93471875" layer="119"/>
<rectangle x1="8.0899" y1="1.92608125" x2="8.52931875" y2="1.93471875" layer="119"/>
<rectangle x1="9.12368125" y1="1.92608125" x2="9.49451875" y2="1.93471875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.93471875" x2="0.3683" y2="1.9431" layer="119"/>
<rectangle x1="1.9685" y1="1.93471875" x2="2.34188125" y2="1.9431" layer="119"/>
<rectangle x1="3.50011875" y1="1.93471875" x2="3.8735" y2="1.9431" layer="119"/>
<rectangle x1="4.10971875" y1="1.93471875" x2="4.4831" y2="1.9431" layer="119"/>
<rectangle x1="5.63371875" y1="1.93471875" x2="6.01471875" y2="1.9431" layer="119"/>
<rectangle x1="6.6421" y1="1.93471875" x2="7.01548125" y2="1.9431" layer="119"/>
<rectangle x1="7.58951875" y1="1.93471875" x2="7.97051875" y2="1.9431" layer="119"/>
<rectangle x1="8.08228125" y1="1.93471875" x2="8.5217" y2="1.9431" layer="119"/>
<rectangle x1="9.12368125" y1="1.93471875" x2="9.49451875" y2="1.9431" layer="119"/>
<rectangle x1="-0.00508125" y1="1.9431" x2="0.3683" y2="1.95148125" layer="119"/>
<rectangle x1="1.9685" y1="1.9431" x2="2.34188125" y2="1.95148125" layer="119"/>
<rectangle x1="3.50011875" y1="1.9431" x2="3.8735" y2="1.95148125" layer="119"/>
<rectangle x1="4.10971875" y1="1.9431" x2="4.4831" y2="1.95148125" layer="119"/>
<rectangle x1="5.63371875" y1="1.9431" x2="6.01471875" y2="1.95148125" layer="119"/>
<rectangle x1="6.6421" y1="1.9431" x2="7.01548125" y2="1.95148125" layer="119"/>
<rectangle x1="7.58951875" y1="1.9431" x2="7.97051875" y2="1.95148125" layer="119"/>
<rectangle x1="8.07211875" y1="1.9431" x2="8.51408125" y2="1.95148125" layer="119"/>
<rectangle x1="9.12368125" y1="1.9431" x2="9.49451875" y2="1.95148125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.95148125" x2="0.3683" y2="1.96011875" layer="119"/>
<rectangle x1="1.9685" y1="1.95148125" x2="2.34188125" y2="1.96011875" layer="119"/>
<rectangle x1="3.50011875" y1="1.95148125" x2="3.8735" y2="1.96011875" layer="119"/>
<rectangle x1="4.10971875" y1="1.95148125" x2="4.4831" y2="1.96011875" layer="119"/>
<rectangle x1="5.63371875" y1="1.95148125" x2="6.01471875" y2="1.96011875" layer="119"/>
<rectangle x1="6.6421" y1="1.95148125" x2="7.01548125" y2="1.96011875" layer="119"/>
<rectangle x1="7.58951875" y1="1.95148125" x2="7.97051875" y2="1.96011875" layer="119"/>
<rectangle x1="8.0645" y1="1.95148125" x2="8.50391875" y2="1.96011875" layer="119"/>
<rectangle x1="9.12368125" y1="1.95148125" x2="9.49451875" y2="1.96011875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.96011875" x2="0.3683" y2="1.9685" layer="119"/>
<rectangle x1="1.9685" y1="1.96011875" x2="2.34188125" y2="1.9685" layer="119"/>
<rectangle x1="3.50011875" y1="1.96011875" x2="3.8735" y2="1.9685" layer="119"/>
<rectangle x1="4.10971875" y1="1.96011875" x2="4.4831" y2="1.9685" layer="119"/>
<rectangle x1="5.63371875" y1="1.96011875" x2="6.01471875" y2="1.9685" layer="119"/>
<rectangle x1="6.6421" y1="1.96011875" x2="7.01548125" y2="1.9685" layer="119"/>
<rectangle x1="7.58951875" y1="1.96011875" x2="7.97051875" y2="1.9685" layer="119"/>
<rectangle x1="8.05688125" y1="1.96011875" x2="8.4963" y2="1.9685" layer="119"/>
<rectangle x1="9.12368125" y1="1.96011875" x2="9.49451875" y2="1.9685" layer="119"/>
<rectangle x1="-0.00508125" y1="1.9685" x2="0.3683" y2="1.97688125" layer="119"/>
<rectangle x1="1.9685" y1="1.9685" x2="2.34188125" y2="1.97688125" layer="119"/>
<rectangle x1="3.50011875" y1="1.9685" x2="3.8735" y2="1.97688125" layer="119"/>
<rectangle x1="4.10971875" y1="1.9685" x2="4.4831" y2="1.97688125" layer="119"/>
<rectangle x1="5.63371875" y1="1.9685" x2="6.01471875" y2="1.97688125" layer="119"/>
<rectangle x1="6.6421" y1="1.9685" x2="7.01548125" y2="1.97688125" layer="119"/>
<rectangle x1="7.58951875" y1="1.9685" x2="7.97051875" y2="1.97688125" layer="119"/>
<rectangle x1="8.04671875" y1="1.9685" x2="8.48868125" y2="1.97688125" layer="119"/>
<rectangle x1="9.12368125" y1="1.9685" x2="9.49451875" y2="1.97688125" layer="119"/>
<rectangle x1="-0.00508125" y1="1.97688125" x2="0.3683" y2="1.98551875" layer="119"/>
<rectangle x1="1.9685" y1="1.97688125" x2="2.34188125" y2="1.98551875" layer="119"/>
<rectangle x1="3.50011875" y1="1.97688125" x2="3.8735" y2="1.98551875" layer="119"/>
<rectangle x1="4.10971875" y1="1.97688125" x2="4.4831" y2="1.98551875" layer="119"/>
<rectangle x1="5.63371875" y1="1.97688125" x2="6.01471875" y2="1.98551875" layer="119"/>
<rectangle x1="6.6421" y1="1.97688125" x2="7.01548125" y2="1.98551875" layer="119"/>
<rectangle x1="7.58951875" y1="1.97688125" x2="7.97051875" y2="1.98551875" layer="119"/>
<rectangle x1="8.0391" y1="1.97688125" x2="8.4709" y2="1.98551875" layer="119"/>
<rectangle x1="9.12368125" y1="1.97688125" x2="9.49451875" y2="1.98551875" layer="119"/>
<rectangle x1="-0.00508125" y1="1.98551875" x2="0.3683" y2="1.9939" layer="119"/>
<rectangle x1="1.9685" y1="1.98551875" x2="2.34188125" y2="1.9939" layer="119"/>
<rectangle x1="3.50011875" y1="1.98551875" x2="3.8735" y2="1.9939" layer="119"/>
<rectangle x1="4.10971875" y1="1.98551875" x2="4.4831" y2="1.9939" layer="119"/>
<rectangle x1="5.63371875" y1="1.98551875" x2="6.01471875" y2="1.9939" layer="119"/>
<rectangle x1="6.6421" y1="1.98551875" x2="7.01548125" y2="1.9939" layer="119"/>
<rectangle x1="7.58951875" y1="1.98551875" x2="7.97051875" y2="1.9939" layer="119"/>
<rectangle x1="8.02131875" y1="1.98551875" x2="8.46328125" y2="1.9939" layer="119"/>
<rectangle x1="9.12368125" y1="1.98551875" x2="9.49451875" y2="1.9939" layer="119"/>
<rectangle x1="-0.00508125" y1="1.9939" x2="0.3683" y2="2.00228125" layer="119"/>
<rectangle x1="1.9685" y1="1.9939" x2="2.34188125" y2="2.00228125" layer="119"/>
<rectangle x1="3.50011875" y1="1.9939" x2="3.8735" y2="2.00228125" layer="119"/>
<rectangle x1="4.10971875" y1="1.9939" x2="4.4831" y2="2.00228125" layer="119"/>
<rectangle x1="5.63371875" y1="1.9939" x2="6.01471875" y2="2.00228125" layer="119"/>
<rectangle x1="6.6421" y1="1.9939" x2="7.01548125" y2="2.00228125" layer="119"/>
<rectangle x1="7.58951875" y1="1.9939" x2="7.97051875" y2="2.00228125" layer="119"/>
<rectangle x1="8.0137" y1="1.9939" x2="8.45311875" y2="2.00228125" layer="119"/>
<rectangle x1="9.12368125" y1="1.9939" x2="9.49451875" y2="2.00228125" layer="119"/>
<rectangle x1="-0.00508125" y1="2.00228125" x2="0.3683" y2="2.01091875" layer="119"/>
<rectangle x1="1.9685" y1="2.00228125" x2="2.34188125" y2="2.01091875" layer="119"/>
<rectangle x1="3.50011875" y1="2.00228125" x2="3.8735" y2="2.01091875" layer="119"/>
<rectangle x1="4.10971875" y1="2.00228125" x2="4.4831" y2="2.01091875" layer="119"/>
<rectangle x1="5.64388125" y1="2.00228125" x2="6.01471875" y2="2.01091875" layer="119"/>
<rectangle x1="6.6421" y1="2.00228125" x2="7.01548125" y2="2.01091875" layer="119"/>
<rectangle x1="7.58951875" y1="2.00228125" x2="7.97051875" y2="2.01091875" layer="119"/>
<rectangle x1="8.00608125" y1="2.00228125" x2="8.4455" y2="2.01091875" layer="119"/>
<rectangle x1="9.12368125" y1="2.00228125" x2="9.49451875" y2="2.01091875" layer="119"/>
<rectangle x1="-0.00508125" y1="2.01091875" x2="0.3683" y2="2.0193" layer="119"/>
<rectangle x1="1.9685" y1="2.01091875" x2="2.34188125" y2="2.0193" layer="119"/>
<rectangle x1="3.50011875" y1="2.01091875" x2="3.8735" y2="2.0193" layer="119"/>
<rectangle x1="4.10971875" y1="2.01091875" x2="4.4831" y2="2.0193" layer="119"/>
<rectangle x1="5.64388125" y1="2.01091875" x2="6.01471875" y2="2.0193" layer="119"/>
<rectangle x1="6.6421" y1="2.01091875" x2="7.01548125" y2="2.0193" layer="119"/>
<rectangle x1="7.58951875" y1="2.01091875" x2="7.97051875" y2="2.0193" layer="119"/>
<rectangle x1="7.99591875" y1="2.01091875" x2="8.43788125" y2="2.0193" layer="119"/>
<rectangle x1="9.12368125" y1="2.01091875" x2="9.49451875" y2="2.0193" layer="119"/>
<rectangle x1="-0.00508125" y1="2.0193" x2="0.3683" y2="2.02768125" layer="119"/>
<rectangle x1="1.9685" y1="2.0193" x2="2.34188125" y2="2.02768125" layer="119"/>
<rectangle x1="3.50011875" y1="2.0193" x2="3.8735" y2="2.02768125" layer="119"/>
<rectangle x1="4.10971875" y1="2.0193" x2="4.4831" y2="2.02768125" layer="119"/>
<rectangle x1="5.64388125" y1="2.0193" x2="6.01471875" y2="2.02768125" layer="119"/>
<rectangle x1="6.6421" y1="2.0193" x2="7.01548125" y2="2.02768125" layer="119"/>
<rectangle x1="7.58951875" y1="2.0193" x2="7.97051875" y2="2.02768125" layer="119"/>
<rectangle x1="7.9883" y1="2.0193" x2="8.42771875" y2="2.02768125" layer="119"/>
<rectangle x1="9.12368125" y1="2.0193" x2="9.49451875" y2="2.02768125" layer="119"/>
<rectangle x1="-0.00508125" y1="2.02768125" x2="0.3683" y2="2.03631875" layer="119"/>
<rectangle x1="1.9685" y1="2.02768125" x2="2.34188125" y2="2.03631875" layer="119"/>
<rectangle x1="3.50011875" y1="2.02768125" x2="3.8735" y2="2.03631875" layer="119"/>
<rectangle x1="4.10971875" y1="2.02768125" x2="4.4831" y2="2.03631875" layer="119"/>
<rectangle x1="5.64388125" y1="2.02768125" x2="6.01471875" y2="2.03631875" layer="119"/>
<rectangle x1="6.6421" y1="2.02768125" x2="7.01548125" y2="2.03631875" layer="119"/>
<rectangle x1="7.58951875" y1="2.02768125" x2="7.97051875" y2="2.03631875" layer="119"/>
<rectangle x1="7.98068125" y1="2.02768125" x2="8.4201" y2="2.03631875" layer="119"/>
<rectangle x1="9.12368125" y1="2.02768125" x2="9.49451875" y2="2.03631875" layer="119"/>
<rectangle x1="-0.00508125" y1="2.03631875" x2="0.3683" y2="2.0447" layer="119"/>
<rectangle x1="1.9685" y1="2.03631875" x2="2.34188125" y2="2.0447" layer="119"/>
<rectangle x1="3.4925" y1="2.03631875" x2="3.86588125" y2="2.0447" layer="119"/>
<rectangle x1="4.10971875" y1="2.03631875" x2="4.4831" y2="2.0447" layer="119"/>
<rectangle x1="5.64388125" y1="2.03631875" x2="6.01471875" y2="2.0447" layer="119"/>
<rectangle x1="6.6421" y1="2.03631875" x2="7.01548125" y2="2.0447" layer="119"/>
<rectangle x1="7.58951875" y1="2.03631875" x2="8.41248125" y2="2.0447" layer="119"/>
<rectangle x1="9.12368125" y1="2.03631875" x2="9.49451875" y2="2.0447" layer="119"/>
<rectangle x1="-0.00508125" y1="2.0447" x2="0.3683" y2="2.05308125" layer="119"/>
<rectangle x1="1.9685" y1="2.0447" x2="2.34188125" y2="2.05308125" layer="119"/>
<rectangle x1="3.4925" y1="2.0447" x2="3.86588125" y2="2.05308125" layer="119"/>
<rectangle x1="4.10971875" y1="2.0447" x2="4.4831" y2="2.05308125" layer="119"/>
<rectangle x1="5.64388125" y1="2.0447" x2="6.01471875" y2="2.05308125" layer="119"/>
<rectangle x1="6.6421" y1="2.0447" x2="7.01548125" y2="2.05308125" layer="119"/>
<rectangle x1="7.58951875" y1="2.0447" x2="8.40231875" y2="2.05308125" layer="119"/>
<rectangle x1="9.12368125" y1="2.0447" x2="9.49451875" y2="2.05308125" layer="119"/>
<rectangle x1="-0.00508125" y1="2.05308125" x2="0.3683" y2="2.06171875" layer="119"/>
<rectangle x1="1.9685" y1="2.05308125" x2="2.34188125" y2="2.06171875" layer="119"/>
<rectangle x1="3.48488125" y1="2.05308125" x2="3.85571875" y2="2.06171875" layer="119"/>
<rectangle x1="4.10971875" y1="2.05308125" x2="4.4831" y2="2.06171875" layer="119"/>
<rectangle x1="5.64388125" y1="2.05308125" x2="6.01471875" y2="2.06171875" layer="119"/>
<rectangle x1="6.6421" y1="2.05308125" x2="7.01548125" y2="2.06171875" layer="119"/>
<rectangle x1="7.58951875" y1="2.05308125" x2="8.3947" y2="2.06171875" layer="119"/>
<rectangle x1="9.12368125" y1="2.05308125" x2="9.49451875" y2="2.06171875" layer="119"/>
<rectangle x1="-0.00508125" y1="2.06171875" x2="0.3683" y2="2.0701" layer="119"/>
<rectangle x1="1.9685" y1="2.06171875" x2="2.34188125" y2="2.0701" layer="119"/>
<rectangle x1="3.48488125" y1="2.06171875" x2="3.85571875" y2="2.0701" layer="119"/>
<rectangle x1="4.10971875" y1="2.06171875" x2="4.4831" y2="2.0701" layer="119"/>
<rectangle x1="5.64388125" y1="2.06171875" x2="6.01471875" y2="2.0701" layer="119"/>
<rectangle x1="6.6421" y1="2.06171875" x2="7.01548125" y2="2.0701" layer="119"/>
<rectangle x1="7.58951875" y1="2.06171875" x2="8.38708125" y2="2.0701" layer="119"/>
<rectangle x1="9.12368125" y1="2.06171875" x2="9.49451875" y2="2.0701" layer="119"/>
<rectangle x1="-0.00508125" y1="2.0701" x2="0.3683" y2="2.07848125" layer="119"/>
<rectangle x1="1.9685" y1="2.0701" x2="2.34188125" y2="2.07848125" layer="119"/>
<rectangle x1="3.47471875" y1="2.0701" x2="3.8481" y2="2.07848125" layer="119"/>
<rectangle x1="4.10971875" y1="2.0701" x2="4.4831" y2="2.07848125" layer="119"/>
<rectangle x1="5.64388125" y1="2.0701" x2="6.01471875" y2="2.07848125" layer="119"/>
<rectangle x1="6.6421" y1="2.0701" x2="7.01548125" y2="2.07848125" layer="119"/>
<rectangle x1="7.58951875" y1="2.0701" x2="8.37691875" y2="2.07848125" layer="119"/>
<rectangle x1="9.12368125" y1="2.0701" x2="9.49451875" y2="2.07848125" layer="119"/>
<rectangle x1="-0.00508125" y1="2.07848125" x2="0.3683" y2="2.08711875" layer="119"/>
<rectangle x1="1.9685" y1="2.07848125" x2="2.34188125" y2="2.08711875" layer="119"/>
<rectangle x1="3.4671" y1="2.07848125" x2="3.8481" y2="2.08711875" layer="119"/>
<rectangle x1="4.10971875" y1="2.07848125" x2="4.4831" y2="2.08711875" layer="119"/>
<rectangle x1="5.64388125" y1="2.07848125" x2="6.01471875" y2="2.08711875" layer="119"/>
<rectangle x1="6.6421" y1="2.07848125" x2="7.01548125" y2="2.08711875" layer="119"/>
<rectangle x1="7.58951875" y1="2.07848125" x2="8.3693" y2="2.08711875" layer="119"/>
<rectangle x1="9.12368125" y1="2.07848125" x2="9.49451875" y2="2.08711875" layer="119"/>
<rectangle x1="-0.00508125" y1="2.08711875" x2="0.3683" y2="2.0955" layer="119"/>
<rectangle x1="1.9685" y1="2.08711875" x2="2.34188125" y2="2.0955" layer="119"/>
<rectangle x1="3.45948125" y1="2.08711875" x2="3.84048125" y2="2.0955" layer="119"/>
<rectangle x1="4.10971875" y1="2.08711875" x2="4.4831" y2="2.0955" layer="119"/>
<rectangle x1="5.64388125" y1="2.08711875" x2="6.01471875" y2="2.0955" layer="119"/>
<rectangle x1="6.6421" y1="2.08711875" x2="7.01548125" y2="2.0955" layer="119"/>
<rectangle x1="7.58951875" y1="2.08711875" x2="8.35151875" y2="2.0955" layer="119"/>
<rectangle x1="9.12368125" y1="2.08711875" x2="9.49451875" y2="2.0955" layer="119"/>
<rectangle x1="-0.00508125" y1="2.0955" x2="0.3683" y2="2.10388125" layer="119"/>
<rectangle x1="1.9685" y1="2.0955" x2="2.34188125" y2="2.10388125" layer="119"/>
<rectangle x1="3.45948125" y1="2.0955" x2="3.84048125" y2="2.10388125" layer="119"/>
<rectangle x1="4.10971875" y1="2.0955" x2="4.4831" y2="2.10388125" layer="119"/>
<rectangle x1="5.64388125" y1="2.0955" x2="6.01471875" y2="2.10388125" layer="119"/>
<rectangle x1="6.6421" y1="2.0955" x2="7.01548125" y2="2.10388125" layer="119"/>
<rectangle x1="7.58951875" y1="2.0955" x2="8.3439" y2="2.10388125" layer="119"/>
<rectangle x1="9.12368125" y1="2.0955" x2="9.49451875" y2="2.10388125" layer="119"/>
<rectangle x1="-0.00508125" y1="2.10388125" x2="0.3683" y2="2.11251875" layer="119"/>
<rectangle x1="1.9685" y1="2.10388125" x2="2.34188125" y2="2.11251875" layer="119"/>
<rectangle x1="3.44931875" y1="2.10388125" x2="3.84048125" y2="2.11251875" layer="119"/>
<rectangle x1="4.10971875" y1="2.10388125" x2="4.4831" y2="2.11251875" layer="119"/>
<rectangle x1="5.64388125" y1="2.10388125" x2="6.01471875" y2="2.11251875" layer="119"/>
<rectangle x1="6.6421" y1="2.10388125" x2="7.01548125" y2="2.11251875" layer="119"/>
<rectangle x1="7.58951875" y1="2.10388125" x2="8.33628125" y2="2.11251875" layer="119"/>
<rectangle x1="9.12368125" y1="2.10388125" x2="9.49451875" y2="2.11251875" layer="119"/>
<rectangle x1="-0.00508125" y1="2.11251875" x2="0.3683" y2="2.1209" layer="119"/>
<rectangle x1="1.9685" y1="2.11251875" x2="2.34188125" y2="2.1209" layer="119"/>
<rectangle x1="3.4417" y1="2.11251875" x2="3.83031875" y2="2.1209" layer="119"/>
<rectangle x1="4.10971875" y1="2.11251875" x2="4.4831" y2="2.1209" layer="119"/>
<rectangle x1="5.64388125" y1="2.11251875" x2="6.01471875" y2="2.1209" layer="119"/>
<rectangle x1="6.6421" y1="2.11251875" x2="7.01548125" y2="2.1209" layer="119"/>
<rectangle x1="7.58951875" y1="2.11251875" x2="8.32611875" y2="2.1209" layer="119"/>
<rectangle x1="9.12368125" y1="2.11251875" x2="9.49451875" y2="2.1209" layer="119"/>
<rectangle x1="-0.00508125" y1="2.1209" x2="0.3683" y2="2.12928125" layer="119"/>
<rectangle x1="1.9685" y1="2.1209" x2="2.34188125" y2="2.12928125" layer="119"/>
<rectangle x1="3.42391875" y1="2.1209" x2="3.83031875" y2="2.12928125" layer="119"/>
<rectangle x1="4.10971875" y1="2.1209" x2="4.4831" y2="2.12928125" layer="119"/>
<rectangle x1="5.64388125" y1="2.1209" x2="6.01471875" y2="2.12928125" layer="119"/>
<rectangle x1="6.6421" y1="2.1209" x2="7.01548125" y2="2.12928125" layer="119"/>
<rectangle x1="7.58951875" y1="2.1209" x2="8.3185" y2="2.12928125" layer="119"/>
<rectangle x1="9.12368125" y1="2.1209" x2="9.49451875" y2="2.12928125" layer="119"/>
<rectangle x1="-0.00508125" y1="2.12928125" x2="0.3683" y2="2.13791875" layer="119"/>
<rectangle x1="1.9685" y1="2.12928125" x2="2.34188125" y2="2.13791875" layer="119"/>
<rectangle x1="3.4163" y1="2.12928125" x2="3.8227" y2="2.13791875" layer="119"/>
<rectangle x1="4.10971875" y1="2.12928125" x2="4.4831" y2="2.13791875" layer="119"/>
<rectangle x1="5.64388125" y1="2.12928125" x2="6.01471875" y2="2.13791875" layer="119"/>
<rectangle x1="6.6421" y1="2.12928125" x2="7.01548125" y2="2.13791875" layer="119"/>
<rectangle x1="7.58951875" y1="2.12928125" x2="8.31088125" y2="2.13791875" layer="119"/>
<rectangle x1="9.12368125" y1="2.12928125" x2="9.49451875" y2="2.13791875" layer="119"/>
<rectangle x1="-0.00508125" y1="2.13791875" x2="0.3683" y2="2.1463" layer="119"/>
<rectangle x1="1.9685" y1="2.13791875" x2="2.34188125" y2="2.1463" layer="119"/>
<rectangle x1="3.40868125" y1="2.13791875" x2="3.8227" y2="2.1463" layer="119"/>
<rectangle x1="4.10971875" y1="2.13791875" x2="4.4831" y2="2.1463" layer="119"/>
<rectangle x1="5.64388125" y1="2.13791875" x2="6.01471875" y2="2.1463" layer="119"/>
<rectangle x1="6.6421" y1="2.13791875" x2="7.01548125" y2="2.1463" layer="119"/>
<rectangle x1="7.58951875" y1="2.13791875" x2="8.30071875" y2="2.1463" layer="119"/>
<rectangle x1="9.12368125" y1="2.13791875" x2="9.49451875" y2="2.1463" layer="119"/>
<rectangle x1="-0.00508125" y1="2.1463" x2="0.3683" y2="2.15468125" layer="119"/>
<rectangle x1="1.9685" y1="2.1463" x2="2.34188125" y2="2.15468125" layer="119"/>
<rectangle x1="3.39851875" y1="2.1463" x2="3.81508125" y2="2.15468125" layer="119"/>
<rectangle x1="4.10971875" y1="2.1463" x2="4.4831" y2="2.15468125" layer="119"/>
<rectangle x1="5.64388125" y1="2.1463" x2="6.01471875" y2="2.15468125" layer="119"/>
<rectangle x1="6.6421" y1="2.1463" x2="7.01548125" y2="2.15468125" layer="119"/>
<rectangle x1="7.58951875" y1="2.1463" x2="8.2931" y2="2.15468125" layer="119"/>
<rectangle x1="9.12368125" y1="2.1463" x2="9.49451875" y2="2.15468125" layer="119"/>
<rectangle x1="-0.00508125" y1="2.15468125" x2="0.3683" y2="2.16331875" layer="119"/>
<rectangle x1="1.9685" y1="2.15468125" x2="2.34188125" y2="2.16331875" layer="119"/>
<rectangle x1="3.38328125" y1="2.15468125" x2="3.80491875" y2="2.16331875" layer="119"/>
<rectangle x1="4.10971875" y1="2.15468125" x2="4.47548125" y2="2.16331875" layer="119"/>
<rectangle x1="5.64388125" y1="2.15468125" x2="6.01471875" y2="2.16331875" layer="119"/>
<rectangle x1="6.6421" y1="2.15468125" x2="7.01548125" y2="2.16331875" layer="119"/>
<rectangle x1="7.58951875" y1="2.15468125" x2="8.28548125" y2="2.16331875" layer="119"/>
<rectangle x1="9.12368125" y1="2.15468125" x2="9.49451875" y2="2.16331875" layer="119"/>
<rectangle x1="-0.00508125" y1="2.16331875" x2="0.3683" y2="2.1717" layer="119"/>
<rectangle x1="1.9685" y1="2.16331875" x2="2.34188125" y2="2.1717" layer="119"/>
<rectangle x1="3.37311875" y1="2.16331875" x2="3.80491875" y2="2.1717" layer="119"/>
<rectangle x1="4.10971875" y1="2.16331875" x2="4.47548125" y2="2.1717" layer="119"/>
<rectangle x1="5.64388125" y1="2.16331875" x2="6.01471875" y2="2.1717" layer="119"/>
<rectangle x1="6.6421" y1="2.16331875" x2="7.01548125" y2="2.1717" layer="119"/>
<rectangle x1="7.58951875" y1="2.16331875" x2="8.27531875" y2="2.1717" layer="119"/>
<rectangle x1="9.12368125" y1="2.16331875" x2="9.49451875" y2="2.1717" layer="119"/>
<rectangle x1="-0.00508125" y1="2.1717" x2="0.3683" y2="2.18008125" layer="119"/>
<rectangle x1="1.9685" y1="2.1717" x2="2.34188125" y2="2.18008125" layer="119"/>
<rectangle x1="3.35788125" y1="2.1717" x2="3.7973" y2="2.18008125" layer="119"/>
<rectangle x1="4.10971875" y1="2.1717" x2="4.47548125" y2="2.18008125" layer="119"/>
<rectangle x1="5.64388125" y1="2.1717" x2="6.01471875" y2="2.18008125" layer="119"/>
<rectangle x1="6.6421" y1="2.1717" x2="7.01548125" y2="2.18008125" layer="119"/>
<rectangle x1="7.58951875" y1="2.1717" x2="8.2677" y2="2.18008125" layer="119"/>
<rectangle x1="9.12368125" y1="2.1717" x2="9.49451875" y2="2.18008125" layer="119"/>
<rectangle x1="-0.00508125" y1="2.18008125" x2="0.3683" y2="2.18871875" layer="119"/>
<rectangle x1="1.9685" y1="2.18008125" x2="2.34188125" y2="2.18871875" layer="119"/>
<rectangle x1="3.34771875" y1="2.18008125" x2="3.78968125" y2="2.18871875" layer="119"/>
<rectangle x1="4.10971875" y1="2.18008125" x2="4.47548125" y2="2.18871875" layer="119"/>
<rectangle x1="5.64388125" y1="2.18008125" x2="6.01471875" y2="2.18871875" layer="119"/>
<rectangle x1="6.6421" y1="2.18008125" x2="7.01548125" y2="2.18871875" layer="119"/>
<rectangle x1="7.58951875" y1="2.18008125" x2="8.26008125" y2="2.18871875" layer="119"/>
<rectangle x1="9.12368125" y1="2.18008125" x2="9.49451875" y2="2.18871875" layer="119"/>
<rectangle x1="-0.00508125" y1="2.18871875" x2="0.3683" y2="2.1971" layer="119"/>
<rectangle x1="1.9685" y1="2.18871875" x2="2.34188125" y2="2.1971" layer="119"/>
<rectangle x1="3.33248125" y1="2.18871875" x2="3.78968125" y2="2.1971" layer="119"/>
<rectangle x1="4.10971875" y1="2.18871875" x2="4.47548125" y2="2.1971" layer="119"/>
<rectangle x1="5.64388125" y1="2.18871875" x2="6.01471875" y2="2.1971" layer="119"/>
<rectangle x1="6.6421" y1="2.18871875" x2="7.01548125" y2="2.1971" layer="119"/>
<rectangle x1="7.58951875" y1="2.18871875" x2="8.24991875" y2="2.1971" layer="119"/>
<rectangle x1="9.12368125" y1="2.18871875" x2="9.49451875" y2="2.1971" layer="119"/>
<rectangle x1="-0.00508125" y1="2.1971" x2="0.3683" y2="2.20548125" layer="119"/>
<rectangle x1="1.9685" y1="2.1971" x2="2.34188125" y2="2.20548125" layer="119"/>
<rectangle x1="3.3147" y1="2.1971" x2="3.77951875" y2="2.20548125" layer="119"/>
<rectangle x1="4.10971875" y1="2.1971" x2="4.47548125" y2="2.20548125" layer="119"/>
<rectangle x1="5.64388125" y1="2.1971" x2="6.01471875" y2="2.20548125" layer="119"/>
<rectangle x1="6.6421" y1="2.1971" x2="7.01548125" y2="2.20548125" layer="119"/>
<rectangle x1="7.58951875" y1="2.1971" x2="8.2423" y2="2.20548125" layer="119"/>
<rectangle x1="9.12368125" y1="2.1971" x2="9.49451875" y2="2.20548125" layer="119"/>
<rectangle x1="-0.00508125" y1="2.20548125" x2="0.3683" y2="2.21411875" layer="119"/>
<rectangle x1="1.9685" y1="2.20548125" x2="2.34188125" y2="2.21411875" layer="119"/>
<rectangle x1="3.30708125" y1="2.20548125" x2="3.7719" y2="2.21411875" layer="119"/>
<rectangle x1="4.10971875" y1="2.20548125" x2="4.47548125" y2="2.21411875" layer="119"/>
<rectangle x1="5.64388125" y1="2.20548125" x2="6.01471875" y2="2.21411875" layer="119"/>
<rectangle x1="6.6421" y1="2.20548125" x2="7.01548125" y2="2.21411875" layer="119"/>
<rectangle x1="7.58951875" y1="2.20548125" x2="8.23468125" y2="2.21411875" layer="119"/>
<rectangle x1="9.12368125" y1="2.20548125" x2="9.49451875" y2="2.21411875" layer="119"/>
<rectangle x1="-0.00508125" y1="2.21411875" x2="0.3683" y2="2.2225" layer="119"/>
<rectangle x1="1.9685" y1="2.21411875" x2="2.34188125" y2="2.2225" layer="119"/>
<rectangle x1="3.2893" y1="2.21411875" x2="3.76428125" y2="2.2225" layer="119"/>
<rectangle x1="4.10971875" y1="2.21411875" x2="4.47548125" y2="2.2225" layer="119"/>
<rectangle x1="5.64388125" y1="2.21411875" x2="6.01471875" y2="2.2225" layer="119"/>
<rectangle x1="6.6421" y1="2.21411875" x2="7.01548125" y2="2.2225" layer="119"/>
<rectangle x1="7.58951875" y1="2.21411875" x2="8.2169" y2="2.2225" layer="119"/>
<rectangle x1="9.12368125" y1="2.21411875" x2="9.49451875" y2="2.2225" layer="119"/>
<rectangle x1="-0.00508125" y1="2.2225" x2="0.3683" y2="2.23088125" layer="119"/>
<rectangle x1="1.9685" y1="2.2225" x2="2.34188125" y2="2.23088125" layer="119"/>
<rectangle x1="3.27151875" y1="2.2225" x2="3.75411875" y2="2.23088125" layer="119"/>
<rectangle x1="4.10971875" y1="2.2225" x2="4.47548125" y2="2.23088125" layer="119"/>
<rectangle x1="5.64388125" y1="2.2225" x2="6.01471875" y2="2.23088125" layer="119"/>
<rectangle x1="6.6421" y1="2.2225" x2="7.01548125" y2="2.23088125" layer="119"/>
<rectangle x1="7.58951875" y1="2.2225" x2="8.20928125" y2="2.23088125" layer="119"/>
<rectangle x1="9.12368125" y1="2.2225" x2="9.49451875" y2="2.23088125" layer="119"/>
<rectangle x1="-0.00508125" y1="2.23088125" x2="0.3683" y2="2.23951875" layer="119"/>
<rectangle x1="1.9685" y1="2.23088125" x2="2.34188125" y2="2.23951875" layer="119"/>
<rectangle x1="3.25628125" y1="2.23088125" x2="3.7465" y2="2.23951875" layer="119"/>
<rectangle x1="4.10971875" y1="2.23088125" x2="4.47548125" y2="2.23951875" layer="119"/>
<rectangle x1="5.64388125" y1="2.23088125" x2="6.01471875" y2="2.23951875" layer="119"/>
<rectangle x1="6.6421" y1="2.23088125" x2="7.01548125" y2="2.23951875" layer="119"/>
<rectangle x1="7.58951875" y1="2.23088125" x2="8.19911875" y2="2.23951875" layer="119"/>
<rectangle x1="9.12368125" y1="2.23088125" x2="9.49451875" y2="2.23951875" layer="119"/>
<rectangle x1="-0.00508125" y1="2.23951875" x2="0.3683" y2="2.2479" layer="119"/>
<rectangle x1="1.9685" y1="2.23951875" x2="2.34188125" y2="2.2479" layer="119"/>
<rectangle x1="3.2385" y1="2.23951875" x2="3.73888125" y2="2.2479" layer="119"/>
<rectangle x1="4.10971875" y1="2.23951875" x2="4.47548125" y2="2.2479" layer="119"/>
<rectangle x1="5.64388125" y1="2.23951875" x2="6.01471875" y2="2.2479" layer="119"/>
<rectangle x1="6.6421" y1="2.23951875" x2="7.01548125" y2="2.2479" layer="119"/>
<rectangle x1="7.58951875" y1="2.23951875" x2="8.1915" y2="2.2479" layer="119"/>
<rectangle x1="9.12368125" y1="2.23951875" x2="9.49451875" y2="2.2479" layer="119"/>
<rectangle x1="-0.00508125" y1="2.2479" x2="0.3683" y2="2.25628125" layer="119"/>
<rectangle x1="1.9685" y1="2.2479" x2="2.34188125" y2="2.25628125" layer="119"/>
<rectangle x1="3.2131" y1="2.2479" x2="3.72871875" y2="2.25628125" layer="119"/>
<rectangle x1="4.10971875" y1="2.2479" x2="4.47548125" y2="2.25628125" layer="119"/>
<rectangle x1="5.64388125" y1="2.2479" x2="6.01471875" y2="2.25628125" layer="119"/>
<rectangle x1="6.6421" y1="2.2479" x2="7.01548125" y2="2.25628125" layer="119"/>
<rectangle x1="7.58951875" y1="2.2479" x2="8.18388125" y2="2.25628125" layer="119"/>
<rectangle x1="9.12368125" y1="2.2479" x2="9.49451875" y2="2.25628125" layer="119"/>
<rectangle x1="-0.00508125" y1="2.25628125" x2="0.3683" y2="2.26491875" layer="119"/>
<rectangle x1="1.9685" y1="2.25628125" x2="2.34188125" y2="2.26491875" layer="119"/>
<rectangle x1="3.19531875" y1="2.25628125" x2="3.7211" y2="2.26491875" layer="119"/>
<rectangle x1="4.10971875" y1="2.25628125" x2="4.47548125" y2="2.26491875" layer="119"/>
<rectangle x1="5.64388125" y1="2.25628125" x2="6.01471875" y2="2.26491875" layer="119"/>
<rectangle x1="6.6421" y1="2.25628125" x2="7.01548125" y2="2.26491875" layer="119"/>
<rectangle x1="7.58951875" y1="2.25628125" x2="8.17371875" y2="2.26491875" layer="119"/>
<rectangle x1="9.12368125" y1="2.25628125" x2="9.49451875" y2="2.26491875" layer="119"/>
<rectangle x1="-0.00508125" y1="2.26491875" x2="0.3683" y2="2.2733" layer="119"/>
<rectangle x1="1.9685" y1="2.26491875" x2="2.34188125" y2="2.2733" layer="119"/>
<rectangle x1="3.16991875" y1="2.26491875" x2="3.71348125" y2="2.2733" layer="119"/>
<rectangle x1="4.10971875" y1="2.26491875" x2="4.47548125" y2="2.2733" layer="119"/>
<rectangle x1="5.64388125" y1="2.26491875" x2="6.01471875" y2="2.2733" layer="119"/>
<rectangle x1="6.6421" y1="2.26491875" x2="7.01548125" y2="2.2733" layer="119"/>
<rectangle x1="7.58951875" y1="2.26491875" x2="8.1661" y2="2.2733" layer="119"/>
<rectangle x1="9.12368125" y1="2.26491875" x2="9.49451875" y2="2.2733" layer="119"/>
<rectangle x1="-0.00508125" y1="2.2733" x2="0.3683" y2="2.28168125" layer="119"/>
<rectangle x1="1.9685" y1="2.2733" x2="2.34188125" y2="2.28168125" layer="119"/>
<rectangle x1="3.14451875" y1="2.2733" x2="3.70331875" y2="2.28168125" layer="119"/>
<rectangle x1="4.10971875" y1="2.2733" x2="4.47548125" y2="2.28168125" layer="119"/>
<rectangle x1="5.64388125" y1="2.2733" x2="6.01471875" y2="2.28168125" layer="119"/>
<rectangle x1="6.6421" y1="2.2733" x2="7.01548125" y2="2.28168125" layer="119"/>
<rectangle x1="7.58951875" y1="2.2733" x2="8.15848125" y2="2.28168125" layer="119"/>
<rectangle x1="9.12368125" y1="2.2733" x2="9.49451875" y2="2.28168125" layer="119"/>
<rectangle x1="-0.00508125" y1="2.28168125" x2="0.3683" y2="2.29031875" layer="119"/>
<rectangle x1="1.9685" y1="2.28168125" x2="2.34188125" y2="2.29031875" layer="119"/>
<rectangle x1="3.11911875" y1="2.28168125" x2="3.68808125" y2="2.29031875" layer="119"/>
<rectangle x1="4.10971875" y1="2.28168125" x2="4.47548125" y2="2.29031875" layer="119"/>
<rectangle x1="5.64388125" y1="2.28168125" x2="6.01471875" y2="2.29031875" layer="119"/>
<rectangle x1="6.6421" y1="2.28168125" x2="7.01548125" y2="2.29031875" layer="119"/>
<rectangle x1="7.58951875" y1="2.28168125" x2="8.14831875" y2="2.29031875" layer="119"/>
<rectangle x1="9.12368125" y1="2.28168125" x2="9.49451875" y2="2.29031875" layer="119"/>
<rectangle x1="-0.00508125" y1="2.29031875" x2="0.3683" y2="2.2987" layer="119"/>
<rectangle x1="1.9685" y1="2.29031875" x2="2.34188125" y2="2.2987" layer="119"/>
<rectangle x1="3.0861" y1="2.29031875" x2="3.67791875" y2="2.2987" layer="119"/>
<rectangle x1="4.10971875" y1="2.29031875" x2="4.47548125" y2="2.2987" layer="119"/>
<rectangle x1="5.64388125" y1="2.29031875" x2="6.01471875" y2="2.2987" layer="119"/>
<rectangle x1="6.6421" y1="2.29031875" x2="7.01548125" y2="2.2987" layer="119"/>
<rectangle x1="7.58951875" y1="2.29031875" x2="8.1407" y2="2.2987" layer="119"/>
<rectangle x1="9.12368125" y1="2.29031875" x2="9.49451875" y2="2.2987" layer="119"/>
<rectangle x1="-0.00508125" y1="2.2987" x2="0.3683" y2="2.30708125" layer="119"/>
<rectangle x1="1.9685" y1="2.2987" x2="2.34188125" y2="2.30708125" layer="119"/>
<rectangle x1="3.0353" y1="2.2987" x2="3.6703" y2="2.30708125" layer="119"/>
<rectangle x1="4.10971875" y1="2.2987" x2="4.47548125" y2="2.30708125" layer="119"/>
<rectangle x1="5.64388125" y1="2.2987" x2="6.01471875" y2="2.30708125" layer="119"/>
<rectangle x1="6.6421" y1="2.2987" x2="7.01548125" y2="2.30708125" layer="119"/>
<rectangle x1="7.58951875" y1="2.2987" x2="8.13308125" y2="2.30708125" layer="119"/>
<rectangle x1="9.12368125" y1="2.2987" x2="9.49451875" y2="2.30708125" layer="119"/>
<rectangle x1="-0.00508125" y1="2.30708125" x2="1.7653" y2="2.31571875" layer="119"/>
<rectangle x1="1.9685" y1="2.30708125" x2="3.66268125" y2="2.31571875" layer="119"/>
<rectangle x1="4.10971875" y1="2.30708125" x2="4.47548125" y2="2.31571875" layer="119"/>
<rectangle x1="5.64388125" y1="2.30708125" x2="6.01471875" y2="2.31571875" layer="119"/>
<rectangle x1="6.38048125" y1="2.30708125" x2="7.2771" y2="2.31571875" layer="119"/>
<rectangle x1="7.58951875" y1="2.30708125" x2="8.12291875" y2="2.31571875" layer="119"/>
<rectangle x1="9.12368125" y1="2.30708125" x2="9.49451875" y2="2.31571875" layer="119"/>
<rectangle x1="-0.00508125" y1="2.31571875" x2="1.80848125" y2="2.3241" layer="119"/>
<rectangle x1="1.9685" y1="2.31571875" x2="3.6449" y2="2.3241" layer="119"/>
<rectangle x1="4.10971875" y1="2.31571875" x2="4.47548125" y2="2.3241" layer="119"/>
<rectangle x1="5.64388125" y1="2.31571875" x2="6.01471875" y2="2.3241" layer="119"/>
<rectangle x1="6.35508125" y1="2.31571875" x2="7.31011875" y2="2.3241" layer="119"/>
<rectangle x1="7.58951875" y1="2.31571875" x2="8.1153" y2="2.3241" layer="119"/>
<rectangle x1="9.12368125" y1="2.31571875" x2="9.49451875" y2="2.3241" layer="119"/>
<rectangle x1="-0.00508125" y1="2.3241" x2="1.83388125" y2="2.33248125" layer="119"/>
<rectangle x1="1.9685" y1="2.3241" x2="3.63728125" y2="2.33248125" layer="119"/>
<rectangle x1="4.10971875" y1="2.3241" x2="4.47548125" y2="2.33248125" layer="119"/>
<rectangle x1="5.64388125" y1="2.3241" x2="6.01471875" y2="2.33248125" layer="119"/>
<rectangle x1="6.3373" y1="2.3241" x2="7.32028125" y2="2.33248125" layer="119"/>
<rectangle x1="7.58951875" y1="2.3241" x2="8.09751875" y2="2.33248125" layer="119"/>
<rectangle x1="9.12368125" y1="2.3241" x2="9.49451875" y2="2.33248125" layer="119"/>
<rectangle x1="-0.00508125" y1="2.33248125" x2="1.84911875" y2="2.34111875" layer="119"/>
<rectangle x1="1.9685" y1="2.33248125" x2="3.6195" y2="2.34111875" layer="119"/>
<rectangle x1="4.10971875" y1="2.33248125" x2="4.47548125" y2="2.34111875" layer="119"/>
<rectangle x1="5.64388125" y1="2.33248125" x2="6.01471875" y2="2.34111875" layer="119"/>
<rectangle x1="6.31951875" y1="2.33248125" x2="7.33551875" y2="2.34111875" layer="119"/>
<rectangle x1="7.58951875" y1="2.33248125" x2="8.0899" y2="2.34111875" layer="119"/>
<rectangle x1="9.12368125" y1="2.33248125" x2="9.49451875" y2="2.34111875" layer="119"/>
<rectangle x1="-0.00508125" y1="2.34111875" x2="1.85928125" y2="2.3495" layer="119"/>
<rectangle x1="1.9685" y1="2.34111875" x2="3.61188125" y2="2.3495" layer="119"/>
<rectangle x1="4.10971875" y1="2.34111875" x2="4.47548125" y2="2.3495" layer="119"/>
<rectangle x1="5.64388125" y1="2.34111875" x2="6.01471875" y2="2.3495" layer="119"/>
<rectangle x1="6.3119" y1="2.34111875" x2="7.34568125" y2="2.3495" layer="119"/>
<rectangle x1="7.58951875" y1="2.34111875" x2="8.08228125" y2="2.3495" layer="119"/>
<rectangle x1="9.12368125" y1="2.34111875" x2="9.49451875" y2="2.3495" layer="119"/>
<rectangle x1="-0.00508125" y1="2.3495" x2="1.8669" y2="2.35788125" layer="119"/>
<rectangle x1="1.9685" y1="2.3495" x2="3.5941" y2="2.35788125" layer="119"/>
<rectangle x1="4.10971875" y1="2.3495" x2="4.47548125" y2="2.35788125" layer="119"/>
<rectangle x1="5.64388125" y1="2.3495" x2="6.01471875" y2="2.35788125" layer="119"/>
<rectangle x1="6.30428125" y1="2.3495" x2="7.3533" y2="2.35788125" layer="119"/>
<rectangle x1="7.58951875" y1="2.3495" x2="8.07211875" y2="2.35788125" layer="119"/>
<rectangle x1="9.12368125" y1="2.3495" x2="9.49451875" y2="2.35788125" layer="119"/>
<rectangle x1="-0.00508125" y1="2.35788125" x2="1.87451875" y2="2.36651875" layer="119"/>
<rectangle x1="1.9685" y1="2.35788125" x2="3.57631875" y2="2.36651875" layer="119"/>
<rectangle x1="4.10971875" y1="2.35788125" x2="4.47548125" y2="2.36651875" layer="119"/>
<rectangle x1="5.64388125" y1="2.35788125" x2="6.01471875" y2="2.36651875" layer="119"/>
<rectangle x1="6.29411875" y1="2.35788125" x2="7.36091875" y2="2.36651875" layer="119"/>
<rectangle x1="7.58951875" y1="2.35788125" x2="8.0645" y2="2.36651875" layer="119"/>
<rectangle x1="9.12368125" y1="2.35788125" x2="9.49451875" y2="2.36651875" layer="119"/>
<rectangle x1="-0.00508125" y1="2.36651875" x2="1.88468125" y2="2.3749" layer="119"/>
<rectangle x1="1.9685" y1="2.36651875" x2="3.5687" y2="2.3749" layer="119"/>
<rectangle x1="4.10971875" y1="2.36651875" x2="4.47548125" y2="2.3749" layer="119"/>
<rectangle x1="5.64388125" y1="2.36651875" x2="6.01471875" y2="2.3749" layer="119"/>
<rectangle x1="6.2865" y1="2.36651875" x2="7.37108125" y2="2.3749" layer="119"/>
<rectangle x1="7.58951875" y1="2.36651875" x2="8.05688125" y2="2.3749" layer="119"/>
<rectangle x1="9.12368125" y1="2.36651875" x2="9.49451875" y2="2.3749" layer="119"/>
<rectangle x1="-0.00508125" y1="2.3749" x2="1.88468125" y2="2.38328125" layer="119"/>
<rectangle x1="1.9685" y1="2.3749" x2="3.55091875" y2="2.38328125" layer="119"/>
<rectangle x1="4.10971875" y1="2.3749" x2="4.47548125" y2="2.38328125" layer="119"/>
<rectangle x1="5.64388125" y1="2.3749" x2="6.01471875" y2="2.38328125" layer="119"/>
<rectangle x1="6.27888125" y1="2.3749" x2="7.3787" y2="2.38328125" layer="119"/>
<rectangle x1="7.58951875" y1="2.3749" x2="8.04671875" y2="2.38328125" layer="119"/>
<rectangle x1="9.12368125" y1="2.3749" x2="9.49451875" y2="2.38328125" layer="119"/>
<rectangle x1="-0.00508125" y1="2.38328125" x2="1.8923" y2="2.39191875" layer="119"/>
<rectangle x1="1.9685" y1="2.38328125" x2="3.53568125" y2="2.39191875" layer="119"/>
<rectangle x1="4.10971875" y1="2.38328125" x2="4.47548125" y2="2.39191875" layer="119"/>
<rectangle x1="5.64388125" y1="2.38328125" x2="6.01471875" y2="2.39191875" layer="119"/>
<rectangle x1="6.26871875" y1="2.38328125" x2="7.38631875" y2="2.39191875" layer="119"/>
<rectangle x1="7.58951875" y1="2.38328125" x2="8.0391" y2="2.39191875" layer="119"/>
<rectangle x1="9.12368125" y1="2.38328125" x2="9.49451875" y2="2.39191875" layer="119"/>
<rectangle x1="-0.00508125" y1="2.39191875" x2="1.8923" y2="2.4003" layer="119"/>
<rectangle x1="1.9685" y1="2.39191875" x2="3.5179" y2="2.4003" layer="119"/>
<rectangle x1="4.10971875" y1="2.39191875" x2="4.47548125" y2="2.4003" layer="119"/>
<rectangle x1="5.64388125" y1="2.39191875" x2="6.01471875" y2="2.4003" layer="119"/>
<rectangle x1="6.2611" y1="2.39191875" x2="7.39648125" y2="2.4003" layer="119"/>
<rectangle x1="7.58951875" y1="2.39191875" x2="8.03148125" y2="2.4003" layer="119"/>
<rectangle x1="9.12368125" y1="2.39191875" x2="9.49451875" y2="2.4003" layer="119"/>
<rectangle x1="-0.00508125" y1="2.4003" x2="1.8923" y2="2.40868125" layer="119"/>
<rectangle x1="1.9685" y1="2.4003" x2="3.51028125" y2="2.40868125" layer="119"/>
<rectangle x1="4.10971875" y1="2.4003" x2="4.47548125" y2="2.40868125" layer="119"/>
<rectangle x1="5.64388125" y1="2.4003" x2="6.01471875" y2="2.40868125" layer="119"/>
<rectangle x1="6.2611" y1="2.4003" x2="7.39648125" y2="2.40868125" layer="119"/>
<rectangle x1="7.58951875" y1="2.4003" x2="8.02131875" y2="2.40868125" layer="119"/>
<rectangle x1="9.12368125" y1="2.4003" x2="9.49451875" y2="2.40868125" layer="119"/>
<rectangle x1="-0.00508125" y1="2.40868125" x2="1.8923" y2="2.41731875" layer="119"/>
<rectangle x1="1.9685" y1="2.40868125" x2="3.4925" y2="2.41731875" layer="119"/>
<rectangle x1="4.10971875" y1="2.40868125" x2="4.47548125" y2="2.41731875" layer="119"/>
<rectangle x1="5.64388125" y1="2.40868125" x2="6.01471875" y2="2.41731875" layer="119"/>
<rectangle x1="6.2611" y1="2.40868125" x2="7.4041" y2="2.41731875" layer="119"/>
<rectangle x1="7.58951875" y1="2.40868125" x2="8.0137" y2="2.41731875" layer="119"/>
<rectangle x1="9.12368125" y1="2.40868125" x2="9.49451875" y2="2.41731875" layer="119"/>
<rectangle x1="-0.00508125" y1="2.41731875" x2="1.8923" y2="2.4257" layer="119"/>
<rectangle x1="1.9685" y1="2.41731875" x2="3.47471875" y2="2.4257" layer="119"/>
<rectangle x1="4.10971875" y1="2.41731875" x2="4.47548125" y2="2.4257" layer="119"/>
<rectangle x1="5.64388125" y1="2.41731875" x2="6.01471875" y2="2.4257" layer="119"/>
<rectangle x1="6.2611" y1="2.41731875" x2="7.4041" y2="2.4257" layer="119"/>
<rectangle x1="7.58951875" y1="2.41731875" x2="8.00608125" y2="2.4257" layer="119"/>
<rectangle x1="9.12368125" y1="2.41731875" x2="9.49451875" y2="2.4257" layer="119"/>
<rectangle x1="-0.00508125" y1="2.4257" x2="1.8923" y2="2.43408125" layer="119"/>
<rectangle x1="1.9685" y1="2.4257" x2="3.45948125" y2="2.43408125" layer="119"/>
<rectangle x1="4.10971875" y1="2.4257" x2="4.47548125" y2="2.43408125" layer="119"/>
<rectangle x1="5.64388125" y1="2.4257" x2="6.01471875" y2="2.43408125" layer="119"/>
<rectangle x1="6.2611" y1="2.4257" x2="7.39648125" y2="2.43408125" layer="119"/>
<rectangle x1="7.58951875" y1="2.4257" x2="7.99591875" y2="2.43408125" layer="119"/>
<rectangle x1="9.12368125" y1="2.4257" x2="9.49451875" y2="2.43408125" layer="119"/>
<rectangle x1="-0.00508125" y1="2.43408125" x2="1.8923" y2="2.44271875" layer="119"/>
<rectangle x1="1.9685" y1="2.43408125" x2="3.4417" y2="2.44271875" layer="119"/>
<rectangle x1="4.10971875" y1="2.43408125" x2="4.47548125" y2="2.44271875" layer="119"/>
<rectangle x1="5.64388125" y1="2.43408125" x2="6.01471875" y2="2.44271875" layer="119"/>
<rectangle x1="6.2611" y1="2.43408125" x2="7.39648125" y2="2.44271875" layer="119"/>
<rectangle x1="7.58951875" y1="2.43408125" x2="7.9883" y2="2.44271875" layer="119"/>
<rectangle x1="9.12368125" y1="2.43408125" x2="9.49451875" y2="2.44271875" layer="119"/>
<rectangle x1="-0.00508125" y1="2.44271875" x2="1.8923" y2="2.4511" layer="119"/>
<rectangle x1="1.9685" y1="2.44271875" x2="3.4163" y2="2.4511" layer="119"/>
<rectangle x1="4.10971875" y1="2.44271875" x2="4.4831" y2="2.4511" layer="119"/>
<rectangle x1="5.64388125" y1="2.44271875" x2="6.01471875" y2="2.4511" layer="119"/>
<rectangle x1="6.2611" y1="2.44271875" x2="7.39648125" y2="2.4511" layer="119"/>
<rectangle x1="7.58951875" y1="2.44271875" x2="7.98068125" y2="2.4511" layer="119"/>
<rectangle x1="9.12368125" y1="2.44271875" x2="9.49451875" y2="2.4511" layer="119"/>
<rectangle x1="-0.00508125" y1="2.4511" x2="1.8923" y2="2.45948125" layer="119"/>
<rectangle x1="1.9685" y1="2.4511" x2="3.39851875" y2="2.45948125" layer="119"/>
<rectangle x1="4.10971875" y1="2.4511" x2="4.47548125" y2="2.45948125" layer="119"/>
<rectangle x1="5.64388125" y1="2.4511" x2="6.01471875" y2="2.45948125" layer="119"/>
<rectangle x1="6.26871875" y1="2.4511" x2="7.39648125" y2="2.45948125" layer="119"/>
<rectangle x1="7.58951875" y1="2.4511" x2="7.9629" y2="2.45948125" layer="119"/>
<rectangle x1="9.1313" y1="2.4511" x2="9.49451875" y2="2.45948125" layer="119"/>
<rectangle x1="-0.00508125" y1="2.45948125" x2="1.88468125" y2="2.46811875" layer="119"/>
<rectangle x1="1.9685" y1="2.45948125" x2="3.38328125" y2="2.46811875" layer="119"/>
<rectangle x1="4.10971875" y1="2.45948125" x2="4.47548125" y2="2.46811875" layer="119"/>
<rectangle x1="5.6515" y1="2.45948125" x2="6.0071" y2="2.46811875" layer="119"/>
<rectangle x1="6.26871875" y1="2.45948125" x2="7.39648125" y2="2.46811875" layer="119"/>
<rectangle x1="7.59968125" y1="2.45948125" x2="7.95528125" y2="2.46811875" layer="119"/>
<rectangle x1="9.1313" y1="2.45948125" x2="9.49451875" y2="2.46811875" layer="119"/>
<rectangle x1="0.00508125" y1="2.46811875" x2="1.88468125" y2="2.4765" layer="119"/>
<rectangle x1="1.97611875" y1="2.46811875" x2="3.35788125" y2="2.4765" layer="119"/>
<rectangle x1="4.11988125" y1="2.46811875" x2="4.46531875" y2="2.4765" layer="119"/>
<rectangle x1="5.6515" y1="2.46811875" x2="6.0071" y2="2.4765" layer="119"/>
<rectangle x1="6.27888125" y1="2.46811875" x2="7.38631875" y2="2.4765" layer="119"/>
<rectangle x1="7.59968125" y1="2.46811875" x2="7.94511875" y2="2.4765" layer="119"/>
<rectangle x1="9.13891875" y1="2.46811875" x2="9.4869" y2="2.4765" layer="119"/>
<rectangle x1="0.0127" y1="2.4765" x2="1.87451875" y2="2.48488125" layer="119"/>
<rectangle x1="1.98628125" y1="2.4765" x2="3.33248125" y2="2.48488125" layer="119"/>
<rectangle x1="4.1275" y1="2.4765" x2="4.4577" y2="2.48488125" layer="119"/>
<rectangle x1="5.65911875" y1="2.4765" x2="5.99948125" y2="2.48488125" layer="119"/>
<rectangle x1="6.27888125" y1="2.4765" x2="7.3787" y2="2.48488125" layer="119"/>
<rectangle x1="7.6073" y1="2.4765" x2="7.9375" y2="2.48488125" layer="119"/>
<rectangle x1="9.14908125" y1="2.4765" x2="9.47928125" y2="2.48488125" layer="119"/>
<rectangle x1="0.02031875" y1="2.48488125" x2="1.8669" y2="2.49351875" layer="119"/>
<rectangle x1="1.9939" y1="2.48488125" x2="3.30708125" y2="2.49351875" layer="119"/>
<rectangle x1="4.13511875" y1="2.48488125" x2="4.45008125" y2="2.49351875" layer="119"/>
<rectangle x1="5.66928125" y1="2.48488125" x2="5.98931875" y2="2.49351875" layer="119"/>
<rectangle x1="6.2865" y1="2.48488125" x2="7.36091875" y2="2.49351875" layer="119"/>
<rectangle x1="7.61491875" y1="2.48488125" x2="7.92988125" y2="2.49351875" layer="119"/>
<rectangle x1="9.1567" y1="2.48488125" x2="9.46911875" y2="2.49351875" layer="119"/>
<rectangle x1="0.0381" y1="2.49351875" x2="1.85928125" y2="2.5019" layer="119"/>
<rectangle x1="2.01168125" y1="2.49351875" x2="3.28168125" y2="2.5019" layer="119"/>
<rectangle x1="4.14528125" y1="2.49351875" x2="4.4323" y2="2.5019" layer="119"/>
<rectangle x1="5.6769" y1="2.49351875" x2="5.97408125" y2="2.5019" layer="119"/>
<rectangle x1="6.29411875" y1="2.49351875" x2="7.3533" y2="2.5019" layer="119"/>
<rectangle x1="7.6327" y1="2.49351875" x2="7.91971875" y2="2.5019" layer="119"/>
<rectangle x1="9.16431875" y1="2.49351875" x2="9.4615" y2="2.5019" layer="119"/>
<rectangle x1="0.05588125" y1="2.5019" x2="1.84911875" y2="2.51028125" layer="119"/>
<rectangle x1="2.02691875" y1="2.5019" x2="3.25628125" y2="2.51028125" layer="119"/>
<rectangle x1="4.16051875" y1="2.5019" x2="4.42468125" y2="2.51028125" layer="119"/>
<rectangle x1="5.69468125" y1="2.5019" x2="5.96391875" y2="2.51028125" layer="119"/>
<rectangle x1="6.30428125" y1="2.5019" x2="7.33551875" y2="2.51028125" layer="119"/>
<rectangle x1="7.64031875" y1="2.5019" x2="7.90448125" y2="2.51028125" layer="119"/>
<rectangle x1="9.1821" y1="2.5019" x2="9.45388125" y2="2.51028125" layer="119"/>
<rectangle x1="0.07111875" y1="2.51028125" x2="1.83388125" y2="2.51891875" layer="119"/>
<rectangle x1="2.0447" y1="2.51028125" x2="3.22071875" y2="2.51891875" layer="119"/>
<rectangle x1="4.18591875" y1="2.51028125" x2="4.4069" y2="2.51891875" layer="119"/>
<rectangle x1="5.70991875" y1="2.51028125" x2="5.93851875" y2="2.51891875" layer="119"/>
<rectangle x1="6.31951875" y1="2.51028125" x2="7.32028125" y2="2.51891875" layer="119"/>
<rectangle x1="7.6581" y1="2.51028125" x2="7.8867" y2="2.51891875" layer="119"/>
<rectangle x1="9.19988125" y1="2.51028125" x2="9.42848125" y2="2.51891875" layer="119"/>
<rectangle x1="0.0889" y1="2.51891875" x2="1.8161" y2="2.5273" layer="119"/>
<rectangle x1="2.06248125" y1="2.51891875" x2="3.18008125" y2="2.5273" layer="119"/>
<rectangle x1="4.2037" y1="2.51891875" x2="4.3815" y2="2.5273" layer="119"/>
<rectangle x1="5.7277" y1="2.51891875" x2="5.91311875" y2="2.5273" layer="119"/>
<rectangle x1="6.3373" y1="2.51891875" x2="7.3025" y2="2.5273" layer="119"/>
<rectangle x1="7.6835" y1="2.51891875" x2="7.8613" y2="2.5273" layer="119"/>
<rectangle x1="9.22528125" y1="2.51891875" x2="9.4107" y2="2.5273" layer="119"/>
<rectangle x1="0.1143" y1="2.5273" x2="1.7907" y2="2.53568125" layer="119"/>
<rectangle x1="2.08788125" y1="2.5273" x2="3.12928125" y2="2.53568125" layer="119"/>
<rectangle x1="4.2291" y1="2.5273" x2="4.3561" y2="2.53568125" layer="119"/>
<rectangle x1="5.76071875" y1="2.5273" x2="5.88771875" y2="2.53568125" layer="119"/>
<rectangle x1="6.3627" y1="2.5273" x2="7.2771" y2="2.53568125" layer="119"/>
<rectangle x1="7.7343" y1="2.5273" x2="7.8105" y2="2.53568125" layer="119"/>
<rectangle x1="9.27608125" y1="2.5273" x2="9.3599" y2="2.53568125" layer="119"/>
<rectangle x1="5.81151875" y1="2.53568125" x2="5.83691875" y2="2.54431875" layer="119"/>
<rectangle x1="0.02031875" y1="2.71348125" x2="0.45211875" y2="2.72211875" layer="119"/>
<rectangle x1="0.51308125" y1="2.71348125" x2="0.5461" y2="2.72211875" layer="119"/>
<rectangle x1="0.61468125" y1="2.71348125" x2="0.6477" y2="2.72211875" layer="119"/>
<rectangle x1="0.7239" y1="2.71348125" x2="0.75691875" y2="2.72211875" layer="119"/>
<rectangle x1="0.83311875" y1="2.71348125" x2="0.86868125" y2="2.72211875" layer="119"/>
<rectangle x1="0.96011875" y1="2.71348125" x2="0.99568125" y2="2.72211875" layer="119"/>
<rectangle x1="1.0795" y1="2.71348125" x2="1.11251875" y2="2.72211875" layer="119"/>
<rectangle x1="1.1811" y1="2.71348125" x2="1.83388125" y2="2.72211875" layer="119"/>
<rectangle x1="1.9685" y1="2.71348125" x2="2.78891875" y2="2.72211875" layer="119"/>
<rectangle x1="2.83971875" y1="2.71348125" x2="2.87528125" y2="2.72211875" layer="119"/>
<rectangle x1="2.92608125" y1="2.71348125" x2="3.68808125" y2="2.72211875" layer="119"/>
<rectangle x1="3.7719" y1="2.71348125" x2="4.4323" y2="2.72211875" layer="119"/>
<rectangle x1="5.10031875" y1="2.71348125" x2="5.72008125" y2="2.72211875" layer="119"/>
<rectangle x1="5.81151875" y1="2.71348125" x2="6.57351875" y2="2.72211875" layer="119"/>
<rectangle x1="6.6167" y1="2.71348125" x2="6.64971875" y2="2.72211875" layer="119"/>
<rectangle x1="6.70051875" y1="2.71348125" x2="7.52348125" y2="2.72211875" layer="119"/>
<rectangle x1="7.6581" y1="2.71348125" x2="8.30071875" y2="2.72211875" layer="119"/>
<rectangle x1="8.37691875" y1="2.71348125" x2="8.41248125" y2="2.72211875" layer="119"/>
<rectangle x1="8.4963" y1="2.71348125" x2="8.52931875" y2="2.72211875" layer="119"/>
<rectangle x1="8.61568125" y1="2.71348125" x2="8.6487" y2="2.72211875" layer="119"/>
<rectangle x1="8.73251875" y1="2.71348125" x2="8.76808125" y2="2.72211875" layer="119"/>
<rectangle x1="8.83411875" y1="2.71348125" x2="8.86968125" y2="2.72211875" layer="119"/>
<rectangle x1="8.93571875" y1="2.71348125" x2="8.97128125" y2="2.72211875" layer="119"/>
<rectangle x1="9.03731875" y1="2.71348125" x2="9.49451875" y2="2.72211875" layer="119"/>
<rectangle x1="0.02031875" y1="2.72211875" x2="0.45211875" y2="2.7305" layer="119"/>
<rectangle x1="0.51308125" y1="2.72211875" x2="0.5461" y2="2.7305" layer="119"/>
<rectangle x1="0.61468125" y1="2.72211875" x2="0.6477" y2="2.7305" layer="119"/>
<rectangle x1="0.7239" y1="2.72211875" x2="0.75691875" y2="2.7305" layer="119"/>
<rectangle x1="0.83311875" y1="2.72211875" x2="0.86868125" y2="2.7305" layer="119"/>
<rectangle x1="0.96011875" y1="2.72211875" x2="0.99568125" y2="2.7305" layer="119"/>
<rectangle x1="1.0795" y1="2.72211875" x2="1.11251875" y2="2.7305" layer="119"/>
<rectangle x1="1.1811" y1="2.72211875" x2="1.83388125" y2="2.7305" layer="119"/>
<rectangle x1="1.9685" y1="2.72211875" x2="2.78891875" y2="2.7305" layer="119"/>
<rectangle x1="2.83971875" y1="2.72211875" x2="2.87528125" y2="2.7305" layer="119"/>
<rectangle x1="2.92608125" y1="2.72211875" x2="3.68808125" y2="2.7305" layer="119"/>
<rectangle x1="3.7719" y1="2.72211875" x2="4.4323" y2="2.7305" layer="119"/>
<rectangle x1="5.10031875" y1="2.72211875" x2="5.72008125" y2="2.7305" layer="119"/>
<rectangle x1="5.81151875" y1="2.72211875" x2="6.57351875" y2="2.7305" layer="119"/>
<rectangle x1="6.6167" y1="2.72211875" x2="6.64971875" y2="2.7305" layer="119"/>
<rectangle x1="6.70051875" y1="2.72211875" x2="7.52348125" y2="2.7305" layer="119"/>
<rectangle x1="7.6581" y1="2.72211875" x2="8.30071875" y2="2.7305" layer="119"/>
<rectangle x1="8.37691875" y1="2.72211875" x2="8.41248125" y2="2.7305" layer="119"/>
<rectangle x1="8.4963" y1="2.72211875" x2="8.52931875" y2="2.7305" layer="119"/>
<rectangle x1="8.61568125" y1="2.72211875" x2="8.6487" y2="2.7305" layer="119"/>
<rectangle x1="8.73251875" y1="2.72211875" x2="8.76808125" y2="2.7305" layer="119"/>
<rectangle x1="8.83411875" y1="2.72211875" x2="8.86968125" y2="2.7305" layer="119"/>
<rectangle x1="8.93571875" y1="2.72211875" x2="8.97128125" y2="2.7305" layer="119"/>
<rectangle x1="9.03731875" y1="2.72211875" x2="9.49451875" y2="2.7305" layer="119"/>
<rectangle x1="0.02031875" y1="2.7305" x2="0.45211875" y2="2.73888125" layer="119"/>
<rectangle x1="0.51308125" y1="2.7305" x2="0.5461" y2="2.73888125" layer="119"/>
<rectangle x1="0.61468125" y1="2.7305" x2="0.6477" y2="2.73888125" layer="119"/>
<rectangle x1="0.7239" y1="2.7305" x2="0.75691875" y2="2.73888125" layer="119"/>
<rectangle x1="0.83311875" y1="2.7305" x2="0.86868125" y2="2.73888125" layer="119"/>
<rectangle x1="0.96011875" y1="2.7305" x2="0.99568125" y2="2.73888125" layer="119"/>
<rectangle x1="1.0795" y1="2.7305" x2="1.11251875" y2="2.73888125" layer="119"/>
<rectangle x1="1.1811" y1="2.7305" x2="1.83388125" y2="2.73888125" layer="119"/>
<rectangle x1="1.9685" y1="2.7305" x2="2.78891875" y2="2.73888125" layer="119"/>
<rectangle x1="2.83971875" y1="2.7305" x2="2.87528125" y2="2.73888125" layer="119"/>
<rectangle x1="2.92608125" y1="2.7305" x2="3.68808125" y2="2.73888125" layer="119"/>
<rectangle x1="3.7719" y1="2.7305" x2="4.4323" y2="2.73888125" layer="119"/>
<rectangle x1="5.10031875" y1="2.7305" x2="5.72008125" y2="2.73888125" layer="119"/>
<rectangle x1="5.81151875" y1="2.7305" x2="6.57351875" y2="2.73888125" layer="119"/>
<rectangle x1="6.6167" y1="2.7305" x2="6.64971875" y2="2.73888125" layer="119"/>
<rectangle x1="6.70051875" y1="2.7305" x2="7.52348125" y2="2.73888125" layer="119"/>
<rectangle x1="7.6581" y1="2.7305" x2="8.30071875" y2="2.73888125" layer="119"/>
<rectangle x1="8.37691875" y1="2.7305" x2="8.41248125" y2="2.73888125" layer="119"/>
<rectangle x1="8.4963" y1="2.7305" x2="8.52931875" y2="2.73888125" layer="119"/>
<rectangle x1="8.61568125" y1="2.7305" x2="8.6487" y2="2.73888125" layer="119"/>
<rectangle x1="8.73251875" y1="2.7305" x2="8.76808125" y2="2.73888125" layer="119"/>
<rectangle x1="8.83411875" y1="2.7305" x2="8.86968125" y2="2.73888125" layer="119"/>
<rectangle x1="8.93571875" y1="2.7305" x2="8.97128125" y2="2.73888125" layer="119"/>
<rectangle x1="9.03731875" y1="2.7305" x2="9.49451875" y2="2.73888125" layer="119"/>
<rectangle x1="0.02031875" y1="2.73888125" x2="0.45211875" y2="2.74751875" layer="119"/>
<rectangle x1="0.51308125" y1="2.73888125" x2="0.5461" y2="2.74751875" layer="119"/>
<rectangle x1="0.61468125" y1="2.73888125" x2="0.6477" y2="2.74751875" layer="119"/>
<rectangle x1="0.7239" y1="2.73888125" x2="0.75691875" y2="2.74751875" layer="119"/>
<rectangle x1="0.83311875" y1="2.73888125" x2="0.86868125" y2="2.74751875" layer="119"/>
<rectangle x1="0.96011875" y1="2.73888125" x2="0.99568125" y2="2.74751875" layer="119"/>
<rectangle x1="1.0795" y1="2.73888125" x2="1.11251875" y2="2.74751875" layer="119"/>
<rectangle x1="1.1811" y1="2.73888125" x2="1.83388125" y2="2.74751875" layer="119"/>
<rectangle x1="1.9685" y1="2.73888125" x2="2.78891875" y2="2.74751875" layer="119"/>
<rectangle x1="2.83971875" y1="2.73888125" x2="2.87528125" y2="2.74751875" layer="119"/>
<rectangle x1="2.92608125" y1="2.73888125" x2="3.68808125" y2="2.74751875" layer="119"/>
<rectangle x1="3.7719" y1="2.73888125" x2="4.4323" y2="2.74751875" layer="119"/>
<rectangle x1="5.10031875" y1="2.73888125" x2="5.72008125" y2="2.74751875" layer="119"/>
<rectangle x1="5.81151875" y1="2.73888125" x2="6.57351875" y2="2.74751875" layer="119"/>
<rectangle x1="6.6167" y1="2.73888125" x2="6.64971875" y2="2.74751875" layer="119"/>
<rectangle x1="6.70051875" y1="2.73888125" x2="7.52348125" y2="2.74751875" layer="119"/>
<rectangle x1="7.6581" y1="2.73888125" x2="8.30071875" y2="2.74751875" layer="119"/>
<rectangle x1="8.37691875" y1="2.73888125" x2="8.41248125" y2="2.74751875" layer="119"/>
<rectangle x1="8.4963" y1="2.73888125" x2="8.52931875" y2="2.74751875" layer="119"/>
<rectangle x1="8.61568125" y1="2.73888125" x2="8.6487" y2="2.74751875" layer="119"/>
<rectangle x1="8.73251875" y1="2.73888125" x2="8.76808125" y2="2.74751875" layer="119"/>
<rectangle x1="8.83411875" y1="2.73888125" x2="8.86968125" y2="2.74751875" layer="119"/>
<rectangle x1="8.93571875" y1="2.73888125" x2="8.97128125" y2="2.74751875" layer="119"/>
<rectangle x1="9.03731875" y1="2.73888125" x2="9.49451875" y2="2.74751875" layer="119"/>
<rectangle x1="0.02031875" y1="2.74751875" x2="0.45211875" y2="2.7559" layer="119"/>
<rectangle x1="0.51308125" y1="2.74751875" x2="0.5461" y2="2.7559" layer="119"/>
<rectangle x1="0.61468125" y1="2.74751875" x2="0.6477" y2="2.7559" layer="119"/>
<rectangle x1="0.7239" y1="2.74751875" x2="0.75691875" y2="2.7559" layer="119"/>
<rectangle x1="0.83311875" y1="2.74751875" x2="0.86868125" y2="2.7559" layer="119"/>
<rectangle x1="0.96011875" y1="2.74751875" x2="0.99568125" y2="2.7559" layer="119"/>
<rectangle x1="1.0795" y1="2.74751875" x2="1.11251875" y2="2.7559" layer="119"/>
<rectangle x1="1.1811" y1="2.74751875" x2="1.83388125" y2="2.7559" layer="119"/>
<rectangle x1="1.9685" y1="2.74751875" x2="2.78891875" y2="2.7559" layer="119"/>
<rectangle x1="2.83971875" y1="2.74751875" x2="2.87528125" y2="2.7559" layer="119"/>
<rectangle x1="2.92608125" y1="2.74751875" x2="3.68808125" y2="2.7559" layer="119"/>
<rectangle x1="3.7719" y1="2.74751875" x2="4.4323" y2="2.7559" layer="119"/>
<rectangle x1="5.10031875" y1="2.74751875" x2="5.72008125" y2="2.7559" layer="119"/>
<rectangle x1="5.81151875" y1="2.74751875" x2="6.57351875" y2="2.7559" layer="119"/>
<rectangle x1="6.6167" y1="2.74751875" x2="6.64971875" y2="2.7559" layer="119"/>
<rectangle x1="6.70051875" y1="2.74751875" x2="7.52348125" y2="2.7559" layer="119"/>
<rectangle x1="7.6581" y1="2.74751875" x2="8.30071875" y2="2.7559" layer="119"/>
<rectangle x1="8.37691875" y1="2.74751875" x2="8.41248125" y2="2.7559" layer="119"/>
<rectangle x1="8.4963" y1="2.74751875" x2="8.52931875" y2="2.7559" layer="119"/>
<rectangle x1="8.61568125" y1="2.74751875" x2="8.6487" y2="2.7559" layer="119"/>
<rectangle x1="8.73251875" y1="2.74751875" x2="8.76808125" y2="2.7559" layer="119"/>
<rectangle x1="8.83411875" y1="2.74751875" x2="8.86968125" y2="2.7559" layer="119"/>
<rectangle x1="8.93571875" y1="2.74751875" x2="8.97128125" y2="2.7559" layer="119"/>
<rectangle x1="9.03731875" y1="2.74751875" x2="9.49451875" y2="2.7559" layer="119"/>
<rectangle x1="0.02031875" y1="2.7559" x2="0.45211875" y2="2.76428125" layer="119"/>
<rectangle x1="0.51308125" y1="2.7559" x2="0.5461" y2="2.76428125" layer="119"/>
<rectangle x1="0.61468125" y1="2.7559" x2="0.6477" y2="2.76428125" layer="119"/>
<rectangle x1="0.7239" y1="2.7559" x2="0.75691875" y2="2.76428125" layer="119"/>
<rectangle x1="0.83311875" y1="2.7559" x2="0.86868125" y2="2.76428125" layer="119"/>
<rectangle x1="0.96011875" y1="2.7559" x2="0.99568125" y2="2.76428125" layer="119"/>
<rectangle x1="1.0795" y1="2.7559" x2="1.11251875" y2="2.76428125" layer="119"/>
<rectangle x1="1.1811" y1="2.7559" x2="1.83388125" y2="2.76428125" layer="119"/>
<rectangle x1="1.9685" y1="2.7559" x2="2.78891875" y2="2.76428125" layer="119"/>
<rectangle x1="2.83971875" y1="2.7559" x2="2.87528125" y2="2.76428125" layer="119"/>
<rectangle x1="2.92608125" y1="2.7559" x2="3.68808125" y2="2.76428125" layer="119"/>
<rectangle x1="3.7719" y1="2.7559" x2="4.4323" y2="2.76428125" layer="119"/>
<rectangle x1="5.10031875" y1="2.7559" x2="5.72008125" y2="2.76428125" layer="119"/>
<rectangle x1="5.81151875" y1="2.7559" x2="6.57351875" y2="2.76428125" layer="119"/>
<rectangle x1="6.6167" y1="2.7559" x2="6.64971875" y2="2.76428125" layer="119"/>
<rectangle x1="6.70051875" y1="2.7559" x2="7.52348125" y2="2.76428125" layer="119"/>
<rectangle x1="7.6581" y1="2.7559" x2="8.30071875" y2="2.76428125" layer="119"/>
<rectangle x1="8.37691875" y1="2.7559" x2="8.41248125" y2="2.76428125" layer="119"/>
<rectangle x1="8.4963" y1="2.7559" x2="8.52931875" y2="2.76428125" layer="119"/>
<rectangle x1="8.61568125" y1="2.7559" x2="8.6487" y2="2.76428125" layer="119"/>
<rectangle x1="8.73251875" y1="2.7559" x2="8.76808125" y2="2.76428125" layer="119"/>
<rectangle x1="8.83411875" y1="2.7559" x2="8.86968125" y2="2.76428125" layer="119"/>
<rectangle x1="8.93571875" y1="2.7559" x2="8.97128125" y2="2.76428125" layer="119"/>
<rectangle x1="9.03731875" y1="2.7559" x2="9.49451875" y2="2.76428125" layer="119"/>
<rectangle x1="0.02031875" y1="2.76428125" x2="0.45211875" y2="2.77291875" layer="119"/>
<rectangle x1="0.51308125" y1="2.76428125" x2="0.5461" y2="2.77291875" layer="119"/>
<rectangle x1="0.61468125" y1="2.76428125" x2="0.6477" y2="2.77291875" layer="119"/>
<rectangle x1="0.7239" y1="2.76428125" x2="0.75691875" y2="2.77291875" layer="119"/>
<rectangle x1="0.83311875" y1="2.76428125" x2="0.86868125" y2="2.77291875" layer="119"/>
<rectangle x1="0.96011875" y1="2.76428125" x2="0.99568125" y2="2.77291875" layer="119"/>
<rectangle x1="1.0795" y1="2.76428125" x2="1.11251875" y2="2.77291875" layer="119"/>
<rectangle x1="1.1811" y1="2.76428125" x2="1.83388125" y2="2.77291875" layer="119"/>
<rectangle x1="1.9685" y1="2.76428125" x2="2.78891875" y2="2.77291875" layer="119"/>
<rectangle x1="2.83971875" y1="2.76428125" x2="2.87528125" y2="2.77291875" layer="119"/>
<rectangle x1="2.92608125" y1="2.76428125" x2="3.68808125" y2="2.77291875" layer="119"/>
<rectangle x1="3.7719" y1="2.76428125" x2="4.4323" y2="2.77291875" layer="119"/>
<rectangle x1="5.10031875" y1="2.76428125" x2="5.72008125" y2="2.77291875" layer="119"/>
<rectangle x1="5.81151875" y1="2.76428125" x2="6.57351875" y2="2.77291875" layer="119"/>
<rectangle x1="6.6167" y1="2.76428125" x2="6.64971875" y2="2.77291875" layer="119"/>
<rectangle x1="6.70051875" y1="2.76428125" x2="7.52348125" y2="2.77291875" layer="119"/>
<rectangle x1="7.6581" y1="2.76428125" x2="8.30071875" y2="2.77291875" layer="119"/>
<rectangle x1="8.37691875" y1="2.76428125" x2="8.41248125" y2="2.77291875" layer="119"/>
<rectangle x1="8.4963" y1="2.76428125" x2="8.52931875" y2="2.77291875" layer="119"/>
<rectangle x1="8.61568125" y1="2.76428125" x2="8.6487" y2="2.77291875" layer="119"/>
<rectangle x1="8.73251875" y1="2.76428125" x2="8.76808125" y2="2.77291875" layer="119"/>
<rectangle x1="8.83411875" y1="2.76428125" x2="8.86968125" y2="2.77291875" layer="119"/>
<rectangle x1="8.93571875" y1="2.76428125" x2="8.97128125" y2="2.77291875" layer="119"/>
<rectangle x1="9.03731875" y1="2.76428125" x2="9.49451875" y2="2.77291875" layer="119"/>
<rectangle x1="0.02031875" y1="2.77291875" x2="0.45211875" y2="2.7813" layer="119"/>
<rectangle x1="0.51308125" y1="2.77291875" x2="0.5461" y2="2.7813" layer="119"/>
<rectangle x1="0.61468125" y1="2.77291875" x2="0.6477" y2="2.7813" layer="119"/>
<rectangle x1="0.7239" y1="2.77291875" x2="0.75691875" y2="2.7813" layer="119"/>
<rectangle x1="0.83311875" y1="2.77291875" x2="0.86868125" y2="2.7813" layer="119"/>
<rectangle x1="0.96011875" y1="2.77291875" x2="0.99568125" y2="2.7813" layer="119"/>
<rectangle x1="1.0795" y1="2.77291875" x2="1.11251875" y2="2.7813" layer="119"/>
<rectangle x1="1.1811" y1="2.77291875" x2="1.83388125" y2="2.7813" layer="119"/>
<rectangle x1="1.9685" y1="2.77291875" x2="2.78891875" y2="2.7813" layer="119"/>
<rectangle x1="2.83971875" y1="2.77291875" x2="2.87528125" y2="2.7813" layer="119"/>
<rectangle x1="2.92608125" y1="2.77291875" x2="3.68808125" y2="2.7813" layer="119"/>
<rectangle x1="3.7719" y1="2.77291875" x2="4.4323" y2="2.7813" layer="119"/>
<rectangle x1="5.10031875" y1="2.77291875" x2="5.72008125" y2="2.7813" layer="119"/>
<rectangle x1="5.81151875" y1="2.77291875" x2="6.57351875" y2="2.7813" layer="119"/>
<rectangle x1="6.6167" y1="2.77291875" x2="6.64971875" y2="2.7813" layer="119"/>
<rectangle x1="6.70051875" y1="2.77291875" x2="7.52348125" y2="2.7813" layer="119"/>
<rectangle x1="7.6581" y1="2.77291875" x2="8.30071875" y2="2.7813" layer="119"/>
<rectangle x1="8.37691875" y1="2.77291875" x2="8.41248125" y2="2.7813" layer="119"/>
<rectangle x1="8.4963" y1="2.77291875" x2="8.52931875" y2="2.7813" layer="119"/>
<rectangle x1="8.61568125" y1="2.77291875" x2="8.6487" y2="2.7813" layer="119"/>
<rectangle x1="8.73251875" y1="2.77291875" x2="8.76808125" y2="2.7813" layer="119"/>
<rectangle x1="8.83411875" y1="2.77291875" x2="8.86968125" y2="2.7813" layer="119"/>
<rectangle x1="8.93571875" y1="2.77291875" x2="8.97128125" y2="2.7813" layer="119"/>
<rectangle x1="9.03731875" y1="2.77291875" x2="9.49451875" y2="2.7813" layer="119"/>
<rectangle x1="0.02031875" y1="2.7813" x2="0.45211875" y2="2.78968125" layer="119"/>
<rectangle x1="0.51308125" y1="2.7813" x2="0.5461" y2="2.78968125" layer="119"/>
<rectangle x1="0.61468125" y1="2.7813" x2="0.6477" y2="2.78968125" layer="119"/>
<rectangle x1="0.7239" y1="2.7813" x2="0.75691875" y2="2.78968125" layer="119"/>
<rectangle x1="0.83311875" y1="2.7813" x2="0.86868125" y2="2.78968125" layer="119"/>
<rectangle x1="0.96011875" y1="2.7813" x2="0.99568125" y2="2.78968125" layer="119"/>
<rectangle x1="1.0795" y1="2.7813" x2="1.11251875" y2="2.78968125" layer="119"/>
<rectangle x1="1.1811" y1="2.7813" x2="1.83388125" y2="2.78968125" layer="119"/>
<rectangle x1="1.9685" y1="2.7813" x2="2.78891875" y2="2.78968125" layer="119"/>
<rectangle x1="2.83971875" y1="2.7813" x2="2.87528125" y2="2.78968125" layer="119"/>
<rectangle x1="2.92608125" y1="2.7813" x2="3.68808125" y2="2.78968125" layer="119"/>
<rectangle x1="3.7719" y1="2.7813" x2="4.4323" y2="2.78968125" layer="119"/>
<rectangle x1="5.10031875" y1="2.7813" x2="5.72008125" y2="2.78968125" layer="119"/>
<rectangle x1="5.81151875" y1="2.7813" x2="6.57351875" y2="2.78968125" layer="119"/>
<rectangle x1="6.6167" y1="2.7813" x2="6.64971875" y2="2.78968125" layer="119"/>
<rectangle x1="6.70051875" y1="2.7813" x2="7.52348125" y2="2.78968125" layer="119"/>
<rectangle x1="7.6581" y1="2.7813" x2="8.30071875" y2="2.78968125" layer="119"/>
<rectangle x1="8.37691875" y1="2.7813" x2="8.41248125" y2="2.78968125" layer="119"/>
<rectangle x1="8.4963" y1="2.7813" x2="8.52931875" y2="2.78968125" layer="119"/>
<rectangle x1="8.61568125" y1="2.7813" x2="8.6487" y2="2.78968125" layer="119"/>
<rectangle x1="8.73251875" y1="2.7813" x2="8.76808125" y2="2.78968125" layer="119"/>
<rectangle x1="8.83411875" y1="2.7813" x2="8.86968125" y2="2.78968125" layer="119"/>
<rectangle x1="8.93571875" y1="2.7813" x2="8.97128125" y2="2.78968125" layer="119"/>
<rectangle x1="9.03731875" y1="2.7813" x2="9.49451875" y2="2.78968125" layer="119"/>
<rectangle x1="0.02031875" y1="2.78968125" x2="0.45211875" y2="2.79831875" layer="119"/>
<rectangle x1="0.51308125" y1="2.78968125" x2="0.5461" y2="2.79831875" layer="119"/>
<rectangle x1="0.61468125" y1="2.78968125" x2="0.6477" y2="2.79831875" layer="119"/>
<rectangle x1="0.7239" y1="2.78968125" x2="0.75691875" y2="2.79831875" layer="119"/>
<rectangle x1="0.83311875" y1="2.78968125" x2="0.86868125" y2="2.79831875" layer="119"/>
<rectangle x1="0.96011875" y1="2.78968125" x2="0.99568125" y2="2.79831875" layer="119"/>
<rectangle x1="1.0795" y1="2.78968125" x2="1.11251875" y2="2.79831875" layer="119"/>
<rectangle x1="1.1811" y1="2.78968125" x2="1.83388125" y2="2.79831875" layer="119"/>
<rectangle x1="1.9685" y1="2.78968125" x2="2.78891875" y2="2.79831875" layer="119"/>
<rectangle x1="2.83971875" y1="2.78968125" x2="2.87528125" y2="2.79831875" layer="119"/>
<rectangle x1="2.92608125" y1="2.78968125" x2="3.68808125" y2="2.79831875" layer="119"/>
<rectangle x1="3.7719" y1="2.78968125" x2="4.4323" y2="2.79831875" layer="119"/>
<rectangle x1="5.10031875" y1="2.78968125" x2="5.72008125" y2="2.79831875" layer="119"/>
<rectangle x1="5.81151875" y1="2.78968125" x2="6.57351875" y2="2.79831875" layer="119"/>
<rectangle x1="6.6167" y1="2.78968125" x2="6.64971875" y2="2.79831875" layer="119"/>
<rectangle x1="6.70051875" y1="2.78968125" x2="7.52348125" y2="2.79831875" layer="119"/>
<rectangle x1="7.6581" y1="2.78968125" x2="8.30071875" y2="2.79831875" layer="119"/>
<rectangle x1="8.37691875" y1="2.78968125" x2="8.41248125" y2="2.79831875" layer="119"/>
<rectangle x1="8.4963" y1="2.78968125" x2="8.52931875" y2="2.79831875" layer="119"/>
<rectangle x1="8.61568125" y1="2.78968125" x2="8.6487" y2="2.79831875" layer="119"/>
<rectangle x1="8.73251875" y1="2.78968125" x2="8.76808125" y2="2.79831875" layer="119"/>
<rectangle x1="8.83411875" y1="2.78968125" x2="8.86968125" y2="2.79831875" layer="119"/>
<rectangle x1="8.93571875" y1="2.78968125" x2="8.97128125" y2="2.79831875" layer="119"/>
<rectangle x1="9.03731875" y1="2.78968125" x2="9.49451875" y2="2.79831875" layer="119"/>
<rectangle x1="0.02031875" y1="2.79831875" x2="0.45211875" y2="2.8067" layer="119"/>
<rectangle x1="0.51308125" y1="2.79831875" x2="0.5461" y2="2.8067" layer="119"/>
<rectangle x1="0.61468125" y1="2.79831875" x2="0.6477" y2="2.8067" layer="119"/>
<rectangle x1="0.7239" y1="2.79831875" x2="0.75691875" y2="2.8067" layer="119"/>
<rectangle x1="0.83311875" y1="2.79831875" x2="0.86868125" y2="2.8067" layer="119"/>
<rectangle x1="0.96011875" y1="2.79831875" x2="0.99568125" y2="2.8067" layer="119"/>
<rectangle x1="1.0795" y1="2.79831875" x2="1.11251875" y2="2.8067" layer="119"/>
<rectangle x1="1.1811" y1="2.79831875" x2="1.83388125" y2="2.8067" layer="119"/>
<rectangle x1="1.9685" y1="2.79831875" x2="2.78891875" y2="2.8067" layer="119"/>
<rectangle x1="2.83971875" y1="2.79831875" x2="2.87528125" y2="2.8067" layer="119"/>
<rectangle x1="2.92608125" y1="2.79831875" x2="3.68808125" y2="2.8067" layer="119"/>
<rectangle x1="3.7719" y1="2.79831875" x2="4.4323" y2="2.8067" layer="119"/>
<rectangle x1="5.10031875" y1="2.79831875" x2="5.72008125" y2="2.8067" layer="119"/>
<rectangle x1="5.81151875" y1="2.79831875" x2="6.57351875" y2="2.8067" layer="119"/>
<rectangle x1="6.6167" y1="2.79831875" x2="6.64971875" y2="2.8067" layer="119"/>
<rectangle x1="6.70051875" y1="2.79831875" x2="7.52348125" y2="2.8067" layer="119"/>
<rectangle x1="7.6581" y1="2.79831875" x2="8.30071875" y2="2.8067" layer="119"/>
<rectangle x1="8.37691875" y1="2.79831875" x2="8.41248125" y2="2.8067" layer="119"/>
<rectangle x1="8.4963" y1="2.79831875" x2="8.52931875" y2="2.8067" layer="119"/>
<rectangle x1="8.61568125" y1="2.79831875" x2="8.6487" y2="2.8067" layer="119"/>
<rectangle x1="8.73251875" y1="2.79831875" x2="8.76808125" y2="2.8067" layer="119"/>
<rectangle x1="8.83411875" y1="2.79831875" x2="8.86968125" y2="2.8067" layer="119"/>
<rectangle x1="8.93571875" y1="2.79831875" x2="8.97128125" y2="2.8067" layer="119"/>
<rectangle x1="9.03731875" y1="2.79831875" x2="9.49451875" y2="2.8067" layer="119"/>
<rectangle x1="0.41148125" y1="2.8067" x2="0.45211875" y2="2.81508125" layer="119"/>
<rectangle x1="0.51308125" y1="2.8067" x2="0.5461" y2="2.81508125" layer="119"/>
<rectangle x1="0.61468125" y1="2.8067" x2="0.6477" y2="2.81508125" layer="119"/>
<rectangle x1="0.7239" y1="2.8067" x2="0.75691875" y2="2.81508125" layer="119"/>
<rectangle x1="0.83311875" y1="2.8067" x2="0.86868125" y2="2.81508125" layer="119"/>
<rectangle x1="0.96011875" y1="2.8067" x2="0.99568125" y2="2.81508125" layer="119"/>
<rectangle x1="1.0795" y1="2.8067" x2="1.11251875" y2="2.81508125" layer="119"/>
<rectangle x1="1.1811" y1="2.8067" x2="1.21411875" y2="2.81508125" layer="119"/>
<rectangle x1="1.79831875" y1="2.8067" x2="1.83388125" y2="2.81508125" layer="119"/>
<rectangle x1="1.9685" y1="2.8067" x2="2.00151875" y2="2.81508125" layer="119"/>
<rectangle x1="2.7559" y1="2.8067" x2="2.78891875" y2="2.81508125" layer="119"/>
<rectangle x1="2.83971875" y1="2.8067" x2="2.87528125" y2="2.81508125" layer="119"/>
<rectangle x1="2.92608125" y1="2.8067" x2="2.9591" y2="2.81508125" layer="119"/>
<rectangle x1="3.65251875" y1="2.8067" x2="3.68808125" y2="2.81508125" layer="119"/>
<rectangle x1="3.7719" y1="2.8067" x2="3.80491875" y2="2.81508125" layer="119"/>
<rectangle x1="4.4069" y1="2.8067" x2="4.4323" y2="2.81508125" layer="119"/>
<rectangle x1="5.10031875" y1="2.8067" x2="5.13588125" y2="2.81508125" layer="119"/>
<rectangle x1="5.68451875" y1="2.8067" x2="5.72008125" y2="2.81508125" layer="119"/>
<rectangle x1="5.81151875" y1="2.8067" x2="5.84708125" y2="2.81508125" layer="119"/>
<rectangle x1="6.5405" y1="2.8067" x2="6.57351875" y2="2.81508125" layer="119"/>
<rectangle x1="6.6167" y1="2.8067" x2="6.64971875" y2="2.81508125" layer="119"/>
<rectangle x1="6.70051875" y1="2.8067" x2="6.73608125" y2="2.81508125" layer="119"/>
<rectangle x1="7.48791875" y1="2.8067" x2="7.52348125" y2="2.81508125" layer="119"/>
<rectangle x1="7.6581" y1="2.8067" x2="7.69111875" y2="2.81508125" layer="119"/>
<rectangle x1="8.2677" y1="2.8067" x2="8.30071875" y2="2.81508125" layer="119"/>
<rectangle x1="8.37691875" y1="2.8067" x2="8.41248125" y2="2.81508125" layer="119"/>
<rectangle x1="8.4963" y1="2.8067" x2="8.52931875" y2="2.81508125" layer="119"/>
<rectangle x1="8.61568125" y1="2.8067" x2="8.6487" y2="2.81508125" layer="119"/>
<rectangle x1="8.73251875" y1="2.8067" x2="8.76808125" y2="2.81508125" layer="119"/>
<rectangle x1="8.83411875" y1="2.8067" x2="8.86968125" y2="2.81508125" layer="119"/>
<rectangle x1="8.93571875" y1="2.8067" x2="8.97128125" y2="2.81508125" layer="119"/>
<rectangle x1="9.03731875" y1="2.8067" x2="9.07288125" y2="2.81508125" layer="119"/>
<rectangle x1="0.4191" y1="2.81508125" x2="0.45211875" y2="2.82371875" layer="119"/>
<rectangle x1="0.51308125" y1="2.81508125" x2="0.5461" y2="2.82371875" layer="119"/>
<rectangle x1="0.61468125" y1="2.81508125" x2="0.6477" y2="2.82371875" layer="119"/>
<rectangle x1="0.7239" y1="2.81508125" x2="0.75691875" y2="2.82371875" layer="119"/>
<rectangle x1="0.83311875" y1="2.81508125" x2="0.86868125" y2="2.82371875" layer="119"/>
<rectangle x1="0.96011875" y1="2.81508125" x2="0.99568125" y2="2.82371875" layer="119"/>
<rectangle x1="1.0795" y1="2.81508125" x2="1.11251875" y2="2.82371875" layer="119"/>
<rectangle x1="1.1811" y1="2.81508125" x2="1.21411875" y2="2.82371875" layer="119"/>
<rectangle x1="1.79831875" y1="2.81508125" x2="1.83388125" y2="2.82371875" layer="119"/>
<rectangle x1="1.9685" y1="2.81508125" x2="2.00151875" y2="2.82371875" layer="119"/>
<rectangle x1="2.7559" y1="2.81508125" x2="2.78891875" y2="2.82371875" layer="119"/>
<rectangle x1="2.83971875" y1="2.81508125" x2="2.87528125" y2="2.82371875" layer="119"/>
<rectangle x1="2.92608125" y1="2.81508125" x2="2.9591" y2="2.82371875" layer="119"/>
<rectangle x1="3.65251875" y1="2.81508125" x2="3.68808125" y2="2.82371875" layer="119"/>
<rectangle x1="3.7719" y1="2.81508125" x2="3.80491875" y2="2.82371875" layer="119"/>
<rectangle x1="4.4069" y1="2.81508125" x2="4.4323" y2="2.82371875" layer="119"/>
<rectangle x1="5.10031875" y1="2.81508125" x2="5.13588125" y2="2.82371875" layer="119"/>
<rectangle x1="5.68451875" y1="2.81508125" x2="5.72008125" y2="2.82371875" layer="119"/>
<rectangle x1="5.81151875" y1="2.81508125" x2="5.84708125" y2="2.82371875" layer="119"/>
<rectangle x1="6.5405" y1="2.81508125" x2="6.57351875" y2="2.82371875" layer="119"/>
<rectangle x1="6.6167" y1="2.81508125" x2="6.64971875" y2="2.82371875" layer="119"/>
<rectangle x1="6.70051875" y1="2.81508125" x2="6.73608125" y2="2.82371875" layer="119"/>
<rectangle x1="7.48791875" y1="2.81508125" x2="7.52348125" y2="2.82371875" layer="119"/>
<rectangle x1="7.6581" y1="2.81508125" x2="7.69111875" y2="2.82371875" layer="119"/>
<rectangle x1="8.2677" y1="2.81508125" x2="8.30071875" y2="2.82371875" layer="119"/>
<rectangle x1="8.37691875" y1="2.81508125" x2="8.41248125" y2="2.82371875" layer="119"/>
<rectangle x1="8.4963" y1="2.81508125" x2="8.52931875" y2="2.82371875" layer="119"/>
<rectangle x1="8.61568125" y1="2.81508125" x2="8.6487" y2="2.82371875" layer="119"/>
<rectangle x1="8.73251875" y1="2.81508125" x2="8.76808125" y2="2.82371875" layer="119"/>
<rectangle x1="8.83411875" y1="2.81508125" x2="8.86968125" y2="2.82371875" layer="119"/>
<rectangle x1="8.93571875" y1="2.81508125" x2="8.97128125" y2="2.82371875" layer="119"/>
<rectangle x1="9.03731875" y1="2.81508125" x2="9.07288125" y2="2.82371875" layer="119"/>
<rectangle x1="0.4191" y1="2.82371875" x2="0.45211875" y2="2.8321" layer="119"/>
<rectangle x1="0.51308125" y1="2.82371875" x2="0.5461" y2="2.8321" layer="119"/>
<rectangle x1="0.61468125" y1="2.82371875" x2="0.6477" y2="2.8321" layer="119"/>
<rectangle x1="0.7239" y1="2.82371875" x2="0.75691875" y2="2.8321" layer="119"/>
<rectangle x1="0.83311875" y1="2.82371875" x2="0.86868125" y2="2.8321" layer="119"/>
<rectangle x1="0.96011875" y1="2.82371875" x2="0.99568125" y2="2.8321" layer="119"/>
<rectangle x1="1.0795" y1="2.82371875" x2="1.11251875" y2="2.8321" layer="119"/>
<rectangle x1="1.1811" y1="2.82371875" x2="1.21411875" y2="2.8321" layer="119"/>
<rectangle x1="1.79831875" y1="2.82371875" x2="1.83388125" y2="2.8321" layer="119"/>
<rectangle x1="1.9685" y1="2.82371875" x2="2.00151875" y2="2.8321" layer="119"/>
<rectangle x1="2.7559" y1="2.82371875" x2="2.78891875" y2="2.8321" layer="119"/>
<rectangle x1="2.83971875" y1="2.82371875" x2="2.87528125" y2="2.8321" layer="119"/>
<rectangle x1="2.92608125" y1="2.82371875" x2="2.9591" y2="2.8321" layer="119"/>
<rectangle x1="3.65251875" y1="2.82371875" x2="3.68808125" y2="2.8321" layer="119"/>
<rectangle x1="3.7719" y1="2.82371875" x2="3.80491875" y2="2.8321" layer="119"/>
<rectangle x1="4.4069" y1="2.82371875" x2="4.4323" y2="2.8321" layer="119"/>
<rectangle x1="5.10031875" y1="2.82371875" x2="5.13588125" y2="2.8321" layer="119"/>
<rectangle x1="5.68451875" y1="2.82371875" x2="5.72008125" y2="2.8321" layer="119"/>
<rectangle x1="5.81151875" y1="2.82371875" x2="5.84708125" y2="2.8321" layer="119"/>
<rectangle x1="6.5405" y1="2.82371875" x2="6.57351875" y2="2.8321" layer="119"/>
<rectangle x1="6.6167" y1="2.82371875" x2="6.64971875" y2="2.8321" layer="119"/>
<rectangle x1="6.70051875" y1="2.82371875" x2="6.73608125" y2="2.8321" layer="119"/>
<rectangle x1="7.48791875" y1="2.82371875" x2="7.52348125" y2="2.8321" layer="119"/>
<rectangle x1="7.6581" y1="2.82371875" x2="7.69111875" y2="2.8321" layer="119"/>
<rectangle x1="8.2677" y1="2.82371875" x2="8.30071875" y2="2.8321" layer="119"/>
<rectangle x1="8.37691875" y1="2.82371875" x2="8.41248125" y2="2.8321" layer="119"/>
<rectangle x1="8.4963" y1="2.82371875" x2="8.52931875" y2="2.8321" layer="119"/>
<rectangle x1="8.61568125" y1="2.82371875" x2="8.6487" y2="2.8321" layer="119"/>
<rectangle x1="8.73251875" y1="2.82371875" x2="8.76808125" y2="2.8321" layer="119"/>
<rectangle x1="8.83411875" y1="2.82371875" x2="8.86968125" y2="2.8321" layer="119"/>
<rectangle x1="8.93571875" y1="2.82371875" x2="8.97128125" y2="2.8321" layer="119"/>
<rectangle x1="9.03731875" y1="2.82371875" x2="9.07288125" y2="2.8321" layer="119"/>
<rectangle x1="0.4191" y1="2.8321" x2="0.45211875" y2="2.84048125" layer="119"/>
<rectangle x1="0.51308125" y1="2.8321" x2="0.5461" y2="2.84048125" layer="119"/>
<rectangle x1="0.61468125" y1="2.8321" x2="0.6477" y2="2.84048125" layer="119"/>
<rectangle x1="0.7239" y1="2.8321" x2="0.75691875" y2="2.84048125" layer="119"/>
<rectangle x1="0.83311875" y1="2.8321" x2="0.86868125" y2="2.84048125" layer="119"/>
<rectangle x1="0.96011875" y1="2.8321" x2="0.99568125" y2="2.84048125" layer="119"/>
<rectangle x1="1.0795" y1="2.8321" x2="1.11251875" y2="2.84048125" layer="119"/>
<rectangle x1="1.1811" y1="2.8321" x2="1.21411875" y2="2.84048125" layer="119"/>
<rectangle x1="1.79831875" y1="2.8321" x2="1.83388125" y2="2.84048125" layer="119"/>
<rectangle x1="1.9685" y1="2.8321" x2="2.00151875" y2="2.84048125" layer="119"/>
<rectangle x1="2.7559" y1="2.8321" x2="2.78891875" y2="2.84048125" layer="119"/>
<rectangle x1="2.83971875" y1="2.8321" x2="2.87528125" y2="2.84048125" layer="119"/>
<rectangle x1="2.92608125" y1="2.8321" x2="2.9591" y2="2.84048125" layer="119"/>
<rectangle x1="3.65251875" y1="2.8321" x2="3.68808125" y2="2.84048125" layer="119"/>
<rectangle x1="3.7719" y1="2.8321" x2="3.80491875" y2="2.84048125" layer="119"/>
<rectangle x1="4.4069" y1="2.8321" x2="4.4323" y2="2.84048125" layer="119"/>
<rectangle x1="5.10031875" y1="2.8321" x2="5.13588125" y2="2.84048125" layer="119"/>
<rectangle x1="5.68451875" y1="2.8321" x2="5.72008125" y2="2.84048125" layer="119"/>
<rectangle x1="5.81151875" y1="2.8321" x2="5.84708125" y2="2.84048125" layer="119"/>
<rectangle x1="6.5405" y1="2.8321" x2="6.57351875" y2="2.84048125" layer="119"/>
<rectangle x1="6.6167" y1="2.8321" x2="6.64971875" y2="2.84048125" layer="119"/>
<rectangle x1="6.70051875" y1="2.8321" x2="6.73608125" y2="2.84048125" layer="119"/>
<rectangle x1="7.48791875" y1="2.8321" x2="7.52348125" y2="2.84048125" layer="119"/>
<rectangle x1="7.6581" y1="2.8321" x2="7.69111875" y2="2.84048125" layer="119"/>
<rectangle x1="8.2677" y1="2.8321" x2="8.30071875" y2="2.84048125" layer="119"/>
<rectangle x1="8.37691875" y1="2.8321" x2="8.41248125" y2="2.84048125" layer="119"/>
<rectangle x1="8.4963" y1="2.8321" x2="8.52931875" y2="2.84048125" layer="119"/>
<rectangle x1="8.61568125" y1="2.8321" x2="8.6487" y2="2.84048125" layer="119"/>
<rectangle x1="8.73251875" y1="2.8321" x2="8.76808125" y2="2.84048125" layer="119"/>
<rectangle x1="8.83411875" y1="2.8321" x2="8.86968125" y2="2.84048125" layer="119"/>
<rectangle x1="8.93571875" y1="2.8321" x2="8.97128125" y2="2.84048125" layer="119"/>
<rectangle x1="9.03731875" y1="2.8321" x2="9.07288125" y2="2.84048125" layer="119"/>
<rectangle x1="0.4191" y1="2.84048125" x2="0.45211875" y2="2.84911875" layer="119"/>
<rectangle x1="0.51308125" y1="2.84048125" x2="0.5461" y2="2.84911875" layer="119"/>
<rectangle x1="0.61468125" y1="2.84048125" x2="0.6477" y2="2.84911875" layer="119"/>
<rectangle x1="0.7239" y1="2.84048125" x2="0.75691875" y2="2.84911875" layer="119"/>
<rectangle x1="0.83311875" y1="2.84048125" x2="0.86868125" y2="2.84911875" layer="119"/>
<rectangle x1="0.96011875" y1="2.84048125" x2="0.99568125" y2="2.84911875" layer="119"/>
<rectangle x1="1.0795" y1="2.84048125" x2="1.11251875" y2="2.84911875" layer="119"/>
<rectangle x1="1.1811" y1="2.84048125" x2="1.21411875" y2="2.84911875" layer="119"/>
<rectangle x1="1.79831875" y1="2.84048125" x2="1.83388125" y2="2.84911875" layer="119"/>
<rectangle x1="1.9685" y1="2.84048125" x2="2.00151875" y2="2.84911875" layer="119"/>
<rectangle x1="2.7559" y1="2.84048125" x2="2.78891875" y2="2.84911875" layer="119"/>
<rectangle x1="2.83971875" y1="2.84048125" x2="2.87528125" y2="2.84911875" layer="119"/>
<rectangle x1="2.92608125" y1="2.84048125" x2="2.9591" y2="2.84911875" layer="119"/>
<rectangle x1="3.65251875" y1="2.84048125" x2="3.68808125" y2="2.84911875" layer="119"/>
<rectangle x1="3.7719" y1="2.84048125" x2="3.80491875" y2="2.84911875" layer="119"/>
<rectangle x1="4.4069" y1="2.84048125" x2="4.4323" y2="2.84911875" layer="119"/>
<rectangle x1="5.10031875" y1="2.84048125" x2="5.13588125" y2="2.84911875" layer="119"/>
<rectangle x1="5.68451875" y1="2.84048125" x2="5.72008125" y2="2.84911875" layer="119"/>
<rectangle x1="5.81151875" y1="2.84048125" x2="5.84708125" y2="2.84911875" layer="119"/>
<rectangle x1="6.5405" y1="2.84048125" x2="6.57351875" y2="2.84911875" layer="119"/>
<rectangle x1="6.6167" y1="2.84048125" x2="6.64971875" y2="2.84911875" layer="119"/>
<rectangle x1="6.70051875" y1="2.84048125" x2="6.73608125" y2="2.84911875" layer="119"/>
<rectangle x1="7.48791875" y1="2.84048125" x2="7.52348125" y2="2.84911875" layer="119"/>
<rectangle x1="7.6581" y1="2.84048125" x2="7.69111875" y2="2.84911875" layer="119"/>
<rectangle x1="8.2677" y1="2.84048125" x2="8.30071875" y2="2.84911875" layer="119"/>
<rectangle x1="8.37691875" y1="2.84048125" x2="8.41248125" y2="2.84911875" layer="119"/>
<rectangle x1="8.4963" y1="2.84048125" x2="8.52931875" y2="2.84911875" layer="119"/>
<rectangle x1="8.61568125" y1="2.84048125" x2="8.6487" y2="2.84911875" layer="119"/>
<rectangle x1="8.73251875" y1="2.84048125" x2="8.76808125" y2="2.84911875" layer="119"/>
<rectangle x1="8.83411875" y1="2.84048125" x2="8.86968125" y2="2.84911875" layer="119"/>
<rectangle x1="8.93571875" y1="2.84048125" x2="8.97128125" y2="2.84911875" layer="119"/>
<rectangle x1="9.03731875" y1="2.84048125" x2="9.07288125" y2="2.84911875" layer="119"/>
<rectangle x1="0.4191" y1="2.84911875" x2="0.45211875" y2="2.8575" layer="119"/>
<rectangle x1="0.51308125" y1="2.84911875" x2="0.5461" y2="2.8575" layer="119"/>
<rectangle x1="0.61468125" y1="2.84911875" x2="0.6477" y2="2.8575" layer="119"/>
<rectangle x1="0.7239" y1="2.84911875" x2="0.75691875" y2="2.8575" layer="119"/>
<rectangle x1="0.83311875" y1="2.84911875" x2="0.86868125" y2="2.8575" layer="119"/>
<rectangle x1="0.96011875" y1="2.84911875" x2="0.99568125" y2="2.8575" layer="119"/>
<rectangle x1="1.0795" y1="2.84911875" x2="1.11251875" y2="2.8575" layer="119"/>
<rectangle x1="1.1811" y1="2.84911875" x2="1.21411875" y2="2.8575" layer="119"/>
<rectangle x1="1.79831875" y1="2.84911875" x2="1.83388125" y2="2.8575" layer="119"/>
<rectangle x1="1.9685" y1="2.84911875" x2="2.00151875" y2="2.8575" layer="119"/>
<rectangle x1="2.7559" y1="2.84911875" x2="2.78891875" y2="2.8575" layer="119"/>
<rectangle x1="2.83971875" y1="2.84911875" x2="2.87528125" y2="2.8575" layer="119"/>
<rectangle x1="2.92608125" y1="2.84911875" x2="2.9591" y2="2.8575" layer="119"/>
<rectangle x1="3.65251875" y1="2.84911875" x2="3.68808125" y2="2.8575" layer="119"/>
<rectangle x1="3.7719" y1="2.84911875" x2="3.80491875" y2="2.8575" layer="119"/>
<rectangle x1="4.4069" y1="2.84911875" x2="4.4323" y2="2.8575" layer="119"/>
<rectangle x1="5.10031875" y1="2.84911875" x2="5.13588125" y2="2.8575" layer="119"/>
<rectangle x1="5.68451875" y1="2.84911875" x2="5.72008125" y2="2.8575" layer="119"/>
<rectangle x1="5.81151875" y1="2.84911875" x2="5.84708125" y2="2.8575" layer="119"/>
<rectangle x1="6.5405" y1="2.84911875" x2="6.57351875" y2="2.8575" layer="119"/>
<rectangle x1="6.6167" y1="2.84911875" x2="6.64971875" y2="2.8575" layer="119"/>
<rectangle x1="6.70051875" y1="2.84911875" x2="6.73608125" y2="2.8575" layer="119"/>
<rectangle x1="7.48791875" y1="2.84911875" x2="7.52348125" y2="2.8575" layer="119"/>
<rectangle x1="7.6581" y1="2.84911875" x2="7.69111875" y2="2.8575" layer="119"/>
<rectangle x1="8.2677" y1="2.84911875" x2="8.30071875" y2="2.8575" layer="119"/>
<rectangle x1="8.37691875" y1="2.84911875" x2="8.41248125" y2="2.8575" layer="119"/>
<rectangle x1="8.4963" y1="2.84911875" x2="8.52931875" y2="2.8575" layer="119"/>
<rectangle x1="8.61568125" y1="2.84911875" x2="8.6487" y2="2.8575" layer="119"/>
<rectangle x1="8.73251875" y1="2.84911875" x2="8.76808125" y2="2.8575" layer="119"/>
<rectangle x1="8.83411875" y1="2.84911875" x2="8.86968125" y2="2.8575" layer="119"/>
<rectangle x1="8.93571875" y1="2.84911875" x2="8.97128125" y2="2.8575" layer="119"/>
<rectangle x1="9.03731875" y1="2.84911875" x2="9.07288125" y2="2.8575" layer="119"/>
<rectangle x1="0.4191" y1="2.8575" x2="0.45211875" y2="2.86588125" layer="119"/>
<rectangle x1="0.51308125" y1="2.8575" x2="0.5461" y2="2.86588125" layer="119"/>
<rectangle x1="0.61468125" y1="2.8575" x2="0.6477" y2="2.86588125" layer="119"/>
<rectangle x1="0.7239" y1="2.8575" x2="0.75691875" y2="2.86588125" layer="119"/>
<rectangle x1="0.83311875" y1="2.8575" x2="0.86868125" y2="2.86588125" layer="119"/>
<rectangle x1="0.96011875" y1="2.8575" x2="0.99568125" y2="2.86588125" layer="119"/>
<rectangle x1="1.0795" y1="2.8575" x2="1.11251875" y2="2.86588125" layer="119"/>
<rectangle x1="1.1811" y1="2.8575" x2="1.21411875" y2="2.86588125" layer="119"/>
<rectangle x1="1.79831875" y1="2.8575" x2="1.83388125" y2="2.86588125" layer="119"/>
<rectangle x1="1.9685" y1="2.8575" x2="2.00151875" y2="2.86588125" layer="119"/>
<rectangle x1="2.7559" y1="2.8575" x2="2.78891875" y2="2.86588125" layer="119"/>
<rectangle x1="2.83971875" y1="2.8575" x2="2.87528125" y2="2.86588125" layer="119"/>
<rectangle x1="2.92608125" y1="2.8575" x2="2.9591" y2="2.86588125" layer="119"/>
<rectangle x1="3.65251875" y1="2.8575" x2="3.68808125" y2="2.86588125" layer="119"/>
<rectangle x1="3.7719" y1="2.8575" x2="3.80491875" y2="2.86588125" layer="119"/>
<rectangle x1="4.4069" y1="2.8575" x2="4.4323" y2="2.86588125" layer="119"/>
<rectangle x1="5.10031875" y1="2.8575" x2="5.13588125" y2="2.86588125" layer="119"/>
<rectangle x1="5.68451875" y1="2.8575" x2="5.72008125" y2="2.86588125" layer="119"/>
<rectangle x1="5.81151875" y1="2.8575" x2="5.84708125" y2="2.86588125" layer="119"/>
<rectangle x1="6.5405" y1="2.8575" x2="6.57351875" y2="2.86588125" layer="119"/>
<rectangle x1="6.6167" y1="2.8575" x2="6.64971875" y2="2.86588125" layer="119"/>
<rectangle x1="6.70051875" y1="2.8575" x2="6.73608125" y2="2.86588125" layer="119"/>
<rectangle x1="7.48791875" y1="2.8575" x2="7.52348125" y2="2.86588125" layer="119"/>
<rectangle x1="7.6581" y1="2.8575" x2="7.69111875" y2="2.86588125" layer="119"/>
<rectangle x1="8.2677" y1="2.8575" x2="8.30071875" y2="2.86588125" layer="119"/>
<rectangle x1="8.37691875" y1="2.8575" x2="8.41248125" y2="2.86588125" layer="119"/>
<rectangle x1="8.4963" y1="2.8575" x2="8.52931875" y2="2.86588125" layer="119"/>
<rectangle x1="8.61568125" y1="2.8575" x2="8.6487" y2="2.86588125" layer="119"/>
<rectangle x1="8.73251875" y1="2.8575" x2="8.76808125" y2="2.86588125" layer="119"/>
<rectangle x1="8.83411875" y1="2.8575" x2="8.86968125" y2="2.86588125" layer="119"/>
<rectangle x1="8.93571875" y1="2.8575" x2="8.97128125" y2="2.86588125" layer="119"/>
<rectangle x1="9.03731875" y1="2.8575" x2="9.07288125" y2="2.86588125" layer="119"/>
<rectangle x1="0.4191" y1="2.86588125" x2="0.45211875" y2="2.87451875" layer="119"/>
<rectangle x1="0.51308125" y1="2.86588125" x2="0.5461" y2="2.87451875" layer="119"/>
<rectangle x1="0.61468125" y1="2.86588125" x2="0.6477" y2="2.87451875" layer="119"/>
<rectangle x1="0.7239" y1="2.86588125" x2="0.75691875" y2="2.87451875" layer="119"/>
<rectangle x1="0.83311875" y1="2.86588125" x2="0.86868125" y2="2.87451875" layer="119"/>
<rectangle x1="0.96011875" y1="2.86588125" x2="0.99568125" y2="2.87451875" layer="119"/>
<rectangle x1="1.0795" y1="2.86588125" x2="1.11251875" y2="2.87451875" layer="119"/>
<rectangle x1="1.1811" y1="2.86588125" x2="1.21411875" y2="2.87451875" layer="119"/>
<rectangle x1="1.79831875" y1="2.86588125" x2="1.83388125" y2="2.87451875" layer="119"/>
<rectangle x1="1.9685" y1="2.86588125" x2="2.00151875" y2="2.87451875" layer="119"/>
<rectangle x1="2.7559" y1="2.86588125" x2="2.78891875" y2="2.87451875" layer="119"/>
<rectangle x1="2.83971875" y1="2.86588125" x2="2.87528125" y2="2.87451875" layer="119"/>
<rectangle x1="2.92608125" y1="2.86588125" x2="2.9591" y2="2.87451875" layer="119"/>
<rectangle x1="3.65251875" y1="2.86588125" x2="3.68808125" y2="2.87451875" layer="119"/>
<rectangle x1="3.7719" y1="2.86588125" x2="3.80491875" y2="2.87451875" layer="119"/>
<rectangle x1="4.4069" y1="2.86588125" x2="4.4323" y2="2.87451875" layer="119"/>
<rectangle x1="5.10031875" y1="2.86588125" x2="5.13588125" y2="2.87451875" layer="119"/>
<rectangle x1="5.68451875" y1="2.86588125" x2="5.72008125" y2="2.87451875" layer="119"/>
<rectangle x1="5.81151875" y1="2.86588125" x2="5.84708125" y2="2.87451875" layer="119"/>
<rectangle x1="6.5405" y1="2.86588125" x2="6.57351875" y2="2.87451875" layer="119"/>
<rectangle x1="6.6167" y1="2.86588125" x2="6.64971875" y2="2.87451875" layer="119"/>
<rectangle x1="6.70051875" y1="2.86588125" x2="6.73608125" y2="2.87451875" layer="119"/>
<rectangle x1="7.48791875" y1="2.86588125" x2="7.52348125" y2="2.87451875" layer="119"/>
<rectangle x1="7.6581" y1="2.86588125" x2="7.69111875" y2="2.87451875" layer="119"/>
<rectangle x1="8.2677" y1="2.86588125" x2="8.30071875" y2="2.87451875" layer="119"/>
<rectangle x1="8.37691875" y1="2.86588125" x2="8.41248125" y2="2.87451875" layer="119"/>
<rectangle x1="8.4963" y1="2.86588125" x2="8.52931875" y2="2.87451875" layer="119"/>
<rectangle x1="8.61568125" y1="2.86588125" x2="8.6487" y2="2.87451875" layer="119"/>
<rectangle x1="8.73251875" y1="2.86588125" x2="8.76808125" y2="2.87451875" layer="119"/>
<rectangle x1="8.83411875" y1="2.86588125" x2="8.86968125" y2="2.87451875" layer="119"/>
<rectangle x1="8.93571875" y1="2.86588125" x2="8.97128125" y2="2.87451875" layer="119"/>
<rectangle x1="9.03731875" y1="2.86588125" x2="9.07288125" y2="2.87451875" layer="119"/>
<rectangle x1="0.4191" y1="2.87451875" x2="0.45211875" y2="2.8829" layer="119"/>
<rectangle x1="0.51308125" y1="2.87451875" x2="0.5461" y2="2.8829" layer="119"/>
<rectangle x1="0.61468125" y1="2.87451875" x2="0.6477" y2="2.8829" layer="119"/>
<rectangle x1="0.7239" y1="2.87451875" x2="0.75691875" y2="2.8829" layer="119"/>
<rectangle x1="0.83311875" y1="2.87451875" x2="0.86868125" y2="2.8829" layer="119"/>
<rectangle x1="0.96011875" y1="2.87451875" x2="0.99568125" y2="2.8829" layer="119"/>
<rectangle x1="1.0795" y1="2.87451875" x2="1.11251875" y2="2.8829" layer="119"/>
<rectangle x1="1.1811" y1="2.87451875" x2="1.21411875" y2="2.8829" layer="119"/>
<rectangle x1="1.79831875" y1="2.87451875" x2="1.83388125" y2="2.8829" layer="119"/>
<rectangle x1="1.9685" y1="2.87451875" x2="2.00151875" y2="2.8829" layer="119"/>
<rectangle x1="2.7559" y1="2.87451875" x2="2.78891875" y2="2.8829" layer="119"/>
<rectangle x1="2.83971875" y1="2.87451875" x2="2.87528125" y2="2.8829" layer="119"/>
<rectangle x1="2.92608125" y1="2.87451875" x2="2.9591" y2="2.8829" layer="119"/>
<rectangle x1="3.65251875" y1="2.87451875" x2="3.68808125" y2="2.8829" layer="119"/>
<rectangle x1="3.7719" y1="2.87451875" x2="3.80491875" y2="2.8829" layer="119"/>
<rectangle x1="4.4069" y1="2.87451875" x2="4.4323" y2="2.8829" layer="119"/>
<rectangle x1="5.10031875" y1="2.87451875" x2="5.13588125" y2="2.8829" layer="119"/>
<rectangle x1="5.68451875" y1="2.87451875" x2="5.72008125" y2="2.8829" layer="119"/>
<rectangle x1="5.81151875" y1="2.87451875" x2="5.84708125" y2="2.8829" layer="119"/>
<rectangle x1="6.5405" y1="2.87451875" x2="6.57351875" y2="2.8829" layer="119"/>
<rectangle x1="6.6167" y1="2.87451875" x2="6.64971875" y2="2.8829" layer="119"/>
<rectangle x1="6.70051875" y1="2.87451875" x2="6.73608125" y2="2.8829" layer="119"/>
<rectangle x1="7.48791875" y1="2.87451875" x2="7.52348125" y2="2.8829" layer="119"/>
<rectangle x1="7.6581" y1="2.87451875" x2="7.69111875" y2="2.8829" layer="119"/>
<rectangle x1="8.2677" y1="2.87451875" x2="8.30071875" y2="2.8829" layer="119"/>
<rectangle x1="8.37691875" y1="2.87451875" x2="8.41248125" y2="2.8829" layer="119"/>
<rectangle x1="8.4963" y1="2.87451875" x2="8.52931875" y2="2.8829" layer="119"/>
<rectangle x1="8.61568125" y1="2.87451875" x2="8.6487" y2="2.8829" layer="119"/>
<rectangle x1="8.73251875" y1="2.87451875" x2="8.76808125" y2="2.8829" layer="119"/>
<rectangle x1="8.83411875" y1="2.87451875" x2="8.86968125" y2="2.8829" layer="119"/>
<rectangle x1="8.93571875" y1="2.87451875" x2="8.97128125" y2="2.8829" layer="119"/>
<rectangle x1="9.03731875" y1="2.87451875" x2="9.07288125" y2="2.8829" layer="119"/>
<rectangle x1="0.4191" y1="2.8829" x2="0.45211875" y2="2.89128125" layer="119"/>
<rectangle x1="0.51308125" y1="2.8829" x2="0.5461" y2="2.89128125" layer="119"/>
<rectangle x1="0.61468125" y1="2.8829" x2="0.6477" y2="2.89128125" layer="119"/>
<rectangle x1="0.7239" y1="2.8829" x2="0.75691875" y2="2.89128125" layer="119"/>
<rectangle x1="0.83311875" y1="2.8829" x2="0.86868125" y2="2.89128125" layer="119"/>
<rectangle x1="0.96011875" y1="2.8829" x2="0.99568125" y2="2.89128125" layer="119"/>
<rectangle x1="1.0795" y1="2.8829" x2="1.11251875" y2="2.89128125" layer="119"/>
<rectangle x1="1.1811" y1="2.8829" x2="1.21411875" y2="2.89128125" layer="119"/>
<rectangle x1="1.79831875" y1="2.8829" x2="1.83388125" y2="2.89128125" layer="119"/>
<rectangle x1="1.9685" y1="2.8829" x2="2.00151875" y2="2.89128125" layer="119"/>
<rectangle x1="2.7559" y1="2.8829" x2="2.78891875" y2="2.89128125" layer="119"/>
<rectangle x1="2.83971875" y1="2.8829" x2="2.87528125" y2="2.89128125" layer="119"/>
<rectangle x1="2.92608125" y1="2.8829" x2="2.9591" y2="2.89128125" layer="119"/>
<rectangle x1="3.65251875" y1="2.8829" x2="3.68808125" y2="2.89128125" layer="119"/>
<rectangle x1="3.7719" y1="2.8829" x2="3.80491875" y2="2.89128125" layer="119"/>
<rectangle x1="4.4069" y1="2.8829" x2="4.4323" y2="2.89128125" layer="119"/>
<rectangle x1="5.10031875" y1="2.8829" x2="5.13588125" y2="2.89128125" layer="119"/>
<rectangle x1="5.68451875" y1="2.8829" x2="5.72008125" y2="2.89128125" layer="119"/>
<rectangle x1="5.81151875" y1="2.8829" x2="5.84708125" y2="2.89128125" layer="119"/>
<rectangle x1="6.5405" y1="2.8829" x2="6.57351875" y2="2.89128125" layer="119"/>
<rectangle x1="6.6167" y1="2.8829" x2="6.64971875" y2="2.89128125" layer="119"/>
<rectangle x1="6.70051875" y1="2.8829" x2="6.73608125" y2="2.89128125" layer="119"/>
<rectangle x1="7.48791875" y1="2.8829" x2="7.52348125" y2="2.89128125" layer="119"/>
<rectangle x1="7.6581" y1="2.8829" x2="7.69111875" y2="2.89128125" layer="119"/>
<rectangle x1="8.2677" y1="2.8829" x2="8.30071875" y2="2.89128125" layer="119"/>
<rectangle x1="8.37691875" y1="2.8829" x2="8.41248125" y2="2.89128125" layer="119"/>
<rectangle x1="8.4963" y1="2.8829" x2="8.52931875" y2="2.89128125" layer="119"/>
<rectangle x1="8.61568125" y1="2.8829" x2="8.6487" y2="2.89128125" layer="119"/>
<rectangle x1="8.73251875" y1="2.8829" x2="8.76808125" y2="2.89128125" layer="119"/>
<rectangle x1="8.83411875" y1="2.8829" x2="8.86968125" y2="2.89128125" layer="119"/>
<rectangle x1="8.93571875" y1="2.8829" x2="8.97128125" y2="2.89128125" layer="119"/>
<rectangle x1="9.03731875" y1="2.8829" x2="9.07288125" y2="2.89128125" layer="119"/>
<rectangle x1="0.4191" y1="2.89128125" x2="0.45211875" y2="2.89991875" layer="119"/>
<rectangle x1="0.51308125" y1="2.89128125" x2="0.5461" y2="2.89991875" layer="119"/>
<rectangle x1="0.61468125" y1="2.89128125" x2="0.6477" y2="2.89991875" layer="119"/>
<rectangle x1="0.7239" y1="2.89128125" x2="0.75691875" y2="2.89991875" layer="119"/>
<rectangle x1="0.83311875" y1="2.89128125" x2="0.86868125" y2="2.89991875" layer="119"/>
<rectangle x1="0.96011875" y1="2.89128125" x2="0.99568125" y2="2.89991875" layer="119"/>
<rectangle x1="1.0795" y1="2.89128125" x2="1.11251875" y2="2.89991875" layer="119"/>
<rectangle x1="1.1811" y1="2.89128125" x2="1.21411875" y2="2.89991875" layer="119"/>
<rectangle x1="1.79831875" y1="2.89128125" x2="1.83388125" y2="2.89991875" layer="119"/>
<rectangle x1="1.9685" y1="2.89128125" x2="2.00151875" y2="2.89991875" layer="119"/>
<rectangle x1="2.7559" y1="2.89128125" x2="2.78891875" y2="2.89991875" layer="119"/>
<rectangle x1="2.83971875" y1="2.89128125" x2="2.87528125" y2="2.89991875" layer="119"/>
<rectangle x1="2.92608125" y1="2.89128125" x2="2.9591" y2="2.89991875" layer="119"/>
<rectangle x1="3.65251875" y1="2.89128125" x2="3.68808125" y2="2.89991875" layer="119"/>
<rectangle x1="3.7719" y1="2.89128125" x2="3.80491875" y2="2.89991875" layer="119"/>
<rectangle x1="4.4069" y1="2.89128125" x2="4.4323" y2="2.89991875" layer="119"/>
<rectangle x1="5.10031875" y1="2.89128125" x2="5.13588125" y2="2.89991875" layer="119"/>
<rectangle x1="5.68451875" y1="2.89128125" x2="5.72008125" y2="2.89991875" layer="119"/>
<rectangle x1="5.81151875" y1="2.89128125" x2="5.84708125" y2="2.89991875" layer="119"/>
<rectangle x1="6.5405" y1="2.89128125" x2="6.57351875" y2="2.89991875" layer="119"/>
<rectangle x1="6.6167" y1="2.89128125" x2="6.64971875" y2="2.89991875" layer="119"/>
<rectangle x1="6.70051875" y1="2.89128125" x2="6.73608125" y2="2.89991875" layer="119"/>
<rectangle x1="7.48791875" y1="2.89128125" x2="7.52348125" y2="2.89991875" layer="119"/>
<rectangle x1="7.6581" y1="2.89128125" x2="7.69111875" y2="2.89991875" layer="119"/>
<rectangle x1="8.2677" y1="2.89128125" x2="8.30071875" y2="2.89991875" layer="119"/>
<rectangle x1="8.37691875" y1="2.89128125" x2="8.41248125" y2="2.89991875" layer="119"/>
<rectangle x1="8.4963" y1="2.89128125" x2="8.52931875" y2="2.89991875" layer="119"/>
<rectangle x1="8.61568125" y1="2.89128125" x2="8.6487" y2="2.89991875" layer="119"/>
<rectangle x1="8.73251875" y1="2.89128125" x2="8.76808125" y2="2.89991875" layer="119"/>
<rectangle x1="8.83411875" y1="2.89128125" x2="8.86968125" y2="2.89991875" layer="119"/>
<rectangle x1="8.93571875" y1="2.89128125" x2="8.97128125" y2="2.89991875" layer="119"/>
<rectangle x1="9.03731875" y1="2.89128125" x2="9.07288125" y2="2.89991875" layer="119"/>
<rectangle x1="0.4191" y1="2.89991875" x2="0.45211875" y2="2.9083" layer="119"/>
<rectangle x1="0.51308125" y1="2.89991875" x2="0.5461" y2="2.9083" layer="119"/>
<rectangle x1="0.61468125" y1="2.89991875" x2="0.6477" y2="2.9083" layer="119"/>
<rectangle x1="0.7239" y1="2.89991875" x2="0.75691875" y2="2.9083" layer="119"/>
<rectangle x1="0.83311875" y1="2.89991875" x2="0.86868125" y2="2.9083" layer="119"/>
<rectangle x1="0.96011875" y1="2.89991875" x2="0.99568125" y2="2.9083" layer="119"/>
<rectangle x1="1.0795" y1="2.89991875" x2="1.11251875" y2="2.9083" layer="119"/>
<rectangle x1="1.1811" y1="2.89991875" x2="1.21411875" y2="2.9083" layer="119"/>
<rectangle x1="1.79831875" y1="2.89991875" x2="1.83388125" y2="2.9083" layer="119"/>
<rectangle x1="1.9685" y1="2.89991875" x2="2.00151875" y2="2.9083" layer="119"/>
<rectangle x1="2.7559" y1="2.89991875" x2="2.78891875" y2="2.9083" layer="119"/>
<rectangle x1="2.83971875" y1="2.89991875" x2="2.87528125" y2="2.9083" layer="119"/>
<rectangle x1="2.92608125" y1="2.89991875" x2="2.9591" y2="2.9083" layer="119"/>
<rectangle x1="3.65251875" y1="2.89991875" x2="3.68808125" y2="2.9083" layer="119"/>
<rectangle x1="3.7719" y1="2.89991875" x2="3.80491875" y2="2.9083" layer="119"/>
<rectangle x1="4.4069" y1="2.89991875" x2="4.4323" y2="2.9083" layer="119"/>
<rectangle x1="5.10031875" y1="2.89991875" x2="5.13588125" y2="2.9083" layer="119"/>
<rectangle x1="5.68451875" y1="2.89991875" x2="5.72008125" y2="2.9083" layer="119"/>
<rectangle x1="5.81151875" y1="2.89991875" x2="5.84708125" y2="2.9083" layer="119"/>
<rectangle x1="6.5405" y1="2.89991875" x2="6.57351875" y2="2.9083" layer="119"/>
<rectangle x1="6.6167" y1="2.89991875" x2="6.64971875" y2="2.9083" layer="119"/>
<rectangle x1="6.70051875" y1="2.89991875" x2="6.73608125" y2="2.9083" layer="119"/>
<rectangle x1="7.48791875" y1="2.89991875" x2="7.52348125" y2="2.9083" layer="119"/>
<rectangle x1="7.6581" y1="2.89991875" x2="7.69111875" y2="2.9083" layer="119"/>
<rectangle x1="8.2677" y1="2.89991875" x2="8.30071875" y2="2.9083" layer="119"/>
<rectangle x1="8.37691875" y1="2.89991875" x2="8.41248125" y2="2.9083" layer="119"/>
<rectangle x1="8.4963" y1="2.89991875" x2="8.52931875" y2="2.9083" layer="119"/>
<rectangle x1="8.61568125" y1="2.89991875" x2="8.6487" y2="2.9083" layer="119"/>
<rectangle x1="8.73251875" y1="2.89991875" x2="8.76808125" y2="2.9083" layer="119"/>
<rectangle x1="8.83411875" y1="2.89991875" x2="8.86968125" y2="2.9083" layer="119"/>
<rectangle x1="8.93571875" y1="2.89991875" x2="8.97128125" y2="2.9083" layer="119"/>
<rectangle x1="9.03731875" y1="2.89991875" x2="9.07288125" y2="2.9083" layer="119"/>
<rectangle x1="0.4191" y1="2.9083" x2="0.45211875" y2="2.91668125" layer="119"/>
<rectangle x1="0.51308125" y1="2.9083" x2="0.5461" y2="2.91668125" layer="119"/>
<rectangle x1="0.61468125" y1="2.9083" x2="0.6477" y2="2.91668125" layer="119"/>
<rectangle x1="0.7239" y1="2.9083" x2="0.75691875" y2="2.91668125" layer="119"/>
<rectangle x1="0.83311875" y1="2.9083" x2="0.86868125" y2="2.91668125" layer="119"/>
<rectangle x1="0.96011875" y1="2.9083" x2="0.99568125" y2="2.91668125" layer="119"/>
<rectangle x1="1.0795" y1="2.9083" x2="1.11251875" y2="2.91668125" layer="119"/>
<rectangle x1="1.1811" y1="2.9083" x2="1.21411875" y2="2.91668125" layer="119"/>
<rectangle x1="1.79831875" y1="2.9083" x2="1.83388125" y2="2.91668125" layer="119"/>
<rectangle x1="1.9685" y1="2.9083" x2="2.00151875" y2="2.91668125" layer="119"/>
<rectangle x1="2.7559" y1="2.9083" x2="2.78891875" y2="2.91668125" layer="119"/>
<rectangle x1="2.83971875" y1="2.9083" x2="2.87528125" y2="2.91668125" layer="119"/>
<rectangle x1="2.92608125" y1="2.9083" x2="2.9591" y2="2.91668125" layer="119"/>
<rectangle x1="3.65251875" y1="2.9083" x2="3.68808125" y2="2.91668125" layer="119"/>
<rectangle x1="3.7719" y1="2.9083" x2="3.80491875" y2="2.91668125" layer="119"/>
<rectangle x1="4.4069" y1="2.9083" x2="4.4323" y2="2.91668125" layer="119"/>
<rectangle x1="5.10031875" y1="2.9083" x2="5.13588125" y2="2.91668125" layer="119"/>
<rectangle x1="5.68451875" y1="2.9083" x2="5.72008125" y2="2.91668125" layer="119"/>
<rectangle x1="5.81151875" y1="2.9083" x2="5.84708125" y2="2.91668125" layer="119"/>
<rectangle x1="6.5405" y1="2.9083" x2="6.57351875" y2="2.91668125" layer="119"/>
<rectangle x1="6.6167" y1="2.9083" x2="6.64971875" y2="2.91668125" layer="119"/>
<rectangle x1="6.70051875" y1="2.9083" x2="6.73608125" y2="2.91668125" layer="119"/>
<rectangle x1="7.48791875" y1="2.9083" x2="7.52348125" y2="2.91668125" layer="119"/>
<rectangle x1="7.6581" y1="2.9083" x2="7.69111875" y2="2.91668125" layer="119"/>
<rectangle x1="8.2677" y1="2.9083" x2="8.30071875" y2="2.91668125" layer="119"/>
<rectangle x1="8.37691875" y1="2.9083" x2="8.41248125" y2="2.91668125" layer="119"/>
<rectangle x1="8.4963" y1="2.9083" x2="8.52931875" y2="2.91668125" layer="119"/>
<rectangle x1="8.61568125" y1="2.9083" x2="8.6487" y2="2.91668125" layer="119"/>
<rectangle x1="8.73251875" y1="2.9083" x2="8.76808125" y2="2.91668125" layer="119"/>
<rectangle x1="8.83411875" y1="2.9083" x2="8.86968125" y2="2.91668125" layer="119"/>
<rectangle x1="8.93571875" y1="2.9083" x2="8.97128125" y2="2.91668125" layer="119"/>
<rectangle x1="9.03731875" y1="2.9083" x2="9.07288125" y2="2.91668125" layer="119"/>
<rectangle x1="0.4191" y1="2.91668125" x2="0.45211875" y2="2.92531875" layer="119"/>
<rectangle x1="0.51308125" y1="2.91668125" x2="0.5461" y2="2.92531875" layer="119"/>
<rectangle x1="0.61468125" y1="2.91668125" x2="0.6477" y2="2.92531875" layer="119"/>
<rectangle x1="0.7239" y1="2.91668125" x2="0.75691875" y2="2.92531875" layer="119"/>
<rectangle x1="0.83311875" y1="2.91668125" x2="0.86868125" y2="2.92531875" layer="119"/>
<rectangle x1="0.96011875" y1="2.91668125" x2="0.99568125" y2="2.92531875" layer="119"/>
<rectangle x1="1.0795" y1="2.91668125" x2="1.11251875" y2="2.92531875" layer="119"/>
<rectangle x1="1.1811" y1="2.91668125" x2="1.21411875" y2="2.92531875" layer="119"/>
<rectangle x1="1.79831875" y1="2.91668125" x2="1.83388125" y2="2.92531875" layer="119"/>
<rectangle x1="1.9685" y1="2.91668125" x2="2.00151875" y2="2.92531875" layer="119"/>
<rectangle x1="2.7559" y1="2.91668125" x2="2.78891875" y2="2.92531875" layer="119"/>
<rectangle x1="2.83971875" y1="2.91668125" x2="2.87528125" y2="2.92531875" layer="119"/>
<rectangle x1="2.92608125" y1="2.91668125" x2="2.9591" y2="2.92531875" layer="119"/>
<rectangle x1="3.65251875" y1="2.91668125" x2="3.68808125" y2="2.92531875" layer="119"/>
<rectangle x1="3.7719" y1="2.91668125" x2="3.80491875" y2="2.92531875" layer="119"/>
<rectangle x1="4.4069" y1="2.91668125" x2="4.4323" y2="2.92531875" layer="119"/>
<rectangle x1="5.10031875" y1="2.91668125" x2="5.13588125" y2="2.92531875" layer="119"/>
<rectangle x1="5.68451875" y1="2.91668125" x2="5.72008125" y2="2.92531875" layer="119"/>
<rectangle x1="5.81151875" y1="2.91668125" x2="5.84708125" y2="2.92531875" layer="119"/>
<rectangle x1="6.5405" y1="2.91668125" x2="6.57351875" y2="2.92531875" layer="119"/>
<rectangle x1="6.6167" y1="2.91668125" x2="6.64971875" y2="2.92531875" layer="119"/>
<rectangle x1="6.70051875" y1="2.91668125" x2="6.73608125" y2="2.92531875" layer="119"/>
<rectangle x1="7.48791875" y1="2.91668125" x2="7.52348125" y2="2.92531875" layer="119"/>
<rectangle x1="7.6581" y1="2.91668125" x2="7.69111875" y2="2.92531875" layer="119"/>
<rectangle x1="8.2677" y1="2.91668125" x2="8.30071875" y2="2.92531875" layer="119"/>
<rectangle x1="8.37691875" y1="2.91668125" x2="8.41248125" y2="2.92531875" layer="119"/>
<rectangle x1="8.4963" y1="2.91668125" x2="8.52931875" y2="2.92531875" layer="119"/>
<rectangle x1="8.61568125" y1="2.91668125" x2="8.6487" y2="2.92531875" layer="119"/>
<rectangle x1="8.73251875" y1="2.91668125" x2="8.76808125" y2="2.92531875" layer="119"/>
<rectangle x1="8.83411875" y1="2.91668125" x2="8.86968125" y2="2.92531875" layer="119"/>
<rectangle x1="8.93571875" y1="2.91668125" x2="8.97128125" y2="2.92531875" layer="119"/>
<rectangle x1="9.03731875" y1="2.91668125" x2="9.07288125" y2="2.92531875" layer="119"/>
<rectangle x1="0.4191" y1="2.92531875" x2="0.45211875" y2="2.9337" layer="119"/>
<rectangle x1="0.51308125" y1="2.92531875" x2="0.5461" y2="2.9337" layer="119"/>
<rectangle x1="0.61468125" y1="2.92531875" x2="0.6477" y2="2.9337" layer="119"/>
<rectangle x1="0.7239" y1="2.92531875" x2="0.75691875" y2="2.9337" layer="119"/>
<rectangle x1="0.83311875" y1="2.92531875" x2="0.86868125" y2="2.9337" layer="119"/>
<rectangle x1="0.96011875" y1="2.92531875" x2="0.99568125" y2="2.9337" layer="119"/>
<rectangle x1="1.0795" y1="2.92531875" x2="1.11251875" y2="2.9337" layer="119"/>
<rectangle x1="1.1811" y1="2.92531875" x2="1.21411875" y2="2.9337" layer="119"/>
<rectangle x1="1.79831875" y1="2.92531875" x2="1.83388125" y2="2.9337" layer="119"/>
<rectangle x1="1.9685" y1="2.92531875" x2="2.00151875" y2="2.9337" layer="119"/>
<rectangle x1="2.7559" y1="2.92531875" x2="2.78891875" y2="2.9337" layer="119"/>
<rectangle x1="2.83971875" y1="2.92531875" x2="2.87528125" y2="2.9337" layer="119"/>
<rectangle x1="2.92608125" y1="2.92531875" x2="2.9591" y2="2.9337" layer="119"/>
<rectangle x1="3.65251875" y1="2.92531875" x2="3.68808125" y2="2.9337" layer="119"/>
<rectangle x1="3.7719" y1="2.92531875" x2="3.80491875" y2="2.9337" layer="119"/>
<rectangle x1="4.4069" y1="2.92531875" x2="4.4323" y2="2.9337" layer="119"/>
<rectangle x1="5.10031875" y1="2.92531875" x2="5.13588125" y2="2.9337" layer="119"/>
<rectangle x1="5.68451875" y1="2.92531875" x2="5.72008125" y2="2.9337" layer="119"/>
<rectangle x1="5.81151875" y1="2.92531875" x2="5.84708125" y2="2.9337" layer="119"/>
<rectangle x1="6.5405" y1="2.92531875" x2="6.57351875" y2="2.9337" layer="119"/>
<rectangle x1="6.6167" y1="2.92531875" x2="6.64971875" y2="2.9337" layer="119"/>
<rectangle x1="6.70051875" y1="2.92531875" x2="6.73608125" y2="2.9337" layer="119"/>
<rectangle x1="7.48791875" y1="2.92531875" x2="7.52348125" y2="2.9337" layer="119"/>
<rectangle x1="7.6581" y1="2.92531875" x2="7.69111875" y2="2.9337" layer="119"/>
<rectangle x1="8.2677" y1="2.92531875" x2="8.30071875" y2="2.9337" layer="119"/>
<rectangle x1="8.37691875" y1="2.92531875" x2="8.41248125" y2="2.9337" layer="119"/>
<rectangle x1="8.4963" y1="2.92531875" x2="8.52931875" y2="2.9337" layer="119"/>
<rectangle x1="8.61568125" y1="2.92531875" x2="8.6487" y2="2.9337" layer="119"/>
<rectangle x1="8.73251875" y1="2.92531875" x2="8.76808125" y2="2.9337" layer="119"/>
<rectangle x1="8.83411875" y1="2.92531875" x2="8.86968125" y2="2.9337" layer="119"/>
<rectangle x1="8.93571875" y1="2.92531875" x2="8.97128125" y2="2.9337" layer="119"/>
<rectangle x1="9.03731875" y1="2.92531875" x2="9.07288125" y2="2.9337" layer="119"/>
<rectangle x1="0.4191" y1="2.9337" x2="0.45211875" y2="2.94208125" layer="119"/>
<rectangle x1="0.51308125" y1="2.9337" x2="0.5461" y2="2.94208125" layer="119"/>
<rectangle x1="0.61468125" y1="2.9337" x2="0.6477" y2="2.94208125" layer="119"/>
<rectangle x1="0.7239" y1="2.9337" x2="0.75691875" y2="2.94208125" layer="119"/>
<rectangle x1="0.83311875" y1="2.9337" x2="0.86868125" y2="2.94208125" layer="119"/>
<rectangle x1="0.96011875" y1="2.9337" x2="0.99568125" y2="2.94208125" layer="119"/>
<rectangle x1="1.0795" y1="2.9337" x2="1.11251875" y2="2.94208125" layer="119"/>
<rectangle x1="1.1811" y1="2.9337" x2="1.21411875" y2="2.94208125" layer="119"/>
<rectangle x1="1.79831875" y1="2.9337" x2="1.83388125" y2="2.94208125" layer="119"/>
<rectangle x1="1.9685" y1="2.9337" x2="2.00151875" y2="2.94208125" layer="119"/>
<rectangle x1="2.7559" y1="2.9337" x2="2.78891875" y2="2.94208125" layer="119"/>
<rectangle x1="2.83971875" y1="2.9337" x2="2.87528125" y2="2.94208125" layer="119"/>
<rectangle x1="2.92608125" y1="2.9337" x2="2.9591" y2="2.94208125" layer="119"/>
<rectangle x1="3.65251875" y1="2.9337" x2="3.68808125" y2="2.94208125" layer="119"/>
<rectangle x1="3.7719" y1="2.9337" x2="3.80491875" y2="2.94208125" layer="119"/>
<rectangle x1="4.4069" y1="2.9337" x2="4.4323" y2="2.94208125" layer="119"/>
<rectangle x1="5.10031875" y1="2.9337" x2="5.13588125" y2="2.94208125" layer="119"/>
<rectangle x1="5.68451875" y1="2.9337" x2="5.72008125" y2="2.94208125" layer="119"/>
<rectangle x1="5.81151875" y1="2.9337" x2="5.84708125" y2="2.94208125" layer="119"/>
<rectangle x1="6.5405" y1="2.9337" x2="6.57351875" y2="2.94208125" layer="119"/>
<rectangle x1="6.6167" y1="2.9337" x2="6.64971875" y2="2.94208125" layer="119"/>
<rectangle x1="6.70051875" y1="2.9337" x2="6.73608125" y2="2.94208125" layer="119"/>
<rectangle x1="7.48791875" y1="2.9337" x2="7.52348125" y2="2.94208125" layer="119"/>
<rectangle x1="7.6581" y1="2.9337" x2="7.69111875" y2="2.94208125" layer="119"/>
<rectangle x1="8.2677" y1="2.9337" x2="8.30071875" y2="2.94208125" layer="119"/>
<rectangle x1="8.37691875" y1="2.9337" x2="8.41248125" y2="2.94208125" layer="119"/>
<rectangle x1="8.4963" y1="2.9337" x2="8.52931875" y2="2.94208125" layer="119"/>
<rectangle x1="8.61568125" y1="2.9337" x2="8.6487" y2="2.94208125" layer="119"/>
<rectangle x1="8.73251875" y1="2.9337" x2="8.76808125" y2="2.94208125" layer="119"/>
<rectangle x1="8.83411875" y1="2.9337" x2="8.86968125" y2="2.94208125" layer="119"/>
<rectangle x1="8.93571875" y1="2.9337" x2="8.97128125" y2="2.94208125" layer="119"/>
<rectangle x1="9.03731875" y1="2.9337" x2="9.07288125" y2="2.94208125" layer="119"/>
<rectangle x1="0.3937" y1="2.94208125" x2="1.24968125" y2="2.95071875" layer="119"/>
<rectangle x1="1.74751875" y1="2.94208125" x2="2.06248125" y2="2.95071875" layer="119"/>
<rectangle x1="2.73811875" y1="2.94208125" x2="2.97688125" y2="2.95071875" layer="119"/>
<rectangle x1="3.60171875" y1="2.94208125" x2="3.85571875" y2="2.95071875" layer="119"/>
<rectangle x1="4.4069" y1="2.94208125" x2="4.4323" y2="2.95071875" layer="119"/>
<rectangle x1="5.10031875" y1="2.94208125" x2="5.13588125" y2="2.95071875" layer="119"/>
<rectangle x1="5.64388125" y1="2.94208125" x2="5.89788125" y2="2.95071875" layer="119"/>
<rectangle x1="6.5151" y1="2.94208125" x2="6.76148125" y2="2.95071875" layer="119"/>
<rectangle x1="7.4295" y1="2.94208125" x2="7.74191875" y2="2.95071875" layer="119"/>
<rectangle x1="8.23468125" y1="2.94208125" x2="9.09828125" y2="2.95071875" layer="119"/>
<rectangle x1="0.3937" y1="2.95071875" x2="1.24968125" y2="2.9591" layer="119"/>
<rectangle x1="1.74751875" y1="2.95071875" x2="2.06248125" y2="2.9591" layer="119"/>
<rectangle x1="2.73811875" y1="2.95071875" x2="2.97688125" y2="2.9591" layer="119"/>
<rectangle x1="3.60171875" y1="2.95071875" x2="3.65251875" y2="2.9591" layer="119"/>
<rectangle x1="3.68808125" y1="2.95071875" x2="3.77951875" y2="2.9591" layer="119"/>
<rectangle x1="3.8227" y1="2.95071875" x2="3.85571875" y2="2.9591" layer="119"/>
<rectangle x1="4.4069" y1="2.95071875" x2="4.4323" y2="2.9591" layer="119"/>
<rectangle x1="5.10031875" y1="2.95071875" x2="5.13588125" y2="2.9591" layer="119"/>
<rectangle x1="5.64388125" y1="2.95071875" x2="5.69468125" y2="2.9591" layer="119"/>
<rectangle x1="5.72008125" y1="2.95071875" x2="5.81151875" y2="2.9591" layer="119"/>
<rectangle x1="5.86231875" y1="2.95071875" x2="5.89788125" y2="2.9591" layer="119"/>
<rectangle x1="6.5151" y1="2.95071875" x2="6.76148125" y2="2.9591" layer="119"/>
<rectangle x1="7.4295" y1="2.95071875" x2="7.74191875" y2="2.9591" layer="119"/>
<rectangle x1="8.23468125" y1="2.95071875" x2="9.09828125" y2="2.9591" layer="119"/>
<rectangle x1="0.3937" y1="2.9591" x2="1.24968125" y2="2.96748125" layer="119"/>
<rectangle x1="1.74751875" y1="2.9591" x2="2.06248125" y2="2.96748125" layer="119"/>
<rectangle x1="2.73811875" y1="2.9591" x2="2.97688125" y2="2.96748125" layer="119"/>
<rectangle x1="3.60171875" y1="2.9591" x2="3.65251875" y2="2.96748125" layer="119"/>
<rectangle x1="3.68808125" y1="2.9591" x2="3.7719" y2="2.96748125" layer="119"/>
<rectangle x1="3.8227" y1="2.9591" x2="3.85571875" y2="2.96748125" layer="119"/>
<rectangle x1="4.4069" y1="2.9591" x2="4.4323" y2="2.96748125" layer="119"/>
<rectangle x1="5.10031875" y1="2.9591" x2="5.13588125" y2="2.96748125" layer="119"/>
<rectangle x1="5.64388125" y1="2.9591" x2="5.69468125" y2="2.96748125" layer="119"/>
<rectangle x1="5.72008125" y1="2.9591" x2="5.81151875" y2="2.96748125" layer="119"/>
<rectangle x1="5.86231875" y1="2.9591" x2="5.89788125" y2="2.96748125" layer="119"/>
<rectangle x1="6.5151" y1="2.9591" x2="6.76148125" y2="2.96748125" layer="119"/>
<rectangle x1="7.4295" y1="2.9591" x2="7.74191875" y2="2.96748125" layer="119"/>
<rectangle x1="8.23468125" y1="2.9591" x2="9.09828125" y2="2.96748125" layer="119"/>
<rectangle x1="0.3937" y1="2.96748125" x2="1.24968125" y2="2.97611875" layer="119"/>
<rectangle x1="1.74751875" y1="2.96748125" x2="2.06248125" y2="2.97611875" layer="119"/>
<rectangle x1="2.73811875" y1="2.96748125" x2="2.97688125" y2="2.97611875" layer="119"/>
<rectangle x1="3.60171875" y1="2.96748125" x2="3.65251875" y2="2.97611875" layer="119"/>
<rectangle x1="3.68808125" y1="2.96748125" x2="3.76428125" y2="2.97611875" layer="119"/>
<rectangle x1="3.8227" y1="2.96748125" x2="3.85571875" y2="2.97611875" layer="119"/>
<rectangle x1="4.4069" y1="2.96748125" x2="4.4323" y2="2.97611875" layer="119"/>
<rectangle x1="5.10031875" y1="2.96748125" x2="5.13588125" y2="2.97611875" layer="119"/>
<rectangle x1="5.64388125" y1="2.96748125" x2="5.69468125" y2="2.97611875" layer="119"/>
<rectangle x1="5.72008125" y1="2.96748125" x2="5.8039" y2="2.97611875" layer="119"/>
<rectangle x1="5.86231875" y1="2.96748125" x2="5.89788125" y2="2.97611875" layer="119"/>
<rectangle x1="6.5151" y1="2.96748125" x2="6.76148125" y2="2.97611875" layer="119"/>
<rectangle x1="7.4295" y1="2.96748125" x2="7.74191875" y2="2.97611875" layer="119"/>
<rectangle x1="8.23468125" y1="2.96748125" x2="9.09828125" y2="2.97611875" layer="119"/>
<rectangle x1="0.3937" y1="2.97611875" x2="1.24968125" y2="2.9845" layer="119"/>
<rectangle x1="1.74751875" y1="2.97611875" x2="2.06248125" y2="2.9845" layer="119"/>
<rectangle x1="2.73811875" y1="2.97611875" x2="2.97688125" y2="2.9845" layer="119"/>
<rectangle x1="3.60171875" y1="2.97611875" x2="3.65251875" y2="2.9845" layer="119"/>
<rectangle x1="3.68808125" y1="2.97611875" x2="3.75411875" y2="2.9845" layer="119"/>
<rectangle x1="3.8227" y1="2.97611875" x2="3.85571875" y2="2.9845" layer="119"/>
<rectangle x1="4.4069" y1="2.97611875" x2="4.4323" y2="2.9845" layer="119"/>
<rectangle x1="4.99871875" y1="2.97611875" x2="5.00888125" y2="2.9845" layer="119"/>
<rectangle x1="5.10031875" y1="2.97611875" x2="5.13588125" y2="2.9845" layer="119"/>
<rectangle x1="5.64388125" y1="2.97611875" x2="5.69468125" y2="2.9845" layer="119"/>
<rectangle x1="5.72008125" y1="2.97611875" x2="5.78611875" y2="2.9845" layer="119"/>
<rectangle x1="5.86231875" y1="2.97611875" x2="5.89788125" y2="2.9845" layer="119"/>
<rectangle x1="6.5151" y1="2.97611875" x2="6.76148125" y2="2.9845" layer="119"/>
<rectangle x1="7.4295" y1="2.97611875" x2="7.74191875" y2="2.9845" layer="119"/>
<rectangle x1="8.23468125" y1="2.97611875" x2="9.09828125" y2="2.9845" layer="119"/>
<rectangle x1="0.3937" y1="2.9845" x2="1.24968125" y2="2.99288125" layer="119"/>
<rectangle x1="1.74751875" y1="2.9845" x2="2.06248125" y2="2.99288125" layer="119"/>
<rectangle x1="2.73811875" y1="2.9845" x2="2.97688125" y2="2.99288125" layer="119"/>
<rectangle x1="3.60171875" y1="2.9845" x2="3.65251875" y2="2.99288125" layer="119"/>
<rectangle x1="3.68808125" y1="2.9845" x2="3.7465" y2="2.99288125" layer="119"/>
<rectangle x1="3.8227" y1="2.9845" x2="3.85571875" y2="2.99288125" layer="119"/>
<rectangle x1="4.4069" y1="2.9845" x2="4.4323" y2="2.99288125" layer="119"/>
<rectangle x1="5.10031875" y1="2.9845" x2="5.13588125" y2="2.99288125" layer="119"/>
<rectangle x1="5.64388125" y1="2.9845" x2="5.69468125" y2="2.99288125" layer="119"/>
<rectangle x1="5.72008125" y1="2.9845" x2="5.78611875" y2="2.99288125" layer="119"/>
<rectangle x1="5.86231875" y1="2.9845" x2="5.89788125" y2="2.99288125" layer="119"/>
<rectangle x1="6.5151" y1="2.9845" x2="6.76148125" y2="2.99288125" layer="119"/>
<rectangle x1="7.4295" y1="2.9845" x2="7.74191875" y2="2.99288125" layer="119"/>
<rectangle x1="8.23468125" y1="2.9845" x2="9.09828125" y2="2.99288125" layer="119"/>
<rectangle x1="0.3937" y1="2.99288125" x2="1.24968125" y2="3.00151875" layer="119"/>
<rectangle x1="1.74751875" y1="2.99288125" x2="2.06248125" y2="3.00151875" layer="119"/>
<rectangle x1="2.73811875" y1="2.99288125" x2="2.97688125" y2="3.00151875" layer="119"/>
<rectangle x1="3.60171875" y1="2.99288125" x2="3.65251875" y2="3.00151875" layer="119"/>
<rectangle x1="3.68808125" y1="2.99288125" x2="3.7465" y2="3.00151875" layer="119"/>
<rectangle x1="3.8227" y1="2.99288125" x2="3.85571875" y2="3.00151875" layer="119"/>
<rectangle x1="4.4069" y1="2.99288125" x2="4.4323" y2="3.00151875" layer="119"/>
<rectangle x1="5.10031875" y1="2.99288125" x2="5.13588125" y2="3.00151875" layer="119"/>
<rectangle x1="5.64388125" y1="2.99288125" x2="5.69468125" y2="3.00151875" layer="119"/>
<rectangle x1="5.72008125" y1="2.99288125" x2="5.78611875" y2="3.00151875" layer="119"/>
<rectangle x1="5.86231875" y1="2.99288125" x2="5.89788125" y2="3.00151875" layer="119"/>
<rectangle x1="6.5151" y1="2.99288125" x2="6.76148125" y2="3.00151875" layer="119"/>
<rectangle x1="7.4295" y1="2.99288125" x2="7.74191875" y2="3.00151875" layer="119"/>
<rectangle x1="8.23468125" y1="2.99288125" x2="9.09828125" y2="3.00151875" layer="119"/>
<rectangle x1="0.3937" y1="3.00151875" x2="1.24968125" y2="3.0099" layer="119"/>
<rectangle x1="1.74751875" y1="3.00151875" x2="2.06248125" y2="3.0099" layer="119"/>
<rectangle x1="2.73811875" y1="3.00151875" x2="2.97688125" y2="3.0099" layer="119"/>
<rectangle x1="3.60171875" y1="3.00151875" x2="3.65251875" y2="3.0099" layer="119"/>
<rectangle x1="3.68808125" y1="3.00151875" x2="3.7465" y2="3.0099" layer="119"/>
<rectangle x1="3.8227" y1="3.00151875" x2="3.85571875" y2="3.0099" layer="119"/>
<rectangle x1="4.4069" y1="3.00151875" x2="4.4323" y2="3.0099" layer="119"/>
<rectangle x1="5.0165" y1="3.00151875" x2="5.02411875" y2="3.0099" layer="119"/>
<rectangle x1="5.10031875" y1="3.00151875" x2="5.13588125" y2="3.0099" layer="119"/>
<rectangle x1="5.64388125" y1="3.00151875" x2="5.69468125" y2="3.0099" layer="119"/>
<rectangle x1="5.72008125" y1="3.00151875" x2="5.78611875" y2="3.0099" layer="119"/>
<rectangle x1="5.86231875" y1="3.00151875" x2="5.89788125" y2="3.0099" layer="119"/>
<rectangle x1="6.5151" y1="3.00151875" x2="6.76148125" y2="3.0099" layer="119"/>
<rectangle x1="7.4295" y1="3.00151875" x2="7.74191875" y2="3.0099" layer="119"/>
<rectangle x1="8.23468125" y1="3.00151875" x2="9.09828125" y2="3.0099" layer="119"/>
<rectangle x1="0.3937" y1="3.0099" x2="1.24968125" y2="3.01828125" layer="119"/>
<rectangle x1="1.74751875" y1="3.0099" x2="2.06248125" y2="3.01828125" layer="119"/>
<rectangle x1="2.73811875" y1="3.0099" x2="2.97688125" y2="3.01828125" layer="119"/>
<rectangle x1="3.60171875" y1="3.0099" x2="3.65251875" y2="3.01828125" layer="119"/>
<rectangle x1="3.68808125" y1="3.0099" x2="3.7465" y2="3.01828125" layer="119"/>
<rectangle x1="3.8227" y1="3.0099" x2="3.85571875" y2="3.01828125" layer="119"/>
<rectangle x1="4.4069" y1="3.0099" x2="4.4323" y2="3.01828125" layer="119"/>
<rectangle x1="4.4831" y1="3.0099" x2="4.54151875" y2="3.01828125" layer="119"/>
<rectangle x1="4.5593" y1="3.0099" x2="4.6609" y2="3.01828125" layer="119"/>
<rectangle x1="4.67868125" y1="3.0099" x2="4.70408125" y2="3.01828125" layer="119"/>
<rectangle x1="4.72948125" y1="3.0099" x2="4.99871875" y2="3.01828125" layer="119"/>
<rectangle x1="5.0165" y1="3.0099" x2="5.05968125" y2="3.01828125" layer="119"/>
<rectangle x1="5.10031875" y1="3.0099" x2="5.13588125" y2="3.01828125" layer="119"/>
<rectangle x1="5.64388125" y1="3.0099" x2="5.69468125" y2="3.01828125" layer="119"/>
<rectangle x1="5.72008125" y1="3.0099" x2="5.78611875" y2="3.01828125" layer="119"/>
<rectangle x1="5.86231875" y1="3.0099" x2="5.89788125" y2="3.01828125" layer="119"/>
<rectangle x1="6.5151" y1="3.0099" x2="6.76148125" y2="3.01828125" layer="119"/>
<rectangle x1="7.4295" y1="3.0099" x2="7.74191875" y2="3.01828125" layer="119"/>
<rectangle x1="8.23468125" y1="3.0099" x2="9.09828125" y2="3.01828125" layer="119"/>
<rectangle x1="0.3937" y1="3.01828125" x2="1.24968125" y2="3.02691875" layer="119"/>
<rectangle x1="1.74751875" y1="3.01828125" x2="2.06248125" y2="3.02691875" layer="119"/>
<rectangle x1="2.73811875" y1="3.01828125" x2="2.97688125" y2="3.02691875" layer="119"/>
<rectangle x1="3.60171875" y1="3.01828125" x2="3.65251875" y2="3.02691875" layer="119"/>
<rectangle x1="3.68808125" y1="3.01828125" x2="3.7465" y2="3.02691875" layer="119"/>
<rectangle x1="3.8227" y1="3.01828125" x2="3.85571875" y2="3.02691875" layer="119"/>
<rectangle x1="4.4069" y1="3.01828125" x2="4.4323" y2="3.02691875" layer="119"/>
<rectangle x1="4.4831" y1="3.01828125" x2="4.54151875" y2="3.02691875" layer="119"/>
<rectangle x1="4.5593" y1="3.01828125" x2="4.6609" y2="3.02691875" layer="119"/>
<rectangle x1="4.67868125" y1="3.01828125" x2="4.70408125" y2="3.02691875" layer="119"/>
<rectangle x1="4.72948125" y1="3.01828125" x2="4.99871875" y2="3.02691875" layer="119"/>
<rectangle x1="5.0165" y1="3.01828125" x2="5.05968125" y2="3.02691875" layer="119"/>
<rectangle x1="5.10031875" y1="3.01828125" x2="5.13588125" y2="3.02691875" layer="119"/>
<rectangle x1="5.64388125" y1="3.01828125" x2="5.69468125" y2="3.02691875" layer="119"/>
<rectangle x1="5.72008125" y1="3.01828125" x2="5.78611875" y2="3.02691875" layer="119"/>
<rectangle x1="5.86231875" y1="3.01828125" x2="5.89788125" y2="3.02691875" layer="119"/>
<rectangle x1="6.5151" y1="3.01828125" x2="6.76148125" y2="3.02691875" layer="119"/>
<rectangle x1="7.4295" y1="3.01828125" x2="7.74191875" y2="3.02691875" layer="119"/>
<rectangle x1="8.23468125" y1="3.01828125" x2="9.09828125" y2="3.02691875" layer="119"/>
<rectangle x1="0.3937" y1="3.02691875" x2="1.24968125" y2="3.0353" layer="119"/>
<rectangle x1="1.74751875" y1="3.02691875" x2="2.06248125" y2="3.0353" layer="119"/>
<rectangle x1="2.73811875" y1="3.02691875" x2="2.97688125" y2="3.0353" layer="119"/>
<rectangle x1="3.60171875" y1="3.02691875" x2="3.65251875" y2="3.0353" layer="119"/>
<rectangle x1="3.68808125" y1="3.02691875" x2="3.7465" y2="3.0353" layer="119"/>
<rectangle x1="3.8227" y1="3.02691875" x2="3.85571875" y2="3.0353" layer="119"/>
<rectangle x1="4.4069" y1="3.02691875" x2="4.4323" y2="3.0353" layer="119"/>
<rectangle x1="4.4831" y1="3.02691875" x2="4.54151875" y2="3.0353" layer="119"/>
<rectangle x1="4.5593" y1="3.02691875" x2="4.6609" y2="3.0353" layer="119"/>
<rectangle x1="4.67868125" y1="3.02691875" x2="4.70408125" y2="3.0353" layer="119"/>
<rectangle x1="4.72948125" y1="3.02691875" x2="4.99871875" y2="3.0353" layer="119"/>
<rectangle x1="5.0165" y1="3.02691875" x2="5.05968125" y2="3.0353" layer="119"/>
<rectangle x1="5.10031875" y1="3.02691875" x2="5.13588125" y2="3.0353" layer="119"/>
<rectangle x1="5.64388125" y1="3.02691875" x2="5.69468125" y2="3.0353" layer="119"/>
<rectangle x1="5.72008125" y1="3.02691875" x2="5.78611875" y2="3.0353" layer="119"/>
<rectangle x1="5.86231875" y1="3.02691875" x2="5.89788125" y2="3.0353" layer="119"/>
<rectangle x1="6.5151" y1="3.02691875" x2="6.76148125" y2="3.0353" layer="119"/>
<rectangle x1="7.4295" y1="3.02691875" x2="7.74191875" y2="3.0353" layer="119"/>
<rectangle x1="8.23468125" y1="3.02691875" x2="9.09828125" y2="3.0353" layer="119"/>
<rectangle x1="0.3937" y1="3.0353" x2="1.24968125" y2="3.04368125" layer="119"/>
<rectangle x1="1.74751875" y1="3.0353" x2="2.06248125" y2="3.04368125" layer="119"/>
<rectangle x1="2.73811875" y1="3.0353" x2="2.97688125" y2="3.04368125" layer="119"/>
<rectangle x1="3.60171875" y1="3.0353" x2="3.65251875" y2="3.04368125" layer="119"/>
<rectangle x1="3.68808125" y1="3.0353" x2="3.7465" y2="3.04368125" layer="119"/>
<rectangle x1="3.8227" y1="3.0353" x2="3.85571875" y2="3.04368125" layer="119"/>
<rectangle x1="4.4069" y1="3.0353" x2="4.54151875" y2="3.04368125" layer="119"/>
<rectangle x1="4.5593" y1="3.0353" x2="4.6609" y2="3.04368125" layer="119"/>
<rectangle x1="4.67868125" y1="3.0353" x2="4.70408125" y2="3.04368125" layer="119"/>
<rectangle x1="4.72948125" y1="3.0353" x2="4.99871875" y2="3.04368125" layer="119"/>
<rectangle x1="5.0165" y1="3.0353" x2="5.13588125" y2="3.04368125" layer="119"/>
<rectangle x1="5.64388125" y1="3.0353" x2="5.69468125" y2="3.04368125" layer="119"/>
<rectangle x1="5.72008125" y1="3.0353" x2="5.78611875" y2="3.04368125" layer="119"/>
<rectangle x1="5.86231875" y1="3.0353" x2="5.89788125" y2="3.04368125" layer="119"/>
<rectangle x1="6.5151" y1="3.0353" x2="6.76148125" y2="3.04368125" layer="119"/>
<rectangle x1="7.4295" y1="3.0353" x2="7.74191875" y2="3.04368125" layer="119"/>
<rectangle x1="8.23468125" y1="3.0353" x2="9.09828125" y2="3.04368125" layer="119"/>
<rectangle x1="0.3937" y1="3.04368125" x2="1.24968125" y2="3.05231875" layer="119"/>
<rectangle x1="1.74751875" y1="3.04368125" x2="2.06248125" y2="3.05231875" layer="119"/>
<rectangle x1="2.73811875" y1="3.04368125" x2="2.97688125" y2="3.05231875" layer="119"/>
<rectangle x1="3.60171875" y1="3.04368125" x2="3.65251875" y2="3.05231875" layer="119"/>
<rectangle x1="3.68808125" y1="3.04368125" x2="3.7465" y2="3.05231875" layer="119"/>
<rectangle x1="3.8227" y1="3.04368125" x2="3.85571875" y2="3.05231875" layer="119"/>
<rectangle x1="4.4069" y1="3.04368125" x2="4.54151875" y2="3.05231875" layer="119"/>
<rectangle x1="4.5593" y1="3.04368125" x2="4.6609" y2="3.05231875" layer="119"/>
<rectangle x1="4.67868125" y1="3.04368125" x2="4.70408125" y2="3.05231875" layer="119"/>
<rectangle x1="4.72948125" y1="3.04368125" x2="4.99871875" y2="3.05231875" layer="119"/>
<rectangle x1="5.0165" y1="3.04368125" x2="5.13588125" y2="3.05231875" layer="119"/>
<rectangle x1="5.64388125" y1="3.04368125" x2="5.69468125" y2="3.05231875" layer="119"/>
<rectangle x1="5.72008125" y1="3.04368125" x2="5.78611875" y2="3.05231875" layer="119"/>
<rectangle x1="5.86231875" y1="3.04368125" x2="5.89788125" y2="3.05231875" layer="119"/>
<rectangle x1="6.5151" y1="3.04368125" x2="6.76148125" y2="3.05231875" layer="119"/>
<rectangle x1="7.4295" y1="3.04368125" x2="7.74191875" y2="3.05231875" layer="119"/>
<rectangle x1="8.23468125" y1="3.04368125" x2="9.09828125" y2="3.05231875" layer="119"/>
<rectangle x1="0.3937" y1="3.05231875" x2="1.24968125" y2="3.0607" layer="119"/>
<rectangle x1="1.74751875" y1="3.05231875" x2="2.06248125" y2="3.0607" layer="119"/>
<rectangle x1="2.73811875" y1="3.05231875" x2="2.97688125" y2="3.0607" layer="119"/>
<rectangle x1="3.60171875" y1="3.05231875" x2="3.65251875" y2="3.0607" layer="119"/>
<rectangle x1="3.68808125" y1="3.05231875" x2="3.7465" y2="3.0607" layer="119"/>
<rectangle x1="3.8227" y1="3.05231875" x2="3.85571875" y2="3.0607" layer="119"/>
<rectangle x1="4.4069" y1="3.05231875" x2="4.54151875" y2="3.0607" layer="119"/>
<rectangle x1="4.5593" y1="3.05231875" x2="4.6609" y2="3.0607" layer="119"/>
<rectangle x1="4.67868125" y1="3.05231875" x2="4.70408125" y2="3.0607" layer="119"/>
<rectangle x1="4.72948125" y1="3.05231875" x2="4.99871875" y2="3.0607" layer="119"/>
<rectangle x1="5.0165" y1="3.05231875" x2="5.13588125" y2="3.0607" layer="119"/>
<rectangle x1="5.64388125" y1="3.05231875" x2="5.69468125" y2="3.0607" layer="119"/>
<rectangle x1="5.72008125" y1="3.05231875" x2="5.78611875" y2="3.0607" layer="119"/>
<rectangle x1="5.86231875" y1="3.05231875" x2="5.89788125" y2="3.0607" layer="119"/>
<rectangle x1="6.5151" y1="3.05231875" x2="6.76148125" y2="3.0607" layer="119"/>
<rectangle x1="7.4295" y1="3.05231875" x2="7.74191875" y2="3.0607" layer="119"/>
<rectangle x1="8.23468125" y1="3.05231875" x2="9.09828125" y2="3.0607" layer="119"/>
<rectangle x1="0.3937" y1="3.0607" x2="1.24968125" y2="3.06908125" layer="119"/>
<rectangle x1="1.74751875" y1="3.0607" x2="2.06248125" y2="3.06908125" layer="119"/>
<rectangle x1="2.73811875" y1="3.0607" x2="2.97688125" y2="3.06908125" layer="119"/>
<rectangle x1="3.60171875" y1="3.0607" x2="3.65251875" y2="3.06908125" layer="119"/>
<rectangle x1="3.68808125" y1="3.0607" x2="3.7465" y2="3.06908125" layer="119"/>
<rectangle x1="3.8227" y1="3.0607" x2="3.85571875" y2="3.06908125" layer="119"/>
<rectangle x1="4.4069" y1="3.0607" x2="4.54151875" y2="3.06908125" layer="119"/>
<rectangle x1="4.5593" y1="3.0607" x2="4.6609" y2="3.06908125" layer="119"/>
<rectangle x1="4.67868125" y1="3.0607" x2="4.70408125" y2="3.06908125" layer="119"/>
<rectangle x1="4.72948125" y1="3.0607" x2="4.99871875" y2="3.06908125" layer="119"/>
<rectangle x1="5.0165" y1="3.0607" x2="5.13588125" y2="3.06908125" layer="119"/>
<rectangle x1="5.64388125" y1="3.0607" x2="5.69468125" y2="3.06908125" layer="119"/>
<rectangle x1="5.72008125" y1="3.0607" x2="5.78611875" y2="3.06908125" layer="119"/>
<rectangle x1="5.86231875" y1="3.0607" x2="5.89788125" y2="3.06908125" layer="119"/>
<rectangle x1="6.5151" y1="3.0607" x2="6.76148125" y2="3.06908125" layer="119"/>
<rectangle x1="7.4295" y1="3.0607" x2="7.74191875" y2="3.06908125" layer="119"/>
<rectangle x1="8.23468125" y1="3.0607" x2="9.09828125" y2="3.06908125" layer="119"/>
<rectangle x1="0.3937" y1="3.06908125" x2="1.24968125" y2="3.07771875" layer="119"/>
<rectangle x1="1.74751875" y1="3.06908125" x2="2.06248125" y2="3.07771875" layer="119"/>
<rectangle x1="2.73811875" y1="3.06908125" x2="2.97688125" y2="3.07771875" layer="119"/>
<rectangle x1="3.60171875" y1="3.06908125" x2="3.65251875" y2="3.07771875" layer="119"/>
<rectangle x1="3.68808125" y1="3.06908125" x2="3.75411875" y2="3.07771875" layer="119"/>
<rectangle x1="3.8227" y1="3.06908125" x2="3.85571875" y2="3.07771875" layer="119"/>
<rectangle x1="4.4069" y1="3.06908125" x2="4.54151875" y2="3.07771875" layer="119"/>
<rectangle x1="4.5593" y1="3.06908125" x2="4.6609" y2="3.07771875" layer="119"/>
<rectangle x1="4.67868125" y1="3.06908125" x2="4.70408125" y2="3.07771875" layer="119"/>
<rectangle x1="4.72948125" y1="3.06908125" x2="4.99871875" y2="3.07771875" layer="119"/>
<rectangle x1="5.0165" y1="3.06908125" x2="5.13588125" y2="3.07771875" layer="119"/>
<rectangle x1="5.64388125" y1="3.06908125" x2="5.69468125" y2="3.07771875" layer="119"/>
<rectangle x1="5.72008125" y1="3.06908125" x2="5.79628125" y2="3.07771875" layer="119"/>
<rectangle x1="5.86231875" y1="3.06908125" x2="5.89788125" y2="3.07771875" layer="119"/>
<rectangle x1="6.5151" y1="3.06908125" x2="6.76148125" y2="3.07771875" layer="119"/>
<rectangle x1="7.4295" y1="3.06908125" x2="7.74191875" y2="3.07771875" layer="119"/>
<rectangle x1="8.23468125" y1="3.06908125" x2="9.09828125" y2="3.07771875" layer="119"/>
<rectangle x1="1.74751875" y1="3.07771875" x2="2.06248125" y2="3.0861" layer="119"/>
<rectangle x1="2.73811875" y1="3.07771875" x2="2.97688125" y2="3.0861" layer="119"/>
<rectangle x1="3.60171875" y1="3.07771875" x2="3.65251875" y2="3.0861" layer="119"/>
<rectangle x1="3.71348125" y1="3.07771875" x2="3.76428125" y2="3.0861" layer="119"/>
<rectangle x1="3.8227" y1="3.07771875" x2="3.85571875" y2="3.0861" layer="119"/>
<rectangle x1="4.4831" y1="3.07771875" x2="4.54151875" y2="3.0861" layer="119"/>
<rectangle x1="4.5593" y1="3.07771875" x2="4.6609" y2="3.0861" layer="119"/>
<rectangle x1="4.67868125" y1="3.07771875" x2="4.70408125" y2="3.0861" layer="119"/>
<rectangle x1="4.72948125" y1="3.07771875" x2="4.99871875" y2="3.0861" layer="119"/>
<rectangle x1="5.0165" y1="3.07771875" x2="5.05968125" y2="3.0861" layer="119"/>
<rectangle x1="5.64388125" y1="3.07771875" x2="5.69468125" y2="3.0861" layer="119"/>
<rectangle x1="5.74548125" y1="3.07771875" x2="5.8039" y2="3.0861" layer="119"/>
<rectangle x1="5.86231875" y1="3.07771875" x2="5.89788125" y2="3.0861" layer="119"/>
<rectangle x1="6.5151" y1="3.07771875" x2="6.76148125" y2="3.0861" layer="119"/>
<rectangle x1="7.4295" y1="3.07771875" x2="7.74191875" y2="3.0861" layer="119"/>
<rectangle x1="1.74751875" y1="3.0861" x2="2.06248125" y2="3.09448125" layer="119"/>
<rectangle x1="2.73811875" y1="3.0861" x2="2.97688125" y2="3.09448125" layer="119"/>
<rectangle x1="3.60171875" y1="3.0861" x2="3.65251875" y2="3.09448125" layer="119"/>
<rectangle x1="3.71348125" y1="3.0861" x2="3.75411875" y2="3.09448125" layer="119"/>
<rectangle x1="3.8227" y1="3.0861" x2="3.85571875" y2="3.09448125" layer="119"/>
<rectangle x1="4.4831" y1="3.0861" x2="4.54151875" y2="3.09448125" layer="119"/>
<rectangle x1="4.5593" y1="3.0861" x2="4.6609" y2="3.09448125" layer="119"/>
<rectangle x1="4.67868125" y1="3.0861" x2="4.70408125" y2="3.09448125" layer="119"/>
<rectangle x1="4.72948125" y1="3.0861" x2="4.99871875" y2="3.09448125" layer="119"/>
<rectangle x1="5.0165" y1="3.0861" x2="5.05968125" y2="3.09448125" layer="119"/>
<rectangle x1="5.64388125" y1="3.0861" x2="5.69468125" y2="3.09448125" layer="119"/>
<rectangle x1="5.74548125" y1="3.0861" x2="5.79628125" y2="3.09448125" layer="119"/>
<rectangle x1="5.86231875" y1="3.0861" x2="5.89788125" y2="3.09448125" layer="119"/>
<rectangle x1="6.5151" y1="3.0861" x2="6.76148125" y2="3.09448125" layer="119"/>
<rectangle x1="7.4295" y1="3.0861" x2="7.74191875" y2="3.09448125" layer="119"/>
<rectangle x1="1.74751875" y1="3.09448125" x2="2.06248125" y2="3.10311875" layer="119"/>
<rectangle x1="2.73811875" y1="3.09448125" x2="2.97688125" y2="3.10311875" layer="119"/>
<rectangle x1="3.60171875" y1="3.09448125" x2="3.65251875" y2="3.10311875" layer="119"/>
<rectangle x1="3.70331875" y1="3.09448125" x2="3.7465" y2="3.10311875" layer="119"/>
<rectangle x1="3.8227" y1="3.09448125" x2="3.85571875" y2="3.10311875" layer="119"/>
<rectangle x1="4.4831" y1="3.09448125" x2="4.54151875" y2="3.10311875" layer="119"/>
<rectangle x1="4.5593" y1="3.09448125" x2="4.6609" y2="3.10311875" layer="119"/>
<rectangle x1="4.67868125" y1="3.09448125" x2="4.70408125" y2="3.10311875" layer="119"/>
<rectangle x1="4.72948125" y1="3.09448125" x2="4.99871875" y2="3.10311875" layer="119"/>
<rectangle x1="5.0165" y1="3.09448125" x2="5.05968125" y2="3.10311875" layer="119"/>
<rectangle x1="5.64388125" y1="3.09448125" x2="5.69468125" y2="3.10311875" layer="119"/>
<rectangle x1="5.74548125" y1="3.09448125" x2="5.7785" y2="3.10311875" layer="119"/>
<rectangle x1="5.86231875" y1="3.09448125" x2="5.89788125" y2="3.10311875" layer="119"/>
<rectangle x1="6.5151" y1="3.09448125" x2="6.76148125" y2="3.10311875" layer="119"/>
<rectangle x1="7.4295" y1="3.09448125" x2="7.74191875" y2="3.10311875" layer="119"/>
<rectangle x1="1.74751875" y1="3.10311875" x2="2.06248125" y2="3.1115" layer="119"/>
<rectangle x1="2.73811875" y1="3.10311875" x2="2.97688125" y2="3.1115" layer="119"/>
<rectangle x1="3.60171875" y1="3.10311875" x2="3.65251875" y2="3.1115" layer="119"/>
<rectangle x1="3.6957" y1="3.10311875" x2="3.72871875" y2="3.1115" layer="119"/>
<rectangle x1="3.8227" y1="3.10311875" x2="3.85571875" y2="3.1115" layer="119"/>
<rectangle x1="4.4831" y1="3.10311875" x2="4.54151875" y2="3.1115" layer="119"/>
<rectangle x1="4.5593" y1="3.10311875" x2="4.6609" y2="3.1115" layer="119"/>
<rectangle x1="4.67868125" y1="3.10311875" x2="4.70408125" y2="3.1115" layer="119"/>
<rectangle x1="4.72948125" y1="3.10311875" x2="4.99871875" y2="3.1115" layer="119"/>
<rectangle x1="5.0165" y1="3.10311875" x2="5.05968125" y2="3.1115" layer="119"/>
<rectangle x1="5.64388125" y1="3.10311875" x2="5.69468125" y2="3.1115" layer="119"/>
<rectangle x1="5.73531875" y1="3.10311875" x2="5.77088125" y2="3.1115" layer="119"/>
<rectangle x1="5.86231875" y1="3.10311875" x2="5.89788125" y2="3.1115" layer="119"/>
<rectangle x1="6.5151" y1="3.10311875" x2="6.76148125" y2="3.1115" layer="119"/>
<rectangle x1="7.4295" y1="3.10311875" x2="7.74191875" y2="3.1115" layer="119"/>
<rectangle x1="1.74751875" y1="3.1115" x2="2.06248125" y2="3.11988125" layer="119"/>
<rectangle x1="2.73811875" y1="3.1115" x2="2.97688125" y2="3.11988125" layer="119"/>
<rectangle x1="3.60171875" y1="3.1115" x2="3.65251875" y2="3.11988125" layer="119"/>
<rectangle x1="3.68808125" y1="3.1115" x2="3.7211" y2="3.11988125" layer="119"/>
<rectangle x1="3.8227" y1="3.1115" x2="3.85571875" y2="3.11988125" layer="119"/>
<rectangle x1="4.4831" y1="3.1115" x2="4.54151875" y2="3.11988125" layer="119"/>
<rectangle x1="4.5593" y1="3.1115" x2="4.6609" y2="3.11988125" layer="119"/>
<rectangle x1="4.67868125" y1="3.1115" x2="4.70408125" y2="3.11988125" layer="119"/>
<rectangle x1="4.72948125" y1="3.1115" x2="4.99871875" y2="3.11988125" layer="119"/>
<rectangle x1="5.0165" y1="3.1115" x2="5.05968125" y2="3.11988125" layer="119"/>
<rectangle x1="5.64388125" y1="3.1115" x2="5.69468125" y2="3.11988125" layer="119"/>
<rectangle x1="5.72008125" y1="3.1115" x2="5.7531" y2="3.11988125" layer="119"/>
<rectangle x1="5.86231875" y1="3.1115" x2="5.89788125" y2="3.11988125" layer="119"/>
<rectangle x1="6.5151" y1="3.1115" x2="6.76148125" y2="3.11988125" layer="119"/>
<rectangle x1="7.4295" y1="3.1115" x2="7.74191875" y2="3.11988125" layer="119"/>
<rectangle x1="1.74751875" y1="3.11988125" x2="2.06248125" y2="3.12851875" layer="119"/>
<rectangle x1="2.73811875" y1="3.11988125" x2="2.97688125" y2="3.12851875" layer="119"/>
<rectangle x1="3.60171875" y1="3.11988125" x2="3.85571875" y2="3.12851875" layer="119"/>
<rectangle x1="4.4831" y1="3.11988125" x2="4.54151875" y2="3.12851875" layer="119"/>
<rectangle x1="4.5593" y1="3.11988125" x2="4.6609" y2="3.12851875" layer="119"/>
<rectangle x1="4.67868125" y1="3.11988125" x2="4.70408125" y2="3.12851875" layer="119"/>
<rectangle x1="4.72948125" y1="3.11988125" x2="4.99871875" y2="3.12851875" layer="119"/>
<rectangle x1="5.0165" y1="3.11988125" x2="5.05968125" y2="3.12851875" layer="119"/>
<rectangle x1="5.64388125" y1="3.11988125" x2="5.89788125" y2="3.12851875" layer="119"/>
<rectangle x1="6.5151" y1="3.11988125" x2="6.76148125" y2="3.12851875" layer="119"/>
<rectangle x1="7.4295" y1="3.11988125" x2="7.74191875" y2="3.12851875" layer="119"/>
<rectangle x1="1.74751875" y1="3.12851875" x2="2.06248125" y2="3.1369" layer="119"/>
<rectangle x1="2.73811875" y1="3.12851875" x2="2.97688125" y2="3.1369" layer="119"/>
<rectangle x1="3.60171875" y1="3.12851875" x2="3.85571875" y2="3.1369" layer="119"/>
<rectangle x1="5.64388125" y1="3.12851875" x2="5.89788125" y2="3.1369" layer="119"/>
<rectangle x1="6.5151" y1="3.12851875" x2="6.76148125" y2="3.1369" layer="119"/>
<rectangle x1="7.4295" y1="3.12851875" x2="7.74191875" y2="3.1369" layer="119"/>
<rectangle x1="1.74751875" y1="3.1369" x2="2.06248125" y2="3.14528125" layer="119"/>
<rectangle x1="2.73811875" y1="3.1369" x2="2.97688125" y2="3.14528125" layer="119"/>
<rectangle x1="3.60171875" y1="3.1369" x2="3.85571875" y2="3.14528125" layer="119"/>
<rectangle x1="5.64388125" y1="3.1369" x2="5.89788125" y2="3.14528125" layer="119"/>
<rectangle x1="6.5151" y1="3.1369" x2="6.76148125" y2="3.14528125" layer="119"/>
<rectangle x1="7.4295" y1="3.1369" x2="7.74191875" y2="3.14528125" layer="119"/>
<rectangle x1="1.74751875" y1="3.14528125" x2="2.06248125" y2="3.15391875" layer="119"/>
<rectangle x1="2.73811875" y1="3.14528125" x2="2.97688125" y2="3.15391875" layer="119"/>
<rectangle x1="3.60171875" y1="3.14528125" x2="3.85571875" y2="3.15391875" layer="119"/>
<rectangle x1="5.64388125" y1="3.14528125" x2="5.89788125" y2="3.15391875" layer="119"/>
<rectangle x1="6.5151" y1="3.14528125" x2="6.76148125" y2="3.15391875" layer="119"/>
<rectangle x1="7.4295" y1="3.14528125" x2="7.74191875" y2="3.15391875" layer="119"/>
<rectangle x1="1.74751875" y1="3.15391875" x2="2.06248125" y2="3.1623" layer="119"/>
<rectangle x1="2.73811875" y1="3.15391875" x2="2.97688125" y2="3.1623" layer="119"/>
<rectangle x1="3.60171875" y1="3.15391875" x2="3.85571875" y2="3.1623" layer="119"/>
<rectangle x1="5.64388125" y1="3.15391875" x2="5.89788125" y2="3.1623" layer="119"/>
<rectangle x1="6.5151" y1="3.15391875" x2="6.76148125" y2="3.1623" layer="119"/>
<rectangle x1="7.4295" y1="3.15391875" x2="7.74191875" y2="3.1623" layer="119"/>
<rectangle x1="1.74751875" y1="3.1623" x2="2.06248125" y2="3.17068125" layer="119"/>
<rectangle x1="3.5941" y1="3.1623" x2="3.85571875" y2="3.17068125" layer="119"/>
<rectangle x1="5.64388125" y1="3.1623" x2="5.89788125" y2="3.17068125" layer="119"/>
<rectangle x1="7.4295" y1="3.1623" x2="7.74191875" y2="3.17068125" layer="119"/>
<rectangle x1="1.74751875" y1="3.17068125" x2="2.06248125" y2="3.17931875" layer="119"/>
<rectangle x1="3.5941" y1="3.17068125" x2="3.85571875" y2="3.17931875" layer="119"/>
<rectangle x1="5.64388125" y1="3.17068125" x2="5.89788125" y2="3.17931875" layer="119"/>
<rectangle x1="7.4295" y1="3.17068125" x2="7.74191875" y2="3.17931875" layer="119"/>
<rectangle x1="1.74751875" y1="3.17931875" x2="2.06248125" y2="3.1877" layer="119"/>
<rectangle x1="3.60171875" y1="3.17931875" x2="3.85571875" y2="3.1877" layer="119"/>
<rectangle x1="5.63371875" y1="3.17931875" x2="5.89788125" y2="3.1877" layer="119"/>
<rectangle x1="7.4295" y1="3.17931875" x2="7.74191875" y2="3.1877" layer="119"/>
<rectangle x1="1.74751875" y1="3.1877" x2="2.06248125" y2="3.19608125" layer="119"/>
<rectangle x1="3.60171875" y1="3.1877" x2="3.85571875" y2="3.19608125" layer="119"/>
<rectangle x1="5.64388125" y1="3.1877" x2="5.88771875" y2="3.19608125" layer="119"/>
<rectangle x1="7.4295" y1="3.1877" x2="7.74191875" y2="3.19608125" layer="119"/>
<rectangle x1="1.74751875" y1="3.19608125" x2="2.06248125" y2="3.20471875" layer="119"/>
<rectangle x1="3.6195" y1="3.19608125" x2="3.84048125" y2="3.20471875" layer="119"/>
<rectangle x1="5.6515" y1="3.19608125" x2="5.8801" y2="3.20471875" layer="119"/>
<rectangle x1="7.4295" y1="3.19608125" x2="7.74191875" y2="3.20471875" layer="119"/>
<rectangle x1="1.74751875" y1="3.20471875" x2="2.06248125" y2="3.2131" layer="119"/>
<rectangle x1="3.63728125" y1="3.20471875" x2="3.8227" y2="3.2131" layer="119"/>
<rectangle x1="5.66928125" y1="3.20471875" x2="5.86231875" y2="3.2131" layer="119"/>
<rectangle x1="7.4295" y1="3.20471875" x2="7.74191875" y2="3.2131" layer="119"/>
<rectangle x1="1.74751875" y1="3.2131" x2="2.06248125" y2="3.22148125" layer="119"/>
<rectangle x1="3.66268125" y1="3.2131" x2="3.7973" y2="3.22148125" layer="119"/>
<rectangle x1="5.69468125" y1="3.2131" x2="5.83691875" y2="3.22148125" layer="119"/>
<rectangle x1="7.4295" y1="3.2131" x2="7.74191875" y2="3.22148125" layer="119"/>
<rectangle x1="1.74751875" y1="3.22148125" x2="2.06248125" y2="3.23011875" layer="119"/>
<rectangle x1="7.4295" y1="3.22148125" x2="7.74191875" y2="3.23011875" layer="119"/>
<rectangle x1="1.74751875" y1="3.23011875" x2="2.06248125" y2="3.2385" layer="119"/>
<rectangle x1="7.4295" y1="3.23011875" x2="7.74191875" y2="3.2385" layer="119"/>
<rectangle x1="1.74751875" y1="3.2385" x2="2.06248125" y2="3.24688125" layer="119"/>
<rectangle x1="7.4295" y1="3.2385" x2="7.74191875" y2="3.24688125" layer="119"/>
<rectangle x1="1.74751875" y1="3.24688125" x2="2.06248125" y2="3.25551875" layer="119"/>
<rectangle x1="7.4295" y1="3.24688125" x2="7.74191875" y2="3.25551875" layer="119"/>
</package>
</packages>
<symbols>
<symbol name="DOT">
<circle x="0" y="0" radius="2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="LOGO-TOP-9.5MMX4.5MM">
<text x="0" y="0" size="1.778" layer="94" font="vector">LOGO-TOP-9.5MMX4.5MM</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="FIDUCIAL">
<description>For use by pick and place machines to calibrate the vision/machine, 1mm
&lt;p&gt;By microbuilder.eu&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="DOT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FIDUCIAL_1MM">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LOGO-TOP-9.5MMX4.5MM">
<gates>
<gate name="G$1" symbol="LOGO-TOP-9.5MMX4.5MM" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LOGO-TOP-9.5MMX4.5MM">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Capacitors">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find resistors, capacitors, inductors, test points, jumper pads, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="0603-CAP">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<wire x1="0" y1="0.027940625" x2="0" y2="-0.027940625" width="0.381" layer="21"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="CAP-PTH-SMALL">
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.7" diameter="1.651"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651"/>
<text x="0.508" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="0.254" y="-1.524" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="CAP-PTH-SMALL2">
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="3.81" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.7" diameter="1.651"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651"/>
<text x="-1.27" y="1.905" size="0.6096" layer="25">&gt;Name</text>
<text x="-1.27" y="-2.54" size="0.6096" layer="27">&gt;Value</text>
</package>
<package name="0805">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="21"/>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="CAP-PTH-LARGE">
<wire x1="0" y1="0.635" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.2032" layer="21"/>
<pad name="1" x="-4.826" y="0" drill="0.9" diameter="1.905"/>
<pad name="2" x="4.572" y="0" drill="0.9" diameter="1.905"/>
<text x="-0.762" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.016" y="-1.524" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="GRM43D">
<wire x1="2.25" y1="1.6" x2="1.1" y2="1.6" width="0.127" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1.1" y2="1.6" width="0.127" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-2.25" y2="1.6" width="0.127" layer="51"/>
<wire x1="-2.25" y1="1.6" x2="-2.25" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-2.25" y1="-1.6" x2="-1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="2.25" y2="-1.6" width="0.127" layer="51"/>
<wire x1="2.25" y1="-1.6" x2="2.25" y2="1.6" width="0.127" layer="51"/>
<wire x1="1.1" y1="1.6" x2="1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-2.3" y1="1.8" x2="2.3" y2="1.8" width="0.127" layer="21"/>
<wire x1="-2.3" y1="-1.8" x2="2.3" y2="-1.8" width="0.127" layer="21"/>
<smd name="A" x="1.927" y="0" dx="3.2" dy="1.65" layer="1" rot="R90"/>
<smd name="C" x="-1.927" y="0" dx="3.2" dy="1.65" layer="1" rot="R90"/>
<text x="-2" y="2" size="0.4064" layer="25">&gt;NAME</text>
<text x="0" y="-2" size="0.4064" layer="27" rot="R180">&gt;VALUE</text>
<rectangle x1="-2.2" y1="-1.6" x2="-1.1" y2="1.6" layer="51"/>
<rectangle x1="1.1" y1="-1.6" x2="2.2" y2="1.6" layer="51"/>
</package>
<package name="0402-CAP">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="0" y1="0.0305" x2="0" y2="-0.0305" width="0.4064" layer="21"/>
<smd name="1" x="-0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<smd name="2" x="0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.143" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="CAP-PTH-5MM">
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="0.7" diameter="1.651"/>
<pad name="2" x="2.5" y="0" drill="0.7" diameter="1.651"/>
<text x="-0.762" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.016" y="-1.524" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="AXIAL-5MM">
<wire x1="-1.14" y1="0.762" x2="1.14" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0.762" x2="1.14" y2="0" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0" x2="1.14" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="-0.762" x2="-1.14" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="-0.762" x2="-1.14" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="0" x2="-1.14" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0" x2="1.394" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="0" x2="-1.394" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-2.5" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="2.5" y="0" drill="0.9" diameter="1.8796"/>
<text x="-2.54" y="1.17" size="0.4" layer="25">&gt;Name</text>
<text x="-1.032" y="-0.208" size="0.4" layer="21" ratio="15">&gt;Value</text>
</package>
<package name="1210">
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.3" x2="1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="-1.3" x2="-1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-1.3" x2="-1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.2032" layer="21"/>
<wire x1="-1.6" y1="-1.3" x2="1.6" y2="-1.3" width="0.2032" layer="21"/>
<smd name="1" x="-1.6" y="0" dx="1.2" dy="2" layer="1"/>
<smd name="2" x="1.6" y="0" dx="1.2" dy="2" layer="1"/>
<text x="-0.8" y="0.5" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.9" y="-0.7" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="CTZ3">
<description>CTZ3 Series land pattern for variable capacitor - CTZ3E-50C-W1-PF</description>
<wire x1="-1.6" y1="1.4" x2="-1.6" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-2.25" x2="1.6" y2="-2.25" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.4" x2="1.6" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-0.5" y1="0" x2="0.5" y2="0" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.4" x2="-1" y2="2.2" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.4" x2="1" y2="2.2" width="0.127" layer="51"/>
<wire x1="-1" y1="2.2" x2="1" y2="2.2" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="0" y2="-0.8" width="0.127" layer="51"/>
<wire x1="-0.8" y1="0" x2="0.8" y2="0" width="0.127" layer="51"/>
<wire x1="-1.05" y1="2.25" x2="-1.7" y2="1.45" width="0.127" layer="21"/>
<wire x1="-1.7" y1="1.45" x2="-1.7" y2="-2.35" width="0.127" layer="21"/>
<wire x1="-1.7" y1="-2.35" x2="-1.05" y2="-2.35" width="0.127" layer="21"/>
<wire x1="1.05" y1="2.25" x2="1.7" y2="1.4" width="0.127" layer="21"/>
<wire x1="1.7" y1="1.4" x2="1.7" y2="-2.35" width="0.127" layer="21"/>
<wire x1="1.7" y1="-2.35" x2="1.05" y2="-2.35" width="0.127" layer="21"/>
<smd name="+" x="0" y="2.05" dx="1.5" dy="1.2" layer="1"/>
<smd name="-" x="0" y="-2.05" dx="1.5" dy="1.2" layer="1"/>
<text x="-2" y="3" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2" y="-3.4" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="CAP-PTH-SMALL-KIT">
<description>&lt;h3&gt;CAP-PTH-SMALL-KIT&lt;/h3&gt;
Commonly used for small ceramic capacitors. Like our 0.1uF (http://www.sparkfun.com/products/8375) or 22pF caps (http://www.sparkfun.com/products/8571).&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.254" layer="21"/>
<wire x1="-2.667" y1="1.27" x2="2.667" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="1.27" x2="2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="-1.27" x2="-2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.667" y1="-1.27" x2="-2.667" y2="1.27" width="0.254" layer="21"/>
<pad name="1" x="-1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="2" x="1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<polygon width="0.127" layer="30">
<vertex x="-1.4021" y="-0.9475" curve="-90"/>
<vertex x="-2.357" y="-0.0178" curve="-90.011749"/>
<vertex x="-1.4046" y="0.9576" curve="-90"/>
<vertex x="-0.4546" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1.4046" y="-0.4395" curve="-90.012891"/>
<vertex x="-1.8491" y="-0.0153" curve="-90"/>
<vertex x="-1.4046" y="0.452" curve="-90"/>
<vertex x="-0.9627" y="-0.0051" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.397" y="-0.9475" curve="-90"/>
<vertex x="0.4421" y="-0.0178" curve="-90.011749"/>
<vertex x="1.3945" y="0.9576" curve="-90"/>
<vertex x="2.3445" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1.3945" y="-0.4395" curve="-90.012891"/>
<vertex x="0.95" y="-0.0153" curve="-90"/>
<vertex x="1.3945" y="0.452" curve="-90"/>
<vertex x="1.8364" y="-0.0051" curve="-90.012967"/>
</polygon>
</package>
<package name="CAP-PTH-SMALLEZ">
<description>This is the "EZ" version of the .1" spaced ceramic thru-hole cap.&lt;br&gt;
It has reduced top mask to make it harder to put the component on the wrong side of the board.</description>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="3.81" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.7" diameter="1.651" stop="no"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651" stop="no"/>
<text x="-1.27" y="1.905" size="0.6096" layer="25">&gt;Name</text>
<text x="-1.27" y="-2.54" size="0.6096" layer="27">&gt;Value</text>
<circle x="0" y="0" radius="0.898025" width="0" layer="30"/>
<circle x="2.54" y="0" radius="0.915809375" width="0" layer="30"/>
<circle x="0" y="0" radius="0.40160625" width="0" layer="29"/>
<circle x="2.54" y="0" radius="0.40160625" width="0" layer="29"/>
</package>
</packages>
<symbols>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAP" prefix="C" uservalue="yes">
<description>&lt;b&gt;Capacitor&lt;/b&gt;
Standard 0603 ceramic capacitor, and 0.1" leaded capacitor.</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="PTH" package="CAP-PTH-SMALL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH2" package="CAP-PTH-SMALL2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH3" package="CAP-PTH-LARGE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="GRM43D">
<connects>
<connect gate="G$1" pin="1" pad="A"/>
<connect gate="G$1" pin="2" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603-CAP" package="0603-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402-CAP" package="0402-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH1" package="CAP-PTH-5MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_" package="AXIAL-5MM">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ASMD" package="CTZ3">
<connects>
<connect gate="G$1" pin="1" pad="+"/>
<connect gate="G$1" pin="2" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KIT" package="CAP-PTH-SMALL-KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EZ" package="CAP-PTH-SMALLEZ">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="LETTER_L">
<frame x1="0" y1="0" x2="248.92" y2="185.42" columns="12" rows="17" layer="94" border-left="no" border-top="no" border-right="no" border-bottom="no"/>
</symbol>
<symbol name="DOCFIELD">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="87.63" y2="15.24" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="22.86" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="15.24" width="0.1016" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94">Date:</text>
<text x="12.7" y="1.27" size="2.54" layer="94">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94">&gt;SHEET</text>
<text x="88.9" y="11.43" size="2.54" layer="94">REV:</text>
<text x="1.27" y="19.05" size="2.54" layer="94">TITLE:</text>
<text x="1.27" y="11.43" size="2.54" layer="94">Document Number:</text>
<text x="17.78" y="19.05" size="2.54" layer="94">&gt;DRAWING_NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="LETTER_L" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
LETTER landscape</description>
<gates>
<gate name="G$1" symbol="LETTER_L" x="0" y="0"/>
<gate name="G$2" symbol="DOCFIELD" x="147.32" y="0" addlevel="must"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U1" library="SparkFun-LED" deviceset="LED-TRICOLOR-5050-IC" device="WS2811" value="WS2811"/>
<part name="C1" library="SparkFun-Capacitors" deviceset="CAP" device="0603-CAP"/>
<part name="X1" library="con-samtec" deviceset="SSW-103-02-S-S" device=""/>
<part name="X2" library="con-samtec" deviceset="SSW-103-02-S-S" device=""/>
<part name="U$1" library="ER" deviceset="FIDUCIAL" device=""/>
<part name="U$2" library="ER" deviceset="FIDUCIAL" device=""/>
<part name="FRAME1" library="frames" deviceset="LETTER_L" device=""/>
<part name="U$3" library="ER" deviceset="LOGO-TOP-9.5MMX4.5MM" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="114.3" y="-7.62" size="5.08" layer="97" font="vector" ratio="12">Edwin Robotics</text>
</plain>
<instances>
<instance part="U1" gate="U1" x="93.98" y="50.8"/>
<instance part="C1" gate="G$1" x="76.2" y="50.8"/>
<instance part="X1" gate="-1" x="38.1" y="55.88"/>
<instance part="X1" gate="-2" x="38.1" y="53.34"/>
<instance part="X1" gate="-3" x="38.1" y="50.8"/>
<instance part="X2" gate="-1" x="38.1" y="43.18"/>
<instance part="X2" gate="-2" x="38.1" y="40.64"/>
<instance part="X2" gate="-3" x="38.1" y="38.1"/>
<instance part="U$1" gate="G$1" x="200.66" y="-7.62"/>
<instance part="U$2" gate="G$1" x="205.74" y="-7.62"/>
<instance part="FRAME1" gate="G$1" x="-35.56" y="-33.02"/>
<instance part="FRAME1" gate="G$2" x="111.76" y="-33.02"/>
<instance part="U$3" gate="G$1" x="180.34" y="-2.54"/>
</instances>
<busses>
</busses>
<nets>
<net name="5V" class="0">
<segment>
<pinref part="X1" gate="-1" pin="1"/>
<wire x1="40.64" y1="55.88" x2="43.18" y2="55.88" width="0.1524" layer="91"/>
<label x="43.18" y="55.88" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="X2" gate="-3" pin="1"/>
<wire x1="40.64" y1="38.1" x2="43.18" y2="38.1" width="0.1524" layer="91"/>
<label x="43.18" y="38.1" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="U1" pin="VDD"/>
<wire x1="78.74" y1="55.88" x2="76.2" y2="55.88" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="76.2" y1="55.88" x2="71.12" y2="55.88" width="0.1524" layer="91"/>
<label x="71.12" y="55.88" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="DI" class="0">
<segment>
<pinref part="X1" gate="-2" pin="1"/>
<wire x1="40.64" y1="53.34" x2="43.18" y2="53.34" width="0.1524" layer="91"/>
<label x="43.18" y="53.34" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="U1" pin="DI"/>
<wire x1="106.68" y1="55.88" x2="109.22" y2="55.88" width="0.1524" layer="91"/>
<label x="109.22" y="55.88" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="X1" gate="-3" pin="1"/>
<wire x1="40.64" y1="50.8" x2="43.18" y2="50.8" width="0.1524" layer="91"/>
<label x="43.18" y="50.8" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="X2" gate="-1" pin="1"/>
<wire x1="40.64" y1="43.18" x2="43.18" y2="43.18" width="0.1524" layer="91"/>
<label x="43.18" y="43.18" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="U1" pin="VSS"/>
<wire x1="78.74" y1="48.26" x2="76.2" y2="48.26" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="76.2" y1="48.26" x2="71.12" y2="48.26" width="0.1524" layer="91"/>
<label x="71.12" y="48.26" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="DO" class="0">
<segment>
<pinref part="X2" gate="-2" pin="1"/>
<wire x1="40.64" y1="40.64" x2="43.18" y2="40.64" width="0.1524" layer="91"/>
<label x="43.18" y="40.64" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="U1" pin="DO"/>
<wire x1="106.68" y1="48.26" x2="109.22" y2="48.26" width="0.1524" layer="91"/>
<label x="109.22" y="48.26" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
